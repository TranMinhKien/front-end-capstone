import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {
  BrowserRouter as Router,
} from "react-router-dom";
import App1 from './components/App1';
import Dashboard from './components/Dashboard';
function App() {
  return (
     <Router>
       <App1></App1>
         <ToastContainer
             position="bottom-left"
             autoClose={3000}
             hideProgressBar={false}
             newestOnTop={false}
             closeOnClick
             rtl={false}
             pauseOnFocusLoss
             draggable
             pauseOnHover
         />
     </Router>
  );
}

export default App;

// import React from 'react';
// import "bootstrap/dist/css/bootstrap.min.css";

// import {
//   BrowserRouter as Router,
// } from "react-router-dom";
// import App1 from './components/App1';
// import Dashboard from './components/Dashboard';
// function App() {
//   return (
//      <Router>
//        <App1></App1>
//      </Router>
//   );
// }

// export default App;
