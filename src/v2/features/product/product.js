import { CloseOutlined, EditOutlined, EyeOutlined } from "@ant-design/icons";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";
import "bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.js";
import { DOMAIN_BE } from "constant/constant";
import React, { useEffect, useState } from "react";
import { Button, Col, Container, Modal, Row } from "react-bootstrap";
import { useHistory } from "react-router";
import AuthService from "services/auth.service";
import authHeader from "services/auth_header";
import { commonFetchAdd } from "utils/add";
import { missingPermission } from "utils/errorMessage";
import { commonFetchDataHandler } from "utils/index";
import { ErrorMess } from "utils/MessageErrors";
import { toastSuccess } from "utils/MessageSuccess";
import validation from "utils/validation";
import { convertColumn, convertMilion } from "v2/components/columns";
import Tableb from "v2/components/table";
import "../V2.css";

const baseURL = DOMAIN_BE + "/api/product/";
const baseURLproductType = DOMAIN_BE + "/api/productType/name";

const useStyles = makeStyles((theme) => ({
  modal: {
    position: "absolute",
    width: 1200,
    backgroundColor: theme.palette.background.paper,
    border: "1px solid #000",
    // boxShadow: theme.shadows[5],
    boxShadow: "20px 10px",
    padding: theme.spacing(2, 4, 3),
    // padding: '1px 1px 1px 1px',
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
  iconos: {
    cursor: "pointer",
  },
  inputMaterial: {
    width: "50%",
    padding: "3px 10px",
    margin: "3px 0",
  },
}));

const validateForm = (errors) => {
  let valid = true;
  if (errors == null) return true;
  Object.values(errors).forEach((val) => val.length > 0 && (valid = false));
  return valid;
};

const validEmailRegex = RegExp(
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
);
const validPhoneRegex = RegExp(/^\d*$/);

function Product() {
  const styles = useStyles();
  // ném dữ liệu vào materialTbale dùng API
  const [data, setData] = useState([]);
  // // để hiện lên modal để tắt và bật
  const [modalInsertar, setModalInsertar] = useState(false);
  // ấn vào hàng nào nó sẽ tích vào hàng đấy
  const [modalEditar, setModalEditar] = useState(false);
  // khai báo state và setstate
  const [modalEliminar, setModalEliminar] = useState(false);
  const [modalViewInfor, setModalViewInfor] = useState(false);
  // cái này để khi nhập sữ liệu sẽ được thêm dữ liệu vào bảng
  // lấy thông tin USER
  const user = AuthService.getCurrentUser();
  const history = useHistory();

  const [artistaSeleccionado, setArtistaSeleccionado] = useState({
    code: null,
    name: null,
    productTypeId: "",
    explanation: "",
    unit: "Bao",
    sellPrice: "",
    sellPrice1: "",
    sellPrice2: "",
    buyPrice: "",
    permanentPrice: "",
    vat: "",
    costUnitPrice: "",
    errors: {
      code: "",
      name: "",
      explanation: "",
      unit: "",
      sellPrice: "",
      sellPrice1: "",
      sellPrice2: "",
      permanentPrice: "",
      vat: "",
      costUnitPrice: "",
    },
  });

  const [productType3, setProductType3] = useState({
    selectProductType: [],
    id: "",
    name: "",
  });

  const getProductType = async () => {
    const responseProductType = await commonFetchDataHandler(
      baseURLproductType
    );
    const optionsProductType = responseProductType.data.map((d) => ({
      value: d.id,
      label: d.name,
    }));
    optionsProductType.unshift({
      value: "",
      label: "-- Không chọn --",
    });
    setProductType3({ selectProductType: optionsProductType });
  };
  const clearErrors = () => {
    setArtistaSeleccionado((prevState) => ({
      ...prevState,
      errors: {},
    }));
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    let errors =
      artistaSeleccionado.errors || (artistaSeleccionado.errors = {});
    validation.value = value;

    switch (name) {
      case "name":
        errors.name = validation.validate("Tên", [validation.notBlank]);
        break;
      case "explanation":
        break;
      case "code":
        errors.code = validation.validate("Mã", [validation.notBlank]);
        break;
      case "buyPrice":
        errors.buyPrice = validation.validate("Đơn giá mua ", [
          validation.number,
          validation.positiveOrZero,
        ]);
        break;
      case "sellPrice":
        errors.sellPrice = validation.validate("Đơn giá bán ", [
          validation.number,
          validation.positiveOrZero,
        ]);
        break;
      case "sellPrice1":
        errors.sellPrice1 = validation.validate("Đơn giá bán 1 ", [
          validation.number,
          validation.positiveOrZero,
        ]);
        break;
      case "sellPrice2":
        errors.sellPrice2 = validation.validate("Đơn giá bán 2", [
          validation.number,
          validation.positiveOrZero,
        ]);
        break;
      case "permanentPrice":
        errors.permanentPrice = validation.validate("Đơn giá bán cố định ", [
          validation.number,
          validation.positiveOrZero,
        ]);
        break;
      case "vat":
        errors.vat = validation.validate("Thuế  ", [
          validation.number,
          validation.rate,
        ]);
        break;
      case "costUnitPrice":
        errors.costUnitPrice = validation.validate("Đơn giá chi phí ", [
          validation.number,
          validation.positiveOrZero,
        ]);
        break;
      default:
        break;
    }
    setArtistaSeleccionado((prevState) => ({
      ...prevState,
      [name]: value || null,
    }));
    console.log(artistaSeleccionado);
  };

  const hasPerm = (user, perm = "") => {
    return user && user.roles && user.roles.includes(perm);
  };

  const getProduct = async () => {
    const response = await commonFetchDataHandler(baseURL);
    response.data.forEach((element) => {
      // TODO 3
      element.productTypeId = element.productType
        ? element.productType.id
        : null;
    });
    setData(response.data);
  };
  // insert dữ liệu
  const createPoduct = async () => {
    // Check validate khi ấn "Thêm"
    for (const name in artistaSeleccionado) {
      const value = artistaSeleccionado[name];
      handleChange({
        target: { name, value },
      });
    }
    // Nếu chưa valid thì không thêm dữ liệu
    if (!validateForm(artistaSeleccionado.errors)) return;

    commonFetchAdd(baseURL, artistaSeleccionado)
      .then((responseSource) => {
        const d = responseSource.data;
        d.productTypeId = d.productType ? d.productType.id : null;
        setData(data.concat(d));
        abrirCerrarModalInsertar();
        toastSuccess("Thêm dữ liệu thành công");
      })
      .catch((error) => {
        if (
          error.response &&
          error.response.data &&
          error.response.data.errors &&
          error.response.data.errors[0].type === "duplicate"
        ) {
          let errors =
            artistaSeleccionado.errors || (artistaSeleccionado.errors = {});
          error.response.data.errors.forEach((e) => {
            errors[e.field] = "Giá trị đã bị trùng";
          });
          setArtistaSeleccionado((prevState) => ({
            ...prevState,
            errors: errors,
          }));
        } else {
          ErrorMess(error);
        }
      });
  };

  // hiện lên để chỉnh sửa trong database
  const editProduct = async () => {
    await axios
      .put(baseURL + artistaSeleccionado.id, artistaSeleccionado, {
        headers: { Authorization: authHeader().Authorization },
      })
      .then((response) => {
        var dataNueva = data;
        dataNueva.map((id) => {
          if (id.id === artistaSeleccionado.id) {
            id.name = artistaSeleccionado.name;

            id.productType = response.data.productType;
            id.productTypeId = id.productType?.id;

            id.sellPrice = artistaSeleccionado.sellPrice;
            id.unit = artistaSeleccionado.unit;
            id.vat = artistaSeleccionado.vat;
            id.code = artistaSeleccionado.code;
            id.explanation = artistaSeleccionado.explanation;
            id.sellPrice1 = artistaSeleccionado.sellPrice1;
            id.sellPrice2 = artistaSeleccionado.sellPrice2;
            id.buyPrice = artistaSeleccionado.buyPrice;
            id.permanentPrice = artistaSeleccionado.permanentPrice;
            id.costUnitPrice = artistaSeleccionado.costUnitPrice;
          }
        });
        setData(dataNueva);
        abrirCerrarModalEditar();
        toastSuccess("Edit thành công");
      })
      .catch((error) => {
        ErrorMess(error);
      });
  };
  // xóa dữ liệu
  const deleteProduct = async () => {
    const ids = Array.isArray(artistaSeleccionado)
      ? artistaSeleccionado.map((a) => a.id)
      : [artistaSeleccionado.id];
    await axios
      .delete(baseURL + ids.join(","), {
        headers: { Authorization: authHeader().Authorization },
      })
      .then((response) => {
        setData(data.filter((d) => !ids.includes(d.id)));
        abrirCerrarModalEliminar();
        toastSuccess("Xóa thành công!");
      })
      .catch((error) => {
        ErrorMess(error);
      });
  };

  const seleccionarArtista = (id, caso) => {
    setArtistaSeleccionado(id);
    caso === "Editar" ? abrirCerrarModalEditar() : abrirCerrarModalEliminar();
  };

  const seleccionarView = (id, caso) => {
    setArtistaSeleccionado(id);
    if (caso === "check") {
      viewInformation();
    }
  };
  // // thực hiện lệnh tắt và bật  modal
  const abrirCerrarModalInsertar = () => {
    setModalInsertar(!modalInsertar);
  };
  // // cũng để bật tắt
  const abrirCerrarModalEditar = () => {
    setModalEditar(!modalEditar);
  };
  // // cũng để bật tắt
  const abrirCerrarModalEliminar = () => {
    setModalEliminar(!modalEliminar);
  };

  const viewInformation = () => {
    setModalViewInfor(!modalViewInfor);
  };
  useEffect(() => {
    getProduct();
    getProductType();
  }, []);

  const hasError = (name) => {
    return (
      artistaSeleccionado &&
      artistaSeleccionado.errors &&
      artistaSeleccionado.errors[name] &&
      artistaSeleccionado.errors[name].length > 0 && (
        <span className="error">{artistaSeleccionado.errors[name]}</span>
      )
    );
  };
  const bodyInsert = (
    <Modal
      size="lg"
      show={modalInsertar}
      onHide={() => abrirCerrarModalInsertar()}
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header>
        <Modal.Title>Thêm Hàng Hóa</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Container>
          <Row>
            <Col md={6}>
              <label className="required">Tên Hàng Hóa </label>
              <input
                type="text"
                class="form-control"
                name="name"
                onChange={handleChange}
              ></input>
              {hasError("name")}
            </Col>
            <Col md={6}>
              <label className="required">Mã Hàng Hóa</label>
              <input
                type="text"
                class="form-control"
                name="code"
                onChange={handleChange}
              ></input>
              {hasError("code")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Loại Hàng Hóa </label>
              <select
                className="form-control controll"
                name="productTypeId"
                onChange={handleChange}
                value={artistaSeleccionado.productTypeId}
              >
                {productType3.selectProductType.map((option2, index) => (
                  <option value={option2.value} key={index}>
                    {" "}
                    {option2.label}
                  </option>
                ))}
              </select>
            </Col>
            <Col md={6}>
              <label> Đơn vị tính chính </label>
              <select
                className="form-control controll "
                value={artistaSeleccionado.unit}
                name="unit"
                onChange={handleChange}
              >
                <option value=""> -- Không chọn - </option>
                <option value="Hộp "> Hộp</option>
                <option value="Bao ">Bao</option>
                <option value="Bình">Bình</option>
                <option value="Bộ">Bộ</option>
                <option value="Cái">Cái</option>
                <option value="Cây">Cây</option>
                <option value="Chai">Chai</option>
                <option value="Chiếc">Chiếc</option>
                <option value="Cuốn">Cuốn</option>
                <option value="Điếu">Điếu</option>
                <option value="Gói">Gói</option>
                <option value="Hộp">Hộp</option>
                <option value="két">két</option>
                <option value="Tấn">Tấn </option>
                <option value="Tạ">Tạ </option>
                <option value="Yến">Yến</option>
              </select>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Diễn giải khi bán</label>
              <input
                type="text"
                class="form-control"
                name="explanation"
                onChange={handleChange}
              ></input>
              {hasError("explanation")}
            </Col>

            <Col md={6}>
              <label>Đơn giá bán</label>
              <input
                type="text"
                class="form-control"
                name="sellPrice"
                onChange={handleChange}
              ></input>
              {hasError("sellPrice")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Đơn giá bán 1</label>
              <input
                type="text"
                class="form-control"
                name="sellPrice1"
                onChange={handleChange}
              ></input>
              {hasError("sellPrice1")}
            </Col>
            <Col md={6}>
              <label>Đơn giá bán 2</label>
              <input
                type="text"
                class="form-control"
                name="sellPrice2"
                onChange={handleChange}
              ></input>
              {hasError("sellPrice2")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Đơn giá bán cố định </label>
              <input
                type="text"
                class="form-control"
                name="permanentPrice"
                onChange={handleChange}
              ></input>
              {hasError("permanentPrice")}
            </Col>
            <Col md={6}>
              <label>Đơn giá mua</label>
              <input
                type="text"
                class="form-control"
                name="buyPrice"
                onChange={handleChange}
              ></input>
              {hasError("buyPrice")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Thuế (%) </label>
              <input
                type="text"
                class="form-control"
                name="vat"
                onChange={handleChange}
              ></input>
              {hasError("vat")}
            </Col>
            <Col md={6}>
              <label>Đơn giá chi phí </label>
              <input
                type="text"
                class="form-control"
                name="costUnitPrice"
                onChange={handleChange}
              ></input>
              {hasError("costUnitPrice")}
            </Col>
          </Row>
        </Container>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={() => {
            clearErrors();
            abrirCerrarModalInsertar();
          }}
        >
          Hủy Bỏ
        </Button>
        <Button
          variant="primary"
          onClick={() => createPoduct()}
          disabled={!validateForm(artistaSeleccionado.errors)}
        >
          {" "}
          Thêm{" "}
        </Button>
      </Modal.Footer>
    </Modal>
  );
  const bodyEdit = (
    <Modal
      size="lg"
      show={modalEditar}
      onHide={() => abrirCerrarModalEditar()}
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header>
        <Modal.Title>Sửa Thông Tin</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Container>
          <Row>
            <Col md={6}>
              <label className="required">Tên Hàng Hóa </label>
              <input
                type="text"
                class="form-control"
                name="name"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.name}
              ></input>
              {hasError("name")}
            </Col>
            <Col md={6}>
              <label className="required">Mã Hàng Hóa</label>
              <input
                type="text"
                class="form-control"
                name="code"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.code}
              ></input>
              {hasError("code")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Loại Hàng Hóa </label>
              <select
                className="form-control controll"
                name="productTypeId"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.productTypeId}
              >
                {productType3.selectProductType.map((option2, index) => (
                  <option value={option2.value} key={index}>
                    {" "}
                    {option2.label}
                  </option>
                ))}
              </select>
            </Col>
            <Col md={6}>
              <label> Đơn vị tính chính </label>
              <select
                className="form-control controll "
                value={artistaSeleccionado && artistaSeleccionado.unit}
                name="unit"
                onChange={handleChange}
              >
                <option value=""> -- Không chọn -- </option>
                <option value="Hộp "> Hộp</option>
                <option value="Bao ">Bao</option>
                <option value="Bình">Bình</option>
                <option value="Bộ">Bộ</option>
                <option value="Cái">Cái</option>
                <option value="Cây">Cây</option>
                <option value="Chai">Chai</option>
                <option value="Chiếc">Chiếc</option>
                <option value="Cuốn">Cuốn</option>
                <option value="Điếu">Điếu</option>
                <option value="Gói">Gói</option>
                <option value="Hộp">Hộp</option>
                <option value="két">két</option>
                <option value="Tấn">Tấn </option>
                <option value="Tạ">Tạ </option>
                <option value="Yến">Yến</option>
              </select>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Diễn giải khi bán</label>
              <input
                type="text"
                class="form-control"
                name="explanation"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.explanation}
              ></input>
              {hasError("explanation")}
            </Col>

            <Col md={6}>
              <label>Đơn giá bán</label>
              <input
                type="text"
                class="form-control"
                name="sellPrice"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.sellPrice}
              ></input>
              {hasError("sellPrice")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Đơn giá bán 1</label>
              <input
                type="text"
                class="form-control"
                name="sellPrice1"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.sellPrice1}
              ></input>
              {hasError("sellPrice1")}
            </Col>
            <Col md={6}>
              <label>Đơn giá bán 2</label>
              <input
                type="text"
                class="form-control"
                name="sellPrice2"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.sellPrice2}
              ></input>
              {hasError("sellPrice2")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Đơn giá bán cố định </label>
              <input
                type="text"
                class="form-control"
                name="permanentPrice"
                onChange={handleChange}
                value={
                  artistaSeleccionado && artistaSeleccionado.permanentPrice
                }
              ></input>
              {hasError("permanentPrice")}
            </Col>
            <Col md={6}>
              <label>Đơn giá mua</label>
              <input
                type="text"
                class="form-control"
                name="buyPrice"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.buyPrice}
              ></input>
              {hasError("buyPrice")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Thuế (%) </label>
              <input
                type="text"
                class="form-control"
                name="vat"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.vat}
              ></input>
              {hasError("vat")}
            </Col>
            <Col md={6}>
              <label>Đơn giá chi phí </label>
              <input
                type="text"
                class="form-control"
                name="costUnitPrice"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.costUnitPrice}
              ></input>
              {hasError("costUnitPrice")}
            </Col>
          </Row>
        </Container>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={() => {
            clearErrors();
            abrirCerrarModalEditar();
          }}
        >
          Hủy Bỏ
        </Button>
        <Button
          variant="primary"
          onClick={() => editProduct()}
          disabled={!validateForm(artistaSeleccionado.errors)}
        >
          Sửa
        </Button>
      </Modal.Footer>
    </Modal>
  );
  const View = (
    <Modal size="lg" show={modalViewInfor} onHide={() => viewInformation()}>
      <Modal.Header>
        <Modal.Title>Xem Thông Tin Chi Tiết</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Container>
          <Row>
            <Col md={6}>
              <label className="required">Tên Hàng Hóa </label>
              <input
                type="text"
                class="form-control"
                name="name"
                value={artistaSeleccionado && artistaSeleccionado.name}
              ></input>
            </Col>
            <Col md={6}>
              <label className="required">Mã Hàng Hóa</label>
              <input
                type="text"
                class="form-control"
                name="code"
                value={artistaSeleccionado && artistaSeleccionado.code}
              ></input>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Loại Hàng Hóa </label>
              <select
                className="form-control controll"
                name="productTypeId"
                value={artistaSeleccionado && artistaSeleccionado.productTypeId}
              >
                {productType3.selectProductType.map((option2, index) => (
                  <option value={option2.value} key={index}>
                    {" "}
                    {option2.label}
                  </option>
                ))}
              </select>
            </Col>
            <Col md={6}>
              <label> Đơn vị tính chính </label>
              <select
                className="form-control controll "
                value={artistaSeleccionado && artistaSeleccionado.unit}
                name="unit"
              >
                <option value=""> -- Không chọn -- </option>
                <option value="Hộp "> Hộp</option>
                <option value="Bao ">Bao</option>
                <option value="Bình">Bình</option>
                <option value="Bộ">Bộ</option>
                <option value="Cái">Cái</option>
                <option value="Cây">Cây</option>
                <option value="Chai">Chai</option>
                <option value="Chiếc">Chiếc</option>
                <option value="Cuốn">Cuốn</option>
                <option value="Điếu">Điếu</option>
                <option value="Gói">Gói</option>
                <option value="Hộp">Hộp</option>
                <option value="két">két</option>
                <option value="Tấn">Tấn </option>
                <option value="Tạ">Tạ </option>
                <option value="Yến">Yến</option>
              </select>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Diễn giải khi bán</label>
              <input
                type="text"
                class="form-control"
                name="explanation"
                value={artistaSeleccionado && artistaSeleccionado.explanation}
              ></input>
            </Col>

            <Col md={6}>
              <label>Đơn giá bán</label>
              <input
                type="text"
                class="form-control"
                name="sellPrice"
                value={artistaSeleccionado && artistaSeleccionado.sellPrice}
              ></input>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Đơn giá bán 1</label>
              <input
                type="text"
                class="form-control"
                name="sellPrice1"
                value={artistaSeleccionado && artistaSeleccionado.sellPrice1}
              ></input>
            </Col>
            <Col md={6}>
              <label>Đơn giá bán 2</label>
              <input
                type="text"
                class="form-control"
                name="sellPrice2"
                value={artistaSeleccionado && artistaSeleccionado.sellPrice2}
              ></input>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Đơn giá bán cố định </label>
              <input
                type="text"
                class="form-control"
                name="permanentPrice"
                value={
                  artistaSeleccionado && artistaSeleccionado.permanentPrice
                }
              ></input>
            </Col>
            <Col md={6}>
              <label>Đơn giá mua</label>
              <input
                type="text"
                class="form-control"
                name="buyPrice"
                value={artistaSeleccionado && artistaSeleccionado.buyPrice}
              ></input>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Thuế (%) </label>
              <input
                type="text"
                class="form-control"
                name="vat"
                value={artistaSeleccionado && artistaSeleccionado.vat}
              ></input>
            </Col>
            <Col md={6}>
              <label>Đơn giá chi phí </label>
              <input
                type="text"
                class="form-control"
                name="costUnitPrice"
                value={artistaSeleccionado && artistaSeleccionado.costUnitPrice}
              ></input>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Ngày Tạo </label>
              <input
                type="text"
                class="form-control"
                name="createdAt"
                value={artistaSeleccionado && artistaSeleccionado.createdAt}
              ></input>
            </Col>
            <Col md={6}>
              <label> Ngày sửa </label>
              <input
                type="text"
                class="form-control"
                name="updatedAt"
                value={artistaSeleccionado && artistaSeleccionado.updatedAt}
              ></input>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Người tạo </label>
              <input
                type="text"
                class="form-control"
                name="createdBy"
                value={
                  artistaSeleccionado &&
                  artistaSeleccionado.createdBy &&
                  artistaSeleccionado.createdBy.username
                }
              ></input>
            </Col>
            <Col md={6}>
              <label> Người Sửa </label>
              <input
                type="text"
                class="form-control"
                name="updatedBy"
                value={
                  artistaSeleccionado &&
                  artistaSeleccionado.updatedBy &&
                  artistaSeleccionado.updatedBy.username
                }
              ></input>
            </Col>
          </Row>
        </Container>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={() => viewInformation()}>
          Hủy Bỏ
        </Button>
      </Modal.Footer>
    </Modal>
  );

  // xác nhận yêu cầu xóa
  const ModalDelete = (
    <Modal
      backdrop="static"
      keyboard={false}
      show={modalEliminar}
      onHide={abrirCerrarModalEliminar}
    >
      <Modal.Header>
        <Modal.Title>Xóa</Modal.Title>
      </Modal.Header>
      <Modal.Body className="show-grid">
        <p> Bạn có chắc chắn muốn xóa không ? </p>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={() => abrirCerrarModalEliminar()}>
          Không
        </Button>
        <Button variant="primary" onClick={() => deleteProduct()}>
          Có
        </Button>
      </Modal.Footer>
    </Modal>
  );
  const listColumn = [
    {
      title: "Mã hàng hóa",
      dataIndex: "code",
    },
    {
      title: "Tên hàng hóa",
      dataIndex: "name",
    },
    {
      title: "Loại hàng hóa",
      dataIndex: "productType",
    },
    {
      title: "Đơn vị tính",
      dataIndex: "unit",
    },
    {
      title: "Đơn giá bán",
      dataIndex: "sellPrice",
    },
    {
      title: "Đơn giá bán 1",
      dataIndex: "sellPrice1",
    },
    {
      title: "Đơn giá bán 2",
      dataIndex: "sellPrice2",
    },
    {
      title: "Đơn giá bán cố định",
      dataIndex: "permanentPrice",
    },
    {
      title: "Đơn giá mua",
      dataIndex: "buyPrice",
    },
    {
      title: "Thuế GTGT",
      dataIndex: "vat",
    },
    {
      title: "Đơn giá chi phí",
      dataIndex: "costUnitPrice",
    },
    {
      title: "Hành động",
      key: "operation",
      fixed: "right",
      width: "150px",
      dataIndex: "action",
    },
  ];
  const handleMultiDelete = (selectedRowKeys) => {
    setArtistaSeleccionado(selectedRowKeys);
    abrirCerrarModalEliminar();
  };
  const handlDelete = (data) => {
    setArtistaSeleccionado(data);
    abrirCerrarModalEliminar();
  };
  const onEdit = (id) => {
    history.push(`/product/edit/${id}`);
  };
  const onView = (id) => {
    history.push(`/product/view/${id}`);
  };
  const onAddNew = () => {
    // setModalInsertar(true);
    // abrirCerrarModalInsertar();
    hasPerm(user, "ROLE_CREATE_PRODUCT")
    ? 
    history.push(`/product/add`)
    : missingPermission()
  };
  const getDataTable = () => {
    let cloneData = data.map((item, index) => {
      return {
        ...item,
        key: index,
        source: item?.source?.name,
        customer: item?.customer?.name,
        productType: item?.productType?.name,
        unit: item?.unit?.name,
        sellPrice:convertMilion(item?.sellPrice),
        sellPrice1:convertMilion(item?.sellPrice1),
        sellPrice2:convertMilion(item?.sellPrice2),
        permanentPrice:convertMilion(item?.permanentPrice),
        buyPrice:convertMilion(item?.buyPrice),
        costUnitPrice:convertMilion(item?.costUnitPrice),
        action: (
          <div className="d-flex align-items-center p-0 justify-content-between">
             <EditOutlined
              className=" btnIcon"
              onClick={() => {
                hasPerm(user, "ROLE_UPDATE_PRODUCT")
                  ? onEdit(item.id)
                  : missingPermission();
              }}
              style={{ color: "#C8DF52" }}
            />
            <CloseOutlined
              className="btnIcon"
              onClick={() => {
                hasPerm(user, "ROLE_DELETE_PRODUCT")
                  ? handlDelete(item)
                  : missingPermission();
              }}
              style={{ color: "#D2042D" }}
            />
            <EyeOutlined
              className=" btnIcon"
              onClick={() => {
                hasPerm(user, "ROLE_READ_PRODUCT")
                ? onView(item.id)
                : missingPermission();
                }}
              style={{ color: "#B3CDE0" }} />
          </div>
        ),
      };
    });
    return cloneData;
  };
  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-md-12">
          <Tableb
            columns={convertColumn(listColumn)}
            handleMultiDelete={handleMultiDelete}
            data={getDataTable()}
            onAddNew={onAddNew}
            titlechucnang = "Hàng Hóa"
          />
          {bodyInsert}
          {bodyEdit}
          {View}
          {ModalDelete}
        </div>
      </div>
    </div>
  );
}

export default Product;
