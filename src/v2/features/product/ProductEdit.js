import React, {useEffect, useState} from 'react';
import {useHistory} from 'react-router-dom';
import {useForm} from 'react-hook-form';
import {yupResolver} from '@hookform/resolvers/yup';
import {Button, Form} from 'antd';
import FormDataCustom from '../../utils/FormDataCustom';
import {TableProduct} from './ProductAddNew';
import {toast} from 'react-toastify';
import {updateProduct} from 'v2/api/updateAPI';
import {getProductById} from 'v2/api/readByIdAPI';
import {validateProductSchema} from '../../config/validate/productValidate';

ProductEdit.propTypes = {};

function ProductEdit(props) {
    const productId = props.match.params.id;
    const history = useHistory();
    const rHF = useForm({
        resolver: yupResolver(validateProductSchema),
        mode: "onChange"
    });
    const [form] = Form.useForm();
    const {control, handleSubmit, formState: {errors}} = rHF;
    const customForm = new FormDataCustom(form, rHF);
    console.log(errors);
    const goToProduct = () => history.push('/product');

    const onSubmit = async (data) => {
        try {
            const response = await updateProduct(data, productId);
            if (response.error) {
                throw new Error(response.error);
            }
            if (!response?.data) {
                throw new Error();
            }
            history.push('/product');
            toast.success('Cập nhận thành công');
        } catch (error) {
            toast.error('Cập nhật không thành công');
        }
    };
    const setValueData = (data) => [
        {
            value: data?.code,
            name: "code"
        },
        {
            value: data?.name,
            name: "name"
        },
        {
            value: data?.productType?.id,
            name: "productTypeId"
        },
        {
            value: data?.unit?.id,
            name: "unit"
        },
        {
            value: data?.sellPrice,
            name: "sellPrice"
        },
        {
            value: data?.sellPrice1,
            name: "sellPrice1"
        },
        {
            value: data?.sellPrice2,
            name: "sellPrice2"
        },
        {
            value: data?.permanentPrice,
            name: "permanentPrice"
        },
        {
            value: data?.buyPrice,
            name: "buyPrice"
        },
        {
            value: data?.vat,
            name: "vat"
        },
        {
            value: data?.costUnitPrice,
            name: "costUnitPrice"
        },
        {
            value: data?.explanation,
            name: "explanation"
        }
    ];

    const [product, setProduct] = useState();
    useEffect(() => {
        const fetchProduct = async () => {
            try {
                setProduct({...product, isLoading: true});
                const response = await getProductById(productId);
                if (response.error) {
                    throw new Error(response.error);
                }
                setProduct({
                    data: response.data,
                    isLoading: false
                });
                customForm.setFormMany(setValueData(response.data));
            } catch (error) {
                console.log("fail to featch product");
                setProduct({
                    data: null,
                    isLoading: false
                });
            }
        };
        fetchProduct();
    }, [productId]);
    return (
        <Form form={form} onFinish={handleSubmit(onSubmit)}>
            <div className={'sticky-bar-top'}>
                <h5 className={'m-0'}> Chỉnh Sửa Hàng Hoá</h5>
                <div className={'ms-auto'}>
                    <Button type="dashed" onClick={goToProduct}>Huỷ</Button>
                    <Button type="primary" htmlType={'submit'}
                            className={'ms-2'}>Lưu</Button>
                </div>
            </div>
            <TableProduct control={control}/>
        </Form>
    );
}

export default ProductEdit;