import React from "react";
import PropTypes from "prop-types";
import InputTextField from "../../components/CustomFields/InputTextField";
import SelectField from "../../components/CustomFields/SelectField";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Button, Form } from "antd";
import FormDataCustom from "../../utils/FormDataCustom";
import {
  TableProductType,

} from "../product-type/ProductTypeAddNew";
import * as yup from "yup";
import ProductTypeField from "v2/components/customElement/ProductTypeField";
import UnitField from "v2/components/customElement/UnitField";
import {setDateToString} from '../../utils/format';
import {createOrder, createProduct} from '../../api/createAPI';
import {toast} from 'react-toastify';
import {validateProductSchema} from '../../config/validate/productValidate';
import InputNumberField from '../../components/CustomFields/InputNumberField';

ProductAddNew.propTypes = {};

function ProductAddNew(props) {
  const history = useHistory();
  const rHF = useForm({
    resolver: yupResolver(validateProductSchema),
  });
  const [form] = Form.useForm();
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = rHF;
  const customForm = new FormDataCustom(form, rHF);
  console.log(errors);
  const goToProduct = () => history.push("/product");

  async function onSubmit(data) {
    try {
      setDateToString(data);
      await createProduct(data);
      history.push('/product');
      toast.success("Thêm thành công");
    } catch (e) {
      toast.error("Thêm thất bại");
    }
  }


  return (
    <Form form={form} onFinish={handleSubmit(onSubmit)}>
      <div className={"sticky-bar-top"}>
        <h5 className={"m-0"}>Thêm Hàng Hoá</h5>
        <div className={"ms-auto"}>
          <Button type="dashed" onClick={goToProduct}>
            Huỷ
          </Button>
          <Button type="primary" htmlType={"submit"} className={"ms-2"}>
            Thêm
          </Button>
        </div>
      </div>
      <TableProduct control={control} />
    </Form>
  );
}

export const TableProduct = ({ control }) => {
  return (
    <div className={"container-lg py-5"}>
      <table className={"mx-auto spacing-table w-75"}>
        <tbody>
          <tr>
            <td colSpan={2}>
              <h5>Thông tin chung</h5>
            </td>
          </tr>
          <tr>
            <td>
              <InputTextField
                name={"name"}
                control={control}
                label={"Tên hàng hóa"}
                isRequired
              />
            </td>
            <td>
              <ProductTypeField name={"productTypeId"} control={control} />
            </td>
          </tr>
          <tr>
            <td>
              <InputTextField
                name={"explanation"}
                control={control}
                label={"Diễn giải khi bán"}
              />
            </td>
            <td>
              <UnitField name={"unit"} control={control} />
            </td>
          </tr>
          <tr>
            <td>
              <InputNumberField
                name={"sellPrice"}
                control={control}
                label={"Đơn giá bán"}
                isRequired
              />
            </td>
            <td>
              <InputNumberField
                name={"sellPrice1"}
                control={control}
                label={"Đơn giá bán 1"}
              />
            </td>
          </tr>
          <tr>
            <td>
              <InputNumberField
                name={"sellPrice2"}
                control={control}
                label={"Đơn giá bán 2"}
              />
            </td>
            <td>
              <InputNumberField
                name={"permanentPrice"}
                control={control}
                label={"Đơn giá bán cố định"}
                isRequired
              />
            </td>
          </tr>
          <tr>
            <td>
              <InputNumberField
                name={"buyPrice"}
                control={control}
                label={"Đơn giá mua"}
              />
            </td>
            <td>
              <InputNumberField name={"vat"} control={control} label={"Thuế GTGT"} />
            </td>
          </tr>
          <tr>
            <td>
              <InputNumberField
                name={"costUnitPrice"}
                control={control}
                label={"Đơn giá chi phí"}
              />
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};
export default ProductAddNew;
