import { yupResolver } from "@hookform/resolvers/yup";
import { Button, Form } from "antd";
import moment from "moment";
import React, { useContext, useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import { getContactById, getCustomerById } from "v2/api/readByIdAPI";
import ContactField from "v2/components/customElement/ContactField";
import CustomerField from "v2/components/customElement/CustomerField";
import OpportunityPhaseField from "v2/components/customElement/OpportunityPhase";
import SourceField from "v2/components/customElement/SourceField";
import InputDateField from "../../components/CustomFields/InputDateField";
import InputNumberField from "../../components/CustomFields/InputNumberField";
import InputTextField from "../../components/CustomFields/InputTextField";
import { validateOpportunitySchema } from "../../config/validate/oppotunityValidate";
import FormDataCustom from "../../utils/FormDataCustom";
import { setDateToString } from "../../utils/format";
import { createOpportunity } from "../../api/createAPI";
import { toast } from "react-toastify";
import AddressField from "v2/components/customElement/AddressField";
import EditableTable from "v2/components/tableEdit/EditTableCell";
import { ProductContext } from "v2/providers/provider";

OpportunityAddNew.propTypes = {};

function OpportunityAddNew(props) {
  const { products: productInfos, setContext } = useContext(ProductContext);
  const history = useHistory();
  const urlParams = new URLSearchParams(props.location.search);
  const contactId = urlParams.get("contactId");
  const customerId = urlParams.get("customerId");
  console.log(props);
  const rHF = useForm({
    resolver: yupResolver(validateOpportunitySchema),
    mode: "onChange",
  });
  const [form] = Form.useForm();
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = rHF;
  const customForm = new FormDataCustom(form, rHF);
  console.log(errors);
  const goToOpportunity = () => history.push("/opportunity");

  // customForm.setFormValue('name','b')
  // customForm.setFormMany([{name: 'name', value: 'a'}, {name: 'shortName', value: 'b'}]);

  async function onSubmit(data) {
    data.productInfos=productInfos;
    try {
      setDateToString(data);
      await createOpportunity(data);
      history.push("/opportunity");
      toast.success("Thêm thành công");
    } catch (e) {
      toast.error("Thêm thất bại");
    }
  }

  const setValueDataForCustomer = (data) => [
    {
      value: data?.id,
      name: "customerId",
    },
    {
      value: data?.contact?.id,
      name: "contactId",
    },
    {
      value: data?.name,
      name: "name",
    },
    {
      value: data?.opportunityPhase?.id,
      name: "opportunityPhaseId",
    },
    {
      value: data?.successRate,
      name: "successRate",
    },
    {
      value: data?.expectedEndDate ? new moment(data?.expectedEndDate) : null,
      name: "expectedEndDate",
    },
    {
      value: data?.source?.id,
      name: "sourceId",
    },
    {
      value: data?.address,
      name: "address",
    },
    {
      value: data?.province?.id,
      name: "provinceId",
    },
    {
      value: data?.district?.id,
      name: "districtId",
    },
    {
      value: data?.ward?.id,
      name: "wardId",
    },
  ];
  const setValueDataForContact = (data) => [
    {
      value: data?.customer?.id,
      name: "customerId",
    },
    {
      value: data?.id,
      name: "contactId",
    },
    {
      value: data?.name,
      name: "name",
    },
    {
      value: data?.opportunityPhase?.id,
      name: "opportunityPhaseId",
    },
    {
      value: data?.successRate,
      name: "successRate",
    },
    {
      value: data?.expectedEndDate ? new moment(data?.expectedEndDate) : null,
      name: "expectedEndDate",
    },
    {
      value: data?.source?.id,
      name: "sourceId",
    },
    {
      value: data?.address,
      name: "address",
    },
    {
      value: data?.province?.id,
      name: "provinceId",
    },
    {
      value: data?.district?.id,
      name: "districtId",
    },
    {
      value: data?.ward?.id,
      name: "wardId",
    },
  ];
  const fetchContact = async () => {
    try {
      const response = await getContactById(contactId);
      if (response.error) {
        throw new Error(response.error);
      }
      customForm.setFormMany(setValueDataForContact(response.data));
    } catch (error) {
      console.log("fail to featch contact");
    }
  };
  const fetchCustomer = async () => {
    try {
      const response = await getCustomerById(customerId);
      customForm.setFormMany(setValueDataForCustomer(response.data));
    } catch (error) {
      console.log("fail to featch contact");
    }
  };
  useEffect(() => {
    if (customerId) fetchCustomer();
    if (contactId) fetchContact();
    return setContext([]);
  }, [contactId, customerId]);
  return (
    <Form form={form} onFinish={handleSubmit(onSubmit)}>
      <div className={"sticky-bar-top"}>
        <h5 className={"m-0"}>Thêm Cơ Hội</h5>
        <div className={"ms-auto"}>
          <Button type="dashed" onClick={goToOpportunity}>
            Huỷ
          </Button>
          <Button type="primary" htmlType={"submit"} className={"ms-2"}>
            Thêm
          </Button>
        </div>
      </div>
      <TableOpportunity
        control={control}
        customForm={customForm}
        productInfos={productInfos}
      />
    </Form>
  );
}

export const TableOpportunity = ({ control, customForm }) => {
  function onCustomerClear() {
    customForm.setFormValue("customerId", undefined);
  }
  function onChangeSelect(e) {
    let rate=0;
    if(e!==6){
      rate=e*20;
    }
    customForm.setFormValue("successRate", rate);
  }


  return (
    <div className={"container-lg py-5"}>
      <table className={"mx-auto spacing-table w-75"}>
        <tbody>
          <tr>
            <td colSpan={2}>
              <h5>Thông tin chung</h5>
            </td>
          </tr>
          <tr>
            <td>
              <CustomerField
                name={"customerId"}
                control={control}
                label={"Tổ chức"}
                onClear={onCustomerClear}
              />
            </td>
            <td>
              <ContactField
                name={"contactId"}
                control={control}
                label={"Liên hệ"}
                customForm={customForm}
              />
            </td>
          </tr>
          <tr>
            <td>
              <InputTextField
                name={"name"}
                control={control}
                label={"Tên cơ hội"}
                isRequired
              />
            </td>
            <td>
              <OpportunityPhaseField
                name={"opportunityPhaseId"}
                control={control}
                label={"Giai đoạn"}
                onChangeSelect={onChangeSelect}
              />
            </td>
          </tr>
          <tr>
            <td>
              <InputNumberField
                name={"successRate"}
                control={control}
                label={"Tỷ lệ thành công"}
                disabled
              />
            </td>
            <td>
              <InputDateField
                name={"expectedEndDate"}
                control={control}
                label={"Ngày kỳ vọng/kết thúc"}
              />
            </td>
          </tr>
          <tr>
            <td>
              <SourceField
                name={"sourceId"}
                control={control}
                label={"Nguồn gốc"}
              />
            </td>
          </tr>
          <tr>
            <td colSpan={2}>
              <h5>Thông tin địa chỉ</h5>
            </td>
          </tr>
          <AddressField control={control} customForm={customForm} />
        </tbody>
      </table>
      <br />
      <EditableTable />
    </div>
  );
};
export default OpportunityAddNew;
