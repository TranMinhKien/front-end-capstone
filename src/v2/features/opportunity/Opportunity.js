import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/js/bootstrap.js";
import axios from "axios";
import "../V2.css";
import authHeader from "services/auth_header";
import { Button, Modal } from "react-bootstrap";
import { DOMAIN_BE } from "constant/constant";
import { commonFetchDataHandler } from "utils/index";
import { missingPermission } from "utils/errorMessage";
import AuthService from "services/auth.service";
import { toastSuccess } from "utils/MessageSuccess";
import { ErrorMess } from "utils/MessageErrors";
import { convertColumn } from "v2/components/columns";
import { CloseOutlined, EditOutlined, EyeOutlined } from "@ant-design/icons";
import Tableb from "v2/components/table";
import { useHistory } from "react-router";
import { toast } from "react-toastify";

// API

const baseURL = DOMAIN_BE + "/api/opportunity/";
const baseURLcustomer = DOMAIN_BE + "/api/customer/name";
const baseURLcontact = DOMAIN_BE + "/api/contact/name";
const baseURLsource = DOMAIN_BE + "/api/source/name";
const baseURLphase = DOMAIN_BE + "/api/opportunityPhase/name";
const baseURLproduct = DOMAIN_BE + "/api/product/name";


function Opportunity() {

    const [data, setData] = useState([]);

    const [modalInsertar, setModalInsertar] = useState(false);

    const [modalEliminar, setModalEliminar] = useState(false);

    // lấy thông tin USER
    const user = AuthService.getCurrentUser();
    const [artistaSeleccionado, setArtistaSeleccionado] = useState({
        id: null,
        customerId: null,
        contactId: null,
        name: null,
        moneyAmount: null,
        opportunityPhaseId: null,
        successRate: null,
        expectedEndDate: null,
        expectedTurnOver: null,
        sourceId: null,
        productInfoDtos: [],
        errors: {
            name: "",
            successRate: "",
            opportunityPhaseId: "",
            expectedEndDate: ""
        }
    });


    const hasPerm = (user, perm = "") => {
        return user && user.roles && user.roles.includes(perm);
    };

    const [source3, setSource3] = useState({
        selectSource: [],
        id: "",
        name: ""
    });

    const [product3, setProduct3] = useState({
        selectProduct: [],
        id: "",
        name: ""
    });

    const [customer3, setCustomer3] = useState({
        selectCustomer: [],
        id: "",
        name: ""
    });


    const [phase3, setPhase3] = useState({
        selectPhase: [],
        id: "",
        name: ""
    });


    // xóa dữ liệu
    const deleteOppotunity = async () => {
        await axios
            .delete(baseURL + artistaSeleccionado.id, {
                headers: { Authorization: authHeader().Authorization }
            })
            .then((response) => {
                const newData = data;
                setData(newData.filter((name) => name.id !== artistaSeleccionado.id));
                abrirCerrarModalEliminar();
                toastSuccess("Xóa dữ liệu thành công");
            })
            .catch((error) => {
                ErrorMess(error);
            });
    };


    const abrirCerrarModalInsertar = () => {
        setModalInsertar(!modalInsertar);
    };

    const abrirCerrarModalEliminar = () => {
        setModalEliminar(!modalEliminar);
    };


    useEffect(() => {
        fetchData();
    }, []);

    const getProduct = async () => {
        const responseProduct = await commonFetchDataHandler(baseURLproduct);
        const optionsProduct = responseProduct.data.map((d) => ({
            value: d.id,
            label: d.name
        }));
        setProduct3({ selectProduct: optionsProduct });
    };

    // hiện lên thong tin bên trong

    const fetchData = async () => {
        const response = await commonFetchDataHandler(baseURL);
        response.data.forEach((element) => {
            //TODO3
            element.sourceId = element.source ? element.source.id : null;
            element.contactId = element.contact ? element.contact.id : null;
            element.customerId = element.customer ? element.customer.id : null;
            element.opportunityPhaseId = element.opportunityPhase
                ? element.opportunityPhase.id
                : null;
        });
        setData(response.data);

        const responsePhase = await commonFetchDataHandler(baseURLphase);
        const optionsPhase = responsePhase.data.map((d) => ({
            value: d.id,
            label: d.name
        }));
        optionsPhase.unshift({
            value: "",
            label: "-- Không chọn --"
        });
        setPhase3({ selectPhase: optionsPhase });


        const responseCustomer = await commonFetchDataHandler(baseURLcustomer);
        const optionsCustomer = responseCustomer.data.map((d) => ({
            value: d.id,
            label: d.name
        }));
        optionsCustomer.unshift({
            value: "",
            label: "-- Không chọn --"
        });
        setCustomer3({ selectCustomer: optionsCustomer });

        const responseSource = await commonFetchDataHandler(baseURLsource);
        const optionsSource = responseSource.data.map((d) => ({
            value: d.id,
            label: d.name
        }));
        optionsSource.unshift({
            value: "",
            label: "-- Không chọn --"
        });
        setSource3({ selectSource: optionsSource });

        getProduct();
    };


    const Delete = (
        <Modal
            show={modalEliminar}
            onHide={() => abrirCerrarModalEliminar()}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header>
                <Modal.Title>Xóa</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p> Bạn có chắc chắn muốn xóa không ? </p>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => abrirCerrarModalEliminar()}>
                    Không
                </Button>
                <Button variant="primary" onClick={() => deleteOppotunity()}>
                    Có
                </Button>
            </Modal.Footer>
        </Modal>
    );


    const history = useHistory();
    const listColumn = [
        {
            title: "Tên cơ hội",
            dataIndex: "name"
        },
        {
            title: "Liên hệ",
            dataIndex: "contact"
        },
        {
            title: "Tổ chức",
            dataIndex: "customer"
        },
        {
            title: "Nguồn gốc",
            dataIndex: "source"
        },
        {
            title: "Giai đoạn",
            dataIndex: "opportunityPhase"
        },
        {
            title: "Ngày kỳ vọng kết thúc",
            dataIndex: "expectedEndDate"
        },
        {
            title: "Hành động",
            key: "operation",
            fixed: "right",
            width: "150px",
            dataIndex: "action"
        }
    ];
    const handleMultiDelete = (selectedRowKeys) => {
        setArtistaSeleccionado(selectedRowKeys);
        abrirCerrarModalEliminar();
    };
    const handlDelete = (data) => {
        setArtistaSeleccionado(data);
        abrirCerrarModalEliminar();
    };
    const onEdit = (id) => {
        history.push(`/opportunity/edit/${id}`);
    };
    const onView = (id) => {
        history.push(`/opportunity/view/${id}`);
    };
    const onAddNew = () => {
            hasPerm(user, "ROLE_CREATE_OPPOTUNITY")
            ?
            history.push(`/opportunity/add`)
            : missingPermission()
    };

    const handleConvert = (item) => {
        if (item.length > 1) {
            toast.warning("Tính năng đang phát triển");
        } else {
            if (item[0].opportunityPhaseId === 5) {
                history.push(`/order/convert?opportunityId=` + item[0].id)
            } else {
                toast.warning("Cơ hội này chưa đến giai đoạn sinh đơn hàng");
            }
        }

    };
    const getDataTable = () => {
        let cloneData = data.map((item, index) => {
            return {
                ...item,
                key: index,
                source: item?.source?.name,
                customer: item?.customer?.name,
                contact: item?.contact?.fullName,
                opportunityPhase: item?.opportunityPhase?.name,
                action: (
                    <div className="d-flex align-items-center p-0 justify-content-between">
                        <EditOutlined
                            className=" btnIcon"
                            onClick={() => onEdit(item.id)}
                            style={{ color: "#C8DF52" }}
                        />
                        <CloseOutlined
                            className="btnIcon"
                            onClick={() => {
                                hasPerm(user, "ROLE_DELETE_CUSTOMER")
                                    ? handlDelete(item)
                                    : missingPermission();
                            }}
                            style={{ color: "#D2042D" }}
                        />
                        <EyeOutlined
                            onClick={() => onView(item.id)}
                            className=" btnIcon"
                            style={{ color: "#B3CDE0" }} />
                    </div>
                )
            };
        });
        return cloneData;
    };
    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-12">
                    <Tableb
                        columns={convertColumn(listColumn)}
                        handleMultiDelete={handleMultiDelete}
                        data={getDataTable()}
                        onAddNew={onAddNew}
                        handleConvert={handleConvert}
                        titlechucnang="Cơ Hội"
                        hasConvert
                    />

                    {Delete}
                </div>
            </div>
        </div>
    );
}

export default Opportunity;
