import React, { useContext, useEffect, useState } from "react";
import PropTypes from "prop-types";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Button, Form } from "antd";
import FormDataCustom from "../../utils/FormDataCustom";
import { TableProductType } from "../product-type/ProductTypeAddNew";
import { TableOpportunity } from "./OpportunityAddNew";
import { validateOpportunitySchema } from "../../config/validate/oppotunityValidate";
import { getOpportunityById } from "v2/api/readByIdAPI";
import moment from "moment";
import { toast } from "react-toastify";
import { updateOpportunity } from "v2/api/updateAPI";
import { ProductContext } from "v2/providers/provider";

OpportunityView.propTypes = {};

function OpportunityView(props) {
  const { products: productInfos, setContext } = useContext(ProductContext);
  const opportunityId = props.match.params.id;
  const history = useHistory();
  const rHF = useForm({
    resolver: yupResolver(validateOpportunitySchema),
  });
  const [form] = Form.useForm();
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = rHF;
  const customForm = new FormDataCustom(form, rHF);
  const goToOpportunity = () => history.push("/opportunity");

  // customForm.setFormValue('name','b')
  // customForm.setFormMany([{name: 'name', value: 'a'}, {name: 'shortName', value: 'b'}]);

  console.log(errors)
  const onSubmit = async (data) => {
    data.productInfoDtos = productInfos;
    data.id = opportunityId;
    try {
      const response = await updateOpportunity(data, opportunityId);
      if (response.error || !response.data) {
        throw new Error();
      }
      history.push("/opportunity");
      toast.success("Chỉnh sửa thành công");
    } catch (error) {
      toast.error("Chỉnh sửa thất bại");
    }
  };
  const [opportunity, setOpportunity] = useState();
  const setValueData = (data) => [
    {
      value: data?.customer?.id,
      name: "customerId"
    },
    {
      value: data?.contact?.id,
      name: "contactId"
    },
    {
      value: data?.name,
      name: "name",
    },
    {
      value: data?.opportunityPhase?.id,
      name: "opportunityPhaseId",
    },
    {
      value: data?.successRate,
      name: "successRate",
    },
    {
      value: data?.expectedEndDate ? new moment(data?.expectedEndDate) : null,
      name: "expectedEndDate",
    },
    {
      value: data?.source?.id,
      name: "sourceId",
    },
    {
      value: data?.address,
      name: "address",
    },  {
      value: data?.province?.id,
      name: "provinceId",
    },  {
      value: data?.district?.id,
      name: "districtId",
    },  {
      value: data?.ward?.id,
      name: "wardId",
    },
  ];
  useEffect(() => {
    const fetchOpportunity = async () => {
      try {
        setOpportunity({ ...opportunity, isLoading: true });
        const response = await getOpportunityById(opportunityId);
        if (response.error) {
          throw new Error(response.error);
        }
        setOpportunity({
          data: response.data,
          isLoading: false,
        });
        setContext(response.data.productInfos);

        customForm.setFormMany(setValueData(response.data));
      } catch (error) {
        console.log("fail to featch opportunity");
        setOpportunity({
          data: null,
          isLoading: false,
        });
      }
    };
    fetchOpportunity();
    return setContext([]);
  }, [opportunityId]);

  return (
    <Form form={form} onFinish={handleSubmit(onSubmit)}>
      <div className={"sticky-bar-top"}>
        <h5 className={"m-0"}>Chi tiết Cơ Hội</h5>
        <div className={"ms-auto"}>
          <Button type="dashed" onClick={goToOpportunity}>
            Huỷ
          </Button>
        </div>
      </div>
      <TableOpportunity control={control} customForm={customForm} />
    </Form>
  );
}

export default OpportunityView;
