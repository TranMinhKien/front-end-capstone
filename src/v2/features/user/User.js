import React, { Component, useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/js/bootstrap.js";
import axios from "axios";
// ES2015 module syntax
import MaterialTable from "material-table";
import { TextField } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import "../V2.css";
import Select from "react-select";
import { DOMAIN_BE } from "constant/constant";
import { commonFetchDataHandler } from "utils/index";
import { commonFetchAdd } from "utils/add";
import authHeader from "services/auth_header";
import { Button, Modal, Container, Row, Col } from "react-bootstrap";
import { missingPermission } from "utils/errorMessage";
import AuthService from "services/auth.service";
import validation from "utils/validation";
import VisibilityIcon from "@material-ui/icons/Visibility";
import { toastSuccess } from "utils/MessageSuccess";
import { ErrorMess } from "utils/MessageErrors";
import { useHistory } from "react-router";
import { CloseOutlined, EditOutlined, EyeOutlined } from "@ant-design/icons";
import Tableb from "v2/components/table";
import { convertColumn } from "v2/components/columns";

const baseURL = DOMAIN_BE + "/api/user/";
const baseURLRoles = DOMAIN_BE + "/api/role/name";

const useStyles = makeStyles((theme) => ({
  modal: {
    position: "absolute",
    width: 1200,

    backgroundColor: theme.palette.background.paper,
    border: "1px solid #000",
    // boxShadow: theme.shadows[5],
    boxShadow: "20px 10px",
    padding: theme.spacing(2, 4, 3),
    // padding: '1px 1px 1px 1px',
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
  iconos: {
    cursor: "pointer",
  },
  inputMaterial: {
    width: "50%",
    padding: "3px 10px",
    margin: "3px 0",
  },
}));
const validateForm = (errors) => {
  let valid = true;
  if (errors == null) return true;
  Object.values(errors).forEach((val) => val.length > 0 && (valid = false));
  return valid;
};

const validEmailRegex = RegExp(
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
);
const validPhoneRegex = RegExp(/^\d*$/);
const validPassword = RegExp("^(?=.*[A-Za-z])(?=.*d)[A-Za-zd]{8,}$");

function ProductType() {
  const styles = useStyles();
  // ném dữ liệu vào materialTbale dùng API
  const [data, setData] = useState([]);
  // // để hiện lên modal để tắt và bật
  const [modalInsertar, setModalInsertar] = useState(false);
  // ấn vào hàng nào nó sẽ tích vào hàng đấy
  const [modalEditar, setModalEditar] = useState(false);
  // khai báo state và setstate
  const [modalEliminar, setModalEliminar] = useState(false);
  const [modalViewInfor, setModalViewInfor] = useState(false);
  // cái này để khi nhập dữ liệu sẽ được thêm dữ liệu vào bảng
  // lấy thông tin USER
  const user = AuthService.getCurrentUser();

  const [artistaSeleccionado, setArtistaSeleccionado] = useState({
    username: null,
    password: null,
    email: null,
    roleIds: [],
    lastName: null,
    name: null,
    phone: null,
    dateOfBirth: null,
    gender: null,
    address: null,
    errors: {
      username: "",
      password: "",
      email: "",
      lastName: "",
      name: "",
      phone: "",
      dateOfBirth: "",
      gender: "",
      address: "",
    },
  });

  const [role2, setRole2] = useState({
    selectRole: [], // nó đã vào mảng này rồi có value và label rồi
    id: "",
    name: "",
  });

  const getRoles = async () => {
    const responseRoles = await commonFetchDataHandler(baseURLRoles);
    const optionsRoles = responseRoles.data.map((d) => ({
      value: d.id,
      label: d.name,
    }));
    setRole2({ selectRole: optionsRoles });
  };
  const clearErrors = () => {
    setArtistaSeleccionado((prevState) => ({
      ...prevState,
      errors: {},
    }));
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    let errors =
      artistaSeleccionado.errors || (artistaSeleccionado.errors = {});
    validation.value = value;

    switch (name) {
      case "username":
        errors.username = validation.validate("Người dùng", [
          validation.notBlank,
          validation.length(8, 32),
          validation.username,
        ]);
        break;
      case "email":
        errors.email = validation.validate("Địa chỉ email", [
          validation.notBlank,
          validation.email,
        ]);
        break;
      case "password":
        errors.password = validation.validate("Mật khẩu", [
          validation.notBlank,
          validation.length(8, 40),
          validation.password,
        ]);
        break;
      default:
        break;
    }
    setArtistaSeleccionado((prevState) => ({
      ...prevState,
      [name]: value || null,
    }));
    console.log(artistaSeleccionado);
  };

  const hasPerm = (user, perm = "") => {
    return user && user.roles && user.roles.includes(perm);
  };

  const handleChange2 = (e) => {
    artistaSeleccionado.roleIds = Array.isArray(e) ? e.map((x) => x.value) : [];
    artistaSeleccionado.roles = Array.isArray(e) ? e.map((x) => x) : [];
  };

  const getUser = async () => {
    const response = await commonFetchDataHandler(baseURL);
    if (response.data) {
      response.data.forEach((element) => {
        element.gender = element.gender ? element.gender.id : null;
        element.roleIds = element.roles
          ? element.roles.map((i) => i?.id)
          : null;
      });
      setData(response.data);
    }
  };

  // insert dữ liệu
  const createUsername = async () => {
    // Check validate khi ấn "Thêm"
    for (const name in artistaSeleccionado) {
      const value = artistaSeleccionado[name];
      handleChange({
        target: { name, value },
      });
    }
    // Nếu chưa valid thì không thêm dữ liệu
    if (!validateForm(artistaSeleccionado.errors)) return;
    // Thêm

    commonFetchAdd(baseURL, artistaSeleccionado)
      .then((responseSource) => {
        const d = responseSource.data;
        d.roleIds = d.role ? d.role.id : null;
        setData(data.concat(d));
        abrirCerrarModalInsertar();
        toastSuccess("Thêm dữ liệu thành công");
      })
      .catch((error) => {
        if (
          error.response &&
          error.response.data &&
          error.response.data.errors &&
          error.response.data.errors[0].type === "duplicate"
        ) {
          let errors =
            artistaSeleccionado.errors || (artistaSeleccionado.errors = {});
          error.response.data.errors.forEach((e) => {
            errors[e.field] = "Giá trị đã bị trùng";
          });
          setArtistaSeleccionado((prevState) => ({
            ...prevState,
            errors: errors,
          }));
          console.log(artistaSeleccionado);
        } else {
          ErrorMess(error);
        }
      });
  };

  // hiện lên để chỉnh sửa trong database
  const editUsername = async () => {
    await axios
      .put(baseURL + artistaSeleccionado.id, artistaSeleccionado, {
        headers: { Authorization: authHeader().Authorization },
      })
      .then((response) => {
        const dataNueva = data;
        dataNueva.map((id) => {
          if (id.id === artistaSeleccionado.id) {
            id.username = artistaSeleccionado.username;
            id.email = artistaSeleccionado.email;
            id.roles = response.data?.roles;
            id.roleIds = id.roles?.map((i) => i.id);
            id.lastName = artistaSeleccionado.lastName;
            id.name = artistaSeleccionado.name;
            id.phone = artistaSeleccionado.phone;
            id.address = artistaSeleccionado.address;
            id.dateOfBirth = artistaSeleccionado.dateOfBirth;
            id.gender = artistaSeleccionado.gender;
          }
        });
        setData(dataNueva);
        abrirCerrarModalEditar();
        toastSuccess("Edit thành công");
      })
      .catch((error) => {
        ErrorMess(error);
      });
  };
  // xóa dữ liệu
  const deleteUsername = async () => {
    await axios
      .delete(baseURL + artistaSeleccionado.id, {
        headers: { Authorization: authHeader().Authorization },
      })
      .then((response) => {
        setData(data.filter((name) => name.id !== artistaSeleccionado.id));
        abrirCerrarModalEliminar();
        toastSuccess("Xóa thành công");
      })
      .catch((error) => {
        ErrorMess(error);
      });
  };

  const seleccionarArtista = (id, caso) => {
    setArtistaSeleccionado(id);
    caso === "Editar"
      ? abrirCerrarModalEditar()
      : // viewInformation
        abrirCerrarModalEliminar();
  };

  const seleccionarView = (id, caso) => {
    setArtistaSeleccionado(id);
    if (caso === "check") {
      viewInformation();
    }
  };

  // // thực hiện lệnh tắt và bật  modal
  const abrirCerrarModalInsertar = () => {
    setModalInsertar(!modalInsertar);
  };
  // // cũng để bật tắt
  const abrirCerrarModalEditar = () => {
    setModalEditar(!modalEditar);
  };
  // // cũng để bật tắt
  const abrirCerrarModalEliminar = () => {
    setModalEliminar(!modalEliminar);
  };
  // dùng để bật tắt thông tin chi tiết

  const viewInformation = () => {
    setModalViewInfor(!modalViewInfor);
  };

  useEffect(() => {
    getUser();
    getRoles();
  }, []);

  const hasError = (name) => {
    return (
      artistaSeleccionado &&
      artistaSeleccionado.errors &&
      artistaSeleccionado.errors[name] &&
      artistaSeleccionado.errors[name].length > 0 && (
        <span className="error">{artistaSeleccionado.errors[name]}</span>
      )
    );
  };

  const bodyInsert = (
    <Modal
      size="lg"
      show={modalInsertar}
      onHide={() => abrirCerrarModalInsertar()}
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header>
        <Modal.Title>Thêm Người Dùng </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Container>
          <Row>
            <Col md={6}>
              <label className="required"> Tên tài khoản </label>
              <input
                type="text"
                class="form-control"
                name="username"
                onChange={handleChange}
              ></input>
              {hasError("username")}
            </Col>
            <Col md={6}>
              <label className="required">Mật Khẩu</label>
              <input
                type="password"
                minlength="8"
                required
                class="form-control"
                name="password"
                onChange={handleChange}
              ></input>
              {hasError("password")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label className="required">Email</label>
              <input
                type="text"
                class="form-control"
                name="email"
                onChange={handleChange}
              ></input>
              {hasError("email")}
            </Col>
            <Col md={6}>
              <label className="required"> Vai trò </label>
              <Select
                className="dropdown formselect controll1"
                placeholder="Select Option"
                isMulti
                options={role2.selectRole}
                onChange={handleChange2}
              ></Select>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Họ và Đệm </label>
              <input
                type="text"
                class="form-control"
                name="lastName"
                onChange={handleChange}
              ></input>
              {hasError("lastName")}
            </Col>
            <Col md={6}>
              <label> Tên </label>
              <input
                type="text"
                minlength="8"
                required
                class="form-control"
                name="name"
                onChange={handleChange}
              ></input>
              {hasError("name")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Điện thoại </label>
              <input
                type="text"
                class="form-control"
                name="phone"
                onChange={handleChange}
              ></input>
              {hasError("phone")}
            </Col>
            <Col md={6}>
              <label> Ngày sinh </label>
              <input
                type="date"
                minlength="8"
                required
                class="form-control"
                name="dateOfBirth"
                onChange={handleChange}
              ></input>
              {hasError("dateOfBirth")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Giới tính </label>
              <select
                className="form-control controll"
                value={artistaSeleccionado.gender}
                name="gender"
                onChange={handleChange}
              >
                <option value=""> -- Không chọn -- </option>
                <option value="MALE"> Nam </option>
                <option value="FEMALE"> Nữ </option>
                <option value="OTHER"> Khác </option>
              </select>
            </Col>
            <Col md={6}>
              <label> Địa chỉ </label>
              <input
                type="text"
                minlength="8"
                required
                class="form-control"
                name="address"
                onChange={handleChange}
              ></input>
              {hasError("address")}
            </Col>
          </Row>
        </Container>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={() => {
            clearErrors();
            abrirCerrarModalInsertar();
          }}
        >
          Hủy Bỏ
        </Button>
        <Button
          variant="primary"
          onClick={() => createUsername()}
          disabled={!validateForm(artistaSeleccionado.errors)}
        >
          {" "}
          Thêm{" "}
        </Button>
      </Modal.Footer>
    </Modal>
  );
  const bodyEdit = (
    <Modal
      size="lg"
      show={modalEditar}
      onHide={() => abrirCerrarModalEditar()}
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header>
        <Modal.Title>Sửa Thông Tin</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Container>
          <Row>
            <Col md={6}>
              <label> Username </label>
              <input
                type="text"
                class="form-control"
                name="username"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.username}
              ></input>
              {hasError("username")}
            </Col>
            <Col md={6}>
              <label>Email</label>
              <input
                type="text"
                class="form-control"
                name="email"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.email}
              ></input>
              {hasError("email")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Quyền</label>
              <Select
                className="dropdown controll1"
                placeholder="Select Option"
                isMulti
                options={role2.selectRole}
                onChange={handleChange2}
                defaultValue={
                  artistaSeleccionado &&
                  artistaSeleccionado.roles &&
                  artistaSeleccionado.roles.map((d) => ({
                    value: d.id ? d.id : d.value,
                    label: d.id ? d.name : d.label,
                  }))
                }
              ></Select>
            </Col>
            <Col md={6}>
              <label> Giới tính </label>
              <select
                className="form-control controll"
                name="gender"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.gender}
              >
                <option value=""> -- Không chọn -- </option>
                <option value="MALE"> Nam </option>
                <option value="FEMALE"> Nữ </option>
                <option value="OTHER"> Khác </option>
              </select>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Họ và Đệm </label>
              <input
                type="text"
                class="form-control"
                name="lastName"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.lastName}
              ></input>
              {hasError("lastName")}
            </Col>
            <Col md={6}>
              <label> Tên </label>
              <input
                type="text"
                minlength="8"
                required
                class="form-control"
                name="name"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.name}
              ></input>
              {hasError("name")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Điện thoại </label>
              <input
                type="text"
                class="form-control"
                name="phone"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.phone}
              ></input>
              {hasError("phone")}
            </Col>
            <Col md={6}>
              <label> Ngày sinh </label>
              <input
                type="date"
                minlength="8"
                required
                class="form-control"
                name="dateOfBirth"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.dateOfBirth}
              ></input>
              {hasError("dateOfBirth")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Địa chỉ </label>
              <input
                type="text"
                minlength="8"
                required
                class="form-control"
                name="address"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.address}
              ></input>
              {hasError("address")}
            </Col>
          </Row>
        </Container>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={() => {
            clearErrors();
            abrirCerrarModalEditar();
          }}
        >
          Hủy Bỏ
        </Button>
        <Button
          variant="primary"
          onClick={() => editUsername()}
          disabled={!validateForm(artistaSeleccionado.errors)}
        >
          Sửa
        </Button>
      </Modal.Footer>
    </Modal>
  );
  const View = (
    <Modal
      size="lg"
      show={modalViewInfor}
      onHide={() => viewInformation()}
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header>
        <Modal.Title>Xem Thông Tin Chi Tiết</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Container>
          <Row>
            <Col md={6}>
              <label> Tên tài khoản </label>
              <input
                type="text"
                class="form-control"
                name="username"
                value={artistaSeleccionado && artistaSeleccionado.username}
              ></input>
            </Col>
            <Col md={6}>
              <label>Email</label>
              <input
                type="text"
                class="form-control"
                name="email"
                value={artistaSeleccionado && artistaSeleccionado.email}
              ></input>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Quyền</label>
              <Select
                className="dropdown controll1"
                placeholder="Select Option"
                isMulti
                options={role2.selectRole}
                defaultValue={
                  artistaSeleccionado &&
                  artistaSeleccionado.roles &&
                  artistaSeleccionado.roles.map((d) => ({
                    value: d.id ? d.id : d.value,
                    label: d.id ? d.name : d.label,
                  }))
                }
              ></Select>
            </Col>
            <Col md={6}>
              <label> Giới tính </label>
              <select
                className="form-control controll"
                value={artistaSeleccionado.gender}
                name="gender"
                value={artistaSeleccionado && artistaSeleccionado.gender}
              >
                <option value=""> -- Không chọn -- </option>
                <option value="MALE"> Nam </option>
                <option value="FEMALE"> Nữ </option>
                <option value="OTHER"> Khác </option>
              </select>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Họ và Đệm </label>
              <input
                type="text"
                class="form-control"
                name="lastName"
                value={artistaSeleccionado && artistaSeleccionado.lastName}
              ></input>
              {hasError("lastName")}
            </Col>
            <Col md={6}>
              <label> Tên </label>
              <input
                type="text"
                minlength="8"
                required
                class="form-control"
                name="name"
                value={artistaSeleccionado && artistaSeleccionado.name}
              ></input>
              {hasError("name")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Điện thoại </label>
              <input
                type="text"
                class="form-control"
                name="phone"
                value={artistaSeleccionado && artistaSeleccionado.phone}
              ></input>
              {hasError("phone")}
            </Col>
            <Col md={6}>
              <label> Ngày sinh </label>
              <input
                type="date"
                minlength="8"
                required
                class="form-control"
                name="dateOfBirth"
                value={artistaSeleccionado && artistaSeleccionado.dateOfBirth}
              ></input>
              {hasError("dateOfBirth")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Địa chỉ </label>
              <input
                type="text"
                minlength="8"
                required
                class="form-control"
                name="address"
                value={artistaSeleccionado && artistaSeleccionado.address}
              ></input>
              {hasError("address")}
            </Col>
            <Col md={6}>
              <label>Người sửa </label>
              <input
                type="text"
                class="form-control"
                name="updatedBy"
                value={
                  artistaSeleccionado &&
                  artistaSeleccionado.updatedBy &&
                  artistaSeleccionado.updatedBy.username
                }
              ></input>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Ngày Tạo </label>
              <input
                type="text"
                class="form-control"
                name="createdAt"
                value={artistaSeleccionado && artistaSeleccionado.createdAt}
              ></input>
            </Col>
            <Col md={6}>
              <label> Ngày sửa </label>
              <input
                type="text"
                class="form-control"
                name="updatedAt"
                value={artistaSeleccionado && artistaSeleccionado.updatedAt}
              ></input>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Người tạo </label>
              <input
                type="text"
                class="form-control"
                name="createdBy"
                value={
                  artistaSeleccionado &&
                  artistaSeleccionado.createdBy &&
                  artistaSeleccionado.createdBy.username
                }
              ></input>
            </Col>
          </Row>
        </Container>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={() => viewInformation()}>
          Hủy Bỏ
        </Button>
      </Modal.Footer>
    </Modal>
  );

  const Delete = (
    <Modal
      show={modalEliminar}
      onHide={() => abrirCerrarModalEliminar()}
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header>
        <Modal.Title>Xóa</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p> Bạn có chắc chắn muốn xóa không ? </p>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={() => abrirCerrarModalEliminar()}>
          Không
        </Button>
        <Button variant="primary" onClick={() => deleteUsername()}>
          Có
        </Button>
      </Modal.Footer>
    </Modal>
  );

  const history = useHistory();
  const listColumn = [
    {
      title: "Tên tài khoản",
      dataIndex: "username",
    },
    {
      title: "Email",
      dataIndex: "email",
      width:250,
    },
    {
      title: "Họ và đệm",
      dataIndex: "lastName",
    },
    {
      title: "Tên",
      dataIndex: "name",
    },
    {
      title: "Điện thoại",
      dataIndex: "phone",
    },
    {
      title: "Ngày sinh",
      dataIndex: "dateOfBirth",
    },
    {
      title: "Giới tính",
      dataIndex: "gender",
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
    },
    {
      title: "Hành động",
      key: "operation",
      fixed: "right",
      width: "150px",
      dataIndex: "action",
    },
  ];
  const handleMultiDelete = (selectedRowKeys) => {
    setArtistaSeleccionado(selectedRowKeys);
    abrirCerrarModalEliminar();
  };
  const handlDelete = (data) => {
    setArtistaSeleccionado(data);
    abrirCerrarModalEliminar();
  };
  const onEdit = (id) => {
    history.push(`/user/edit/${id}`);
  };
  const onView = (id) => {
    history.push(`/user/view/${id}`);
  };
  const onAddNew = () => {
    // setModalInsertar(true);
    // abrirCerrarModalInsertar();
    hasPerm(user, "ROLE_CREATE_USER")
     ? 
     history.push(`/user/add`)
     : missingPermission()

  };
  const getDataTable = () => {
    let cloneData = data.map((item, index) => {
      return {
        ...item,
        key: index,
        gender: item?.gender?.name,
        action: (
          <div className="d-flex align-items-center p-0 justify-content-between">
            <EditOutlined
              className=" btnIcon"
              onClick={() => {
                hasPerm(user, "ROLE_UPDATE_USER")
                  ? onEdit(item.id)
                  : missingPermission()
              }}
              style={{ color: "#C8DF52" }}
            />
            <CloseOutlined
              className="btnIcon"
              onClick={() => {
                hasPerm(user, "ROLE_DELETE_USER")
                  ? handlDelete(item)
                  : missingPermission();
              }}
              style={{ color: "#D2042D" }}
            />
            <EyeOutlined
              onClick={() => {
                hasPerm(user, "ROLE_READ_USER")
                  ? onView(item.id)
                  : missingPermission()
              }}
              className=" btnIcon"
              style={{ color: "#B3CDE0" }} />
          </div>
        ),
      };
    });
    return cloneData;
  };
  return (
    <div className="container-fluid">
      {/* <div className="row backgroud-insert">
                <div className="col-md-10"></div>
                <div className="col-md-2 them-customer">
                    <Button variant="primary" onClick={hasPerm(user, "ROLE_UPDATE_USER") ? () => abrirCerrarModalInsertar() : () => missingPermission()} >Thêm</Button>
                </div>
            </div> */}
      <div className="row">
        <div className="col-md-12">
          <Tableb
            columns={convertColumn(listColumn)}
            handleMultiDelete={handleMultiDelete}
            data={getDataTable()}
            onAddNew={onAddNew}
            titlechucnang = "Người Dùng "
          />
          {bodyInsert}
          {bodyEdit}
          {View}
          {Delete}
        </div>
      </div>
    </div>
  );
}

export default ProductType;
