import React, {useEffect, useState} from 'react';
import {useHistory} from 'react-router-dom';
import {useForm} from 'react-hook-form';
import {yupResolver} from '@hookform/resolvers/yup';
import {Button, Form} from 'antd';
import FormDataCustom from '../../utils/FormDataCustom';
import {toast} from 'react-toastify';
import {updateProduct, updateUser} from 'v2/api/updateAPI';
import {getProductById, getUserById} from 'v2/api/readByIdAPI';
import {validateUserSchema} from '../../config/validate/userValidate';
import { TableUser } from './UserAddNew';
import { convertToSingleArray } from 'v2/components/columns';

UserView.propTypes = {};

function UserView(props) {
    const userId = props.match.params.id;

    const history = useHistory();

    const rHF = useForm({
        resolver: yupResolver(validateUserSchema),
        mode: "onChange"
    });

    const [form] = Form.useForm();

    const {control, handleSubmit, formState: {errors}} = rHF;

    const customForm = new FormDataCustom(form, rHF);

    console.log(errors);

    const goToUser = () => history.push('/user');

    const onSubmit = async (data) => {
        try {
            const response = await updateUser(data, userId);
            if (response.error) {
                throw new Error(response.error);
            }
            if (!response?.data) {
                throw new Error();
            }
            history.push('/user');
            toast.success('Cập nhận thành công');
        } catch (error) {
            toast.error('Cập nhật không thành công');
        }
    };

    const setValueData = (data) => [
        {
            value: data?.username,
            name: "username"
        },
        {
            value: data?.email,
            name: "email"
        },
        {
            value: convertToSingleArray(data.roles),
            name: "roleIds"
        },
        {
            value: data?.lastName,
            name: "lastName"
        },
        {
            value: data?.name,
            name: "name"
        },
        {
            value: data?.phone,
            name: "phone"
        },
        {
            value: data?.dateOfBirth,
            name: "dateOfBirth"
        },
        {
            value: data?.address?.id,
            name: "address"
        },
        {
            value: data?.genderId?.id,
            name: "genderId"
        }
    ];

    const [user, setUser] = useState();

    useEffect(() => {
        const fetchUser = async () => {
            try {
                setUser({...user, isLoading: true});
                const response = await getUserById(userId);
                if (response.error) {
                    throw new Error(response.error);
                }
                setUser({
                    data: response.data,
                    isLoading: false
                });
                customForm.setFormMany(setValueData(response.data));
            } catch (error) {
                console.log("fail to featch product");
                setUser({
                    data: null,
                    isLoading: false
                });
            }
        };
        fetchUser();
    }, [userId]);
    return (
        <Form form={form} onFinish={handleSubmit(onSubmit)}>
            <div className={'sticky-bar-top'}>
                <h5 className={'m-0'}>Chỉnh Sửa người dùng</h5>
                <div className={'ms-auto'}>
                    <Button type="dashed" onClick={goToUser}>Huỷ</Button>
                </div>
            </div>
            <TableUser control={control}/>
        </Form>
    );
}

export default UserView;
