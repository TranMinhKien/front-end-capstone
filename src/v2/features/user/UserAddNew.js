import React from "react";
import PropTypes from "prop-types";
import InputTextField from "../../components/CustomFields/InputTextField";
import SelectField from "../../components/CustomFields/SelectField";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Button, Form } from "antd";
import FormDataCustom from "../../utils/FormDataCustom";
import {
  TableProductType,

} from "../product-type/ProductTypeAddNew";
import GenderField from "v2/components/customElement/GenderField";

import InputDateField from "v2/components/CustomFields/InputDateField";
import * as yup from "yup";
import InputPhoneField from '../../components/CustomFields/InputPhoneField';
import UnitField from "v2/components/customElement/UnitField";
import { setDateToString } from '../../utils/format';
import { createOrder, createUser } from '../../api/createAPI';
import { toast } from 'react-toastify';
import { validateUserSchema } from '../../config/validate/userValidate';
import InputNumberField from '../../components/CustomFields/InputNumberField';
import PermissionField from "v2/components/customElement/PermissionField";

UserAddNew.propTypes = {};

function UserAddNew(props) {
  const history = useHistory();

  const rHF = useForm({
    resolver: yupResolver(validateUserSchema),
  });

  const [form] = Form.useForm();
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = rHF;
  const customForm = new FormDataCustom(form, rHF);

  console.log(errors);

  const goToUser = () => history.push("/user");

  async function onSubmit(data) {
    try {
      setDateToString(data);
      await createUser(data);
      history.push('/user');
      toast.success("Thêm thành công");
    } catch (e) {
      toast.error("Thêm thất bại");
    }
  }

  return (
    <Form form={form} onFinish={handleSubmit(onSubmit)}>
      <div className={"sticky-bar-top"}>
        <h5 className={"m-0"}>Thêm Tài Khoản</h5>
        <div className={"ms-auto"}>
          <Button type="dashed" onClick={goToUser}>
            Huỷ
          </Button>
          <Button type="primary" htmlType={"submit"} className={"ms-2"}>
            Thêm
          </Button>
        </div>
      </div>
      <TableUser control={control} />
    </Form>
  );
}

export const TableUserEdit = ({ control }) => {
  return (
    <div className={"container-lg py-5"}>
      <table className={"mx-auto spacing-table w-75"}>
        <tbody>
          <tr>
            <td colSpan={2}>
              <h5>Thông tin chung</h5>
            </td>
          </tr>
          <tr>
            <td>
              <InputTextField
                name={"username"}
                control={control}
                label={"Tên tài khoản"}
                isRequired
              />
            </td>
            <td>
              <InputTextField
                name={"email"}
                control={control}
                label={"Email"}
                isRequired
              />
            </td>
          </tr>
          <tr>
            <td>
              <PermissionField name={"roleIds"} control={control} />

            </td>
            <td>
              <InputTextField
                control={control}
                name={"lastName"}
                label={"Họ và đệm"}
              />
            </td>
          </tr>
          <tr>
            <td>
              <InputTextField
                control={control}
                name={"name"}
                label={"Tên"}
              />
            </td>
            <td>
              <InputPhoneField
                control={control}
                inputName={"phone"}
                selectName={"prefixPhone"}
                label={"Điện thoại"}
              />
            </td>
          </tr>
          <tr>
            <td>
              <InputDateField
                control={control}
                name={"dateOfBirth"}
                label={"Ngày sinh"}
              />
            </td>
            <td>
              <GenderField control={control} name="genderId" />

            </td>
          </tr>
          <tr>
            <td>
              <InputTextField
                name={"address"}
                control={control}
                label={"Địa chỉ "}
              />
            </td>

          </tr>
        </tbody>
      </table>
    </div>
  )
}

export const TableUser = ({ control }) => {
  return (
    <div className={"container-lg py-5"}>
      <table className={"mx-auto spacing-table w-75"}>
        <tbody>
          <tr>
            <td colSpan={2}>
              <h5>Thông tin chung</h5>
            </td>
          </tr>
          <tr>
            <td>
              <InputTextField
                name={"username"}
                control={control}
                label={"Tên tài khoản"}
                isRequired
              />
            </td>
            <td>
              <InputTextField
                name={"password"}
                control={control}
                label={"Mật Khẩu"}
                isRequired
              />
            </td>
          </tr>
          <tr>
            <td>
              <InputTextField
                name={"email"}
                control={control}
                label={"Email"}
                isRequired
              />
            </td>
            <td>
              <PermissionField name={"roleIds"} control={control} />
            </td>
          </tr>
          <tr>
            <td>
              <InputTextField
                control={control}
                name={"lastName"}
                label={"Họ và đệm"}
              />
            </td>
            <td>
              <InputTextField
                control={control}
                name={"name"}
                label={"Tên"}
              />
            </td>
          </tr>
          <tr>
            <td>
              <InputPhoneField
                control={control}
                inputName={"phone"}
                selectName={"prefixPhone"}
                label={"Điện thoại"}
              />
            </td>
            <td>
              <InputDateField
                control={control}
                name={"dateOfBirth"}
                label={"Ngày sinh"}
              />
            </td>
          </tr>
          <tr>
            <td>
              <GenderField control={control} name="genderId" />

            </td>
            <td>
              <InputTextField
                name={"address"}
                control={control}
                label={"Địa chỉ "}
              />
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}

export default UserAddNew;
