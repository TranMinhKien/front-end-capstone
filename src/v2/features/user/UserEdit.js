import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Form } from 'antd';
import FormDataCustom from '../../utils/FormDataCustom';
import { toast } from 'react-toastify';
import { updateProduct, updateUser } from 'v2/api/updateAPI';
import { getProductById, getUserById } from 'v2/api/readByIdAPI';
import { validateUser2 } from '../../config/validate/uservalidate2';
import { TableUser } from './UserAddNew';
import { convertToSingleArray } from 'v2/components/columns';
import InputTextField from "../../components/CustomFields/InputTextField";
import PermissionField from "v2/components/customElement/PermissionField";
import InputDateField from "v2/components/CustomFields/InputDateField";
import InputPhoneField from '../../components/CustomFields/InputPhoneField';
import GenderField from "v2/components/customElement/GenderField";

UserEdit.propTypes = {};

function UserEdit(props) {
    const userId = props.match.params.id;

    const history = useHistory();

    const rHF = useForm({
        resolver: yupResolver(validateUser2),
        mode: "onChange"
    });

    const [form] = Form.useForm();

    const { control, handleSubmit, formState: { errors } } = rHF;

    const customForm = new FormDataCustom(form, rHF);

    console.log(errors);

    const goToUser = () => history.push('/user');

    const onSubmit = async (data) => {
        try {
            const response = await updateUser(data, userId);
            if (response.error) {
                throw new Error(response.error);
            }
            if (!response?.data) {
                throw new Error();
            }
            history.push('/user');
            toast.success('Cập nhận thành công');
        } catch (error) {
            toast.error('Cập nhật không thành công');
        }
    };

    const setValueData = (data) => [
        {
            value: data?.username,
            name: "username"
        },
        {
            value: data?.email,
            name: "email"
        },
        {
            value: convertToSingleArray(data.roles),
            name: "roleIds"
        },
        {
            value: data?.lastName,
            name: "lastName"
        },
        {
            value: data?.name,
            name: "name"
        },
        {
            value: data?.phone,
            name: "phone"
        },
        {
            value: data?.dateOfBirth,
            name: "dateOfBirth"
        },
        {
            value: data?.address?.id,
            name: "address"
        },
        {
            value: data?.genderId?.id,
            name: "genderId"
        }
    ];

    const [user, setUser] = useState();

    useEffect(() => {
        const fetchUser = async () => {
            try {
                setUser({ ...user, isLoading: true });
                const response = await getUserById(userId);
                if (response.error) {
                    throw new Error(response.error);
                }
                setUser({
                    data: response.data,
                    isLoading: false
                });
                customForm.setFormMany(setValueData(response.data));
            } catch (error) {
                console.log("fail to featch product");
                setUser({
                    data: null,
                    isLoading: false
                });
            }
        };
        fetchUser();
    }, [userId]);
    return (
        <Form form={form} onFinish={handleSubmit(onSubmit)}>
            <div className={'sticky-bar-top'}>
                <h5 className={'m-0'}>Chỉnh Sửa người dùng</h5>
                <div className={'ms-auto'}>
                    <Button type="dashed" onClick={goToUser}>Huỷ</Button>
                    <Button type="primary" htmlType={'submit'}
                        className={'ms-2'}>Lưu</Button>
                </div>
            </div>
            <TableUserEdit control={control} />
        </Form>
    );
}

export const TableUserEdit = ({ control }) => {
    return (
        <div className={"container-lg py-5"}>
            <table className={"mx-auto spacing-table w-75"}>
                <tbody>
                    <tr>
                        <td colSpan={2}>
                            <h5>Thông tin chung</h5>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <InputTextField
                                name="username"
                                control={control}
                                label={"Tên tài khoản"}
                                isRequired
                                disabled
                            />
                        </td>
                        <td>
                            <InputTextField
                                name={"email"}
                                control={control}
                                label={"Email"}
                                isRequired
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <PermissionField name={"roleIds"} control={control} />
                        </td>
                        <td>
                            <InputTextField
                                control={control}
                                name={"lastName"}
                                label={"Họ và đệm"}
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <InputTextField
                                control={control}
                                name={"name"}
                                label={"Tên"}
                            />
                        </td>
                        <td>
                            <InputPhoneField
                                control={control}
                                inputName={"phone"}
                                selectName={"prefixPhone"}
                                label={"Điện thoại"}
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <InputDateField
                                control={control}
                                name={"dateOfBirth"}
                                label={"Ngày sinh"}
                            />
                        </td>
                        <td>
                            <GenderField control={control} name="genderId" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <InputTextField
                                name={"address"}
                                control={control}
                                label={"Địa chỉ "}
                            />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}
export default UserEdit;
