import { ArrowLeftOutlined } from "@ant-design/icons";
import { Button, Modal, Radio, Space } from "antd";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router";
import { toast } from "react-toastify";
import { potentialSingleConvert } from "v2/api/convertAPI";
import { getCustomerByName, getPotentialById } from "v2/api/readByIdAPI";

function PotentialConvert(props) {
  const potentialId = props.match.params.id;
  const history = useHistory();
  const [potential, setPotential] = useState();
  const [listCustomer, setlistCustomer] = useState();
  const [selected, setSelected] = useState(0);


  const handleChange = (e) => {
    setSelected(e.target.value);
  };
  const onConvert = async () => {
    try{
      const response = await potentialSingleConvert(potentialId,selected);
      if(response.error){
        throw new Error(response.error);
      }
      toast.success("Chuyển đổi thành công")
      history.push("/contact");
    }
    catch(error){
      toast.error(error.data)
    }
    
  };
  const info=(customer)=> {
    Modal.info({
      title: 'Thông tin tổ chức',
      content: (
        <table>
          <tbody>
            <tr>
              <th>Tên tổ chức:</th>
              <td>{customer?.name}</td>
            </tr>
            <tr>
              <th>Mã số thuế:</th>
              <td>{customer?.taxCode}</td>
            </tr>
            <tr>
              <th>Loại khách hàng:</th>
              <td>{customer?.type?.name}</td>
            </tr>
          </tbody>
        </table>
      ),
      onOk() {},
    });
  }

  const fetchPotential = async () => {
    try {
      const response = await getPotentialById(potentialId);
      if (response.error) {
        throw new Error(response.error);
      }
      setPotential(response.data);
      if (response?.data?.customer) {
        fetchCustomerByName(response?.data?.customer);
      }
    } catch (error) {
      console.log("fail to featch potential");
    }
  };
  const fetchCustomerByName = async (name) => {
    try {
      const response = await getCustomerByName(name);
      if (response.error) {
        throw new Error(response.error);
      }
      setlistCustomer(response.data);
    } catch (error) {
      console.log("fail to featch potential");
    }
  };
  useEffect(() => {
    fetchPotential();
  }, [potentialId]);
  return (
    <div>
      <div className={"sticky-bar-top"}>
        <h5 className={"m-0"}>
          <Button
            type="text"
            onClick={() => history.push("/potential")}
            icon={<ArrowLeftOutlined />}
          />
          Chuyển đổi tiềm năng
        </h5>
      </div>
      <div>
        <div className={"p-5 d-flex flex-column"}>
          <Radio.Group>
            <Space direction="vertical" onChange={handleChange}>
              {potential?.customer ? (
                <>
                  {listCustomer?.length > 0 &&
                    listCustomer.map((item, i) => (
                      <Radio value={item.id} key={i}>
                        Thêm vào tổ chức đã tồn tại :{" "}
                        <b> {potential?.customer}</b>
                        <Button
                          type="link"
                          onClick={() => {
                            info(item)
                          }}
                        >
                          Xem
                        </Button>
                      </Radio>
                    ))}
                  <Radio value={0}>
                    Thêm mới tổ chức : <b> {potential?.customer}</b>
                  </Radio>
                </>
              ) : (
                ""
              )}
            </Space>
          </Radio.Group>
          <p></p>
          <p>
            Thêm mới liên hệ <b>{potential?.name}</b>
          </p>
          <div className="mt-3 d-flex">
            <Button type="default" onClick={() => history.push("/potential")}>
              Hủy
            </Button>
            <div className="ps-2">
              <Button type="primary" disabled={!selected&&potential?.customer} onClick={onConvert}>
                Chuyển đổi
              </Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PotentialConvert;
