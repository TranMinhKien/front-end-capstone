import React from "react";
import ClassificationField from "v2/components/customElement/ClassificationField";
import DepartmentField from "v2/components/customElement/DepartmentField";
import GenderField from "v2/components/customElement/GenderField";
import PositionField from "v2/components/customElement/PositionField";
import SourceField from "v2/components/customElement/SourceField";
import TypeField from "v2/components/customElement/TypeField";
import VocativeField from "v2/components/customElement/VocativeField";
import InputDateField from "v2/components/CustomFields/InputDateField";
import InputTextField from "v2/components/CustomFields/InputTextField";
import "./css/addNew.css";
import InputPhoneField from "../../components/CustomFields/InputPhoneField";
import BusinessTypeField from "v2/components/customElement/BusinessTypeField";
import CheckBoxField from "v2/components/CustomFields/CheckBoxField";

TablePotential.propTypes = {};

function TablePotential(props) {
	const { control } = props;
	return (
		<div>
			<table className={"mx-auto spacing-table w-75 "}>
				<tbody>
					<tr>
						<td>
							<h5>Thông tin chung</h5>
						</td>
					</tr>
					<tr>
						<td>
							<VocativeField name={"vocativeId"} control={control} />
						</td>
						<td>
							<InputTextField
								control={control}
								name="lastName"
								label={"Họ và đệm"}
							/>
						</td>
					</tr>
					<tr>
						<td>
							<InputTextField
								control={control}
								name="name"
								label={"Tên"}
								isRequired
							/>
						</td>
						<td>
							<PositionField name={"positionId"} control={control} />
						</td>
					</tr>
					<tr>
						<td>
							<DepartmentField name={"departmentId"} control={control} />
						</td>
						<td>
							<InputPhoneField
								control={control}
								inputName="phone"
								selectName={"prefixPhone"}
								label={"Điện thoại cá nhân"}
							/>
						</td>
					</tr>
					<tr>
						<td>
							<InputPhoneField
								control={control}
								inputName="officePhone"
								selectName="phoneAreaCode"
								label={"Điện thoại cơ quan"}
							/>
						</td>
						<td>
							<InputPhoneField
								control={control}
								inputName="otherPhone"
								selectName="prefixOtherPhone"
								label={"Điện thoại khác"}
							/>
						</td>
					</tr>
					<tr>
						<td>
							<ClassificationField
								name={"classificationIds"}
								control={control}
								label={"Loại tiềm năng"}
							/>
						</td>
						<td>
							<SourceField name={"sourceId"} control={control} />
						</td>
					</tr>
					<tr>
						<td className={'align-bottom'}>

							<CheckBoxField
								name={"notSendEmail"}
								control={control}
								label={"Không gửi email"}
							/>
						</td>
						<td className={'align-bottom'}>
							<CheckBoxField
								name={"notCallPhone"}
								control={control}
								label={"Không gọi điện"}
							/>
						</td>
					</tr>
					<tr>
						<td>
							<InputTextField control={control} name="email" label={"Email"} />
						</td>
						<td>
							<InputTextField
								control={control}
								name="officeEmail"
								label={"Email cơ quan"}
							/>
						</td>
					</tr>
					<tr>
						<td>
							<InputTextField
								control={control}
								name="customer"
								label={"Tổ chức"}
							/>
						</td>
						<td>
							<InputTextField
								control={control}
								name="taxCode"
								label={"Mã số thuế"}
							/>
						</td>
					</tr>
					<tr>
						<td>
							<InputTextField
								control={control}
								name="customerTaxCode"
								label={"Mã số thuế tổ chức"}
							/>
						</td>
					</tr>
					<tr>
						<h5>Thông tin cá nhân</h5>
					</tr>
					<tr>
						<td>
							<GenderField control={control} name="genderId" isRequired />
						</td>
						<td>
							<InputDateField
								control={control}
								name="dateOfBirth"
								label={"Ngày sinh"}
							/>
						</td>
					</tr>
					<tr>
						<td>
							<InputTextField
								control={control}
								name="facebook"
								label={"Facebook"}
							/>
						</td>
					</tr>
					<tr>
						<h5>Thông tin tổ chức</h5>
					</tr>
					<tr>
						<td>
							<InputTextField
								control={control}
								name="bankAccount"
								label={"Tài khoản ngân hàng"}
							/>
						</td>
						<td>
							<InputTextField
								control={control}
								name="bank"
								label={"Mở tại ngân hàng"}
							/>
						</td>
					</tr>
					<tr>
						<td>
							<InputDateField
								control={control}
								name="foundedDate"
								label={"Ngày thành lập"}
							/>
						</td>
						<td>
							<BusinessTypeField control={control} name="businessTypeId" />
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	);
}

export default TablePotential;
