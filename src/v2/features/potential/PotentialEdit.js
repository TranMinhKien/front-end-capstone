import {yupResolver} from "@hookform/resolvers/yup";
import {Button, Form} from "antd";
import moment from "moment";
import React, {useEffect, useState} from "react";
import {useForm} from "react-hook-form";
import {useHistory} from "react-router";
import {toast} from "react-toastify";
import {getPotentialById} from "v2/api/readByIdAPI";
import {updatePotential} from "v2/api/updateAPI";
import FormDataCustom from "v2/utils/FormDataCustom";
import {validatePotentialSchema} from '../../config/validate/potentialValidate';
import "./css/addNew.css";
import TablePotential from "./TablePotential";
import {setDateToString} from '../../utils/format';
import { convertToSingleArray } from "v2/components/columns";


PotentialEdit.propTypes = {};

function PotentialEdit(props) {
    const [form] = Form.useForm();
    const rHF = useForm({
        resolver: yupResolver(validatePotentialSchema),
        mode: "onChange"
    });
    const potentialId = props.match.params.id;
    const history = useHistory();
    const [potential, setPotential] = useState({
        data: null,
        isLoading: false
    });
    const {
        control,
        formState: {errors},
        watch,
        handleSubmit,
        setValue
    } = rHF;
    const onSubmit = async (data) => {

        try {

            setDateToString(data);
            await updatePotential(data, potentialId);
            history.push('/potential');
            toast.success("Cập nhật thành công");
        } catch (error) {
            toast.error('Cập nhật không thành công');
        }
    };

    const formCustom = new FormDataCustom(form, rHF);
    const goToPotential = () => {
        history.push("/potential");
    };


    const setValuePotential = (data) => [
        {
            value: data?.vocative?.id,
            name: "vocativeId"
        },
        {
            value: data?.lastName,
            name: "lastName"
        },
        {
            value: data?.name,
            name: "name"
        },
        {
            value: data?.position?.id,
            name: "positionId"
        },
        {
            value: data?.phone,
            name: "phone"
        },
        {
            value: data?.officePhone,
            name: "officePhone"
        },
        {
            value: data?.otherPhone,
            name: "otherPhone"
        },
        {
            value: data?.email,
            name: "email"
        },
        {
            value: data?.department?.id,
            name: "departmentId"
        },
        {
            value: convertToSingleArray(data?.classifications),
            name: "classificationIds"
        },
        {
            value: data?.source?.id,
            name: "sourceId"
        },
        {
            value: data?.notCallPhone,
            name: "notCallPhone"
        },
        {
            value: data?.notSendEmail,
            name: "notSendEmail"
        },
        {
            value: data?.officeEmail,
            name: "officeEmail"
        },
        {
            value: data?.customer,
            name: "customer"
        },
        {
            value: data?.taxCode,
            name: "taxCode"
        },
        {
            value: data?.gender?.id,
            name: "genderId"
        },
        {
            value: data?.customerTaxCode,
            name: "customerTaxCode"
        },
        {
            value: data?.dateOfBirth ? new moment(data?.dateOfBirth) : null,
            name: "dateOfBirth"
        },
        {
            value: data?.facebook,
            name: "facebook"
        },
        {
            value: data?.bankAccount,
            name: "bankAccount"
        },
        {
            value: data?.bank,
            name: "bank"
        },
        {
            value: data?.foundedDate ? new moment(data?.foundedDate) : null,
            name: "foundedDate"
        },
        {
            value: data?.businessType?.id,
            name: "businessTypeId"
        }
    ];


    useEffect(() => {
        const fetchPotential = async () => {
            try {
                setPotential({...potential, isLoading: true});
                const response = await getPotentialById(potentialId);
                if (response.error) {
                    throw new Error(response.error);
                }
                setPotential({
                    data: response.data,
                    isLoading: false
                });
                formCustom.setFormMany(setValuePotential(response.data));
            } catch (error) {
                console.log("fail to featch potential");
                setPotential({
                    data: null,
                    isLoading: false
                });
            }
        };
        fetchPotential();
    }, [potentialId]);
    return (
        <div className="">
            <Form form={form} onFinish={handleSubmit(onSubmit)}>
                <div className={"sticky-bar-top"}>
                <h5 className={"m-0"}>Chỉnh Sửa Khách Hàng Tiềm Năng</h5>
                    <div className={"ms-auto"}>
                        <Button type="dashed" onClick={goToPotential}>
                            Huỷ
                        </Button>{" "}
                        <Button type="primary" htmlType={"submit"} className={"ms-2"}>
                            Lưu
                        </Button>
                    </div>
                </div>
                <div className="py-4">
                    <TablePotential control={control}/>
                </div>
            </Form>
        </div>
    );
}

export default PotentialEdit;
