import { yupResolver } from "@hookform/resolvers/yup";
import { Form } from "antd";
import { Button } from "antd";
import React from "react";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router";
import FormDataCustom from "v2/utils/FormDataCustom";
import * as yup from "yup";
import "./css/addNew.css";
import TablePotential from "./TablePotential";

PotentialView.propTypes = {};

function PotentialView(props) {
  const schema = yup.object().shape({});
  const [form] = Form.useForm();
  const rHF = useForm({
    resolver: yupResolver(schema),
    mode: "onChange",
  });
  const history = useHistory();

  const {
    control,
    formState: { errors },
    watch,
    handleSubmit,
    setValue,
  } = rHF;
  const onSubmit = (data) => {};

  const formCustom = new FormDataCustom(form, rHF);
  const goToPotential = () => {
    history.push("/potential");
  };
  return (
    <div className="">
      <Form form={form} onFinish={handleSubmit(onSubmit)}>
        <div className={"sticky-bar-top"}>
          <div className={"ms-auto"}>
            <Button type="dashed" onClick={goToPotential}>
              Huỷ
            </Button>{" "}
            <Button type="primary" htmlType={"submit"} className={"ms-2"}>
              Thêm
            </Button>
          </div>
        </div>
        <div className="py-4">
          <TablePotential control={control} />
        </div>
      </Form>
    </div>
  );
}

export default PotentialView;