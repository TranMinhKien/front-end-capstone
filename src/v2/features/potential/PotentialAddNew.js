import { yupResolver } from "@hookform/resolvers/yup";
import { Form } from "antd";
import { Button } from "antd";
import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router";
import InputDateField from "v2/components/CustomFields/InputDateField";
import InputTextField from "v2/components/CustomFields/InputTextField";
import SelectField from "v2/components/CustomFields/SelectField";
import FormDataCustom from "v2/utils/FormDataCustom";
import * as yup from "yup";
import "./css/addNew.css";
import TablePotential from "./TablePotential";
import { validatePotentialSchema } from "../../config/validate/potentialValidate";
import { createPotential } from "v2/api/createAPI";
import { toast } from "react-toastify";

PotentialAddNew.propTypes = {};

function PotentialAddNew(props) {
  const [form] = Form.useForm();
  const rHF = useForm({
    resolver: yupResolver(validatePotentialSchema),
    mode: "onChange",
  });
  const history = useHistory();

  const {
    control,
    formState: { errors },
    watch,
    handleSubmit,
    setValue,
  } = rHF;
  const [loading, setLoading] = useState(false);
  const onSubmit = async (data) => {
    try {
      const response = await createPotential(data);
      if (response.errors || !response.data) {
        throw new Error();
      }
      setLoading(false);
      toast.success("Thêm mới thành công");
      history.push("/potential");
    } catch (error) {
      toast.error("Thêm mới thất bại");
    }
  };

  const formCustom = new FormDataCustom(form, rHF);

  const goToPotential = () => {
    history.push("/potential");
  };

  useEffect(() => {}, []);

  return (
    <div className="">
      <Form form={form} onFinish={handleSubmit(onSubmit)}>
        <div className={"sticky-bar-top"}>
        <h5 className={"m-0"}>Thêm Khách Hàng Tiềm Năng </h5>
          <div className={"ms-auto"}>
            <Button type="dashed" onClick={goToPotential}>
              Huỷ
            </Button>{" "}
            <Button type="primary" loading={loading} htmlType={"submit"} className={"ms-2"}>
              Thêm
            </Button>
          </div>
        </div>
        <div className="py-4">
          <TablePotential control={control} />
        </div>
      </Form>
    </div>
  );
}

export default PotentialAddNew;
