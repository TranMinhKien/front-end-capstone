import React, {useEffect, useState} from 'react';
import {useHistory} from 'react-router-dom';
import {useForm} from 'react-hook-form';
import {yupResolver} from '@hookform/resolvers/yup';
import {Button, Form} from 'antd';
import FormDataCustom from '../../utils/FormDataCustom';
import {toast} from 'react-toastify';
import {updateProduct, updateRole} from 'v2/api/updateAPI';
import {getProductById, getUserById , getRoleById} from 'v2/api/readByIdAPI';
import { validateRoleSchema } from 'v2/config/validate/roleValidate';
import { TableRole } from './RoleAddNew';
import { convertToSingleArray } from 'v2/components/columns';

RoleView.propTypes = {};

function RoleView(props) {
    const roleId = props.match.params.id;

    const history = useHistory();

    const rHF = useForm({
        resolver: yupResolver(validateRoleSchema),
        mode: "onChange"
    });

    const [form] = Form.useForm();

    const {control, handleSubmit, formState: {errors}} = rHF;

    const customForm = new FormDataCustom(form, rHF);

    console.log(errors);

    const goToRole = () => history.push('/role');

    const onSubmit = async (data) => {
        try {
            const response = await updateRole(data, roleId);
            if (response.error) {
                throw new Error(response.error);
            }
            if (!response?.data) {
                throw new Error();
            }
            history.push('/role');
            toast.success('Cập nhận thành công');
        } catch (error) {
            toast.error('Cập nhật không thành công');
        }
    };

    const setValueData = (data) => [
        {
            value: data?.name,
            name: "name"
        },
        {
            value: data?.description,
            name: "description"
        },
        {
            value: convertToSingleArray(data.permissionFunctionActions),
            name: "permissionFunctionActionIds"
        },
    ];

    const [role, setRole] = useState();

    useEffect(() => {
        const fetchRole = async () => {
            try {
                setRole({...role, isLoading: true});
                const response = await getRoleById(roleId);
                if (response.error) {
                    throw new Error(response.error);
                }
                setRole({
                    data: response.data,
                    isLoading: false
                });
                customForm.setFormMany(setValueData(response.data));
            } catch (error) {
                console.log("fail to featch product");
                setRole({
                    data: null,
                    isLoading: false
                });
            }
        };
        fetchRole();
    }, [roleId]);
    return (
        <Form form={form} onFinish={handleSubmit(onSubmit)}>
            <div className={'sticky-bar-top'}>
                <h5 className={'m-0'}>Sửa  Vai trò</h5>
                <div className={'ms-auto'}>
                    <Button type="dashed" onClick={goToRole}>Huỷ</Button>
                </div>
            </div>
            <TableRole control={control}/>
        </Form>
    );
}

export default RoleView;
