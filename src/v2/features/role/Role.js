import React, { Component, useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/js/bootstrap.js";
import axios from "axios";
import MaterialTable from "material-table";
import { TextField } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import "../V2.css";
import authHeader from "services/auth_header";
import Select from "react-select";
import { DOMAIN_BE } from "constant/constant";
import { commonFetchDataHandler } from "utils/index";
import { commonFetchAdd } from "utils/add";
import { ErrorMess } from "utils/MessageErrors";
import { Button, Modal, Container, Row, Col } from "react-bootstrap";
import { missingPermission } from "utils/errorMessage";
import AuthService from "services/auth.service";
import VisibilityIcon from "@material-ui/icons/Visibility";
import { toastSuccess } from "utils/MessageSuccess";
import { CloseOutlined, EditOutlined, EyeOutlined } from "@ant-design/icons";
import { useHistory } from "react-router";
import Tableb from "v2/components/table";
import { convertColumn, convertToSingleArray } from "v2/components/columns";

// API

const baseURL = DOMAIN_BE + "/api/role/";
const baseURLpermission = DOMAIN_BE + "/api/permission/";

const useStyles = makeStyles((theme) => ({
  modal: {
    position: "absolute",
    width: 1200,

    backgroundColor: theme.palette.background.paper,
    border: "1px solid #000",
    // boxShadow: theme.shadows[5],
    boxShadow: "20px 10px",
    padding: theme.spacing(2, 4, 3),
    // padding: '1px 1px 1px 1px',
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
  iconos: {
    cursor: "pointer",
  },
  inputMaterial: {
    width: "50%",
    padding: "3px 10px",
    margin: "3px 0",
  },
}));
const validateForm = (errors) => {
  let valid = true;
  if (errors == null) return true;
  Object.values(errors).forEach((val) => val.length > 0 && (valid = false));
  return valid;
};

const validEmailRegex = RegExp(
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
);
const validPhoneRegex = RegExp(/^\d*$/);
function Role() {
  const styles = useStyles();
  const [data, setData] = useState([]);
  const [modalInsertar, setModalInsertar] = useState(false);
  const [modalEditar, setModalEditar] = useState(false);
  const [modalEliminar, setModalEliminar] = useState(false);
  const [modalViewInfor, setModalViewInfor] = useState(false);
  // lấy thông tin USER
  const user = AuthService.getCurrentUser();
  const [artistaSeleccionado, setArtistaSeleccionado] = useState({
    name: "",
    description: "",
    permissionFunctionActionIds: [],
    permissionFunctionActions: [],
    errors: {
      name: "",
      description: "",
    },
  });
  const hasPerm = (user, perm = "") => {
    return user && user.roles && user.roles.includes(perm);
  };

  const [permission3, setPermission3] = useState({
    selectpermission: [],
    id: "",
    name: "",
  });

  const getPermission = async () => {
    try {
      const responsePermission = await commonFetchDataHandler(
        baseURLpermission
      );
      const optionsPermission = responsePermission.data.map((d) => ({
        value: d.id,
        label: d.viName,
      }));
      setPermission3({ selectpermission: optionsPermission });
    } catch (e) {
      if (e.response && e.response.data) {
        alert(JSON.stringify(e.response.data)); // some reason error message
      }
    }
  };

  const handleChange2 = (e) => {
    artistaSeleccionado.permissionFunctionActionIds = Array.isArray(e)
      ? e.map((x) => x.value)
      : [];
    artistaSeleccionado.permissionFunctionActions = Array.isArray(e)
      ? e.map((x) => x)
      : [];
  };

  const clearErrors = () => {
    setArtistaSeleccionado((prevState) => ({
      ...prevState,
      errors: {},
    }));
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    let errors =
      artistaSeleccionado.errors || (artistaSeleccionado.errors = {});

    switch (name) {
      case "name":
        errors.name =
          value.length < 5 ? "Full Name must be 5 characters long!" : "";
        break;
      case "description":
        break;
      default:
        break;
    }
    setArtistaSeleccionado((prevState) => ({
      ...prevState,
      [name]: value || null,
    }));
    console.log(artistaSeleccionado);
  };

  const getRole = async () => {
    // hiện thông dữ liệu
    const response = await commonFetchDataHandler(baseURL);
    response.data.forEach((element) => {
      // element.permissionFunctionActionIds = element.permissionFunctionAction ? element.permissionFunctionAction.id : null
      element.permissionFunctionActionIds = element.permissionFunctionActions
        ? element.permissionFunctionActions.map((i) => i?.id)
        : [];
    });
    setData(response.data);
  };

  // insert dữ liệu
  const createRole = async () => {
    try {
      // Check validate khi ấn "Thêm"
      for (const name in artistaSeleccionado) {
        const value = artistaSeleccionado[name];
        handleChange({
          target: { name, value },
        });
      }
      // Nếu chưa valid thì không thêm dữ liệu
      if (!validateForm(artistaSeleccionado.errors)) return;
      // Thêm
      commonFetchAdd(baseURL, artistaSeleccionado)
        .then((responseSource) => {
          const d = responseSource.data;
          d.permissionFunctionAction = d.permissionFunctionAction
            ? d.permissionFunctionAction.id
            : null;
          setData(data.concat(d));
          abrirCerrarModalInsertar();
          toastSuccess("Thêm dữ liệu thành công");
        })
        .catch((error) => {
          if (
            error.response &&
            error.response.data &&
            error.response.data.errors &&
            error.response.data.errors[0].type === "duplicate"
          ) {
            let errors =
              artistaSeleccionado.errors || (artistaSeleccionado.errors = {});
            error.response.data.errors.forEach((e) => {
              errors[e.field] = "Giá trị đã bị trùng";
            });
            setArtistaSeleccionado((prevState) => ({
              ...prevState,
              errors: errors,
            }));
            console.log(artistaSeleccionado);
          } else {
            ErrorMess(error);
          }
        });
    } catch (e) {
      ErrorMess(e);
    }
  };

  // hiện lên để chỉnh sửa trong database
  const editRole = async () => {
    await axios
      .put(baseURL + artistaSeleccionado.id, artistaSeleccionado, {
        headers: { Authorization: authHeader().Authorization },
      })
      .then((response) => {
        const dataNueva = data;
        dataNueva.map((id) => {
          if (id.id === artistaSeleccionado.id) {
            id.name = artistaSeleccionado.name;
            id.description = artistaSeleccionado.description;
            id.permissionFunctionActions =
              artistaSeleccionado.permissionFunctionActions;
            id.permissionFunctionActionIds =
              artistaSeleccionado.permissionFunctionActionIds;
          }
        });

        setData(dataNueva);
        abrirCerrarModalEditar();
        toastSuccess("Sửa dữ liệu thành công");
      })
      .catch((error) => {
        ErrorMess(error);
      });
  };
  // xóa dữ liệu
  const deleteRole = async () => {
    await axios
      .delete(baseURL + artistaSeleccionado.id, {
        headers: { Authorization: authHeader().Authorization },
      })
      .then((response) => {
        console.log("xoa thanh cong");
        setData(data.filter((name) => name.id !== artistaSeleccionado.id));
        abrirCerrarModalEliminar();
        toastSuccess("Xóa dữ liệu thành công");
      })
      .catch((error) => {
        ErrorMess(error);
      });
  };

  const seleccionarArtista = (id, caso) => {
    setArtistaSeleccionado(id);
    caso === "Editar" ? abrirCerrarModalEditar() : abrirCerrarModalEliminar();
  };

  const seleccionarView = (id, caso) => {
    setArtistaSeleccionado(id);
    if (caso === "check") {
      viewInformation();
    }
  };

  // // // thực hiện lệnh tắt và bật  modal
  const abrirCerrarModalInsertar = () => {
    setModalInsertar(!modalInsertar);
  };
  // // cũng để bật tắt
  const abrirCerrarModalEditar = () => {
    setModalEditar(!modalEditar);
  };
  // // cũng để bật tắt
  const abrirCerrarModalEliminar = () => {
    setModalEliminar(!modalEliminar);
  };

  const viewInformation = () => {
    setModalViewInfor(!modalViewInfor);
  };

  useEffect(() => {
    getRole();
    getPermission();
  }, []);

  const ListCareers = (data) => {
    let a = [];
    data?.map((o) => a.push(o.name));
    return a;
  };
  const hasError = (name) => {
    return (
      artistaSeleccionado &&
      artistaSeleccionado.errors &&
      artistaSeleccionado.errors[name] &&
      artistaSeleccionado.errors[name].length > 0 && (
        <span className="error">{artistaSeleccionado.errors[name]}</span>
      )
    );
  };
  const bodyInsert = (
    <Modal
      size="lg"
      show={modalInsertar}
      onHide={() => abrirCerrarModalInsertar()}
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header>
        <Modal.Title>Thêm Vai Trò </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Container>
          <Row>
            <Col md={6}>
              <label className="required"> Tên </label>
              <input
                type="text"
                class="form-control"
                name="name"
                onChange={handleChange}
              ></input>
              {hasError("name")}
            </Col>
            <Col md={6}>
              <label className="required">Mô tả</label>
              <input
                type="text"
                class="form-control"
                name="description"
                onChange={handleChange}
              ></input>
              {hasError("description")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label className="required"> Quyền </label>
              <Select
                className="dropdown controll1 formselect"
                placeholder="Select Option"
                // value={classifications3.selectClassification.filter(obj => artistaSeleccionado.classificationIds.includes(obj.value))}
                isMulti
                options={permission3.selectpermission}
                onChange={handleChange2}
              ></Select>
            </Col>
          </Row>
        </Container>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={() => {
            clearErrors();
            abrirCerrarModalInsertar();
          }}
        >
          Hủy Bỏ
        </Button>
        <Button
          variant="primary"
          onClick={() => createRole()}
          disabled={!validateForm(artistaSeleccionado.errors)}
        >
          {" "}
          Thêm{" "}
        </Button>
      </Modal.Footer>
    </Modal>
  );
  const bodyEdit = (
    <Modal
      size="lg"
      show={modalEditar}
      onHide={() => abrirCerrarModalEditar()}
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header>
        <Modal.Title>Sửa Thông Tin</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Container>
          <Row>
            <Col md={6}>
              <label className="required">Tên </label>
              <input
                type="text"
                class="form-control"
                name="name"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.name}
              ></input>
              {hasError("name")}
            </Col>
            <Col md={6}>
              <label>Mô tả</label>
              <input
                type="text"
                class="form-control"
                name="description"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.description}
              ></input>
              {hasError("description")}
            </Col>
          </Row>
          <Row>
            <Col md={12}>
              <label className="required"> Quyền </label>
              <Select
                className="dropdown "
                placeholder="Select Option"
                isMulti
                options={permission3.selectpermission}
                onChange={handleChange2}
                defaultValue={
                  artistaSeleccionado &&
                  artistaSeleccionado.permissionFunctionActions &&
                  artistaSeleccionado.permissionFunctionActions.map((d) => ({
                    value: d.id ? d.id : d.value,
                    label: d.id ? d.viName : d.label,
                  }))
                }
              ></Select>
            </Col>
          </Row>
        </Container>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={() => {
            clearErrors();
            abrirCerrarModalEditar();
          }}
        >
          Hủy Bỏ
        </Button>
        <Button
          variant="primary"
          onClick={() => editRole()}
          disabled={!validateForm(artistaSeleccionado.errors)}
        >
          Sửa
        </Button>
      </Modal.Footer>
    </Modal>
  );
  const View = (
    <Modal
      size="lg"
      show={modalViewInfor}
      onHide={() => viewInformation()}
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header>
        <Modal.Title>Xem Thông Tin Chi Tiết</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Container>
          <Row>
            <Col md={6}>
              <label>Tên </label>
              <input
                type="text"
                class="form-control"
                name="name"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.name}
              ></input>
            </Col>
            <Col md={6}>
              <label>Mô tả</label>
              <input
                type="text"
                class="form-control"
                name="description"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.description}
              ></input>
            </Col>
          </Row>
          <Row>
            <Col md={12}>
              <label> Quyền </label>
              <Select
                className="dropdown"
                placeholder="Select Option"
                isMulti
                options={permission3.selectpermission}
                onChange={handleChange2}
                defaultValue={
                  artistaSeleccionado &&
                  artistaSeleccionado.permissionFunctionActions &&
                  artistaSeleccionado.permissionFunctionActions.map((d) => ({
                    value: d.id ? d.id : d.value,
                    label: d.id ? d.name : d.label,
                  }))
                }
              ></Select>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Ngày Tạo </label>
              <input
                type="text"
                class="form-control"
                name="createdAt"
                value={artistaSeleccionado && artistaSeleccionado.createdAt}
              ></input>
            </Col>
            <Col md={6}>
              <label> Ngày sửa </label>
              <input
                type="text"
                class="form-control"
                name="updatedAt"
                value={artistaSeleccionado && artistaSeleccionado.updatedAt}
              ></input>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Người tạo </label>
              <input
                type="text"
                class="form-control"
                name="createdBy"
                value={
                  artistaSeleccionado &&
                  artistaSeleccionado.createdBy &&
                  artistaSeleccionado.createdBy.username
                }
              ></input>
            </Col>
            <Col md={6}>
              <label>Người sửa </label>
              <input
                type="text"
                class="form-control"
                name="updatedBy"
                value={
                  artistaSeleccionado &&
                  artistaSeleccionado.updatedBy &&
                  artistaSeleccionado.updatedBy.username
                }
              ></input>
            </Col>
          </Row>
        </Container>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={() => viewInformation()}>
          Hủy Bỏ
        </Button>
      </Modal.Footer>
    </Modal>
  );

  const Delete = (
    <Modal
      show={modalEliminar}
      onHide={() => abrirCerrarModalEliminar()}
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header>
        <Modal.Title>Xóa</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p> Bạn có chắc chắn muốn xóa không ? </p>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={() => abrirCerrarModalEliminar()}>
          Không
        </Button>
        <Button variant="primary" onClick={() => deleteRole()}>
          Có
        </Button>
      </Modal.Footer>
    </Modal>
  );
  const history = useHistory();
  const listColumn = [
    {
      title: "Tên",
      dataIndex: "name",
    },
    {
      title: "Mô tả",
      dataIndex: "description",
      width: 400,
    },
    {
      title: "Hành động",
      key: "operation",
      fixed: "right",
      width: "150px",
      dataIndex: "action",
    },
  ];
  const handleMultiDelete = (selectedRowKeys) => {
    setArtistaSeleccionado(selectedRowKeys);
    abrirCerrarModalEliminar();
  };
  const handlDelete = (data) => {
    setArtistaSeleccionado(data);
    abrirCerrarModalEliminar();
  };
  const onEdit = (id) => {
    history.push(`/role/edit/${id}`);
  };
  const onView = (id) => {
    history.push(`/role/view/${id}`);
  };
  const onAddNew = () => {
    // setModalInsertar(true);
    // abrirCerrarModalInsertar();
    hasPerm(user, "ROLE_CREATE_ROLE")
     ? 
     history.push(`/role/add`)
     : missingPermission()

  };
  const getDataTable = () => {
    let cloneData = data.map((item, index) => {
      return {
        ...item,
        key: index,
        action: (
          <div className="d-flex align-items-center p-0 justify-content-between">
           <EditOutlined
              className=" btnIcon"
              onClick={() => {
                hasPerm(user, "ROLE_UPDATE_ROLE")
                  ? onEdit(item.id)
                  : missingPermission()
              }}
              style={{ color: "#C8DF52" }}
            />
            <CloseOutlined
              className="btnIcon"
              onClick={() => {
                hasPerm(user, "ROLE_DELETE_ROLE")
                  ? handlDelete(item)
                  : missingPermission();
              }}
              style={{ color: "#D2042D" }}
            />
            <EyeOutlined
              onClick={() => {
                hasPerm(user, "ROLE_READ_ROLE")
                  ? onView(item.id)
                  : missingPermission()
              }}
              className=" btnIcon"
              style={{ color: "#B3CDE0" }} />
          </div>
        ),
      };
    });
    return cloneData;
  };
  return (
    <div className="container-fluid">
      {/* <div className="row backgroud-insert">
                <div className="col-md-10"></div>
                <div className="col-md-2 them-customer">
                    <Button variant="primary" onClick={() => abrirCerrarModalInsertar()} >Thêm</Button>
                </div>
            </div> */}
      <div className="row">
        <div className="col-md-12">
          <Tableb
            columns={convertColumn(listColumn)}
            handleMultiDelete={handleMultiDelete}
            data={getDataTable()}
            onAddNew={onAddNew}
            titlechucnang="Vai trò"
          />
          {bodyInsert}
          {bodyEdit}
          {View}
          {Delete}
        </div>
      </div>
    </div>
  );
}

export default Role;
