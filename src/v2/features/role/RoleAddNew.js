import React from "react";
import PropTypes from "prop-types";
import InputTextField from "../../components/CustomFields/InputTextField";
import SelectField from "../../components/CustomFields/SelectField";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Button, Form } from "antd";
import FormDataCustom from "../../utils/FormDataCustom";
import {
  TableProductType,

} from "../product-type/ProductTypeAddNew";
import GenderField from "v2/components/customElement/GenderField";

import InputDateField from "v2/components/CustomFields/InputDateField";
import * as yup from "yup";
import InputPhoneField from '../../components/CustomFields/InputPhoneField';
import UnitField from "v2/components/customElement/UnitField";
import { setDateToString } from '../../utils/format';
import { createOrder, createRole, createUser } from '../../api/createAPI';
import { toast } from 'react-toastify';
import InputNumberField from '../../components/CustomFields/InputNumberField';
import PermissionField from "v2/components/customElement/PermissionField";
import { validateRoleSchema } from "v2/config/validate/roleValidate";
import RoleField from "v2/components/customElement/RoleField";

RoleAddNew.propTypes = {};

function RoleAddNew(props) {
  const history = useHistory();

  const rHF = useForm({
    resolver: yupResolver(validateRoleSchema),
  });

  const [form] = Form.useForm();
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = rHF;
  const customForm = new FormDataCustom(form, rHF);

  console.log(errors);

  const goToRole = () => history.push("/role");

  async function onSubmit(data) {
    try {
      setDateToString(data);
      await createRole(data);
      history.push('/role');
      toast.success("Thêm thành công");
    } catch (e) {
      toast.error("Thêm thất bại");
    }
  }

  return (
    <Form form={form} onFinish={handleSubmit(onSubmit)}>
      <div className={"sticky-bar-top"}>
        <h5 className={"m-0"}>Thêm Vai trò </h5>
        <div className={"ms-auto"}>
          <Button type="dashed" onClick={goToRole}>
            Huỷ
          </Button>
          <Button type="primary" htmlType={"submit"} className={"ms-2"}>
            Thêm
          </Button>
        </div>
      </div>
      <TableRole control={control} />
    </Form>
  );
}

export const TableRole = ({ control }) => {
  return (
    <div className={"container-lg py-5"}>
      <table className={"mx-auto spacing-table w-75"}>
        <tbody>
          <tr>
            <td colSpan={2}>
              <h5>Thông tin chung</h5>
            </td>
          </tr>
          <tr>
            <td>
              <InputTextField
                name={"name"}
                control={control}
                label={"Tên"}
                isRequired
              />
            </td>
            <td>
              <InputTextField
                name={"description"}
                control={control}
                label={"Mô tả"}
                isRequired
              />
            </td>
          </tr>
          <tr>
                  <RoleField
                      name = {"permissionFunctionActionIds"}
                      control = {control}
                      label = {"Quyền"}
                  />
          </tr>
        </tbody>
      </table>
    </div>
  )
}

export default RoleAddNew;
