import React, {useEffect} from 'react';
import {useHistory} from 'react-router-dom';
import {useForm} from 'react-hook-form';
import {yupResolver} from '@hookform/resolvers/yup';
import {Button, Form} from 'antd';
import FormDataCustom from '../../utils/FormDataCustom';
import * as yup from 'yup';
import InputTextField from '../../components/CustomFields/InputTextField';
import InputPhoneField from '../../components/CustomFields/InputPhoneField';
import GenderField from '../../components/customElement/GenderField';
import InputDateField from '../../components/CustomFields/InputDateField';
import {editPersonalInfo, getPersonalInfo} from '../../api/personalAPI';
import {validatePersonalSchema} from '../../config/validate/personalValidate';
import {toast} from 'react-toastify';

function Personal(props) {
    const history = useHistory();
    const rHF = useForm({
        resolver: yupResolver(validatePersonalSchema)
    });
    const [form] = Form.useForm();
    const {
        control,
        handleSubmit,
        formState: {errors}
    } = rHF;
    const customForm = new FormDataCustom(form, rHF);
    console.log(errors);
    const goHome = () => history.push("/");



    const setupFormValue = (data) => [
        {name: "username", value: data?.username},
        {name: "email", value: data.email},
        {name: "lastName", value: data?.lastName},
        {name: "name", value: data.name},
        {name: "phone", value: data.phone},
        {name: "prefixPhone", value: '+84'},
        {name: "genderId", value: data?.gender?.id},
        {name: "dateOfBirth", value: data?.dateOfBirth},
        {name: "address", value: data?.address}
    ];
    useEffect(() => {
        const fetchPersonalData = async () => {
            const response = await getPersonalInfo();
            customForm.setFormMany(setupFormValue(response.data));
        };
        fetchPersonalData();
    }, []);
   async function onSubmit(data) {
        try {
            await editPersonalInfo(data);
            toast.success('Cập nhật thành công');
        }catch (e) {
            toast.error('Cập nhật thất bại');
        }
    }
    return (
        <Form form={form} onFinish={handleSubmit(onSubmit)}>
            <div className={"sticky-bar-top"}>
                <h5 className={"m-0"}>Sửa thông tin cá nhân</h5>
                <div className={"ms-auto"}>
                    <Button type="dashed" onClick={goHome}>
                        Huỷ
                    </Button>
                    <Button type="primary" htmlType={"submit"} className={"ms-2"}>
                        Xong
                    </Button>
                </div>
            </div>
            <div className={"container-lg py-5"}>
                <table className={"mx-auto spacing-table w-75"}>
                    <tbody>
                    <tr>
                        <td>
                            <h5>Thông tin chung</h5>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <InputTextField name={'username'} label={'Tài khoản'} control={control} disabled/>
                        </td>
                        <td>
                            <InputTextField name={'email'} label={'Email'} control={control} disabled/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <InputTextField name={'lastName'} label={'Họ và đệm'} control={control}/>
                        </td>
                        <td>
                            <InputTextField name={'name'} label={'Tên'} control={control}/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <InputPhoneField inputName={'phone'} selectName={'prefixPhone'} label={'Điện thoại'}
                                             control={control}/>
                        </td>
                        <td>
                            <GenderField name={'genderId'} control={control} label={'Giới tính'}/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <InputDateField name={'dateOfBirth'} label={'Ngày sinh'}
                                            control={control}/>
                        </td>
                        <td>
                            <InputTextField name={'address'} label={"Địa chỉ"} control={control}/>
                        </td>
                    </tr>
                    <tr>

                    </tr>
                    </tbody>
                </table>
            </div>
        </Form>

    );
}

export default Personal;