import React, { useContext, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Button, Form } from "antd";
import FormDataCustom from "../../utils/FormDataCustom";
import { TableOrder } from "./OrderAddNew";
import { toast } from "react-toastify";
import { updateOrder } from "../../api/updateAPI";
import { getOrderById } from "../../api/readByIdAPI";
import moment from "moment";
import { validateOrderSchema } from "../../config/validate/orderValidate";
import { setDateToString } from "../../utils/format";
import { ProductContext } from "v2/providers/provider";

OrderEdit.propTypes = {};

function OrderEdit(props) {
  const { products: productInfos, setContext } = useContext(ProductContext);
  const id = props.match.params.id;

  const history = useHistory();
  const rHF = useForm({
    resolver: yupResolver(validateOrderSchema),
  });
  const [form] = Form.useForm();
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = rHF;
  const customForm = new FormDataCustom(form, rHF);
  console.log(errors);
  const goToOrder = () => history.push("/order");

  const setupFormData = (data) => [
    { name: "orderDate", value: new moment(data?.orderDate) },
    { name: "customerId", value: data?.customer?.id },
    { name: "contactId", value: data?.contact?.id },
    { name: "opportunity", value: data?.opportunity?.id },
    { name: "explanation", value: data?.explanation },
    { name: "orderValue", value: data?.orderValue },
    { name: "deliveryDeadline", value: new moment(data?.deliveryDeadline) },
    {
      name: "liquidationDeadline",
      value: new moment(data?.liquidationDeadline),
    },
    { name: "paymentStatus", value: data?.paymentStatus },
    { name: "receivedMoney", value: data?.receivedMoney },

    {
      value: data?.address,
      name: "address",
    },
    {
      value: data?.province?.id,
      name: "provinceId",
    },
    {
      value: data?.district?.id,
      name: "districtId",
    },
    {
      value: data?.ward?.id,
      name: "wardId",
    },
  ];
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await getOrderById(id);
        customForm.setFormMany(setupFormData(response.data));

        setContext(response.data.productInfos);
      } catch (e) {
        toast.error("Lỗi tải dữ liệu");
      }
    };
    fetchData();
    return setContext([]);
  }, []);

  async function onSubmit(data) {
    setDateToString(data);

    data.productInfoDtos = productInfos;
    data.id = id;
    try {
      await updateOrder(data, id);
      history.push("/order");
      toast.success("Cập nhật thành công");
    } catch (e) {
      toast.error("Cập nhật thất bại");
    }
  }

  return (
    <Form form={form} onFinish={handleSubmit(onSubmit)}>
      <div className={"sticky-bar-top"}>
        <h5 className={"m-0"}>Chỉnh sửa Đơn Hàng</h5>
        <div className={"ms-auto"}>
          <Button type="dashed" onClick={goToOrder}>
            Huỷ
          </Button>
          <Button type="primary" htmlType={"submit"} className={"ms-2"}>
            Lưu
          </Button>
        </div>
      </div>
      <TableOrder control={control} customForm={customForm} />
    </Form>
  );
}

export default OrderEdit;
