import { yupResolver } from "@hookform/resolvers/yup";
import { Button, Form } from "antd";
import React, { useContext, useEffect } from "react";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import ContactField from "v2/components/customElement/ContactField";
import CustomerField from "v2/components/customElement/CustomerField";
import OpportunityField from "v2/components/customElement/OpportunityField";
import EditableTable from "v2/components/tableEdit/EditTableCell";
import { ProductContext } from "v2/providers/provider";
import { createOrder } from "../../api/createAPI";
import { getOpportunityById } from "../../api/readByIdAPI";
import AddressField from "../../components/customElement/AddressField";
import InputDateField from "../../components/CustomFields/InputDateField";
import InputNumberField from "../../components/CustomFields/InputNumberField";
import InputTextField from "../../components/CustomFields/InputTextField";
import { validateOrderSchema } from "../../config/validate/orderValidate";
import { setDateToString } from "../../utils/format";
import FormDataCustom from "../../utils/FormDataCustom";

OrderAddNew.propTypes = {};

function OrderAddNew(props) {
  const { products: productInfos, setContext } = useContext(ProductContext);
  const urlParams = new URLSearchParams(props.location.search);
  const opportunityId = urlParams.get("opportunityId");
  const history = useHistory();
  const rHF = useForm({
    resolver: yupResolver(validateOrderSchema),
  });
  const [form] = Form.useForm();
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = rHF;
  const customForm = new FormDataCustom(form, rHF);
  console.log(errors);
  const goToOrder = () => history.push("/order");

  async function onSubmit(data) {
    data.productInfos = productInfos;
    try {
      setDateToString(data);
      await createOrder(data);
      history.push("/order");
      toast.success("Thêm thành công");
    } catch (e) {
      toast.error("Thêm thất bại");
    }
  }

  const setupFormData = (data) => [
    { name: "customerId", value: data?.customer?.id },
    { name: "contactId", value: data?.contact?.id },
    { name: "opportunityPhaseId", value: data?.opportunityPhase?.id },
    {name : "opportunityId" , value : data?.id },
    {
      value: data?.address,
      name: "address",
    },
    {
      value: data?.province?.id,
      name: "provinceId",
    },
    {
      value: data?.district?.id,
      name: "districtId",
    },
    {
      value: data?.ward?.id,
      name: "wardId",
    },
  ];
  async function fetchOpportunityData() {
    try {
      const response = await getOpportunityById(opportunityId);
      customForm.setFormMany(setupFormData(response.data));
      setContext(response.data.productInfos);
    } catch (e) {}
  }
  useEffect(() => {
    if (opportunityId) {
      fetchOpportunityData();
    }
    return setContext([]);
  }, [opportunityId]);

  return (
    <Form form={form} onFinish={handleSubmit(onSubmit)}>
      <div className={"sticky-bar-top"}>
        <h5 className={"m-0"}>Thêm Đơn Hàng</h5>
        <div className={"ms-auto"}>
          <Button type="dashed" onClick={goToOrder}>
            Huỷ
          </Button>
          <Button type="primary" htmlType={"submit"} className={"ms-2"}>
            Thêm
          </Button>
        </div>
      </div>
      <TableOrder control={control} customForm={customForm} />
    </Form>
  );
}

export const TableOrder = ({ control, customForm }) => {

  function onCustomerClear() {
    customForm.setFormValue('customerId', undefined);
  }
  function onCustomerChange() {
    customForm.setFormValue('contactId', undefined);
  }
  const { products: productInfos } = useContext(ProductContext);
  const calTotal = () => {
    return productInfos.reduce((acc, item) => {
      return (acc +=
        item.price * item.amount -
        (item.discount * (item.price * item.amount)) / 100 +
        ((item.price * item.amount) / 100) * item.vat);
    }, 0);
  };
  useEffect(() => {
    customForm.setFormValue("orderValue", Number(calTotal()));
  }, [productInfos]);
  return (

    <div className={"container-lg py-5"}>
      <table className={"mx-auto spacing-table w-75"}>
        <tbody>
          <tr>
            <td colSpan={2}>
              <h5>Thông tin chung</h5>
            </td>
          </tr>
          <tr>
            <td>
              <CustomerField
                name={"customerId"}
                control={control}
                label={"Tổ chức"}
                onClear={onCustomerClear}
                onChange={onCustomerChange}
                isLoadByOpportunity
              />
            </td>
            <td>
              <InputDateField
                name={"orderDate"}
                control={control}
                label={"Ngày đặt hàng"}
                isRequired
              />
            </td>
          </tr>
          <tr>
            <td>
              <InputNumberField
                name={"orderValue"}
                control={control}
                label={"Giá trị đơn hàng"}
                disabled
              />
            </td>
            <td>
              <ContactField
                name={"contactId"}
                control={control}
                label={"Liên hệ"}
                customForm={customForm}
              />
            </td>
          </tr>
          <tr>
            <td>
              <OpportunityField
                name={"opportunityId"}
                control={control}
                label={"Cơ hội"}
                customForm={customForm}
              />
            </td>
            <td>
              <InputTextField
                name={"explanation"}
                control={control}
                label={"Diễn giải"}
              />
            </td>
          </tr>
          <tr>
            <td>
              <InputDateField
                name={"liquidationDeadline"}
                control={control}
                label={"Hạn thanh toán"}
                isRequired
              />
            </td>
            <td>
              <InputDateField
                name={"deliveryDeadline"}
                control={control}
                label={"Hạn giao hàng"}
                isRequired
              />
            </td>
          </tr>

          <tr>
            <td>
              <InputNumberField
                name={"receivedMoney"}
                control={control}
                label={"Thực thu"}
                isRequired
              />
            </td>
            <td>
              <InputTextField
                name={"paymentStatus"}
                control={control}
                label={"Tình trạng thanh toán"}
                disabled
              />
            </td>
          </tr>
          <tr>
            <td>
              <h5>Địa chỉ giao hàng</h5>
            </td>
          </tr>
          <AddressField customForm={customForm} control={control} />
        </tbody>
      </table>
      <h5>Thông tin hàng hóa</h5>
      <EditableTable />
    </div>
  );
};
export default OrderAddNew;
