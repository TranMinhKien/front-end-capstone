import React, {useEffect, useState} from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/js/bootstrap.js";
import axios from "axios";
import "../V2.css";
import authHeader from "services/auth_header";
import {Button, Modal} from "react-bootstrap";
import {DOMAIN_BE} from "constant/constant";
import {formatDate} from "utils/date";
import {commonFetchDataHandler} from "utils/index";
import {missingPermission} from "utils/errorMessage";
import AuthService from "services/auth.service";
import {toastSuccess} from "utils/MessageSuccess";
import {ErrorMess} from "utils/MessageErrors";
import {CloseOutlined, EditOutlined, EyeOutlined} from "@ant-design/icons";
import {useHistory} from "react-router";
import {convertColumn, convertMilion} from "v2/components/columns";
import Tableb from "v2/components/table";
import {toast} from 'react-toastify';

//API
const baseURLopportunity = DOMAIN_BE + "/api/opportunity/name";
const baseURLcustomer = DOMAIN_BE + "/api/customer/name";
const baseURL = DOMAIN_BE + "/api/order/";
const baseURLproduct = DOMAIN_BE + "/api/product/name";


const formatedDateNow = formatDate(new Date());

function Order() {

    const [data, setData] = useState([]);
    const [modalEliminar, setModalEliminar] = useState(false);

    // lấy thông tin USER
    const user = AuthService.getCurrentUser();

    // cái này để khi nhập sữ liệu sẽ được thêm dữ liệu vào bảng
    const [artistaSeleccionado, setArtistaSeleccionado] = useState({
        orderDate: formatedDateNow,
        customerId: null,
        contactId: null,
        opportunityId: null,
        orderValue: null,
        liquidationValue: null,
        liquidationDeadline: formatedDateNow,
        deliveryDeadline: formatedDateNow,
        code: null,
        name: null,
        // Cái này để mặc định là đã thanh toán
        paid: true,
        errors: {
            code: "",
            name: "",
            orderValue: "",
            liquidationValue: ""
        }
    });


    const hasPerm = (user, perm = "") => {
        return user && user.roles && user.roles.includes(perm);
    };

    const [opportunity3, SetOpportunity3] = useState({
        selectOpportunity: [],
        id: "",
        name: ""
    });

    const [customer3, setCustomer3] = useState({
        selectCustomer: [],
        id: "",
        name: ""
    });


    const [product3, setProduct3] = useState({
        selectProduct: [],
        id: "",
        name: ""
    });

    const getOpportunity = async () => {
        const responseOpporunity = await commonFetchDataHandler(baseURLopportunity);
        const optionsOpporunity = responseOpporunity.data.map((d) => ({
            value: d.id,
            label: d.name
        }));
        optionsOpporunity.unshift({
            value: "",
            label: "-- Không chọn --"
        });
        SetOpportunity3({selectOpportunity: optionsOpporunity});
    };

    const getContact = async () => {

    };

    const getCustomer = async () => {
        const responseCustomer = await commonFetchDataHandler(baseURLcustomer);
        const optionsCustomer = responseCustomer.data.map((d) => ({
            value: d.id,
            label: d.name
        }));
        optionsCustomer.unshift({
            value: "",
            label: "-- Không chọn --"
        });
        setCustomer3({selectCustomer: optionsCustomer});
    };


    const getOrder = async () => {
        // TODO 3
        const response = await commonFetchDataHandler(baseURL);
        response.data.forEach((element) => {
            element.customerId = element.customer ? element.customer.id : null;
            element.contactId = element.contact ? element.contact.id : null;
            element.opportunityId = element.opportunity
                ? element.opportunity.id
                : null;
        });
        setData(response.data);
    };


    const getProduct = async () => {
        const responseProduct = await commonFetchDataHandler(baseURLproduct);
        const optionsProduct = responseProduct.data.map((d) => ({
            value: d.id,
            label: d.name
        }));
        setProduct3({selectProduct: optionsProduct});
    };

    // xóa dữ liệu
    const deleteOrder = async () => {
        await axios
            .delete(baseURL + artistaSeleccionado.id, {
                headers: {Authorization: authHeader().Authorization}
            })
            .then((response) => {
                setData(data.filter((name) => name.id !== artistaSeleccionado.id));
                abrirCerrarModalEliminar();
                toastSuccess("Xóa dữ liệu thành công");
            })
            .catch((error) => {
                ErrorMess(error);
            });
    };


    // // cũng để bật tắt
    const abrirCerrarModalEliminar = () => {
        setModalEliminar(!modalEliminar);
    };

    useEffect(() => {
        getOrder();
        getContact();
        getCustomer();
        getOpportunity();
        getProduct();
    }, []);


    const ModalDelete = (
        <Modal
            backdrop="static"
            keyboard={false}
            show={modalEliminar}
            onHide={abrirCerrarModalEliminar}
        >
            <Modal.Header>
                <Modal.Title>Xóa</Modal.Title>
            </Modal.Header>
            <Modal.Body className="show-grid">
                <p> Bạn có chắc chắn muốn xóa không ? </p>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => abrirCerrarModalEliminar()}>
                    Không
                </Button>
                <Button variant="primary" onClick={() => deleteOrder()}>
                    Có
                </Button>
            </Modal.Footer>
        </Modal>
    );
    const history = useHistory();
    const listColumn = [
        {
            title: "Mã đơn hàng",
            dataIndex: "code"
        },
        {
            title: "Liên hệ",
            dataIndex: "contact"
        },
        {
            title: "Tổ chức",
            dataIndex: "customer"
        },
        {
            title: "Ngày đặt hàng",
            dataIndex: "orderDate"
        },
        {
            title: "Hạn giao hàng",
            dataIndex: "deliveryDeadline"
        },
        {
            title: "Hạn thanh toán",
            dataIndex: "liquidationDeadline"
        },
        {
            title: "Diễn giải",
            dataIndex: "explanation"
        },
        {
            title: "Thực thu",
            dataIndex: "receivedMoney"
        },
        {
            title: "Hành động",
            key: "operation",
            fixed: "right",
            width: "150px",
            dataIndex: "action"
        }
    ];
    const handleMultiDelete = (selectedRowKeys) => {
        setArtistaSeleccionado(selectedRowKeys);
        abrirCerrarModalEliminar();
    };
    const handlDelete = (data) => {
        setArtistaSeleccionado(data);
        abrirCerrarModalEliminar();
    };
    const onEdit = (id) => {
        history.push(`/order/edit/${id}`);
    };
    const onView = (id) => {
        history.push(`/order/view/${id}`);
    };
    const onAddNew = () => {
        hasPerm(user, "ROLE_CREATE_ORDER")
            ?
            history.push(`/order/add`)
            : missingPermission()
    };
    const getDataTable = () => {
        let cloneData = data.map((item, index) => {
            return {
                ...item,
                key: index,
                customer: item?.customer?.name,
                contact: item?.contact?.fullName,
                receivedMoney: convertMilion(item?.receivedMoney),
                action: (
                    <div className="d-flex align-items-center p-0 justify-content-between">
                        <EditOutlined
                            className=" btnIcon"
                            onClick={() => {
                                hasPerm(user, "ROLE_UPDATE_ORDER")
                                    ? onEdit(item.id)
                                    : missingPermission()
                            }}
                            style={{ color: "#C8DF52" }}
                        />
                        <CloseOutlined
                            className="btnIcon"
                            onClick={() => {
                                hasPerm(user, "ROLE_DELETE_ORDER")
                                    ? handlDelete(item)
                                    : missingPermission();
                            }}
                            style={{ color: "#D2042D" }}
                        />
                        <EyeOutlined
                            onClick={() => {
                                hasPerm(user, "ROLE_READ_ORDER")
                                    ? onView(item.id)
                                    : missingPermission()
                            }}
                            className=" btnIcon"
                            style={{ color: "#B3CDE0" }} />
                    </div>
                )
            };
        });
        return cloneData;
    };
    const handleConvert = (rowSelected) => {
        console.log(rowSelected);
        const element = rowSelected[0];
        if(element?.paymentStatus === 'Đã thanh toán'){
            history.push('/invoice/convert?orderId='+element.id)
        }else{
            toast.warn('Đơn hàng chưa thanh toán toàn phần')
        }
     
    };
    return (
        <div className="container-fluid">
            {/* <div className="row backgroud-insert">
                <div className="col-md-10"></div>
                <div className="col-md-2 them-customer">
                    <Button variant="primary" onClick={ hasPerm(user, "ROLE_CREATE_ORDER") ? () => handleShow() : () => missingPermission()} >Thêm</Button>
                </div>
            </div> */}
            <div className="row">
                <div className="col-md-12">
                    <Tableb
                        columns={convertColumn(listColumn)}
                        handleMultiDelete={handleMultiDelete}
                        data={getDataTable()}
                        onAddNew={onAddNew}
                        handleConvert={handleConvert}
                        titlechucnang = "Đơn Hàng"
                        hasConvert
                    />
                    {ModalDelete}
                </div>
            </div>
        </div>
    );
}

export default Order;
