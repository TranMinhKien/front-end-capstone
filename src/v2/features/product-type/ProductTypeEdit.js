import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Button, Form } from "antd";
import FormDataCustom from "../../utils/FormDataCustom";
import { TableProductType } from "./ProductTypeAddNew";
import { validateProductTypeSchema } from "../../config/validate/productTypeValidate";
import { getProductTypeById } from "v2/api/readByIdAPI";
import { updateProductType } from "v2/api/updateAPI";
import { toast } from "react-toastify";

ProductTypeEdit.propTypes = {};

function ProductTypeEdit(props) {
  const productTypeId = props.match.params.id;
  const history = useHistory();
  const rHF = useForm({
    resolver: yupResolver(validateProductTypeSchema),
  });
  const [form] = Form.useForm();
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = rHF;
  const customForm = new FormDataCustom(form, rHF);
  console.log(errors);
  const goToProductType = () => history.push("/productType");


  const onSubmit = async (data) => {
    try {
      const response = await updateProductType(data, productTypeId);
      if (response.error) {
        throw new Error(response.error);
      }
      if (!response?.data) {
        throw new Error();
      }
      history.push('/productType')
      toast.success("Cập nhận thành công");
    } catch (error) {
      toast.error("Cập nhật không thành công");
    }
  };
  const [productType, setProductType] = useState();
  const setValueData = (data) => [
    {
      value: data?.name,
      name: "name",
    },
    {
      value: data?.productType?.id,
      name: "productTypeId",
    },
  ];
  useEffect(() => {
    const fetchProductType = async () => {
      try {
        setProductType({ ...productType, isLoading: true });
        const response = await getProductTypeById(productTypeId);
        if (response.error) {
          throw new Error(response.error);
        }
        setProductType({
          data: response.data,
          isLoading: false,
        });
        customForm.setFormMany(setValueData(response.data));
      } catch (error) {
        console.log("fail to featch productType");
        setProductType({
          data: null,
          isLoading: false,
        });
      }
    };
    fetchProductType();
  }, [productTypeId]);
  return (
    <Form form={form} onFinish={handleSubmit(onSubmit)}>
      <div className={"sticky-bar-top"}>
        <h5 className={"m-0"}>Chỉnh sửa Loại Hàng Hoá</h5>
        <div className={"ms-auto"}>
          <Button type="dashed" onClick={goToProductType}>
            Huỷ
          </Button>
          <Button type="primary" htmlType={"submit"} className={"ms-2"}>
            Lưu
          </Button>
        </div>
      </div>
      <TableProductType control={control} />
    </Form>
  );
}

export default ProductTypeEdit;
