import React from "react";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Button, Form } from "antd";
import FormDataCustom from "../../utils/FormDataCustom";
import * as yup from "yup";
import InputTextField from "../../components/CustomFields/InputTextField";
import SelectField from "../../components/CustomFields/SelectField";
import ProductTypeField from "v2/components/customElement/ProductTypeField";
import {validateProductTypeSchema} from '../../config/validate/productTypeValidate';
import {setDateToString} from '../../utils/format';
import {createOrder, createProductType} from '../../api/createAPI';
import {toast} from 'react-toastify';

ProductTypeAddNew.propTypes = {};

function ProductTypeAddNew(props) {
  const history = useHistory();
  const rHF = useForm({
    resolver: yupResolver(validateProductTypeSchema),
  });
  const [form] = Form.useForm();
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = rHF;
  const customForm = new FormDataCustom(form, rHF);
  console.log(errors);
  const goToProductType = () => history.push("/productType");

  async function onSubmit(data) {
    try {
      setDateToString(data);
      await createProductType(data);
      history.push('/productType');
      toast.success("Thêm thành công");
    } catch (e) {
      toast.error("Thêm thất bại");
    }
  }

  return (
    <Form form={form} onFinish={handleSubmit(onSubmit)}>
      <div className={"sticky-bar-top"}>
        <h5 className={"m-0"}>Thêm Loại Hàng Hoá</h5>
        <div className={"ms-auto"}>
          <Button type="dashed" onClick={goToProductType}>
            Huỷ
          </Button>
          <Button type="primary" htmlType={"submit"} className={"ms-2"}>
            Thêm
          </Button>
        </div>
      </div>
      <TableProductType control={control} />
    </Form>
  );
}


export const TableProductType = ({ control }) => {
  return (
    <div className={"container-lg py-5"}>
      <table className={"mx-auto spacing-table w-75"}>
        <tbody>
          <tr>
            <td colSpan={2}>
              <h5>Thông tin chung</h5>
            </td>
          </tr>
          <tr>
            <td>
              <InputTextField
                name={"name"}
                control={control}
                label={"Tên loại hàng hóa"}
                isRequired
              />
            </td>
            <td >
              <ProductTypeField
                name={"productTypeId"}
                control={control}
              />
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};
export default ProductTypeAddNew;
