import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/js/bootstrap.js";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";
import "../V2.css";
import { DOMAIN_BE } from "constant/constant";
import { commonFetchDataHandler } from "utils/index";
import authHeader from "services/auth_header";
import { Button, Modal } from "react-bootstrap";
import AuthService from "services/auth.service";
import { missingPermission } from "utils/errorMessage";
import { toastSuccess } from "utils/MessageSuccess";
import { ErrorMess } from "utils/MessageErrors";
import { CloseOutlined, EditOutlined, EyeOutlined } from "@ant-design/icons";
import Tableb from "v2/components/table";
import { convertColumn } from "v2/components/columns";
import { useHistory } from "react-router";

const baseURLproductType = DOMAIN_BE + "/api/productType/";

const useStyles = makeStyles((theme) => ({
    modal: {
        position: "absolute",
        width: 1200,

        backgroundColor: theme.palette.background.paper,
        border: "1px solid #000",
        // boxShadow: theme.shadows[5],
        boxShadow: "20px 10px",
        padding: theme.spacing(2, 4, 3),
        // padding: '1px 1px 1px 1px',
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)"
    },
    iconos: {
        cursor: "pointer"
    },
    inputMaterial: {
        width: "100%",
        //  padding: '3px 10px',
        margin: "3px 0"
    }
}));


function ProductType() {
    const styles = useStyles();
    const [data, setData] = useState([]);
    const [modalInsertar, setModalInsertar] = useState(false);
    const [modalEditar, setModalEditar] = useState(false);
    const [modalEliminar, setModalEliminar] = useState(false);
    const [modalViewInfor, setModalViewInfor] = useState(false);
    // lấy thông tin USER
    const user = AuthService.getCurrentUser();
    const [artistaSeleccionado, setArtistaSeleccionado] = useState({
        name: "",
        code: "",
        productTypeId: null,
        createdAt: "",
        createdBy: "",
        updatedAt: "",
        updatedBy: "",
        errors: {
            name: "",
            code: ""
        }
    });


    const hasPerm = (user, perm = "") => {
        return user && user.roles && user.roles.includes(perm);
    };

    // hiện dữ liệu lên bảng
    const getProductType = async () => {
        const response = await commonFetchDataHandler(baseURLproductType);
        // response.data.forEach(element => {
        //     element.productTypeId = element.productType ? element.productType.id : null
        // });
        setData(response.data);
    };

    // API cũ insert dropdown
    const getProductTypeChil = async () => {
        // const responseProductTypeChil = await commonFetchDataHandler(baseURLproductType)
        // const optionsProductTypeChil = responseProductTypeChil.data.map(d => ({
        //     "value": d.id,
        //     "label": d.name
        // }))
        // optionsProductTypeChil.unshift({
        //     "value": "",
        //     "label": "-- Không chọn --"
        // })
        // setchilProductType({ selectProductType: optionsProductTypeChil })
    };


    // xóa dữ liệu
    const deleteProductType = async () => {
        const ids = Array.isArray(artistaSeleccionado)
            ? artistaSeleccionado.map((a) => a.id)
            : [artistaSeleccionado.id];
        await axios
            .delete(baseURLproductType + ids.join(","), {
                headers: { Authorization: authHeader().Authorization }
            })
            .then((response) => {
                setData(data.filter((d) => !ids.includes(d.id)));
                abrirCerrarModalEliminar();
                toastSuccess("Xóa thành công");
                getProductType();
            })
            .catch((error) => {
                ErrorMess(error);
            });
    };


    const seleccionarView = (id, caso) => {
        setArtistaSeleccionado(id);
        if (caso === "check") {
            viewInformation();
        }
    };

    // // thực hiện lệnh tắt và bật  modal
    const abrirCerrarModalInsertar = () => {
        setModalInsertar(!modalInsertar);
    };
    // // cũng để bật tắt
    const abrirCerrarModalEditar = () => {
        setModalEditar(!modalEditar);
    };
    // // cũng để bật tắt
    const abrirCerrarModalEliminar = () => {
        setModalEliminar(!modalEliminar);
    };
    // dùng để bật tắt thông tin chi tiết

    const viewInformation = () => {
        setModalViewInfor(!modalViewInfor);
    };

    useEffect(() => {
        getProductType();
        getProductTypeChil();
    }, []);

    const hasError = (name) => {
        return (
            artistaSeleccionado &&
            artistaSeleccionado.errors &&
            artistaSeleccionado.errors[name] &&
            artistaSeleccionado.errors[name].length > 0 && (
                <span className="error">{artistaSeleccionado.errors[name]}</span>
            )
        );
    };


    const Delete = (
        <Modal
            show={modalEliminar}
            onHide={() => abrirCerrarModalEliminar()}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header>
                <Modal.Title>Xóa</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p> Bạn có chắc chắn muốn xóa không ? </p>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => abrirCerrarModalEliminar()}>
                    Không
                </Button>
                <Button variant="primary" onClick={() => deleteProductType()}>
                    Có
                </Button>
            </Modal.Footer>
        </Modal>
    );


    const history = useHistory();
    const listColumn = [
        {
            title: "Mã hàng hóa",
            dataIndex: "code"
        },
        {
            title: "Tên hàng hóa",
            dataIndex: "name"
        },
        {
            title: "Loại hàng hóa",
            dataIndex: "productType"
        },
        {
            title: "Hành động",
            key: "operation",
            fixed: "right",
            width: "150px",
            dataIndex: "action"
        }
    ];
    const handleMultiDelete = (selectedRowKeys) => {
        setArtistaSeleccionado(selectedRowKeys);
        abrirCerrarModalEliminar();
    };
    const handlDelete = (data) => {
        setArtistaSeleccionado(data);
        abrirCerrarModalEliminar();
    };
    const onEdit = (id) => {
        history.push(`/productType/edit/${id}`);
    };
    const onView= (id) => {
        history.push(`/productType/view/${id}`);
    };
    const onAddNew = () => {
        hasPerm(user, "ROLE_CREATE_PRODUCT_TYPE")
        ? 
        history.push(`/productType/add`)
        : missingPermission()
    };

    function addAction(array) {
        for (let item of array) {
            if (item?.children?.length) {
                addAction(item.children);
                item.key = item.code;
                item.productType = item.productType?.name || item.productType;
                item.action = (
                    <div className="d-flex align-items-center p-0 justify-content-between">
                        <EditOutlined
                            className=" btnIcon"
                            onClick={() => {
                                hasPerm(user, "ROLE_UPDATE_PRODUCT_TYPE")
                                    ? onEdit(item.id)
                                    : missingPermission();
                            }}
                            style={{ color: "#C8DF52" }}
                        />
                        <CloseOutlined
                            className="btnIcon"
                            onClick={() => {
                                hasPerm(user, "ROLE_DELETE_PRODUCT_TYPE")
                                    ? handlDelete(item)
                                    : missingPermission();
                            }}
                            style={{ color: "#D2042D" }}
                        />
                        <EyeOutlined
                            onClick={() => {
                                hasPerm(user, "ROLE_READ_PRODUCT_TYPE")
                                    ? onView(item.id)
                                    : missingPermission()
                            }}
                            className=" btnIcon"
                            style={{ color: "#B3CDE0" }} />
                    </div>
                );
            } else {
                item.key = item.code;
                item.productType = item.productType?.name || item.productType;
                item.action = (
                    <div className="d-flex align-items-center p-0 justify-content-between">
                        <EditOutlined
                            className=" btnIcon"
                            onClick={() => {
                                hasPerm(user, "ROLE_UPDATE_PRODUCT_TYPE")
                                    ? onEdit(item.id)
                                    : missingPermission();
                            }}
                            style={{ color: "#C8DF52" }}
                        />
                        <CloseOutlined
                            className="btnIcon"
                            onClick={() => {
                                hasPerm(user, "ROLE_DELETE_PRODUCT_TYPE")
                                    ? handlDelete(item)
                                    : missingPermission();
                            }}
                            style={{ color: "#D2042D" }}
                        />
                        <EyeOutlined
                            onClick={() => {
                                hasPerm(user, "ROLE_READ_PRODUCT_TYPE")
                                    ? onView(item.id)
                                    : missingPermission()
                            }}
                            className=" btnIcon"
                            style={{ color: "#B3CDE0" }} />
                    </div>
                );
            }
        }
        return array;
    }

    const getDataTable = () => {
        if (!data || !data.length) return null;
        return addAction(data);
    };
    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-12">
                    <Tableb
                        columns={convertColumn(listColumn)}
                        handleMultiDelete={handleMultiDelete}
                        data={getDataTable()}
                        onAddNew={onAddNew}
                        titlechucnang="Loại Hàng Hóa"
                    />
                    {Delete}
                </div>
            </div>
        </div>
    );
}

export default ProductType;
