import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Button, Form } from "antd";
import FormDataCustom from "../../utils/FormDataCustom";
import { TableContact } from "./ContactAddNew";
import { validateContactSchema } from "../../config/validate/contactValidate";
import moment from "moment";
import { convertToSingleArray } from "v2/components/columns";
import { toast } from "react-toastify";
import { toastSuccess } from "utils/MessageSuccess";
import { getContactById } from "v2/api/readByIdAPI";
import { updateContact } from "v2/api/updateAPI";
import {setDateToString} from '../../utils/format';

ContactView.propTypes = {};

function ContactView(props) {
  const contactId = props.match.params.id;
  const history = useHistory();
  const rHF = useForm({
    resolver: yupResolver(validateContactSchema),
    mode: "onChange",
  });
  const [form] = Form.useForm();
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = rHF;
  const formCustom = new FormDataCustom(form, rHF);
  const goToContact = () => history.push("/contact");
  // customForm.setFormValue('name','b')
  // customForm.setFormMany([{name: 'name', value: 'a'}, {name: 'shortName', value: 'b'}]);

  const onSubmit = async(data)=> {
    //   data.dateOfBirth=new Date(data.dateOfBirth)
    setDateToString(data)
    try{
        const response = await updateContact(data,contactId);
        if(response.error){
          throw new Error(response.error)
        }
        if(!response?.data){
          throw new Error()
        }
        console.log("success to update");
        toastSuccess("Cập nhật thành công");
        history.push("/contact")
      }
      catch(error){
          console.log("fail to update");
        toast.error('Cập nhật không thành công')
      }
  }
  const [contact, setContact] = useState();
  const setValueData = (data) => [
    {
      value: data?.vocative?.id,
      name: "vocativeId",
    },
    {
      value: data?.lastName,
      name: "lastName",
    },
    {
      value: data?.name,
      name: "name",
    },
    {
      value: data?.position?.id,
      name: "positionId",
    },
    {
      value: data?.phone,
      name: "phone",
    },
    {
      value: data?.officePhone,
      name: "officePhone",
    },
    {
      value: data?.otherPhone,
      name: "otherPhone",
    },
    {
      value: data?.source?.id,
      name: "sourceId",
    },
    {
      value: data?.email,
      name: "email",
    },
    {
      value: data?.department?.id,
      name: "departmentId",
    },
    {
      value: convertToSingleArray(data?.classifications),
      name: "classificationIds",
    },
    {
      value: data?.customer?.id,
      name: "customerId",
    },
    {
      value: data?.notCallPhone,
      name: "notCallPhone",
    },
    {
      value: data?.notSendEmail,
      name: "notSendEmail",
    },
    {
      value: data?.officeEmail,
      name: "officeEmail",
    },
    {
      value: data?.taxCode,
      name: "taxCode",
    },
    {
      value: data?.gender?.id,
      name: "genderId",
    },
    {
      value: data?.dateOfBirth ? new moment(data?.dateOfBirth) : null,
      name: "dateOfBirth",
    },
    {
      value: data?.facebook,
      name: "facebook",
    },
    {
      value: data?.bankAccount,
      name: "bankAccount",
    },
    {
      value: data?.bank,
      name: "bank",
    },
    {
      value: data?.foundedDate ? new moment(data?.foundedDate) : null,
      name: "foundedDate",
    },
    {
      value: data?.type?.id,
      name: "typeId",
    },
    {
      value: data?.address,
      name: "address",
    },  {
      value: data?.province?.id,
      name: "provinceId",
    },  {
      value: data?.district?.id,
      name: "districtId",
    },  {
      value: data?.ward?.id,
      name: "wardId",
    },
  ];
  useEffect(() => {
    const fetchContact = async () => {
      try {
        setContact({ ...contact, isLoading: true });
        const response = await getContactById(contactId);

        setContact({
          data: response.data,
          isLoading: false,
        });
        formCustom.setFormMany(setValueData(response.data));
      } catch (error) {
        console.log("fail to featch contact");
        setContact({
          data: null,
          isLoading: false,
        });
      }
    };
    fetchContact();
  }, [contactId]);
  console.log(errors);
  return (
    <Form form={form} onFinish={handleSubmit(onSubmit)}>
      <div className={"sticky-bar-top"}>
        <h5 className={"m-0"}>Chỉnh sửa Liên Hệ</h5>
        <div className={"ms-auto"}>
          <Button type="dashed" onClick={goToContact}>
            Huỷ
          </Button>
        </div>
      </div>
      <TableContact control={control} customForm={formCustom} />
    </Form>
  );
}

export default ContactView;
