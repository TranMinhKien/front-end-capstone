import React from "react";
import "v2/assets/css/_base.css";
import {useHistory} from "react-router-dom";
import {useForm} from "react-hook-form";
import {yupResolver} from "@hookform/resolvers/yup";
import {Button, Form} from "antd";
import FormDataCustom from "../../utils/FormDataCustom";
import InputTextField from "../../components/CustomFields/InputTextField";
import InputDateField from "../../components/CustomFields/InputDateField";
import InputPhoneField from "../../components/CustomFields/InputPhoneField";
import VocativeField from "v2/components/customElement/VocativeField";
import DepartmentField from "v2/components/customElement/DepartmentField";
import PositionField from "v2/components/customElement/PositionField";
import CustomerField from "v2/components/customElement/CustomerField";
import ClassificationField from "v2/components/customElement/ClassificationField";
import GenderField from "v2/components/customElement/GenderField";
import SourceField from "v2/components/customElement/SourceField";
import {validateContactSchema} from '../../config/validate/contactValidate';
import {setDateToString} from '../../utils/format';
import {toast} from 'react-toastify';
import {createContact} from '../../api/createAPI';
import AddressField from '../../components/customElement/AddressField';
import CheckBoxField from '../../components/CustomFields/CheckBoxField';

function ContactAddNew(props) {
    const history = useHistory();
    const rHF = useForm({
        resolver: yupResolver(validateContactSchema)
    });
    const [form] = Form.useForm();
    const {
        control,
        handleSubmit,
        formState: {errors}
    } = rHF;
    const customForm = new FormDataCustom(form, rHF);
    console.log(errors);
    const goToContact = () => history.push("/contact");

    async function onSubmit(data) {
        // console.log(
        //     data
        // );
        try {
            setDateToString(data);
            await createContact(data);
            // history.push('/contact');
            toast.success("Thêm thành công");
        } catch (e) {
            toast.error("Thêm thất bại");
        }
    }


    return (
        <Form form={form} onFinish={handleSubmit(onSubmit)}>
            <div className={"sticky-bar-top"}>
                <h5 className={"m-0"}>Thêm Liên Hệ</h5>
                <div className={"ms-auto"}>
                    <Button type="dashed" onClick={goToContact}>
                        Huỷ
                    </Button>
                    <Button type="primary" htmlType={"submit"} className={"ms-2"}>
                        Thêm
                    </Button>
                </div>
            </div>
            <TableContact control={control} customForm={customForm}/>
        </Form>
    );
}

export const TableContact = ({control, customForm}) => {
    return (
        <div className={"container-lg py-5"}>
            <table className={"mx-auto spacing-table w-75"}>
                <tbody>
                <tr>
                    <td colSpan={2}>
                        <h5>Thông tin chung</h5>
                    </td>
                </tr>
                <tr>
                    <td>
                        <VocativeField name={"vocativeId"} control={control}/>
                    </td>
                    <td>
                        <InputTextField
                            name={"lastName"}
                            control={control}
                            label={"Họ và đệm"}
                            isRequired
                        />
                    </td>
                </tr>
                <tr>
                    <td>
                        <InputTextField
                            name={"name"}
                            control={control}
                            label={"Tên"}
                            isRequired
                        />
                    </td>
                    <td>
                        <DepartmentField name={"departmentId"} control={control}/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <PositionField name={"positionId"} control={control}/>
                    </td>
                    <td>
                        <CustomerField
                            name={"customerId"}
                            control={control}
                            label={"Tổ chức"}
                        />
                    </td>
                </tr>
                <tr>

                    <td className={'align-bottom'}>

                        <CheckBoxField
                            name={"notSendEmail"}
                            control={control}
                            label={"Không gửi email"}
                        />
                    </td>
                    <td className={'align-bottom'}>
                        <CheckBoxField
                            name={"notCallPhone"}
                            control={control}
                            label={"Không gọi điện"}
                        />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ClassificationField
                            name={"classificationIds"}
                            control={control}
                            label={"Phân loại khách hàng"}
                        />
                    </td>
                    <td>
                        <InputPhoneField
                            inputName={"phone"}
                            selectName={"prefixPhone"}
                            control={control}
                            label={"Điện thoại"}
                            isRequired
                        />
                    </td>
                </tr>
                <tr>
                    <td>
                        <InputTextField
                            name={"officePhone"}
                            control={control}
                            label={"Điện thoại cơ quan"}
                        />
                    </td>
                    <td>
                        <InputTextField
                            name={"otherPhone"}
                            control={control}
                            label={"Điện thoại khác"}
                        />
                    </td>
                </tr>
                <tr>
                    <td>
                        <InputTextField
                            name={"email"}
                            control={control}
                            label={"Email"}
                            isRequired
                        />
                    </td>
                    <td>
                        <InputTextField
                            name={"officeEmail"}
                            control={control}
                            label={"Email cơ quan"}
                        />
                    </td>
                </tr>

                <tr>
                    {/*<td>*/}
                    {/*    <InputTextField*/}
                    {/*        name={"dateOfBirth"}*/}
                    {/*        control={control}*/}
                    {/*        label={"Tên tổ chức"}*/}

                    {/*    />*/}
                    {/*</td>*/}
                    <td>
                        <InputDateField
                            name={"dateOfBirth"}
                            control={control}
                            label={"Ngày sinh"}

                        />
                    </td>
                    <td>
                        <GenderField
                            name={"genderId"}
                            control={control}
                        />
                    </td>
                </tr>

                <tr>

                    <td>
                        <SourceField
                            name={"sourceId"}
                            control={control}
                        />
                    </td>
                </tr>
                <tr>
                    <td colSpan={2}>
                        <h5>Thông bổ sung</h5>
                    </td>
                </tr>
                <tr>
                    <td>
                        <InputTextField
                            name={"bankAccount"}
                            control={control}
                            label={"Tài khoản ngân hàng"}
                        />
                    </td>
                    <td>
                        <InputTextField
                            name={"bank"}
                            control={control}
                            label={"Mở tại ngân hàng"}
                        />
                    </td>
                </tr>


                <AddressField customForm={customForm} control={control}/>
                <tr>

                    <td>
                        <InputTextField
                            name={"facebook"}
                            control={control}
                            label={"Facebook"}
                        />
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    );
};
export default ContactAddNew;
