import { CloseOutlined, EditOutlined, EyeOutlined } from "@ant-design/icons";
import { makeStyles } from "@material-ui/core/styles";
import VisibilityIcon from "@material-ui/icons/Visibility";
import axios from "axios";
import "bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.js";
import { DOMAIN_BE } from "constant/constant";
import MaterialTable from "material-table";
import React, { useEffect, useState } from "react";
import { Button, Col, Container, Modal, Row } from "react-bootstrap";
import { useHistory } from "react-router";
import Select from "react-select";
import { toast } from "react-toastify";
import AuthService from "services/auth.service";
import authHeader from "services/auth_header";
import { commonFetchAdd } from "utils/add";
import { missingPermission } from "utils/errorMessage";
import { commonFetchDataHandler } from "utils/index";
import { ErrorMess } from "utils/MessageErrors";
import { toastSuccess } from "utils/MessageSuccess";
import validation from "utils/validation";
import { convertColumn, convertListname } from "v2/components/columns";
import Tableb from "v2/components/table";
import "./contact.css";

const baseURL = DOMAIN_BE + "/api/contact/";
const baseURLsource = DOMAIN_BE + "/api/source/name";
const baseURLclassification = DOMAIN_BE + "/api/classification/name";
const baseURLcustomer = DOMAIN_BE + "/api/customer/name";

const useStyles = makeStyles((theme) => ({
  modal: {
    position: "absolute",
    width: 1200,
    backgroundColor: theme.palette.background.paper,
    border: "1px solid #000",
    // boxShadow: theme.shadows[5],
    boxShadow: "20px 10px",
    padding: theme.spacing(2, 4, 3),
    // padding: '1px 1px 1px 1px',
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
  iconos: {
    cursor: "pointer",
  },
  inputMaterial: {
    width: "100%",
    // padding: '3px 10px',
    margin: "3px 0",
  },
  inputMaterial2: {
    color: "blue",
  },
}));

const validateForm = (errors) => {
  let valid = true;
  if (errors == null) return true;
  Object.values(errors).forEach((val) => val.length > 0 && (valid = false));
  return valid;
};

const validEmailRegex = RegExp(
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
);
const validPhoneRegex = RegExp(/^\d*$/);

function Contact() {
  const styles = useStyles();
  const [data, setData] = useState([]);
  const [modalInsertar, setModalInsertar] = useState(false);
  const [modalEditar, setModalEditar] = useState(false);
  const [modalEliminar, setModalEliminar] = useState(false);
  const [modalViewInfor, setModalViewInfor] = useState(false);
  const history = useHistory();
  // lấy thông tin USER
  const user = AuthService.getCurrentUser();
  const [artistaSeleccionado, setArtistaSeleccionado] = useState({
    code: null,
    vocative: null,
    lastName: null,
    position: null,
    department: null,
    customerId: null, // insert ID nó vào
    classificationIds: [],
    phone: null,
    email: null,
    sourceId: null, // insert ID nó vào
    address: null,
    name: null,
    officePhone: null,
    otherPhone: null,
    officeEmail: null,
    errors: {
      code: "",
      lastName: "",
      phone: "",
      email: "",
      address: "",
      name: "",
      officePhone: "",
      otherPhone: "",
      officeEmail: "",
    },
  });
  const clearErrors = () => {
    setArtistaSeleccionado((prevState) => ({
      ...prevState,
      errors: {},
    }));
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    let errors =
      artistaSeleccionado.errors || (artistaSeleccionado.errors = {});
    validation.value = value;

    switch (name) {
      case "name":
        errors.name = validation.validate("Tên", [validation.notBlank]);
        break;
      case "email":
        errors.email = validation.validate("Địa chỉ email", [
          validation.notBlank,
          validation.email,
        ]);
        break;
      case "officeEmail":
        errors.officeEmail = validation.validate("Email cơ quan", [
          validation.email,
        ]);
        break;
      case "lastName":
        break;
      case "phone":
        errors.phone = validation.validate("Số điện thoại", [
          validation.number,
          validation.phone,
        ]);
        break;
      case "officePhone":
        errors.officePhone = validation.validate("Điện thoại cơ quan", [
          validation.phone,
        ]);
        break;
      case "otherPhone":
        errors.otherPhone = validation.validate("Điện thoại khác", [
          validation.phone,
        ]);
        break;
      case "address":
        break;
      case "code":
        errors.code = validation.validate("Mã", [validation.notBlank]);
        break;

      default:
        break;
    }
    setArtistaSeleccionado((prevState) => ({
      ...prevState,
      [name]: value || null,
    }));
  };

  const vocative3 = ["Ông", "Bà", "Anh", "Chị"].map((d) => ({
    value: d,
    label: d,
  }));
  vocative3.unshift({
    value: "",
    label: "-- Không chọn --",
  });

  const department3 = [
    "Phòng nhân sự",
    "Ban giám đốc",
    "Phòng tài chính",
    "Phòng marketing",
    "Phòng chăm sóc khách hàng",
    "Phòng hàng chính tổng hợp",
    "Phòng kinh doanh",
  ].map((d) => ({
    value: d,
    label: d,
  }));
  department3.unshift({
    value: "",
    label: "-- Không chọn --",
  });

  const position3 = [
    "Trưởng phòng",
    "Chủ tịch",
    "Phó chủ tịch",
    "Tổng giám đốc",
    "Phó tổng giám đốc",
    "Giám đốc",
    "Kế toán trưởng",
    "Trợ lý",
    "Nhân viên",
  ].map((d) => ({
    value: d,
    label: d,
  }));
  position3.unshift({
    value: "",
    label: "-- Không chọn --",
  });

  const hasPerm = (user, perm = "") => {
    return user && user.roles && user.roles.includes(perm);
  };

  const [source3, setSource3] = useState({
    selectSource: [],
    id: "",
    name: "",
  });

  const getSource = async () => {
    const responseSource = await commonFetchDataHandler(baseURLsource);
    const optionsSource = responseSource.data.map((d) => ({
      value: d.id,
      label: d.name,
    }));
    optionsSource.unshift({
      value: "",
      label: "-- Không chọn --",
    });
    setSource3({ selectSource: optionsSource });
  };

  const [customer3, setCustomer3] = useState({
    selectCustomer: [],
    id: "",
    name: "",
  });
  const getCustomer = async () => {
    const responseCustomer = await commonFetchDataHandler(baseURLcustomer);
    const optionsCustomer = responseCustomer.data.map((d) => ({
      value: d.id,
      label: d.name,
    }));
    optionsCustomer.unshift({
      value: "",
      label: "-- Không chọn --",
    });
    setCustomer3({ selectCustomer: optionsCustomer });
  };

  const [classifications3, setClassifications3] = useState({
    selectClassification: [],
    id: "",
    name: "",
  });

  const getClasstification = async () => {
    const responseClasstification = await commonFetchDataHandler(
      baseURLclassification
    );
    const optionsClasstification = responseClasstification.data.map((d) => ({
      value: d.id,
      label: d.name,
    }));
    setClassifications3({ selectClassification: optionsClasstification });
  };

  const handleChange2 = (e) => {
    artistaSeleccionado.classificationIds = Array.isArray(e)
      ? e.map((x) => x.value)
      : [];
    artistaSeleccionado.classifications = Array.isArray(e)
      ? e.map((x) => x)
      : [];
  };

  const getContact = async () => {
    const response = await commonFetchDataHandler(baseURL);
    response.data.forEach((element) => {
      // TODO 2
      element.sourceId = element.source ? element.source.id : null;
      element.customerId = element.customer ? element.customer.id : null;
      // element.classificationIds = element.classification ? element.classification.id : null
      element.classificationIds = element.classifications
        ? element.classifications.map((i) => i?.id)
        : [];
    });
    setData(response.data);
  };

  // insert dữ liệu
  const createContact = async () => {
    // Check validate khi ấn "Thêm"
    for (const name in artistaSeleccionado) {
      const value = artistaSeleccionado[name];
      handleChange({
        target: { name, value },
      });
    }
    // Nếu chưa valid thì không thêm dữ liệu
    if (!validateForm(artistaSeleccionado.errors)) return;

    commonFetchAdd(baseURL, artistaSeleccionado)
      .then((responseSource) => {
        const d = responseSource.data;
        d.sourceId = d.source ? d.source.id : null;
        d.customerId = d.customer ? d.customer.id : null;
        d.classificationIds = d.classification ? d.classification.id : null;
        setData(data.concat(d));
        abrirCerrarModalInsertar();
        toastSuccess("Thêm dữ liệu thành công");
      })
      .catch((error) => {
        if (
          error.response &&
          error.response.data &&
          error.response.data.errors &&
          error.response.data.errors[0].type === "duplicate"
        ) {
          let errors =
            artistaSeleccionado.errors || (artistaSeleccionado.errors = {});
          error.response.data.errors.forEach((e) => {
            errors[e.field] = "Giá trị đã bị trùng";
          });
          setArtistaSeleccionado((prevState) => ({
            ...prevState,
            errors: errors,
          }));
          console.log(artistaSeleccionado);
        } else {
          ErrorMess(error);
        }
      });
  };

  // hiện lên để chỉnh sửa trong database
  const editContact = async () => {
    await axios
      .put(baseURL + artistaSeleccionado.id, artistaSeleccionado, {
        headers: { Authorization: authHeader().Authorization },
      })
      .then((response) => {
        const dataNueva = data;
        dataNueva.map((id) => {
          if (id.id === artistaSeleccionado.id) {
            id.code = artistaSeleccionado.code;
            id.vocative = artistaSeleccionado.vocative;
            id.lastName = artistaSeleccionado.lastName;
            id.position = artistaSeleccionado.position;
            id.department = artistaSeleccionado.department;
            //TODO 4
            id.customer = response.data.customer;
            id.customerId = id.customer?.id;
            id.source = response.data.source;
            id.sourceId = id.source?.id;
            id.officeEmail = artistaSeleccionado.officeEmail;
            id.officePhone = artistaSeleccionado.officePhone;
            id.otherPhone = artistaSeleccionado.otherPhone;
            id.classifications = artistaSeleccionado.classifications;
            id.phone = artistaSeleccionado.phone;
            id.address = artistaSeleccionado.address;
            id.email = artistaSeleccionado.email;
            id.name = artistaSeleccionado.name;
          }
        });
        setData(dataNueva);
        abrirCerrarModalEditar();
        toastSuccess("Edit dữ liệu thành công");
      })
      .catch((error) => {
        ErrorMess(error);
      });
  };

  // xóa dữ liệu
  const deleteContact = async () => {
    const ids = Array.isArray(artistaSeleccionado)
      ? artistaSeleccionado.map((a) => a.id)
      : [artistaSeleccionado.id];
    await axios
      .delete(baseURL + ids.join(","), {
        headers: { Authorization: authHeader().Authorization },
      })
      .then((response) => {
        setData(data.filter((d) => !ids.includes(d.id)));
        abrirCerrarModalEliminar();
        toastSuccess("Xóa dữ liệu thành công");
      })
      .catch((error) => {
        ErrorMess(error);
      });
  };

  const seleccionarArtista = (id, caso) => {
    setArtistaSeleccionado(id);
    caso === "Editar" ? abrirCerrarModalEditar() : abrirCerrarModalEliminar();
  };
  const seleccionarView = (id, caso) => {
    setArtistaSeleccionado(id);
    if (caso === "check") {
      viewInformation();
    }
  };

  const abrirCerrarModalInsertar = () => {
    setModalInsertar(!modalInsertar);
  };
  const abrirCerrarModalEditar = () => {
    setModalEditar(!modalEditar);
  };
  const abrirCerrarModalEliminar = () => {
    setModalEliminar(!modalEliminar);
  };
  const viewInformation = () => {
    setModalViewInfor(!modalViewInfor);
  };
  useEffect(() => {
    getContact();
    getSource();
    getCustomer();
    getClasstification();
  }, []);

  const hasError = (name) => {
    return (
      artistaSeleccionado &&
      artistaSeleccionado.errors &&
      artistaSeleccionado.errors[name] &&
      artistaSeleccionado.errors[name].length > 0 && (
        <span className="error">{artistaSeleccionado.errors[name]}</span>
      )
    );
  };
  const bodyInsert = (
    <Modal
      size="lg"
      show={modalInsertar}
      onHide={() => abrirCerrarModalInsertar()}
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header>
        <Modal.Title>Thêm Liên Hệ</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Container>
          <Row>
            <Col md={6}>
              <label> Họ và Đệm </label>
              <input
                type="text"
                class="form-control"
                name="lastName"
                onChange={handleChange}
              ></input>
              {hasError("lastName")}
            </Col>
            <Col md={6}>
              <label>Tên</label>
              <input
                type="text"
                class="form-control"
                name="name"
                onChange={handleChange}
              ></input>
              {hasError("name")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Mã liên hệ</label>
              <input
                type="text"
                class="form-control"
                name="code"
                onChange={handleChange}
              ></input>
              {hasError("code")}
            </Col>
            <Col md={6}>
              <label>Điện thoại</label>
              <input
                type="text"
                class="form-control"
                name="phone"
                onChange={handleChange}
              ></input>
              {hasError("phone")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Điện thoại cơ quan</label>
              <input
                type="text"
                class="form-control"
                name="officePhone"
                onChange={handleChange}
              ></input>
              {hasError("officePhone")}
            </Col>
            <Col md={6}>
              <label> Điện thoại khác </label>
              <input
                type="text"
                class="form-control"
                name="otherPhone"
                onChange={handleChange}
              ></input>
              {hasError("otherPhone")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Email</label>
              <input
                type="text"
                class="form-control"
                name="email"
                onChange={handleChange}
              ></input>
              {hasError("email")}
            </Col>
            <Col md={6}>
              <label> Email cơ quan </label>
              <input
                type="text"
                class="form-control"
                name="officeEmail"
                onChange={handleChange}
              ></input>
              {hasError("officeEmail")}
            </Col>
          </Row>

          <Row>
            <Col md={6}>
              <label> Xưng hô </label>
              <select
                className="form-control controll "
                value={artistaSeleccionado.vocative}
                name="vocative"
                onChange={handleChange}
              >
                {vocative3.map((option2, index) => (
                  <option value={option2.value} key={index}>
                    {" "}
                    {option2.label}
                  </option>
                ))}
              </select>
            </Col>
            <Col md={6}>
              <label>Nguồn gốc </label>
              <select
                className="form-control controll1 formsingle"
                name="sourceId"
                onChange={handleChange}
              >
                {source3.selectSource.map((option2, index) => (
                  <option value={option2.value} key={index}>
                    {" "}
                    {option2.label}
                  </option>
                ))}
              </select>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Tổ chức </label>
              <select
                className="form-control controll formsingle"
                name="customerId"
                onChange={handleChange}
              >
                {customer3.selectCustomer.map((option2, index) => (
                  <option value={option2.value} key={index}>
                    {" "}
                    {option2.label}
                  </option>
                ))}
              </select>
            </Col>
            <Col md={6}>
              <label> Phân Loại Khách Hàng </label>
              <Select
                className="dropdown controll1 formselect"
                placeholder="Phân Loại Khách Hàng"
                isMulti
                options={classifications3.selectClassification}
                onChange={handleChange2}
              ></Select>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Phòng Ban </label>
              <select
                className="form-control controll formsingle"
                value={artistaSeleccionado.department}
                name="department"
                onChange={handleChange}
              >
                {department3.map((option2, index) => (
                  <option value={option2.value} key={index}>
                    {" "}
                    {option2.label}
                  </option>
                ))}
              </select>
            </Col>
            <Col md={6}>
              <label> Chức Danh </label>
              <select
                className="form-control controll formsingle"
                value={artistaSeleccionado.position}
                name="position"
                onChange={handleChange}
              >
                {position3.map((option2, index) => (
                  <option value={option2.value} key={index}>
                    {" "}
                    {option2.label}
                  </option>
                ))}
              </select>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Địa chỉ </label>
              <input
                type="text"
                class="form-control"
                name="address"
                onChange={handleChange}
              ></input>
              {hasError("address")}
            </Col>
          </Row>
        </Container>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={() => {
            clearErrors();
            abrirCerrarModalInsertar();
          }}
        >
          Hủy Bỏ
        </Button>
        <Button
          variant="primary"
          onClick={() => createContact()}
          disabled={!validateForm(artistaSeleccionado.errors)}
        >
          {" "}
          Thêm{" "}
        </Button>
      </Modal.Footer>
    </Modal>
  );

  const bodyEdit = (
    <Modal
      size="lg"
      show={modalEditar}
      onHide={() => abrirCerrarModalEditar()}
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header>
        <Modal.Title>Sửa Thông Tin</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Container>
          <Row>
            <Col md={6}>
              <label> Họ và Đệm </label>
              <input
                type="text"
                class="form-control"
                name="lastName"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.lastName}
              ></input>
              {hasError("lastName")}
            </Col>
            <Col md={6}>
              <label className="required">Tên</label>
              <input
                type="text"
                class="form-control"
                name="name"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.name}
              ></input>
              {hasError("name")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label className="required">Mã liên hệ</label>
              <input
                type="text"
                class="form-control"
                name="code"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.code}
              ></input>
              {hasError("code")}
            </Col>

            <Col md={6}>
              <label>Điện thoại</label>
              <input
                type="text"
                class="form-control"
                name="phone"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.phone}
              ></input>
              {hasError("phone")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Điện thoại cơ quan</label>
              <input
                type="text"
                class="form-control"
                name="officePhone"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.officePhone}
              ></input>
              {hasError("officePhone")}
            </Col>
            <Col md={6}>
              <label> Điện thoại khác </label>
              <input
                type="text"
                class="form-control"
                name="otherPhone"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.otherPhone}
              ></input>
              {hasError("otherPhone")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label className="required">Email</label>
              <input
                type="text"
                class="form-control"
                name="email"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.email}
              ></input>
              {hasError("email")}
            </Col>
            <Col md={6}>
              <label> Email cơ quan </label>
              <input
                type="text"
                class="form-control"
                name="officeEmail"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.officeEmail}
              ></input>
              {hasError("officeEmail")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Xưng hô </label>
              <select
                className="form-control controll"
                value={artistaSeleccionado && artistaSeleccionado.vocative}
                name="vocative"
                onChange={handleChange}
              >
                <option value="">-- Không chọn --</option>
                <option value="Ông"> Ông </option>
                <option value="Bà"> Bà </option>
                <option value="Anh"> Anh </option>
                <option value="Chị"> Chị </option>
                <option value="Tôi"> Tôi</option>
              </select>
            </Col>
            <Col md={6}>
              <label>Nguồn gốc </label>
              <select
                className="form-control controll1 formsingle"
                name="sourceId"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.sourceId}
              >
                {source3.selectSource.map((option2, index) => (
                  <option value={option2.value} key={index}>
                    {" "}
                    {option2.label}
                  </option>
                ))}
              </select>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Tổ chức </label>
              <select
                className="form-control controll formsingle"
                name="customerId"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.customerId}
              >
                {customer3.selectCustomer.map((option2, index) => (
                  <option value={option2.value} key={index}>
                    {" "}
                    {option2.label}
                  </option>
                ))}
              </select>
            </Col>
            <Col md={6}>
              <label> Phân Loại Khách Hàng </label>
              <Select
                className="dropdown controll1 formselect"
                placeholder="Phân Loại Khách Hàng"
                isMulti
                options={classifications3.selectClassification}
                onChange={handleChange2}
                defaultValue={
                  artistaSeleccionado &&
                  artistaSeleccionado.classifications &&
                  artistaSeleccionado.classifications.map((d) => ({
                    value: d.id ? d.id : d.value,
                    label: d.id ? d.name : d.label,
                  }))
                }
              ></Select>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Phòng Ban </label>
              <select
                className="form-control controll formsingle"
                value={artistaSeleccionado && artistaSeleccionado.department}
                name="department"
                onChange={handleChange}
              >
                <option value="">-- Không chọn --</option>
                <option value="Phòng nhân sự"> Phòng nhân sự</option>
                <option value="Ban giám đốc">Ban Giám Đốc</option>
                <option value="Phòng tài chính">Phòng tài chính </option>
                <option value="Phòng marketing">Phòng Marketing</option>
                <option value="Phòng chăm sóc khách hàng">
                  Phòng chăm sóc khách hàng
                </option>
                <option value="Phòng hàng chính tổng hợp">
                  Phòng hàng chính tổng hợp
                </option>
                <option value="Phòng kinh doanh">Phòng Kinh Doanh</option>
              </select>
            </Col>
            <Col md={6}>
              <label> Chức Danh </label>
              <select
                className="form-control controll formsingle"
                value={artistaSeleccionado && artistaSeleccionado.position}
                name="position"
                onChange={handleChange}
              >
                <option value="">-- Không chọn --</option>
                <option value="Trưởng phòng"> Trưởng Phòng</option>
                <option value="Chủ tịch">Chủ tịch</option>
                <option value="Phó chủ tịch">Phó Chủ Tịch</option>
                <option value="Tổng giám đốc">Tổng Giám Đốc</option>
                <option value="Phó tổng giám đốc">Phó Tổng Giám Đốc</option>
                <option value="Giám đốc">Giám Đốc</option>
                <option value="Kế toán trưởng">Kế Toán Trưởng </option>
                <option value="Trợ lý">Trợ Lý </option>
                <option value="Nhân viên"> Nhân viên</option>
              </select>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Địa chỉ </label>
              <input
                type="text"
                class="form-control"
                name="address"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.address}
              ></input>
              {hasError("address")}
            </Col>
          </Row>
        </Container>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={() => {
            clearErrors();
            abrirCerrarModalEditar();
          }}
        >
          Hủy Bỏ
        </Button>
        <Button
          variant="primary"
          onClick={() => editContact()}
          disabled={!validateForm(artistaSeleccionado.errors)}
        >
          Sửa
        </Button>
      </Modal.Footer>
    </Modal>
  );

  const View = (
    <Modal
      size="lg"
      show={modalViewInfor}
      onHide={() => viewInformation()}
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header>
        <Modal.Title>Xem Thông Tin Chi Tiết</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Container>
          <Row>
            <Col md={6}>
              <label> Họ và Đệm </label>
              <input
                type="text"
                class="form-control"
                name="lastName"
                value={artistaSeleccionado && artistaSeleccionado.lastName}
              ></input>
            </Col>
            <Col md={6}>
              <label className="required">Tên</label>
              <input
                type="text"
                class="form-control"
                name="name"
                value={artistaSeleccionado && artistaSeleccionado.name}
              ></input>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label className="required">Mã liên hệ</label>
              <input
                type="text"
                class="form-control"
                name="code"
                value={artistaSeleccionado && artistaSeleccionado.code}
              ></input>
            </Col>

            <Col md={6}>
              <label>Điện thoại</label>
              <input
                type="text"
                class="form-control"
                name="phone"
                value={artistaSeleccionado && artistaSeleccionado.phone}
              ></input>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Điện thoại cơ quan</label>
              <input
                type="text"
                class="form-control"
                name="officePhone"
                value={artistaSeleccionado && artistaSeleccionado.officePhone}
              ></input>
              {hasError("officePhone")}
            </Col>
            <Col md={6}>
              <label> Điện thoại khác </label>
              <input
                type="text"
                class="form-control"
                name="otherPhone"
                value={artistaSeleccionado && artistaSeleccionado.otherPhone}
              ></input>
              {hasError("otherPhone")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label className="required">Email</label>
              <input
                type="text"
                class="form-control"
                name="email"
                value={artistaSeleccionado && artistaSeleccionado.email}
              ></input>
            </Col>
            <Col md={6}>
              <label> Email cơ quan </label>
              <input
                type="text"
                class="form-control"
                name="officeEmail"
                value={artistaSeleccionado && artistaSeleccionado.officeEmail}
              ></input>
              {hasError("officeEmail")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Xưng hô </label>
              <select
                className="form-control"
                value={artistaSeleccionado && artistaSeleccionado.vocative}
                name="vocative"
              >
                <option value="">-- Không chọn --</option>
                <option value="Ông"> Ông </option>
                <option value="Bà"> Bà </option>
                <option value="Anh"> Anh </option>
                <option value="Chị"> Chị </option>
                <option value="Tôi"> Tôi</option>
              </select>
            </Col>
            <Col md={6}>
              <label>Nguồn gốc </label>
              <select
                className="form-control controll1 formsingle"
                name="sourceId"
                value={artistaSeleccionado && artistaSeleccionado.sourceId}
              >
                {source3.selectSource.map((option2, index) => (
                  <option value={option2.value} key={index}>
                    {" "}
                    {option2.label}
                  </option>
                ))}
              </select>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Tổ chức </label>
              <select
                className="form-control formsingle"
                name="customerId"
                value={artistaSeleccionado && artistaSeleccionado.customerId}
              >
                {customer3.selectCustomer.map((option2, index) => (
                  <option value={option2.value} key={index}>
                    {" "}
                    {option2.label}
                  </option>
                ))}
              </select>
            </Col>
            <Col md={6}>
              <label> Phân Loại Khách Hàng </label>
              <Select
                className="dropdown controll1 formselect"
                placeholder="Phân Loại Khách Hàng"
                isMulti
                options={classifications3.selectClassification}
                defaultValue={
                  artistaSeleccionado &&
                  artistaSeleccionado.classifications &&
                  artistaSeleccionado.classifications.map((d) => ({
                    value: d.id ? d.id : d.value,
                    label: d.id ? d.name : d.label,
                  }))
                }
              ></Select>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Phòng Ban </label>
              <select
                className="form-control formsingle"
                value={artistaSeleccionado && artistaSeleccionado.department}
                name="department"
              >
                <option value="">-- Không chọn --</option>
                <option value="Phòng nhân sự"> Phòng nhân sự</option>
                <option value="Ban giám đốc">Ban Giám Đốc</option>
                <option value="Phòng tài chính">Phòng tài chính </option>
                <option value="Phòng marketing">Phòng Marketing</option>
                <option value="Phòng chăm sóc khách hàng">
                  Phòng chăm sóc khách hàng
                </option>
                <option value="Phòng hàng chính tổng hợp">
                  Phòng hàng chính tổng hợp
                </option>
                <option value="Phòng kinh doanh">Phòng Kinh Doanh</option>
              </select>
            </Col>
            <Col md={6}>
              <label> Chức Danh </label>
              <select
                className="form-control formsingle"
                value={artistaSeleccionado && artistaSeleccionado.position}
                name="position"
              >
                <option value="">-- Không chọn --</option>
                <option value="Trưởng phòng"> Trưởng Phòng</option>
                <option value="Chủ tịch">Chủ tịch</option>
                <option value="Phó chủ tịch">Phó Chủ Tịch</option>
                <option value="Tổng giám đốc">Tổng Giám Đốc</option>
                <option value="Phó tổng giám đốc">Phó Tổng Giám Đốc</option>
                <option value="Giám đốc">Giám Đốc</option>
                <option value="Kế toán trưởng">Kế Toán Trưởng </option>
                <option value="Trợ lý">Trợ Lý </option>
                <option value="Nhân viên"> Nhân viên</option>
              </select>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Địa chỉ </label>
              <input
                type="text"
                class="form-control"
                name="address"
                value={artistaSeleccionado && artistaSeleccionado.address}
              ></input>
            </Col>
            <Col md={6}>
              <label> Ngày Tạo </label>
              <input
                type="text"
                class="form-control"
                name="createdAt"
                value={artistaSeleccionado && artistaSeleccionado.createdAt}
              ></input>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Người tạo </label>
              <input
                type="text"
                class="form-control"
                name="createdBy"
                value={
                  artistaSeleccionado &&
                  artistaSeleccionado.createdBy &&
                  artistaSeleccionado.createdBy.username
                }
              ></input>
            </Col>
            <Col md={6}>
              <label> Ngày sửa </label>
              <input
                type="text"
                class="form-control"
                name="updatedAt"
                value={artistaSeleccionado && artistaSeleccionado.updatedAt}
              ></input>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Người sửa </label>
              <input
                type="text"
                class="form-control"
                name="updatedBy"
                value={
                  artistaSeleccionado &&
                  artistaSeleccionado.updatedBy &&
                  artistaSeleccionado.updatedBy.username
                }
              ></input>
            </Col>
          </Row>
        </Container>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" onClick={() => viewInformation()}>
          Hủy Bỏ
        </Button>
      </Modal.Footer>
    </Modal>
  );

  // xác nhận yêu cầu xóa
  const Delete = (
    <Modal
      show={modalEliminar}
      onHide={() => abrirCerrarModalEliminar()}
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header>
        <Modal.Title>Xóa</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p> Bạn có chắc chắn muốn xóa không ? </p>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={() => abrirCerrarModalEliminar()}>
          Không
        </Button>
        <Button variant="primary" onClick={() => deleteContact()}>
          Có
        </Button>
      </Modal.Footer>
    </Modal>
  );

  const actions = [
    {
      icon: "add_box",
      tooltip: "Thêm",
      position: "toolbar",
      onClick: () =>
        hasPerm(user, "ROLE_CREATE_CONTACT")
          ? abrirCerrarModalInsertar()
          : missingPermission(),
    },
    {
      icon: "delete",
      tooltip: "Xóa tất cả đang chọn",
      onClick: (event, rowData) =>
        hasPerm(user, "ROLE_DELETE_CONTACT")
          ? seleccionarArtista(rowData, "Eliminar")
          : missingPermission(),
      position: "toolbarOnSelect",
    },
    {
      icon: "edit",
      tooltip: "Sửa thông tin",
      onClick: (event, rowData) =>
        hasPerm(user, "ROLE_UPDATE_CONTACT")
          ? seleccionarArtista(rowData, "Editar")
          : missingPermission(),
      position: "row",
    },
    {
      icon: "delete",
      tooltip: "Xóa thông tin",
      onClick: (event, rowData) =>
        hasPerm(user, "ROLE_DELETE_CONTACT")
          ? seleccionarArtista(rowData, "Eliminar")
          : missingPermission(),
      position: "row",
    },
    {
      icon: () => <VisibilityIcon />,
      tooltip: "Xem chi tiết",
      onClick: (event, rowData) =>
        hasPerm(user, "ROLE_READ_CONTACT")
          ? seleccionarView(rowData, "check")
          : missingPermission(),
      position: "row",
    },
  ];
  const lookUp = (e) =>
    e
      ?.map((item) => ({ [item.value]: item.label }))
      .reduce(function (result, item) {
        var key = Object.keys(item)[0];
        result[key] = item[key];
        return result;
      }, {});

  const listColumn = [
    {
      title: "Mã liên hệ",
      dataIndex: "code",
    },
    {
      title: "Xưng hô",
      dataIndex: "vocative",
    },
    {
      title: "Họ và đệm",
      dataIndex: "lastName",
    },
    {
      title: "Tên",
      dataIndex: "name",
    },
    {
      title: "Chức danh",
      dataIndex: "position",
    },
    {
      title: "Phòng ban",
      dataIndex: "department",
    },
    {
      title: "Điện thoại",
      dataIndex: "phone",
    },
    {
      title: "Email",
      dataIndex: "email",
      width: 200,
    },
    {
      title: "Nguồn gốc",
      dataIndex: "source",
      width: 200,
    },
    {
      title: "Tổ chức",
      dataIndex: "customer",
      width: 300,
    },
    {
      title: "Hành động",
      key: "operation",
      fixed: "right",
      width: "150px",
      dataIndex: "action",
    },
  ];
  const handleMultiDelete = (selectedRowKeys) => {
    setArtistaSeleccionado(selectedRowKeys);
    abrirCerrarModalEliminar();
  };
  const handlDelete = (data) => {
    setArtistaSeleccionado(data);
    abrirCerrarModalEliminar();
  };
  const onEdit = (id) => {
    history.push(`/contact/edit/${id}`);
  };
  const onView = (id) => {
    history.push(`/contact/view/${id}`);
  };
  const handleConvert = (item) => {
    if(item.length>1){
      toast.warning("Chức năng đang được phát triển");
    }
    else{
      history.push(`/opportunity/convert?contactId=${item[0].id}`)
    }
  };
  const onAddNew = () => {
    hasPerm(user, "ROLE_CREATE_CONTACT")
     ? 
    history.push(`/contact/add`) : missingPermission()
  };
  const getDataTable = () => {
    let cloneData = data.map((item, index) => {
      return {
        ...item,
        key: index,
        source: item?.source?.name,
        customer: item?.customer?.name,
        vocative: item?.vocative?.name,
        position: item?.position?.name,
        department: item?.department?.name,
        action: (
          <div className="d-flex align-items-center p-0 justify-content-between">
           <EditOutlined
              className=" btnIcon"
              onClick={() => {
                hasPerm(user, "ROLE_UPDATE_CONTACT")
                  ? onEdit(item?.id)
                  : missingPermission()
              }}
              style={{ color: "#C8DF52" }}
            />
            <CloseOutlined
              className="btnIcon"
              onClick={() => {
                hasPerm(user, "ROLE_DELETE_CONTACT")
                  ? handlDelete(item)
                  : missingPermission();
              }}
              style={{ color: "#D2042D" }}
            />
            <EyeOutlined
              className=" btnIcon"
              onClick={() => {
                hasPerm(user, "ROLE_READ_CONTACT")
                  ? onView(item?.id)
                  : missingPermission()
              }}

              style={{ color: "#B3CDE0" }} />
          </div>
        ),
      };
    });
    return cloneData;
  };
  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-md-12">
          <Tableb
            columns={convertColumn(listColumn)}
            handleMultiDelete={handleMultiDelete}
            data={getDataTable()}
            onAddNew={onAddNew}
            handleConvert={handleConvert}
            titlechucnang = "Liên Hệ"
            hasConvert
          ></Tableb>
        </div>
        {Delete}
      </div>
    </div>
  );
}

export default Contact;