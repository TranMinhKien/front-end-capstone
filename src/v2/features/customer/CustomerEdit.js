import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Button, Form } from "antd";
import FormDataCustom from "../../utils/FormDataCustom";
import { goToCustomer, TableCustomer } from "./CustomerAddNew";
import { useHistory } from "react-router-dom";
import { validateCustomerSchema } from "../../config/validate/customerValidate";
import moment from "moment";
import { getCustomerById } from "v2/api/readByIdAPI";
import { toast } from "react-toastify";
import { updateCustomer } from "v2/api/updateAPI";
import { convertToSingleArray } from "v2/components/columns";
import {setDateToString} from '../../utils/format';

CustomerEdit.propTypes = {};

function CustomerEdit(props) {
  const customerId = props.match.params.id;
  const history = useHistory();
  const rHF = useForm({
    resolver: yupResolver(validateCustomerSchema),
    mode:"onChange"
  });
  const {
    control,
    formState: { errors, isSubmitting },
    watch,
    handleSubmit,
    setValue,
  } = rHF;
  const [form] = Form.useForm();
  const formCustom = new FormDataCustom(form, rHF);
  const goToCustomer = () => history.push("/customer");
  const [customer, setCustomer] = useState();
  const setValueData = (data) => [
    {
      value: data?.shortName,
      name: "shortName",
    },
    {
      value: data?.name,
      name: "name",
    },
    {
      value: data?.position?.id,
      name: "positionId",
    },
    {
      value: data?.phone,
      name: "phone",
    },
    {
      value: data?.officePhone,
      name: "officePhone",
    },
    {
      value: data?.otherPhone,
      name: "otherPhone",
    },
    {
      value: data?.email,
      name: "email",
    },
    {
      value: data?.department?.id,
      name: "departmentId",
    },
    {
      value: convertToSingleArray(data?.classifications),
      name: "classificationIds",
    },
    {
      value: convertToSingleArray(data?.careers),
      name: "careerIds",
    },
    {
      value: convertToSingleArray(data?.fields),
      name: "fieldIds",
    },
    {
      value: data?.source?.id,
      name: "sourceId",
    },
    {
      value: data?.notCallPhone,
      name: "notCallPhone",
    },
    {
      value: data?.notSendEmail,
      name: "notSendEmail",
    },
    {
      value: data?.officeEmail,
      name: "officeEmail",
    },
    {
      value: data?.customer,
      name: "customer",
    },
    {
      value: data?.taxCode,
      name: "taxCode",
    },
    {
      value: data?.gender?.id,
      name: "genderId",
    },
    {
      value: data?.customerTaxCode,
      name: "customerTaxCode",
    },
    {
      value: data?.dateOfBirth ? new moment(data?.dateOfBirth) : null,
      name: "dateOfBirth",
    },
    {
      value: data?.facebook,
      name: "facebook",
    },
    {
      value: data?.bankAccount,
      name: "bankAccount",
    },
    {
      value: data?.bank,
      name: "bank",
    },
    {
      value: data?.foundedDate ? new moment(data?.foundedDate) : null,
      name: "foundedDate",
    },
    {
      value: data?.customerSince ? new moment(data?.customerSince) : null,
      name: "customerSince",
    },
    {
      value: data?.type?.id,
      name: "typeId",
    },
    {
      value: data?.income?.id,
      name: "incomeId",
    },
    {
      value: data?.website,
      name: "website",
    },
    {
      value: data?.province?.id,
      name: "provinceId",
    },
    {
      value: data?.district?.id,
      name: "districtId",
    },
    {
      value: data?.ward?.id,
      name: "wardId",
    },{
      value: data?.address,
      name: "address",
    },
  ];
  console.log(errors);
  const onSubmit = async (data) => {
    setDateToString(data);
    try {
      const response = await updateCustomer(data, customerId);
      if (response.error) {
        throw new Error(response.error);
      }
      if (!response.data) {
        throw new Error("Cập nhật thất bại");
      }
      history.push('/customer')
      toast.success("Cập nhật thành công");
    } catch (error) {
      toast.error(error.data);
    }
  };
  useEffect(() => {
    const fetchCustomer = async () => {
      try {
        setCustomer({ ...customer, isLoading: true });
        const response = await getCustomerById(customerId);
        if (response.error) {
          throw new Error(response.error);
        }
        setCustomer({
          data: response.data,
          isLoading: false,
        });
        formCustom.setFormMany(setValueData(response.data));
      } catch (error) {
        console.log("fail to featch customer");
        setCustomer({
          data: null,
          isLoading: false,
        });
      }
    };
    fetchCustomer();
  }, [customerId]);

  return (
    <Form form={form} onFinish={handleSubmit(onSubmit)}>
      <div className={"sticky-bar-top"}>
      <h5 className={"m-0"}>Chỉnh sửa Tổ Chức </h5>
        <div className={"ms-auto"}>
          <Button type="dashed" onClick={goToCustomer}>Huỷ</Button>
          <Button type="primary" htmlType="submit" className={"ms-2"} loading={isSubmitting}>
            Lưu
          </Button>
        </div>
      </div>
      <TableCustomer control={control} customForm={formCustom} />
    </Form>
  );
}

export default CustomerEdit;
