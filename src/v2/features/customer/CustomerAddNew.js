import React from "react";
import InputTextField from "../../components/CustomFields/InputTextField";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { Button, Form } from "antd";
import FormDataCustom from "../../utils/FormDataCustom";
import SelectField from "../../components/CustomFields/SelectField";
import InputDateField from "../../components/CustomFields/InputDateField";
import "./customer.css";
import "v2/assets/css/_base.css";
import { useHistory } from "react-router-dom";
import InputPhoneField from "../../components/CustomFields/InputPhoneField";
import SourceField from "v2/components/customElement/SourceField";
import TypeField from "v2/components/customElement/TypeField";
import ClassificationField from "v2/components/customElement/ClassificationField";
import FieldField from "v2/components/customElement/FieldField";
import CareerField from "v2/components/customElement/CareerField";
import { validateCustomerSchema } from "../../config/validate/customerValidate";
import { createContact, createCustomer } from "../../api/createAPI";
import { toast } from "react-toastify";
import { setDateToString } from "../../utils/format";
import IncomeField from "v2/components/customElement/IncomeField";
import AddressField from '../../components/customElement/AddressField';

function CustomerAddNew(props) {
  const history = useHistory();
  const rHF = useForm({
    resolver: yupResolver(validateCustomerSchema),
  });
  const [form] = Form.useForm();
  const { control, handleSubmit } = rHF;
  const customForm = new FormDataCustom(form, rHF);

  const goToCustomer = () => history.push("/customer");

  async function onSubmit(data) {
    console.log(data);
    try {
      setDateToString(data);
      await createCustomer(data);
      history.push('/customer');
      toast.success("Thêm thành công");
    } catch (e) {

      toast.error("Mã số thuế trùng");
    }
  }
  return (
    <Form form={form} onFinish={handleSubmit(onSubmit)}>
      <div className={"sticky-bar-top"}>
      <h5 className={"m-0"}>Thêm Tổ Chức </h5>
        <div className={"ms-auto"}>
          <Button type="dashed" onClick={goToCustomer}>
            Huỷ
          </Button>{" "}
          <Button type="primary" htmlType={"submit"} className={"ms-2"}>
            Thêm
          </Button>
        </div>
      </div>
      <TableCustomer control={control} customForm={customForm} />
    </Form>
  );
}

export const TableCustomer = ({ control,customForm }) => {
  return (
    <div className={"container-lg py-5"}>
      <table className={"mx-auto spacing-table w-75 "}>
        <tbody>
          <tr>
            <td colSpan={2}>
              <h5>Thông tin chung</h5>
            </td>
          </tr>
          <tr>
            <td>
              <InputTextField
                name={"name"}
                control={control}
                label={"Tên tổ chức"}
                isRequired
              />
            </td>
            <td>
              <InputTextField
                name={"shortName"}
                control={control}
                label={"Tên viết tắt"}
              />
            </td>
          </tr>
          <tr>
            <td>
              <InputTextField
                name={"taxCode"}
                control={control}
                label={"Mã số thuế"}
              />
            </td>
            <td>
              <InputPhoneField
                inputName={"phone"}
                selectName={"prefixPhone"}
                control={control}
                label={"Điện thoại"}
                isRequired
              />
            </td>
          </tr>
          <tr>
            <td>
              <InputTextField
                name={"email"}
                control={control}
                label={"Email"}
                isRequired
              />
            </td>
            <td>
              <SourceField name={"sourceId"} control={control} />
            </td>
          </tr>
          <tr>
            <td>
              <ClassificationField
                name={"classificationIds"}
                control={control}
                label={"Phân loại khách hàng"}
              />
            </td>
            <td>
              <FieldField name={"fieldIds"} control={control} />
            </td>
          </tr>
          <tr>
            <td>
              <TypeField name={"typeId"} control={control} />
            </td>
            <td>
              <CareerField
                name={"careerIds"}
                control={control}
                label={"Ngành nghề"}
              />
            </td>
          </tr>

          <tr>
            <td>
              <InputDateField
                name={"customerSince"}
                control={control}
                label={"Là khách hàng từ"}
              />
            </td>
            <td>
              <IncomeField name={"incomeId"} control={control} />
            </td>
          </tr>


          <tr>
            <td colSpan={2}>
              <h5 className={"mt-3"}>Thông bổ sung</h5>
            </td>
          </tr>
          <tr>
            <td>
              <InputTextField
                  name={"website"}
                  control={control}
                  label={"Website"}
              />
            </td>
            <td>
              <InputTextField
                name={"bankAccount"}
                control={control}
                label={"Tài khoản ngân hàng"}
              />
            </td>
          </tr>
          <tr>
            <td>
              <InputTextField
                name={"bank"}
                control={control}
                label={"Mở tại ngân hàng"}
              />
            </td>
            <td>
              <InputDateField
                name={"foundedDate"}
                control={control}
                label={"Ngày thành lập"}
                isRequired
              />
            </td>
          </tr>
        <AddressField customForm={customForm} control={control}/>
        </tbody>
      </table>
    </div>
  );
};
export default CustomerAddNew;
