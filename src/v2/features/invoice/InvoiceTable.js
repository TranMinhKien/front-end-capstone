import React from 'react';
import PropTypes from 'prop-types';
import InputTextField from '../../components/CustomFields/InputTextField';
import InputPhoneField from '../../components/CustomFields/InputPhoneField';
import CustomerField from '../../components/customElement/CustomerField';
import InputNumberField from '../../components/CustomFields/InputNumberField';
import ContactField from '../../components/customElement/ContactField';
import AddressField from '../../components/customElement/AddressField';
import OrderField from '../../components/customElement/OrderField';
import EditableTable from 'v2/components/tableEdit/EditTableCell';


const InvoiceTable = ({control, customForm}) => {
    function onCustomerClear() {
        customForm.setFormValue('customerId', undefined);
    }
    function onCustomerChange() {
        customForm.setFormValue('buyerId', undefined);
    }

    return (
        <div className={"container-lg py-5"}>
            <table className={"mx-auto spacing-table w-75 "}>
                <tbody>
                <tr>
                    <td colSpan={2}>
                        <h5>Thông tin chung</h5>
                    </td>
                </tr>
                <tr>
                    <td>
                        <CustomerField name={'customerId'} control={control} label={'Tổ chức'} customForm={customForm}
                                       onClear={onCustomerClear} onChange={onCustomerChange}/>
                    </td>
                    <td>
                        <InputNumberField
                            name={"bankAccount"}
                            control={control}
                            label={"Tài khoản ngân hàng"}
                        />
                    </td>
                </tr>
                <tr>
                    <td>
                        <InputTextField
                            name={"bank"}
                            control={control}
                            label={"Mở tại ngân hàng"}
                        />
                    </td>
                    <td>
                        <InputNumberField
                            name={"taxCode"}
                            control={control}
                            label={"Mã số thuế"}

                        />
                    </td>
                </tr>
                <tr>
                    <td>
                        <InputPhoneField isRequired selectName={'prefixPhone'} inputName={'receiverPhone'}
                                         control={control} label={'Điện thoại người nhận'}/>
                    </td>
                    <td>
                        <ContactField name={"buyerId"} control={control} label={'Người mua hàng'}
                                      customForm={customForm}/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <OrderField name={'orderId'} control={control} label={'Đơn hàng'} customForm={customForm}/>
                    </td>

                    <td>
                        <InputTextField name={'receiverEmail'} label={'Email người nhận'} control={control}/>

                    </td>
                </tr>
                <tr>

                    <td colSpan={2}>
                        <InputTextField name={'receiverName'} label={'Tên người nhận'} control={control} isRequired/>
                    </td>
                </tr>
                <AddressField control={control} customForm={customForm}/>
                </tbody>
            </table>
            <h5>Thông tin hàng hóa</h5>
            <EditableTable/>
        </div>
    );
};
InvoiceTable.propTypes = {
    control: PropTypes.any
};
export default InvoiceTable;