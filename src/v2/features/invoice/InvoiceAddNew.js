import React, {useContext, useEffect} from "react";
import {useHistory} from "react-router-dom";
import {useForm} from "react-hook-form";
import {yupResolver} from "@hookform/resolvers/yup";
import {validateCustomerSchema} from "../../config/validate/customerValidate";
import {Button, Form} from "antd";
import FormDataCustom from "../../utils/FormDataCustom";
import InvoiceTable from "./InvoiceTable";
import {setDateToString} from "../../utils/format";
import {toast} from "react-toastify";
import {createInvoice} from "../../api/createAPI";
import {ProductContext} from "v2/providers/provider";
import {getOrderById} from '../../api/readByIdAPI';
import { validateInvoicerSchema } from "v2/config/validate/invoiceValidate";

InvoiceAddNew.propTypes = {};

function InvoiceAddNew(props) {
    const urlParams = new URLSearchParams(props.location.search);
    const orderId = urlParams.get("orderId");
    const {products: productInfos, setContext} = useContext(ProductContext);
    const history = useHistory();
    const rHF = useForm({
        resolver: yupResolver(validateInvoicerSchema)
    });
    const [form] = Form.useForm();
    const {control, handleSubmit} = rHF;
    const customForm = new FormDataCustom(form, rHF);

    const goToInvoice = () => history.push("/invoice");

    async function onSubmit(data) {
        data.productInfos = productInfos;
        try {
            setDateToString(data);
            await createInvoice(data);
            history.push("/invoice");
            toast.success("Thêm thành công");
        } catch (e) {
            toast.error("Thêm thất bại");
        }
    }

    const setupFormData = (data) => [
        {name: "customerId", value: data?.customer?.id},

        {name: "bank", value: data?.contact?.bank},
        {name: "bankAccount", value: data?.contact?.bankAccount},
        {name : "taxCode" , value : data?.customer?.taxCode},
        // {name: "buyerId", value: data?.buyer?.id},
        // {name: "receiverEmail", value: data?.receiverEmail},
        // {name: "receiverPhone", value: data?.receiverPhone},
        // {name: "prefixPhone", value: "+84"},
        //
        {name: "orderId", value: data?.id || undefined},
        {name: "provinceId", value: data?.province?.id || undefined},
        {name: "districtId", value: data?.district?.id || undefined},
        {name: "wardId", value: data?.ward?.id || undefined},
        {name: "address", value: data?.address || undefined}
    ];

    async function fetchOrderData() {
        try {
            const response = await getOrderById(orderId);
            customForm.setFormMany(setupFormData(response.data));
            setContext(response.data.productInfos);
        } catch (e) {
            toast.error('Lấy giữ liệu đơn hàng thất bại');
        }
    }

    useEffect(() => {
        if (orderId) fetchOrderData();
        return setContext([]);
    }, []);
    return (
        <Form form={form} onFinish={handleSubmit(onSubmit)}>
            <div className={"sticky-bar-top"}>
                <h5 className={"m-0"}>Thêm Hoá Đơn</h5>
                <div className={"ms-auto"}>
                    <Button type="dashed" onClick={goToInvoice}>
                        Huỷ
                    </Button>{" "}
                    <Button type="primary" htmlType={"submit"} className={"ms-2"}>
                        Thêm
                    </Button>
                </div>
            </div>
            <InvoiceTable control={control} customForm={customForm}/>
        </Form>
    );
}

export default InvoiceAddNew;
