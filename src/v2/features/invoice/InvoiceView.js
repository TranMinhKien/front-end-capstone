import React, { useContext, useEffect } from "react";
import PropTypes from "prop-types";
import { useHistory, useLocation } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { validateCustomerSchema } from "../../config/validate/customerValidate";
import { Button, Form } from "antd";
import FormDataCustom from "../../utils/FormDataCustom";
import InvoiceTable from "./InvoiceTable";
import { getInvoiceById } from "../../api/readByIdAPI";
import { toast } from "react-toastify";
import { updateInvoice } from "../../api/updateAPI";
import { ProductContext } from "v2/providers/provider";

InvoiceView.propTypes = {};

function InvoiceView(props) {
  const { products: productInfos, setContext } = useContext(ProductContext);
  const id = props.match.params.id;
  const history = useHistory();
  const rHF = useForm({
    resolver: yupResolver(validateCustomerSchema),
  });
  const [form] = Form.useForm();
  const {
    control,
    handleSubmit,
    formState: { errors, isSubmitting },
  } = rHF;
  const customForm = new FormDataCustom(form, rHF);
  console.log(errors);
  const goToInvoice = () => history.push("/invoice");
  const setupFormData = (data) => [
    { name: "customerId", value: data?.customer?.id },
    { name: "bankAccount", value: data?.bankAccount },
    { name: "bank", value: data?.bank },
    { name: "taxCode", value: data?.taxCode },
    { name: "email", value: data?.email },
    { name: "receiverName", value: data?.receiverName },
    { name: "buyerId", value: data?.buyer?.id },
    { name: "receiverEmail", value: data?.receiverEmail },
    { name: "receiverPhone", value: data?.receiverPhone },
    { name: "prefixPhone", value: "+84" },
    { name: "orderId", value: data?.order?.id || undefined },
    { name: "provinceId", value: data?.province?.id || undefined },
    { name: "districtId", value: data?.district?.id || undefined },
    { name: "wardId", value: data?.ward?.id || undefined },
  ];
  useEffect(() => {
    const fetchInvoiceData = async () => {
      try {
        const response = await getInvoiceById(id);
        console.log(response);
        customForm.setFormMany(setupFormData(response.data));
        setContext(response.data.productInfos);
      } catch (e) {
        toast.error("Lỗi tải dữ liệu");
      }
    };
    fetchInvoiceData();
    return setContext([]);
  }, []);

  async function onSubmit(data) {
    console.log(data);
    data.productInfoDtos = productInfos;
    data.id = id;
    try {
      await updateInvoice(data, id);
      history.push("/invoice");
      toast.success("Cập nhật thành công");
    } catch (e) {
      toast.error("Cập nhật thất bại");
    }
  }

  return (
    <Form form={form} onFinish={handleSubmit(onSubmit)}>
      <div className={"sticky-bar-top"}>
        <h5 className={"m-0"}>Chi Tiết Hoá Đơn</h5>
        <div className={"ms-auto"}>
          <Button type="dashed" onClick={goToInvoice}>
            Huỷ
          </Button>{" "}
         
        </div>
      </div>
      <InvoiceTable control={control} customForm={customForm} />
    </Form>
  );
}

export default InvoiceView;
