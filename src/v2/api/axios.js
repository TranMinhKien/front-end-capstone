import axios from "axios";
import queryString from "query-string";
import { DOMAIN_BE } from "../../constant/constant";

const Axios = axios.create({
  baseURL: DOMAIN_BE,
  headers: {
    "Content-Type": "application/json",
  },
  paramsSerializer: (params) => queryString.stringify(params),
});

// Add a request interceptor
Axios.interceptors.request.use(async (config) => {
  // Do something before request is sent
  if (!config.url.includes("signin") || !config.url.includes("signup")) {
    
    const user = JSON.parse(localStorage.getItem('user'));
    if(user?.accessToken)
    config.headers.Authorization = await `Bearer ${user.accessToken}`;
  }
  return config;
});

Axios.interceptors.response.use(
  function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    if (response.error) {
      throw new Error(response.error);
    }
    return response;
  },
  function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    // console.log(error.response.data);
    // console.log(error.response.status);
    // console.log(error.response.headers);
    // console.log(error.request);
    // console.log(error.config);

    throw error;
  }
);
export default Axios;
