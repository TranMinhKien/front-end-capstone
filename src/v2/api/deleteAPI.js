import Axios from "./axios";

const DeleteAPI = {
  deletePotential:(params)=>{
    const url = `/api/potential/${params.id}`;
    return Axios.put(url,params);
  },
  
};
export const { deletePotential } = DeleteAPI;
export default DeleteAPI;
