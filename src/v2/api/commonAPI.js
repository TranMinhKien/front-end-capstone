import Axios from "./axios";

const CommonAPI = {
  getVocative: () => {
    const url = "/api/vocative/name";
    return Axios.get(url);
  },
  getPermission : () => {
     const url = `/api/role/name` ;
     return Axios.get(url);
  },
  getPosition: () => {
    const url = "/api/position/name";
    return Axios.get(url);
  },
  getSource: () => {
    const url = "/api/source/name";
    return Axios.get(url);
  },
  getGender: () => {
    const url = "/api/gender/name";
    return Axios.get(url);
  },
  getClassification: () => {
    const url = "/api/classification/name";
    return Axios.get(url);
  },
  getRole : () => {
    const url = "/api/permission/name"
    return Axios.get(url) ; 
  },
  getDepartment: () => {
    const url = "/api/department/name";
    return Axios.get(url);
  },
  getType: () => {
    const url = "/api/type/name";
    return Axios.get(url);
  },
  getField: () => {
    const url = "/api/field/name";
    return Axios.get(url);
  },
  getCareer: () => {
    const url = "/api/career/name";
    return Axios.get(url);
  },
  getCustomer: () => {
    const url = "/api/customer/name";
    return Axios.get(url);
  },
  getProductType: () => {
    const url = "/api/productType/name";
    return Axios.get(url);
  },
  getUnit: () => {
    const url = "/api/unit/name";
    return Axios.get(url);
  },
  getOpportunityPhase: () => {
    const url = "/api/opportunityPhase/name";
    return Axios.get(url);
  },
  getOpportunity: () => {
    const url = "/api/opportunity/name";
    return Axios.get(url);
  },
  getContact: () => {
    const url = "/api/contact";
    return Axios.get(url);
  },
  getBusinessType: () => {
    const url = "/api/businessType/name";
    return Axios.get(url);
  },
  getIncome: () => {
    const url = "/api/income/name";
    return Axios.get(url);
  },
};
export const {
  getPermission,
  getVocative,
  getPosition,
  getSource,
  getGender,
  getClassification,
  getDepartment,
  getType,
  getField,
  getCareer,
  getCustomer,
  getProductType,
  getUnit,
  getContact,
  getOpportunityPhase,
  getOpportunity,
  getIncome,
  getRole ,
  getBusinessType,
} = CommonAPI;
export default CommonAPI;
