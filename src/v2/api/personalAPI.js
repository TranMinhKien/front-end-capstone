import Axios from "./axios";
import {DOMAIN_BE} from '../../constant/constant';

const URL = DOMAIN_BE + "/api/profile";
const personalAPI = {
    editPersonalInfo: (params) => {
        return Axios.put(URL, params);
    },
    getPersonalInfo: () => {
        return Axios.get(URL);
    }
};

export const {editPersonalInfo, getPersonalInfo} = personalAPI;


