import Axios from "./axios";

const ReadAPI = {
  getContact: () => {
    const url = "/api/contact/";
    return Axios.get(url);
  },
  getCustomer: () => {
    const url = "/api/customer/";
    return Axios.get(url);
  },
  getProduct: () => {
    const url = "/api/product/";
    return Axios.get(url);
  },
  getPotentialAPI: () => {
    const url = "/api/potential/";
    return Axios.get(url);
  },
  getOrderName: () => {
    const url = "/api/order/name";
    return Axios.get(url);
  },
  getAllProvince: () => {
    const url = "/api/province/name";
    return Axios.get(url);
  },
  getDistrictByProvinceId: (id) => {
    const url = `api/district/search?provinceId=${id}`;
    return Axios.get(url);
  },
  getWardByDistrictId: (id) => {
    const url = `/api/ward/search?districtId=${id}`;
    return Axios.get(url);
  },
};
export const {
  getContact,
  getCustomer,
  getOrderName,
  getAllProvince,
  getPotentialAPI,
  getDistrictByProvinceId,
  getWardByDistrictId,
  getProduct,
} = ReadAPI;
export default ReadAPI;
