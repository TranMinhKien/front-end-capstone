import Axios from "./axios";

const ConvertAPI = {
  potentialSingleConvert: (id) => {
    const url = `/api/potential/single-convert/${id}`;
    return Axios.post(url);
  },
};
export const {
  potentialSingleConvert,
} = ConvertAPI;
export default ConvertAPI;
