import Axios from "./axios";

const LoginAPI = {
  login: (params) => {
    const url = "/api/auth/signin";
    return Axios.post(url, params);
  },
};
export const { login } = LoginAPI;
export default LoginAPI;
