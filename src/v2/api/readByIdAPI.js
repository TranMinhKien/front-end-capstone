import Axios from "./axios";

const GetByIdAPI = {
	getContactById: (id) => {
		const url = `/api/contact/${id}`;
		return Axios.get(url);
	},
	getCustomerById: (id) => {
		const url = `/api/customer/${id}`;
		return Axios.get(url);
	},
	getCustomerByName: (name) => {
		const url = `/api/customer/search?name=${name}`;
		return Axios.get(url);
	},
	getProductById: (id) => {
		const url = `/api/product/${id}`;
		return Axios.get(url);
	},
	getPotentialById: (id) => {
		const url = `/api/potential/${id}`;
		return Axios.get(url);
	},
	getInvoiceById: (id) => {
		const url = "/api/invoice/" + id;
		return Axios.get(url);
	},
	getProductTypeById: (id) => {
		const url = "/api/productType/" + id;
		return Axios.get(url);
	},
	getOpportunityById: (id) => {
		const url = "/api/opportunity/" + id;
		return Axios.get(url);
	},
	getOrderById: (id) => {
		const url = "/api/order/" + id;
		return Axios.get(url);
	},
	getUserById: (id) => {
		const url = "/api/user/" + id;
		return Axios.get(url);
	},
	getRoleById: (id) => {
		const url = "/api/role/" + id;
		return Axios.get(url);
	}, getContactByCustomerId: (id) => {
		const url = "/api/contact/search?customerId=" + id;
		return Axios.get(url);
	},
	getContactByOrderId: (id) => {
		const url = `/api/order/${id}/contact`;
		return Axios.get(url);
	},
	getCustomerByOrderId: (id) => {
		const url = `/api/order/${id}/customer`;
		return Axios.get(url);
	},
	getContactByOpportunityId: (id) => {
		const url = `/api/opportunity/${id}/contact`;
		return Axios.get(url);
	}
	, getCustomerByOpportunityId: (id) => {
		const url = `/api/opportunity/${id}/customer`;
		return Axios.get(url);
	},
};
export const {
	getContactById,
	getCustomerById,
	getProductById,
	getPotentialById,
	getInvoiceById,
	getRoleById,
	getUserById,
	getOrderById,
	getOpportunityById,
	getProductTypeById,
	getCustomerByName,
	getContactByCustomerId,
	getCustomerByOpportunityId,
	getContactByOrderId,
	getCustomerByOrderId,
	getContactByOpportunityId,
} = GetByIdAPI;
export default GetByIdAPI;
