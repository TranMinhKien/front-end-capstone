import Axios from "./axios";

const CreateAPI = {
    createPotential: (params) => {
        const url = `/api/potential`;
        return Axios.post(url, params);
    },
    createOrder: (params) => {
        const url = `/api/order`;
        return Axios.post(url, params);

    }
    , createInvoice: (params) => {
        const url = `/api/invoice`;
        return Axios.post(url, params);

    } ,
      createUser : (params) => {
          const url = `/api/user` ; 
          return Axios.post(url , params);
      }
      ,
      createRole : (params) => {
          const url = `/api/role` ; 
          return Axios.post(url , params) ; 
      }
    , createOpportunity: (params) => {
        const url = `/api/opportunity`;
        return Axios.post(url, params);
    }
    , createProduct: (params) => {
        const url = `/api/product`;
        return Axios.post(url, params);
    }
    , createProductType: (params) => {
        const url = `/api/productType`;
        return Axios.post(url, params);
    }
    , createCustomer: (params) => {
        const url = `/api/customer`;
        return Axios.post(url, params);
    } , createContact: (params) => {
        const url = `/api/contact`;
        return Axios.post(url, params);
    }
};
export const {
    createPotential, createRole , createOrder, createUser ,  createInvoice,createCustomer, createOpportunity, createProduct, createProductType, createContact
} = CreateAPI;
export default CreateAPI;
