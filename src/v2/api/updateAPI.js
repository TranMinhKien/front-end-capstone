import Axios from "./axios";

const UpdateAPI = {
  updateContact: (params, id) => {
    const url = `/api/contact/${id}`;
    return Axios.put(url, params);
  },
  updateCustomer: (params,id) => {
    const url = `/api/customer/${id}`;
    return Axios.put(url, params);
  },
  updatePotential: (params, id) => {
    const url = `/api/potential/${id}`;
    return Axios.put(url, params);
  },
  updateProduct: (params, id) => {
    const url = `/api/product/${id}`;
    return Axios.put(url, params);
  },
  updateProductType: (params, id) => {
    const url = `/api/productType/${id}`;
    return Axios.put(url, params);
  },
  updateOpportunity: (params, id) => {
    const url = `/api/opportunity/${id}`;
    return Axios.put(url, params);
  },
  updateOrder: (params, id) => {
    const url = `/api/order/${id}`;
    return Axios.put(url, params);
  },
  updateInvoice: (params, id) => {
    const url = `/api/invoice/${id}`;
    return Axios.put(url, params);
  },
  updateUser: (params, id) => {
    const url = `/api/user/${id}`;
    return Axios.put(url, params);
  },
  updateRole: (params, id) => {
    const url = `/api/role/${id}`;
    return Axios.put(url, params);
  },
};
export const {
  updateContact,
  updateCustomer,
  updatePotential,
  updateProduct,
  updateProductType,
  updateRole,
  updateUser,
  updateInvoice,
  updateOrder,
  updateOpportunity,
} = UpdateAPI;
export default UpdateAPI;
