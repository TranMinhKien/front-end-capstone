// handle format here
import moment from 'moment';
import {DATE_FORMAT} from '../config/date/dateFormat';

export function setDateToString(data) {

    for(const  [key,value] of Object.entries(data)){
        if(value instanceof Date){
            data[key] = moment(value).format( DATE_FORMAT)
        }
    }
}
