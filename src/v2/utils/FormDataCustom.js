export default class FormDataCustom {
    #formAntd;
    #rHF;

    constructor(formAntd, rHF) {
        this.#formAntd = formAntd;
        this.#rHF = rHF;
    }

    setFormValue(name, value) {
        this.#formAntd.setFieldsValue({[name]: value});
        this.#rHF.setValue(name, value);
    }

    setFormMany(values) {
        values.forEach((item) => {
            const name = item.name;
            const value = item.value;
            this.#formAntd.setFieldsValue({[name]: value});
            this.#rHF.setValue(name, value);
        });
    }
}