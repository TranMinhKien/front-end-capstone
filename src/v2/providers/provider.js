import React, { createContext, useState } from "react";

export const ProductContext = createContext();

const ProductProvider = ({ children }) => {
  const [products, setProducts] = useState([]);

  //FUNCTION EDIT PRODUCT
  const setContext = (data) => {
    setProducts(data);
  };

  //contex data
  const productContexData = { products,setContext };

  return (
    <ProductContext.Provider value={productContexData}>
      {children}
    </ProductContext.Provider>
  );
};

export default ProductProvider;
