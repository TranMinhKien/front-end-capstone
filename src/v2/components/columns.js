const Columns = {
    convertColumn: (columnData) => {
        console.log(columnData);
        let convertData = columnData.map((item, index) => {
            return {
                ...item,
                title: item.title,
                dataIndex: item.dataIndex,
                key: index,
                width: item.width ? item.width : 150,
                ellipsis: item.ellipsis ? item.ellipsis : true,
                fixed: item.fixed ? item.fixed : false,
                sorter: (a, b) => {
                    if (typeof a[item.dataIndex] === "string") {
                        return a[item.dataIndex].localeCompare(b[item.dataIndex])
                    } else {
                        console.log(typeof a[item.dataIndex])
                        return a[item.dataIndex] - b[item.dataIndex]
                    }
                }
            };
        });
        return convertData;
    },
    convertListname: (list) => {
        if (!list) return null;
        let convertName = list.map((item) => item.name);
        return convertName.join(", ");
    },
    convertMilion: (num) => {
        if (!num) return null;
        let result = Number(num).toLocaleString(undefined, {
            minimumFractionDigits: 0,
            maximumFractionDigits: 2,
        });
        return result;
    },
    convertToSingleArray: (array) => {
        if (!array) return null;
        let result = [];
        for (let o of array) {
            result.push(o.id)
        }
        return result;
    }
};
export const {convertColumn, convertListname, convertMilion, convertToSingleArray} = Columns;
export default Columns;
