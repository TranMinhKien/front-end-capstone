import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {useController} from "react-hook-form";
import {Form, Input, InputNumber, Select} from 'antd';
import './custom-field.css';
import SelectField from './SelectField';

const {Option} = Select;
InputPhoneField.propTypes = {
    selectName: PropTypes.string.isRequired,
    inputName: PropTypes.string.isRequired,

    placeholder: PropTypes.string,
    control: PropTypes.any.isRequired,
    isRequired: PropTypes.bool,
    option: PropTypes.array,
    defaultValue: PropTypes.string,
    disabled: PropTypes.bool,
    label: PropTypes.string,

};
InputPhoneField.defaultProps = {
    placeholder: '',
    isRequired: false,
    disabled: false,
    option: [{value: '+84', label: '+84'}]

};

function InputPhoneField(props) {
    const {placeholder, inputName, control, label, isRequired, disabled, option, selectName} = props;
    const {
        field: {ref, onChange, ...inputProps},
        fieldState: {invalid, isTouched, isDirty, error},
        formState: {touchedFields, dirtyFields}
    } = useController({
        name: inputName,
        control
    });

    const handleChangePhone = (e) => {
        const value = e.target.value;
        if (value.startsWith('0')) {
            console.log('value', e.target.value);
            e.target.value = value.replace(/^0/g, '');
            ;
        }

        onChange(e);
    };
    return (
        <Form.Item
            label={label}
            name={inputName}
            required={isRequired}
            help={invalid && error?.message}
            validateStatus={invalid ? "error" : 'validating'}
            labelCol={{span: 24, offset: 0}}
        >
            <Input.Group compact>
                <div className={'d-flex no-wrap '}>
                    <div className={'w-25'}>

                        <SelectField control={control} name={selectName} disabled={disabled} option={option} placeholder={'+84'}
                        allowClear={false}/>
                    </div>
                    <Input className={'w-75 input-phone'} {...inputProps} ref={ref} placeholder={placeholder}
                           type={'number'}
                           disabled={disabled} style={{borderLeft: 'none'}} onChange={handleChangePhone}/>

                </div>

            </Input.Group>
        </Form.Item>

    );
}

export default InputPhoneField;