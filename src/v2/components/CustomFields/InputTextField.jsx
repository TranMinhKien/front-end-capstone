import React from 'react';
import PropTypes from 'prop-types';
import {useController} from "react-hook-form";
import {Form, Input} from 'antd';
import './custom-field.css';

InputTextField.propTypes = {
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    control: PropTypes.any,
    isRequired: PropTypes.bool,
    prefixIcon: PropTypes.object,
    suffixIcon: PropTypes.object,
    defaultValue: PropTypes.string,
    disabled: PropTypes.bool,
    label: PropTypes.string
};
InputTextField.defaultProps = {
    placeholder: '',
    isRequired: false,
    disabled: false
};

function InputTextField(props) {
    const {placeholder, name, prefixIcon, suffixIcon, control, label, isRequired, disabled} = props;
    const {
        field: {...inputProps},
        fieldState: {invalid, isTouched, isDirty, error},
        formState: {touchedFields, dirtyFields}
    } = useController({
        name,
        control
    });


    return (
        <Form.Item
            label={label}
            name={name}
            required={isRequired}
            help={invalid && error?.message}
            validateStatus={invalid ? "error" : 'validating'}
            labelCol={{span: 24, offset: 0}}
        >
            <Input  {...inputProps} prefix={prefixIcon} suffix={suffixIcon} placeholder={placeholder}
                    disabled={disabled}
            />
        </Form.Item>

    );
}

export default InputTextField;