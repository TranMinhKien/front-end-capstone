import React from 'react';
import PropTypes from 'prop-types';
import {useController} from 'react-hook-form';
import {Form, Select} from 'antd';
import './custom-field.css';

const {Option} = Select;

SelectField.propTypes = {
    name: PropTypes.string.isRequired,
    control: PropTypes.any,
    option: PropTypes.array,
    isRequired: PropTypes.bool,
    isMultiple: PropTypes.bool,
    label: PropTypes.string,
    placeholder: PropTypes.string,
    disabled: PropTypes.bool,
    allowClear: PropTypes.bool,
    onChangeSelect: PropTypes.func,
    onClearSelect: PropTypes.func
};
SelectField.defaultProps = {
    option: [],
    isRequired: false,
    isMultiple: false,
    label: '',
    disabled: false,
    placeholder: "----Chọn----",
    allowClear: true

};

function SelectField(props) {
    const {
        name,
        control,
        option,
        allowClear,
        isMultiple,
        label,
        isRequired,
        placeholder,
        disabled,
        onChangeSelect,
        onClearSelect
    } = props;
    const {
        field: {onChange, ref, ...inputProps},
        fieldState: {invalid, isTouched, isDirty, error},
        formState: {touchedFields, dirtyFields}
    } = useController({
        name,
        control
    });
    const handleOnchange = (e) => {
        onChange(e);
        if (onChangeSelect) {
            onChangeSelect(e);
        }
    };
    const handleOnClear = (e) => {
        onChange(e);
        if (onClearSelect) {
            onClearSelect(e);
        }
    };
    return (
        <Form.Item
            label={label}
            name={name}
            required={isRequired}
            help={invalid && error?.message}
            validateStatus={invalid ? "error" : 'validating'}
            labelCol={{span: 24, offset: 0}}
        >
            <Select
                mode={isMultiple ? "multiple" : "single"}
                showSearch
                allowClear={!isMultiple && allowClear}
                style={{width: '100%'}}
                placeholder={placeholder}
                optionFilterProp="children"
                disabled={disabled}
                onChange={handleOnchange}
                // onFocus={onFocus}
                // onBlur={onBlur}
                // onSearch={onSearch}
                onClear={handleOnClear}
                filterOption={(input, option) =>
                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
                ref={ref}
                {...inputProps}
            >
                {option?.length && option?.map((item, index) =>
                    <Option key={name + index} value={item.value}>{item.label}</Option>
                )}
            </Select>
        </Form.Item>
    );
}

export default SelectField;