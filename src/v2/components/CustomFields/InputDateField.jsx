import React from 'react';
import PropTypes from 'prop-types';
import {useController} from "react-hook-form";
import {DatePicker, Form, Input} from 'antd';
import  './custom-field.css'
InputDateField.propTypes = {
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    control: PropTypes.any.isRequired,
    isRequired: PropTypes.bool,
    prefixIcon: PropTypes.object,
    suffixIcon: PropTypes.object,
    defaultValue: PropTypes.string,
    disabled : PropTypes.bool,
    label: PropTypes.string
};
InputDateField.defaultProps = {
    placeholder: 'Chọn ngày',
    isRequired: false,
    disabled: false
};

function InputDateField(props) {
    const {placeholder, name,  control, label, isRequired, disabled} = props;
    const {
        field: {ref, ...inputProps},
        fieldState: {invalid, isTouched, isDirty, error},
        formState: {touchedFields, dirtyFields}
    } = useController({
        name,
        control,

    });


    return (
        <Form.Item
            label={label}
            name={name}
            required={isRequired}
            help={invalid && error?.message}
            validateStatus={invalid ? "error" : 'validating'}
            labelCol={{span: 24, offset: 0}}

        >
            <DatePicker   {...inputProps} ref={ref}
                    disabled={disabled} placeholder={placeholder}
            />
        </Form.Item>

    );
}

export default InputDateField;