import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {useController} from 'react-hook-form';
import {Checkbox} from 'antd';

CheckBoxField.propTypes = {
    defaultChecked: PropTypes.bool,
    control: PropTypes.any.isRequired,
    name: PropTypes.string.isRequired,
    disabled: PropTypes.bool,
    onCheckboxChange: PropTypes.func,
    label: PropTypes.string.isRequired
};
CheckBoxField.defaultProps = {
    defaultChecked: false,
    disabled: false,
    onCheckboxChange: null
};

function CheckBoxField(props) {
    const {onCheckboxChange, control, name, disabled, defaultChecked, label} = props;
    const {
        field: {onChange, ...inputProps},
        fieldState: {invalid, isTouched, isDirty, error},
        formState: {touchedFields, dirtyFields}
    } = useController({
        name,
        control
    });
    const [check, setCheck] = useState(defaultChecked);
    const handleOnchange = (e) => {
        setCheck(e.target.checked);
        onChange(e);
        if (onCheckboxChange) {
            onCheckboxChange();
        }
    };
    return (
        <Checkbox
            {...inputProps}
            disabled={disabled}
            checked={check}
            onChange={handleOnchange}
        >
            {label}
        </Checkbox>
    );
}

export default CheckBoxField;