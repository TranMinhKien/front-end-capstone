import React from 'react';
import PropTypes from 'prop-types';
import {useController} from "react-hook-form";
import {Form, Input, InputNumber} from 'antd';

InputNumberField.propTypes = {
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    control: PropTypes.any,
    isRequired: PropTypes.bool,
    prefixIcon: PropTypes.object,
    suffixIcon: PropTypes.object,
    defaultValue: PropTypes.string,
    disabled: PropTypes.bool,
    label: PropTypes.string,
    step: PropTypes.number

};
InputNumberField.defaultProps = {
    placeholder: '',
    isRequired: false,
    disabled: false,
    step: 1
};

function InputNumberField(props) {
    const {placeholder, name, step, control, label, isRequired, disabled} = props;
    const {
        field: {ref, ...inputProps},
        fieldState: {invalid, isTouched, isDirty, error},
        formState: {touchedFields, dirtyFields}
    } = useController({
        name,
        control
    });

    return (
        <Form.Item
            label={label}
            name={name}
            required={isRequired}
            help={invalid && error?.message}
            validateStatus={invalid ? "error" : 'validating'}
            labelCol={{span: 24, offset: 0}}
        >
            <InputNumber    {...inputProps} ref={ref} placeholder={placeholder} disabled={disabled} step={step}
                            style={{width: '100%'}}/>
        </Form.Item>

    );
}

export default InputNumberField;