import { CloseOutlined } from "@ant-design/icons";
import { Button, Form, Input, Popconfirm, Table, Typography } from "antd";
import React, { useContext, useEffect, useRef, useState } from "react";
import { getProduct } from "v2/api/readAPI";
import { ProductContext } from "v2/providers/provider";
import ProductModal from "./productModal";
const { Text } = Typography;
const EditableContext = React.createContext(null);

const EditableRow = ({ index, ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

const EditableCell = ({
  title,
  editable,
  children,
  dataIndex,
  record,
  handleSave,
  ...restProps
}) => {
  const [editing, setEditing] = useState(false);
  const inputRef = useRef(null);
  const form = useContext(EditableContext);
  useEffect(() => {
    if (editing) {
      inputRef.current.focus();
    }
  }, [editing]);

  const toggleEdit = () => {
    setEditing(!editing);
    form.setFieldsValue({
      [dataIndex]: record[dataIndex],
    });
  };

  const save = async () => {
    try {
      const values = await form.validateFields();
      toggleEdit();
      handleSave({ ...record, ...values });
    } catch (errInfo) {
      console.log("Save failed:", errInfo);
    }
  };

  let childNode = children;
  let typeH = dataIndex === "explanation" ? "text" : "number";

  if (editable) {
    childNode = editing ? (
      <Form.Item
        style={{
          margin: 0,
        }}
        name={dataIndex}
        rules={[
          {
            required: true,
            message: `${title} không được để trống.`,
          },
        ]}
      >
        <Input type={typeH} ref={inputRef} onPressEnter={save} onBlur={save} />
      </Form.Item>
    ) : (
      <div
        className="editable-cell-value-wrap"
        style={{
          paddingRight: 24,
        }}
        onClick={toggleEdit}
      >
        {children}
      </div>
    );
  }

  return <td {...restProps}>{childNode}</td>;
};

function EditableTable() {
  const { products: productInfos, setContext } = useContext(ProductContext);
  const [products, setProducts] = useState([]);
  useEffect(() => {
    const fetchProduct = async () => {
      try {
        const response = await getProduct();
        if (response.error || !response.data) {
          throw new Error();
        }
        setProducts(response.data);
      } catch (error) {
        console.log("fail to fetch product");
      }
    };
    fetchProduct();
  }, []);

  const columnsData = [
    {
      title: "Mã hàng hóa",
      dataIndex: "productCode",
    },
    {
      title: "Diễn giải",
      dataIndex: "explanation",
      editable: true,
    },
    {
      title: "Đơn vị tính",
      dataIndex: "unit",
    },
    {
      title: "Số lượng",
      dataIndex: "amount",
      editable: true,
    },
    {
      title: "Đơn giá",
      dataIndex: "price",
      editable: true,
    },
    {
      title: "Thành tiền",
      dataIndex: "totalPrice",
    },
    {
      title: "Tỉ lệ chiết khấu",
      dataIndex: "discount",
      editable: true,
    },
    {
      title: "Tiền chiết khấu",
      dataIndex: "discountMoney",
    },
    {
      title: "Thuế GTGT",
      dataIndex: "vat",
      editable: true,
    },
    {
      title: "Tiền thuế",
      dataIndex: "vatMoney",
    },
    {
      title: "Tổng tiền",
      dataIndex: "totalMoney",
    },
    {
      title: "Xóa",
      key: "operation",
      width: "150px",
      dataIndex: "action",
    },
  ];
  const handleDelete = (key) => {
    setContext(productInfos.filter((item, index) => index !== key));
  };

  const handleConvertAtt = (oldData) => {
    if (!oldData) return [];
    else {
      let newData = oldData.map((item, index) => {
        return {
          ...item,
          price: item.sellPrice,
          discount: 0,
          amount: 1,
          productId: item.id,
          id: null,
          productCode: item.code,
        };
      });
      return newData;
    }
  };
  const handleAdd = (data) => {
    if (data.length) {
      let cvv = handleConvertAtt(data);
      setContext(productInfos.concat(cvv));
    }
    setVisible(false);
  };
  const handleSave = (row) => {
    const newData = [...productInfos];
    const index = newData.findIndex((item) => row.id === item.id);
    const item = newData[index];
    newData.splice(index, 1, {
      ...item,
      explanation: row.explanation,
      vat: row.vat > 100 ? 100 : row.vat < 0 ? 0 : row.vat,
      amount: row.amount < 0 ? 0 : row.amount,
      price: row.price < 0 ? 0 : row.price,
      discount: row.discount < 0 ? 0 : row.discount,
      discount: row.discount > 100 ? 100 : row.discount < 0 ? 0 : row.discount,
    });
    setContext(newData);
  };
  const components = {
    body: {
      row: EditableRow,
      cell: EditableCell,
    },
  };
  const columns = columnsData.map((col) => {
    if (!col.editable) {
      return col;
    }

    return {
      ...col,
      onCell: (record) => ({
        record,
        editable: col.editable,
        dataIndex: col.dataIndex,
        title: col.title,
        handleSave: handleSave,
      }),
    };
  });
  const getDataTable = () => {
    if (!productInfos) return [];
    let cloneData = productInfos.map((item, index) => {
      return {
        ...item,
        key: index,
        unit: item?.unit?.name || item?.unit,
        // discountMoney: convertMilion(item.discountMoney),
        // price: convertMilion(item.price),

        //tổng tiền
        totalPrice: item.price * item.amount,

        //tiền chiết khấu = % chiết khấu nhân tổng tiền
        discountMoney: (item.discount * (item.price * item.amount)) / 100,

        //tiền thuế = (tổng tiền ) nhân % thuế
        vatMoney: ((item.price * item.amount) / 100) * item.vat,
        //tổng thanh toán = tổng tiền - chiết khấu + thuế
        totalMoney:
          item.price * item.amount -
          (item.discount * (item.price * item.amount)) / 100 +
          ((item.price * item.amount) / 100) * item.vat,

        action: (
          <div className="d-flex align-items-center p-0 justify-content-between">
            <Popconfirm
              title="Bạn chắc chắn muốn xóa?"
              onConfirm={() => handleDelete(index)}
            >
              <a>
                <CloseOutlined
                  className="btnIcon"
                  style={{ color: "#D2042D" }}
                />
              </a>
            </Popconfirm>
          </div>
        ),
      };
    });
    return cloneData;
  };

  const [visible, setVisible] = useState(false);
  return (
    <div className="table_common">
      <Button
        onClick={() => setVisible(true)}
        type="primary"
        style={{
          marginBottom: 16,
        }}
      >
        Thêm hàng hóa
      </Button>
      <Table
        components={components}
        rowClassName={() => "editable-row"}
        bordered
        dataSource={getDataTable()}
        columns={columns}
        scroll={{ x: "100%" }}
        summary={(pageData) => {
          let totalPrices = 0;
          let totalamount = 0;
          let totalTotalPrice = 0;
          let totalDiscounts = 0;
          let totalvatMoney = 0;
          let total = 0;

          pageData.forEach(
            ({
              amount,
              price,
              totalPrice,
              discountMoney,
              vatMoney,
              totalMoney,
            }) => {
              totalamount += Number(amount);
              totalPrices += Number(price);
              totalTotalPrice += Number(totalPrice);
              totalDiscounts += Number(discountMoney);
              totalvatMoney += Number(vatMoney);
              total += Number(totalMoney);
            }
          );

          return (
            <>
              <Table.Summary.Row>
                <Table.Summary.Cell>Total</Table.Summary.Cell>
                <Table.Summary.Cell></Table.Summary.Cell>
                <Table.Summary.Cell></Table.Summary.Cell>
                <Table.Summary.Cell>
                  <Text type="danger">{totalamount}</Text>
                </Table.Summary.Cell>
                <Table.Summary.Cell>
                  <Text type="danger">{totalPrices}</Text>
                </Table.Summary.Cell>
                <Table.Summary.Cell>
                  <Text type="danger">{totalTotalPrice}</Text>
                </Table.Summary.Cell>
                <Table.Summary.Cell></Table.Summary.Cell>
                <Table.Summary.Cell>
                  <Text type="danger">{totalDiscounts}</Text>
                </Table.Summary.Cell>
                <Table.Summary.Cell></Table.Summary.Cell>
                <Table.Summary.Cell>
                  <Text type="danger">{totalvatMoney}</Text>
                </Table.Summary.Cell>
                <Table.Summary.Cell>
                  <Text type="danger">{total}</Text>
                </Table.Summary.Cell>
                <Table.Summary.Cell></Table.Summary.Cell>
              </Table.Summary.Row>
            </>
          );
        }}
      />
      <ProductModal
        setVisible={setVisible}
        visible={visible}
        title={"Chọn hàng hóa"}
        handleAdd={handleAdd}
        data={products}
      />
    </div>
  );
}
export default EditableTable;
