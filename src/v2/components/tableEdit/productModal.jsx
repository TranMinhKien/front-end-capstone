import React, { useState } from "react";
import { Table, Modal, Radio, Divider } from "antd";

function ProductModal({ title, visible, setVisible,handleAdd , data }) {
  const columns = [
    {
        title: "Mã hàng hóa",
        dataIndex: "code",
      },
      {
        title: "Diễn giải",
        dataIndex: "explanation",
      },
      {
        title: "Loại hàng hóa",
        dataIndex: "productType",
      },
      {
        title: "Đơn vị tính",
        dataIndex: "unit",
      },
      {
        title: "Đơn giá",
        dataIndex: "sellPrice",
      },
  ];
  // rowSelection object indicates the need for row selection
  const [rowsSelected,setRowsSelected]=useState([]);
  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(
        "selectedRows: ",
        selectedRows
      );
      setRowsSelected(selectedRows)
    },
  };
  const getDataTable = () => {
    if (!data) return [];
    let cloneData = data.map((item, index) => {
      return {
        ...item,
        key: index,
      };
    });
    return cloneData;
  };
  return (
    <Modal
      title={title}
      visible={visible}
      onOk={() => handleAdd(rowsSelected)}
      onCancel={() => setVisible(false)}
      width={1000}
    >
      <Divider />

      <Table
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        columns={columns}
        dataSource={getDataTable()}
      />
    </Modal>
  );
}

export default ProductModal;
