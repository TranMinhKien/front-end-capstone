import {Table, Space, Input, Button, Modal} from "antd";
import React, {useEffect} from "react";
import "../assets/css/table.css";
import {
    EditOutlined,
    CloseOutlined,
    EyeOutlined,
    DownloadOutlined,
    PlusSquareOutlined,
    DeleteOutlined,
    SendOutlined
} from "@ant-design/icons";

import PropTypes from 'prop-types';
import {forEach} from "react-bootstrap/ElementChildren";

Tableb.propTypes = {
    hasConvert: PropTypes.bool
};
Tableb.defaultProps = {
    hasConvert: false
};

function Tableb(props) {
    const {columns, handleMultiDelete, data, onAddNew, handleConvert, hasConvert , titlechucnang} = props;
    const [isModalVisible, setIsModalVisible] = React.useState(false);
    const [selectedRowKeys, setSelectedRowKeys] = React.useState([]);
    const [contentModel, setContentModel] = React.useState('');

    const [titleFeature, setTitleFeature] = React.useState(
        titlechucnang
    );

    const [result, setResult] = React.useState(null);
    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        handleMultiDelete(selectedRowKeys);
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const {Search} = Input;


    const onSearch = (value) => {
        const filterTable = data.filter(o =>
            Object.keys(o).some(k =>
                String(o[k])
                    .toLowerCase()
                    .includes(value.toLowerCase())
            )
        );
        console.log(filterTable);
        setResult(filterTable);

    };

    const onMultiDelete = () => {
        // setContentModel(`Bạn có chắc muốn xoá ${selectedRowKeys.length} mục`);
        // showModal();
        if(handleMultiDelete)
            handleMultiDelete(selectedRowKeys);
    };
    const onTransferData = () => {
        if (handleConvert) {
            handleConvert(selectedRowKeys);
        }
    };
    const title = () => (
        <div className="d-flex align-items-center justify-content-between table_title ">
            <div className="d-flex align-items-center">
                <p className="mb-0 mx-2">
                    {" "}
                    {hasSelected
                        ? `Selected ${selectedRowKeys.length} items`
                        : titleFeature}
                </p>
                {/* <Button
                    type="primary"
                    className={!hasSelected ? "d-none" : "d-block"}
                    icon={<DeleteOutlined/>}
                    onClick={onMultiDelete}
                /> */}
                {/*tranfer dater*/}
                <Button
                    type="primary"
                    className={!hasConvert || selectedRowKeys.length !== 1 ? "d-none" : "d-block mx-2"}
                    icon={<SendOutlined/>}
                    onClick={onTransferData}
                />
            </div>
            <div>
                <Search
                    placeholder="Tìm kiếm"
                    allowClear="true"
                    onSearch={onSearch}
                    style={{width: 200}}
                />
                {/* export button */}
                {/*<Button type="primary" className="mx-2" icon={<DownloadOutlined/>}/>*/}
                {/* add button */}
                <Button type="primary" className={'ms-2'} onClick={onAddNew} icon={<PlusSquareOutlined/>}/>
            </div>
        </div>
    );

    const data1 = [];
    for (let i = 0; i < 100; i++) {
        data1.push({
            key: i,
            name: `Edrward ${i}`,
            age: 32,
            address: `London Park no. ${i}`
        });
    }
    const hasSelected = selectedRowKeys.length > 0;
    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            setSelectedRowKeys(selectedRows);
        },
        onSelect: (record, selected, selectedRows) => {
            setSelectedRowKeys(selectedRows);
        },
        onSelectAll: (selected, selectedRows, changeRows) => {
            setSelectedRowKeys(selectedRows);
        }
    };
    const row_class_common = () => "row_color";

    return (
        <div className="table_common">
            <Table
                columns={columns}
                dataSource={result == null ? data : result}
                rowClassName={row_class_common}
                rowSelection={{...rowSelection}}
                scroll={{x: '100%', y: '60vh'}}
                title={title}
            />
            <Modal
                title="Thông Báo Xoá"
                visible={isModalVisible}
                onOk={handleOk}
                onCancel={handleCancel}
            >
                <div>
                    {contentModel && hasSelected
                        ? contentModel
                        : "Bạn chắc muốn xoá thông tin này không?"}
                </div>
            </Modal>
        </div>
    );
};
export default Tableb;
