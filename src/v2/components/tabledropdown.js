import {Table, Badge, Menu, Dropdown, Space, Button, Input, Modal} from 'antd';
import {
    CloseOutlined,
    DeleteOutlined,
    DownloadOutlined,
    DownOutlined,
    EditOutlined,
    EyeOutlined, PlusSquareOutlined,
    SendOutlined
} from '@ant-design/icons';
import React from 'react';
import '../assets/css/table.css'

const Table2 = () => {
    const [selectedRowKeys, setSelectedRowKeys] = React.useState([]);
    const [selectedInnerRowKeys, setSelectedInnerRowKeys] = React.useState([]);
    const [contentModel, setContentModel] = React.useState('');
    const [isModalVisible, setIsModalVisible] = React.useState(false);
    const menu = (
        <Menu>
            <Menu.Item>Action 1</Menu.Item>
            <Menu.Item>Action 2</Menu.Item>
        </Menu>
    );


    const expandedRowRender = () => {
        const columns = [
            {title: 'Date', dataIndex: 'date', key: 'date'},
            {title: 'Name', dataIndex: 'name', key: 'name'},
            {
                title: 'Status',
                key: 'state',
                render: () => (
                    <span>
            <Badge status="success"/>
            Finished
          </span>
                ),
            },
            {title: 'Upgrade Status', dataIndex: 'upgradeNum', key: 'upgradeNum'},
            {
                title: 'Action',
                key: 'operation',
                fixed: 'right',
                width: '100px',
                render: () =>
                    <div className="d-flex align-items-center p-0 justify-content-between">
                        <EditOutlined className=" btnIcon" style={{color: '#C8DF52'}}/>
                        <CloseOutlined className="btnIcon" style={{color: '#D2042D'}}/>
                        <EyeOutlined className=" btnIcon" style={{color: '#B3CDE0'}}/>
                    </div>,
            },
        ];

        const data = [];
        for (let i = 0; i < 3; ++i) {
            data.push({
                key: i,
                date: '2014-12-24 23:12:00',
                name: 'This is production name',
                upgradeNum: 'Upgraded: 56',
            });
        }
        const rowclass = () => 'red';
        return <Table columns={columns}  rowClassName={rowclass} dataSource={data}
                      pagination={false}/>;
    };
    const columns = [
        {title: 'Name', dataIndex: 'name', key: 'name'},
        {title: 'Platform', dataIndex: 'platform', key: 'platform'},
        {title: 'Version', dataIndex: 'version', key: 'version'},
        {title: 'Upgraded', dataIndex: 'upgradeNum', key: 'upgradeNum'},
        {title: 'Creator', dataIndex: 'creator', key: 'creator'},
        {title: 'Date', dataIndex: 'createdAt', key: 'createdAt'},
        {title: 'Column 8', dataIndex: 'address', key: '8'},
        {
            title: 'Action',
            key: 'operation',
            fixed: 'right',
            width: '150px',
            render: () =>
                <div className="d-flex align-items-center p-0 justify-content-between">
                    <EditOutlined className=" btnIcon" style={{color: '#C8DF52'}}/>
                    <CloseOutlined className="btnIcon" style={{color: '#D2042D'}}/>
                    <EyeOutlined className=" btnIcon" style={{color: '#B3CDE0'}}/>
                </div>,
        },
    ];

    const data = [];
    for (let i = 0; i < 100; ++i) {
        data.push({
            key: i,
            name: 'Screem',
            platform: 'iOS',
            version: '10.3.4.5654',
            upgradeNum: 500,
            creator: 'Jack',
            createdAt: '2014-12-24 23:12:00',
        });
    };
    const {Search} = Input;
    const onSearch = (value) => console.log(value);
    const onMultiDelete = () => {
        setContentModel(`Bạn có chắc muốn xoá ${selectedRowKeys.length + selectedInnerRowKeys.length} mục`);
        showModal();
    };
    const onTransferData = () => {
        if (selectedRowKeys[0].name && selectedRowKeys[0].customer) {
            setContentModel('Bạn muốn thêm mới ' + selectedRowKeys[0].name + ' vào liên hệ và khách hàng')
        } else
            setContentModel(`Bạn muốn thêm mới liên hệ : ${selectedRowKeys[0].name}`)
        showModal();
    }
    const handleOkModal = () => {
        setIsModalVisible(false);
    };

    const handleCancelModal = () => {
        setIsModalVisible(false);
    };
    const showModal = () => {
        setIsModalVisible(true);
    };
    const hasSelected = selectedRowKeys.length > 0;
    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            setSelectedRowKeys(selectedRows);
        },
        onSelect: (record, selected, selectedRows) => {
            setSelectedRowKeys(selectedRows);
        },
        onSelectAll: (selected, selectedRows, changeRows) => {
            setSelectedRowKeys(selectedRows);
        },
    };
    const title = () => (
        <div className="d-flex align-items-center justify-content-between table_title ">
            <div className="d-flex align-items-center">
                <p className="mb-0 mx-2">
                    {" "}
                    {hasSelected
                        ? `Selected ${selectedRowKeys.length} items`
                        : "Loại hàng hoá"}
                </p>
                <Button
                    type="primary"
                    className={!hasSelected ? "d-none" : "d-block"}
                    icon={<DeleteOutlined/>}
                    onClick={onMultiDelete}
                />
                {/*tranfer dater*/}
                <Button
                    type="primary"
                    className={!hasSelected ? "d-none" : "d-block mx-2"}
                    icon={<SendOutlined/>}
                    onClick={onTransferData}
                />
            </div>
            <div>
                <Search
                    placeholder="input search text"
                    allowClear="true"
                    onSearch={onSearch}
                    style={{width: 200}}
                />
                {/* export button */}
                <Button type="primary" className="mx-2" icon={<DownloadOutlined/>}/>
                {/* add button */}
                <Button type="primary" className="" icon={<PlusSquareOutlined/>}/>
            </div>
        </div>
    );
    return (
        <div>
            <Table
                className="components-table-demo-nested"
                columns={columns}
                rowSelection={rowSelection}
                expandable={{expandedRowRender}}
                dataSource={data}
                scroll={{x: 1500, y: '65vh'}}
                title={title}
            />
            <Modal
                title="Thông Báo Xoá"
                visible={isModalVisible}
                onOk={handleOkModal}
                onCancel={handleCancelModal}
            >
                <div>
                    {contentModel && hasSelected
                        ? contentModel
                        : "Bạn chắc muốn xoá thông tin này không?"}
                </div>
            </Modal>
        </div>
    );

}
export default Table2
