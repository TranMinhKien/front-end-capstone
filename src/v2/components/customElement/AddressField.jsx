import React, {useEffect, useLayoutEffect, useState} from 'react';
import PropTypes from 'prop-types';
import InputTextField from '../CustomFields/InputTextField';
import SelectField from '../CustomFields/SelectField';
import {getAllProvince, getDistrictByProvinceId, getWardByDistrictId} from '../../api/readAPI';
import {useWatch} from 'react-hook-form';

AddressField.propTypes = {
    control: PropTypes.any.isRequired,
    customForm: PropTypes.any.isRequired,
    provinceName: PropTypes.string,
    districtName: PropTypes.string,
    wardName: PropTypes.string,
    addressName: PropTypes.string,
    isRequired: PropTypes.bool
};
AddressField.defaultProps = {
    provinceName: 'provinceId',
    districtName: 'districtId',
    wardName: 'wardId',
    addressName: 'address'
};


function AddressField(props) {
    const {control, provinceName, districtName, wardName, addressName, isRequired, customForm} = props;
    const watchProvince = useWatch({
        control,
        name: provinceName // without supply name will watch the entire form, or ['firstName', 'lastName'] to watch both
    });
    const watchDistrict = useWatch({
        control,
        name: districtName // without supply name will watch the entire form, or ['firstName', 'lastName'] to watch both
    });

    const [isDisableDistrict, setIsDisableDistrict] = useState();
    const [isDisableWard, setIsDisableWard] = useState();

    const [provinceOption, setProvinceOption] = useState([{label: '', value: ''}]);
    const fetchProvinceData = async () => {
        const response = await getAllProvince();
        const formatProvinceOption = response.data?.map((item) => ({label: item.name, value: item.id}));
        setProvinceOption(formatProvinceOption);
    };
    const fetchDistrictByProvinceId = async (id) => {
        const response = await getDistrictByProvinceId(id);
        const format = response.data?.map((item) => ({label: item.name, value: item.id}));
        setDistrictOption(format);
    };
    const fetchWardByDistrictId = async (id) => {
        const response = await getWardByDistrictId(id);
        const format = response.data?.map((item) => ({label: item.name, value: item.id}));
        setWardOption(format);
    };
    useEffect(() => {

        fetchProvinceData();
        // if (watchProvince) fetchDistrictByProvinceId(watchProvince);
        // if (watchDistrict) fetchWardByDistrictId(watchDistrict);

    }, []);
    //----province
    const [districtOption, setDistrictOption] = useState();

    const handleSelectProvince = async (e) => {
        // e : selected value
        if (typeof e === "number") {
            // await fetchDistrictByProvinceId(e);
            customForm.setFormValue(districtName, undefined);
            customForm.setFormValue(wardName, undefined);
            setIsDisableDistrict(false);
        }
    };


    const handleClearProvince = () => {
        // e : selected value
        // reset other field
        customForm.setFormValue(districtName, undefined);
        customForm.setFormValue(wardName, undefined);
        // disable other field
        setIsDisableDistrict(true);
        setIsDisableWard(true);
    };
    //---district
    const [wardOption, setWardOption] = useState();

    const handleSelectDistrict = async (e) => {
        // e : selected value
        if (typeof e === "number") {
            // await fetchWardByDistrictId(e);
            customForm.setFormValue(wardName, undefined);
        }

    };

    const handleClearDistrict = () => {
        // e : selected value
        customForm.setFormValue(wardName, undefined);
        setIsDisableWard(true);
    };
    useLayoutEffect(() => {
        if (typeof watchProvince !== 'number') {
            handleClearProvince();
        }
        if (typeof watchDistrict !== 'number') {
            handleClearDistrict();
        }

        if (typeof watchProvince === 'number') {
            setIsDisableDistrict(false);
            fetchDistrictByProvinceId(watchProvince);
        }
        if (typeof watchDistrict === 'number') {
            setIsDisableWard(false);
            fetchWardByDistrictId(watchDistrict);
        }

    }, [watchProvince, watchDistrict]);
    return (
        <>
            <tr>
                <td><SelectField name={provinceName} control={control} label={"Tỉnh/Thành phố"}
                                 isRequired={isRequired} option={provinceOption} onChangeSelect={handleSelectProvince}
                                 onClearSelect={handleClearProvince}
                />
                </td>
                <td><SelectField name={districtName} control={control} label={"Quận/Huyện"} isRequired={isRequired}
                                 option={districtOption} disabled={isDisableDistrict}
                                 onClearSelect={handleClearDistrict} onChangeSelect={handleSelectDistrict}/>
                </td>
            </tr>
            <tr>
                <td><SelectField name={wardName} control={control} label={"Phường/Xã"} isRequired={isRequired}
                                 option={wardOption}
                                 disabled={isDisableWard}/></td>
                <td><InputTextField name={addressName} control={control} label={'Khu/Số nhà'} isRequired={isRequired}/>
                </td>
            </tr>
        </>
    );
}

export default AddressField;