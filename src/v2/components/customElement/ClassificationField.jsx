import React, { useEffect, useState } from "react";
import { getClassification } from "v2/api/commonAPI";
import SelectField from "../CustomFields/SelectField";

function ClassificationField(props) {
  const { control, name,label } = props;
  const [classification, setClassification] = useState();
  const getDataClassification = async () => {
    try {
      const response = await getClassification();
      if (response.error) {
        throw new Error(response.error);
      }
      setClassification(
        response.data.map((item) => {
          return {
            value: item?.id,
            label: item?.name,
          };
        })
      );
    } catch (error) {
      console.log("fail to fetch classification");
    }
  };
  useEffect(() => {
    getDataClassification();
  }, []);
  return (
    <SelectField
      label={label}
      control={control}
      name={name}
      option={classification}
      isMultiple
    />
  );
}

export default ClassificationField;
