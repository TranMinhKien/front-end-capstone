import React, { useEffect, useState } from "react";
import { getType } from "v2/api/commonAPI";
import SelectField from "../CustomFields/SelectField";

function TypeField(props) {
  const { control, name } = props;
  const [type, setType] = useState();
  const getDataType = async () => {
    try {
      const response = await getType();
      if (response.error) {
        throw new Error(response.error);
      }
      setType(
        response.data.map((item) => {
          return {
            value: item?.id,
            label: item?.name,
          };
        })
      );
    } catch (error) {
      console.log("fail to fetch type");
    }
  };
  useEffect(() => {
    getDataType();
  }, []);
  return (
    <SelectField
      {...props}
      label={"Loại hình"}
      control={control}
      name={name}
      option={type}
    />
  );
}

export default TypeField;
