import React, { useEffect, useState } from "react";
import SelectField from "../CustomFields/SelectField";
import { getOrderName } from "../../api/readAPI";
import { getContactByOrderId, getCustomerByOrderId } from "../../api/readByIdAPI";
import PropTypes from "prop-types";

OrderField.prototype = {
	customerName: PropTypes.string,
	contactName: PropTypes.string,
};
OrderField.defaultProps = {
	customerName: "customerId",
	contactName: "buyerId",
};

function OrderField(props) {
	const { control, name, label, customForm, customerName, contactName, ...otherProps } = props;
	const [contact, setContact] = useState();
	const getDataOrder = async () => {
		try {
			const response = await getOrderName();
			if (response.error) {
				throw new Error(response.error);
			}
			setContact(
				response.data.map((item) => {
					return {
						value: item?.id,
						label: item?.name,
					};
				}),
			);
		} catch (error) {
			console.log("fail to fetch contact");
		}
	};
	useEffect(() => {
		getDataOrder();
	}, []);

	async function handleSelectCustomer(id) {
		try {
			const response = await getCustomerByOrderId(id);
			customForm.setFormValue(customerName, response.data.id);
		} catch (e) {

		}
	}

	async function handleSelectContact(id) {
		try {
			const response = await getContactByOrderId(id);
			customForm.setFormValue(contactName, response.data.id);
		} catch (e) {

		}
	}

	async function handleChangeOrder(id) {
		if (id) {
			await handleSelectCustomer(id);
			await handleSelectContact(id);
		}

	}

	return (
		<SelectField
			label={label}
			control={control}
			name={name}
			option={contact}
			onChangeSelect={handleChangeOrder}
			{...otherProps}
		/>
	);
}

export default OrderField;
