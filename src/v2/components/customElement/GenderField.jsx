import React, { useEffect, useState } from "react";
import { getGender } from "v2/api/commonAPI";
import SelectField from "../CustomFields/SelectField";
import PropTypes from 'prop-types';
GenderField.propTypes = {
  name: PropTypes.string.isRequired,
  control: PropTypes.any,
  option: PropTypes.array,
  isRequired: PropTypes.bool,
  isMultiple: PropTypes.bool,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,

};
function GenderField(props) {
  const { control, name, ...nestedProps } = props;
  const [gender, setGender] = useState();
  const getDatagender = async () => {
    try {
      const response = await getGender();
      if (response.error) {
        throw new Error(response.error);
      }
      setGender(
        response.data.map((item) => {
          return {
            value: item?.id,
            label: item?.name,
          };
        })
      );
    } catch (error) {
      console.log("fail to fetch gender");
    }
  };
  useEffect(() => {
    getDatagender();
  }, []);
  return (
    <SelectField
      label={"Giới tính"}
      control={control}
      name={name}
      option={gender}
      {...nestedProps}
    />
  );
}

export default GenderField;
