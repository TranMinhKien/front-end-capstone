import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import SelectField from "../CustomFields/SelectField";
import { getPosition } from "v2/api/commonAPI";

PositionField.propTypes = {};

function PositionField(props) {
  const { control, name } = props;
  const [position, setPosition] = useState();
  const getDataPosition = async () => {
    try {
      const response = await getPosition();
      if (response.error) {
        throw new Error(response.error);
      }
      setPosition(
        response.data.map((item) => {
          return {
            value: item?.id,
            label: item?.name,
          };
        })
      );
    } catch (error) {
      console.log("fail to fetch position");
    }
  };
  useEffect(() => {
    getDataPosition();
  }, []);
  return (
    <SelectField
      {...props}
      label={"Chức danh"}
      control={control}
      name={name}
      option={position}
    />
  );
}

export default PositionField;
