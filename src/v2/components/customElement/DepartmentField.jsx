import React, { useEffect, useState } from "react";
import { getDepartment } from "v2/api/commonAPI";
import SelectField from "../CustomFields/SelectField";

function DepartmentField(props) {
  const { control, name } = props;
  const [department, setDepartment] = useState();
  const getDatadepartment = async () => {
    try {
      const response = await getDepartment();
      if (response.error) {
        throw new Error(response.error);
      }
      setDepartment(
        response.data.map((item) => {
          return {
            value: item?.id,
            label: item?.name,
          };
        })
      );
    } catch (error) {
      console.log("fail to fetch department");
    }
  };
  useEffect(() => {
    getDatadepartment();
  }, []);
  return (
    <SelectField
      label={"Phòng ban"}
      control={control}
      name={name}
      option={department}
    />
  );
}

export default DepartmentField;
