import React, { useEffect, useState } from "react";
import { getCustomer } from "v2/api/commonAPI";
import SelectField from "../CustomFields/SelectField";
import PropTypes from 'prop-types';
import {useWatch} from 'react-hook-form';
import {getCustomerByOpportunityId} from '../../api/readByIdAPI';

CustomerField.propTypes= {
  onChange: PropTypes.func,
  onClear: PropTypes.func,
  isLoadByOpportunity: PropTypes.bool
}
CustomerField.defaultProps= {
  isLoadByOpportunity: false
}
function CustomerField(props) {
  const { control, name,label,onChange, onClear, isLoadByOpportunity,customForm, ...properties  } = props;
  const watchOpportunity = useWatch({
    name: 'opportunity',
    control
  })
  const [customer, setCustomer] = useState();
  const getDataCustomer = async () => {
    try {
      let response ;
      if(watchOpportunity && isLoadByOpportunity){
        response = getCustomerByOpportunityId(watchOpportunity);
      }else{
      response = await getCustomer();
      }
      setCustomer(
          response.data.map((item) => {
            return {
              value: item?.id,
              label: item?.name,
            };
          })
      );
    } catch (error) {
      console.log("fail to fetch customer");
    }
  };
  useEffect(() => {
    getDataCustomer();
  }, [watchOpportunity]);

  function handleChangeCustomer(selectedValue) {

    if(onChange){
      onChange(selectedValue)
    }
  }
  return (
    <SelectField
      label={label}
      control={control}
      name={name}
      option={customer}
      onChangeSelect={handleChangeCustomer}
      onClearSelect={onClear}
      {...properties}
    />
  );
}

export default CustomerField;
