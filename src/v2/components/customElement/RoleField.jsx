import React, { useEffect, useState } from "react";
import { getClassification, getRole } from "v2/api/commonAPI";
import SelectField from "../CustomFields/SelectField";

function RoleField(props) {
  const { control, name,label } = props;

  const [Role, setRole] = useState();

  const getDataRole = async () => {
    try {
      const response = await getRole();
      if (response.error) {
        throw new Error(response.error);
      }
      setRole(
        response.data.map((item) => {
          return {
            value: item?.id,
            label: item?.name,
          };
        })
      );
    } catch (error) {
      console.log("Lỗi kết nối API với Role");
    }
  };
  useEffect(() => {
    getDataRole();
  }, []);
  return (
    <SelectField
      label={label}
      control={control}
      name={name}
      option={Role}
      isMultiple
    />
  );
}

export default RoleField;
