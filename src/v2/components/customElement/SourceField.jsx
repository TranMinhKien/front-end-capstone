import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import SelectField from "../CustomFields/SelectField";
import { getSource } from "v2/api/commonAPI";

SourceField.propTypes = {};

function SourceField(props) {
  const { control, name, ...nestedProps } = props;
  const [source, setSource] = useState();
  const getDataSource = async () => {
    try {
      console.log("object");
      const response = await getSource();
      if (response.error) {
        throw new Error(response.error);
      }
      setSource(
        response.data.map((item) => {
          return {
            value: item?.id,
            label: item?.name,
          };
        })
      );
    } catch (error) {
      console.log("fail to fetch source");
    }
  };
  useEffect(() => {
    getDataSource();
  }, []);
  return (
    <SelectField
      label={"Nguồn gốc"}
      control={control}
      name={name}
      option={source}
      {...nestedProps}
    />
  );
}

export default SourceField;
