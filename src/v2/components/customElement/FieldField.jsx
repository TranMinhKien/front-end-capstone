import React, { useEffect, useState } from "react";
import { getField } from "v2/api/commonAPI";
import SelectField from "../CustomFields/SelectField";

function FieldField(props) {
  const { control, name } = props;
  const [field, setField] = useState();
  const getDataField = async () => {
    try {
      const response = await getField();
      if (response.error) {
        throw new Error(response.error);
      }
      setField(
        response.data.map((item) => {
          return {
            value: item?.id,
            label: item?.name,
          };
        })
      );
    } catch (error) {
      console.log("fail to fetch field");
    }
  };
  useEffect(() => {
    getDataField();
  }, []);
  return (
    <SelectField
      label={"Lĩnh vực"}
      control={control}
      name={name}
      option={field}
      isMultiple
    />
  );
}

export default FieldField;
