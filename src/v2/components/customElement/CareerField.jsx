import React, { useEffect, useState } from "react";
import { getCareer } from "v2/api/commonAPI";
import SelectField from "../CustomFields/SelectField";

function CareerField(props) {
  const { control, name,label } = props;
  const [career, setCareer] = useState();
  const getDataCareer = async () => {
    try {
      const response = await getCareer();
      if (response.error) {
        throw new Error(response.error);
      }
      setCareer(
        response.data.map((item) => {
          return {
            value: item?.id,
            label: item?.name,
          };
        })
      );
    } catch (error) {
      console.log("fail to fetch career");
    }
  };
  useEffect(() => {
    getDataCareer();
  }, []);
  return (
    <SelectField
      label={label}
      control={control}
      name={name}
      option={career}
      isMultiple
    />
  );
}

export default CareerField;
