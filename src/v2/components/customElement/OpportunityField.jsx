import React, { useEffect, useState } from "react";
import { getOpportunity } from "v2/api/commonAPI";
import SelectField from "../CustomFields/SelectField";
import { getContactByOpportunityId, getCustomerByOpportunityId } from "../../api/readByIdAPI";
import PropTypes from "prop-types";

OpportunityField.prototype = {
	customerName: PropTypes.string,
	contactName: PropTypes.string,
};
OpportunityField.defaultProps = {
	customerName: "customerId",
	contactName: "contactId",
};

function OpportunityField(props) {
	const { control, name, label, customForm, customerName, contactName } = props;
	const [opportunity, setOpportunity] = useState();
	const getDataopportunity = async () => {
		try {
			const response = await getOpportunity();
			if (response.error) {
				throw new Error(response.error);
			}
			setOpportunity(
				response.data.map((item) => {
					return {
						value: item?.id,
						label: item?.name,
					};
				}),
			);
		} catch (error) {
			console.log("fail to fetch opportunity");
		}
	};
	useEffect(() => {
		getDataopportunity();
	}, []);

	function onClear() {
		customForm.setFormValue("opportunityId", undefined);
	}

	async function handleSelectCustomer(id) {
		try {
			const response = await getCustomerByOpportunityId(id);
			customForm.setFormValue(customerName, response.data.id);
		} catch (e) {

		}
	}

	async function handleSelectContact(id) {
		try {
			const response = await getContactByOpportunityId(id);
			customForm.setFormValue(contactName, response.data.id);
		} catch (e) {

		}
	}

	async function handleChangeOrder(id) {
		if (id) {
			await handleSelectCustomer(id);
			await handleSelectContact(id);
		}

	}

	return (
		<SelectField
			{...props}
			label={label}
			control={control}
			name={name}
			onChangeSelect={handleChangeOrder}
			option={opportunity}
			onClearSelect={onClear}
		/>
	);
}

export default OpportunityField;
