import React, { useEffect, useState } from "react";
import { getIncome } from "v2/api/commonAPI";
import SelectField from "../CustomFields/SelectField";

function IncomeField(props) {
  const { control, name } = props;
  const [type, setType] = useState();
  const getDataType = async () => {
    try {
      const response = await getIncome();
      if (response.error) {
        throw new Error(response.error);
      }
      setType(
        response.data.map((item) => {
          return {
            value: item?.id,
            label: item?.name,
          };
        })
      );
    } catch (error) {
      console.log("fail to fetch type");
    }
  };
  useEffect(() => {
    getDataType();
  }, []);
  return (
    <SelectField
      {...props}
      label={"Doanh thu"}
      control={control}
      name={name}
      option={type}
    />
  );
}

export default IncomeField;
