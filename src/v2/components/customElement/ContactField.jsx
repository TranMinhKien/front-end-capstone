import React, { useEffect, useState } from "react";
import { getContact } from "v2/api/commonAPI";
import SelectField from "../CustomFields/SelectField";
import { useWatch } from "react-hook-form";
import PropTypes from "prop-types";
import { getContactByCustomerId } from "../../api/readByIdAPI";

ContactField.propTypes = {
  customerFieldName: PropTypes.string,
  customForm: PropTypes.any.isRequired,
};
ContactField.defaultProps = {
  customerFieldName: "customerId",
};

function ContactField(props) {
  const { control, name, label, customerFieldName, customForm, ...otherProps } =
    props;
  const watchCustomer = useWatch({
    control,
    name: customerFieldName,
  });
  const [contact, setContact] = useState();
  const getDataContact = async (watchCustomer) => {
    try {
      let response;
      if (watchCustomer) {
        response = await getContactByCustomerId(watchCustomer);
      } else {
        response = await getContact();
      }

      if (response.error) {
        throw new Error(response.error);
      }
      setContact(
        response.data.map((item) => {
          return {
            value: item?.id,
            label: item?.fullName,
          };
        })
      );
    } catch (error) {
      console.log("fail to fetch contact");
    }
  };
  useEffect(() => {
    getDataContact(watchCustomer);
  }, [watchCustomer]);

  return (
    <SelectField
      label={label}
      control={control}
      name={name}
      option={contact}
      {...otherProps}
    />
  );
}

export default ContactField;
