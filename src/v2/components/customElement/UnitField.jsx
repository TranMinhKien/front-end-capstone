import React, { useEffect, useState } from "react";
import { getUnit } from "v2/api/commonAPI";
import SelectField from "../CustomFields/SelectField";

function UnitField(props) {
  const { control, name } = props;
  const [unit, setUnit] = useState();
  const getDataUnit = async () => {
    try {
      const response = await getUnit();
      if (response.error) {
        throw new Error(response.error);
      }
      setUnit(
        response.data.map((item) => {
          return {
            value: item?.id,
            label: item?.name,
          };
        })
      );
    } catch (error) {
      console.log("fail to fetch product type");
    }
  };
  useEffect(() => {
    getDataUnit();
  }, []);
  return (
    <SelectField
      {...props}
      label={"Đợn vị tính"}
      control={control}
      name={name}
      option={unit}
    />
  );
}

export default UnitField;
