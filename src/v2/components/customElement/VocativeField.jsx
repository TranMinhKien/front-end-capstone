import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import SelectField from "../CustomFields/SelectField";
import { getVocative } from "v2/api/commonAPI";

VocativeField.propTypes = {};

function VocativeField(props) {
  const { control, name } = props;
  const [vocative, setVocative] = useState();
  const getDataVocative = async () => {
    try {
      console.log("object");
      const response = await getVocative();
      if (response.error) {
        throw new Error(response.error);
      }
      setVocative(
        response.data.map((item) => {
          return {
            value: item?.id,
            label: item?.name,
          };
        })
      );
    } catch (error) {
      console.log("fail to fetch vocative");
    }
  };
  useEffect(() => {
    getDataVocative();
  }, []);
  return (
    <SelectField
      {...props}
      label={"Xưng hô"}
      isRequired
      control={control}
      name={name}
      option={vocative}
    />
  );
}

export default VocativeField;
