import React, { useEffect, useState } from "react";
import { getProductType } from "v2/api/commonAPI";
import SelectField from "../CustomFields/SelectField";

function ProductTypeField(props) {
  const { control, name } = props;
  const [productType, setProductType] = useState();
  const getDataProductType = async () => {
    try {
      const response = await getProductType();
      if (response.error) {
        throw new Error(response.error);
      }
      setProductType(
        response.data.map((item) => {
          return {
            value: item?.id,
            label: item?.name,
          };
        })
      );
    } catch (error) {
      console.log("fail to fetch product type");
    }
  };
  useEffect(() => {
    getDataProductType();
  }, []);
  return (
    <SelectField
      {...props}
      label={"Loại hàng hóa"}
      control={control}
      name={name}
      option={productType}
    />
  );
}

export default ProductTypeField;
