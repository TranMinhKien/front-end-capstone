import React, { useEffect, useState } from "react";
import { getOpportunityPhase } from "v2/api/commonAPI";
import SelectField from "../CustomFields/SelectField";

function OpportunityPhaseField(props) {
  const { control, name, label } = props;
  const [opportunityPhase, setOpportunityPhase] = useState();
  const getDataopportunityPhase = async () => {
    try {
      const response = await getOpportunityPhase();
      if (response.error) {
        throw new Error(response.error);
      }
      setOpportunityPhase(
        response.data.map((item) => {
          return {
            value: item?.id,
            label: item?.name,
          };
        })
      );
    } catch (error) {
      console.log("fail to fetch opportunityPhase");
    }
  };
  useEffect(() => {
    getDataopportunityPhase();
  }, []);
  return (
    <SelectField
      {...props}
      label={label}
      control={control}
      name={name}
      option={opportunityPhase}
    />
  );
}

export default OpportunityPhaseField;
