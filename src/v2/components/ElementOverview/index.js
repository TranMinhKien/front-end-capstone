import React, {useEffect, useState} from 'react';
import InputTextField from "../CustomFields/InputTextField";
import {useForm, Controller, useController} from "react-hook-form";
import {UserOutlined, HomeOutlined, SendOutlined, DownloadOutlined} from '@ant-design/icons';
import {Button, Form, Input} from 'antd';
import {Link} from "react-router-dom";
import {yupResolver} from '@hookform/resolvers/yup';
import * as yup from "yup";
import SelectField from '../CustomFields/SelectField';
import InputNuber from '../CustomFields/InputNumberField';
import FormDataCustom from '../../utils/FormDataCustom';
import InputDateField from '../CustomFields/InputDateField';
import moment from 'moment';
import InputPhoneField from '../CustomFields/InputPhoneField';
import CheckBoxField from '../CustomFields/CheckBoxField';
import { getCustomer } from 'v2/api/commonAPI';

function ElementOverview(props) {
    const name = "address";
    const schema = yup.object().shape({
        address: yup.number().positive().integer().typeError('Nhập số'),
        user: yup.string().required('Must select one'),
        birthday : yup.date().min('2021-09-03',' Chọn ngày lớn hơn hoặc bằng 2021-09-03')
    });
    const rHF = useForm(
        {
            resolver: yupResolver(schema),
            mode: 'onChange'
        }
    );
    const {control, formState: {errors}, watch, handleSubmit, setValue} = rHF;
    const onSubmit = data =>{
        data.birthday = data.birthday?.format('YYYY-MM-DD');
        console.log('submit', data);
    };

    const [data, setData] = useState();
    const [form] = Form.useForm();
    const formCustom = new FormDataCustom(form, rHF);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await getCustomer();
                console.log(response.data[0].address);
                // setData(response.data[0].address);
                formCustom.setFormValue(name, response.data[0].id);
                formCustom.setFormValue('birthday',new moment('2021-05-05'))
            } catch (e) {
                console.log(e);
            }
        };
        fetchData();
    }, []);

    return (
        <Form form={form} onFinish={handleSubmit(onSubmit)}>
            <div className={'container mt-5'}>
                <InputTextField control={control} name={name} placeholder="Your address" label={"Address"}
                                prefixIcon={<HomeOutlined/>} disabled/>
                <InputNuber name={"age"} control={control} isRequired label={'Age'}/>
                <div>
                    <SelectField name={'user'} control={control} option={[{value: 'david', label: 'david'},
                        {value: 'multi', label: 'multi'}, {value: 'select', label: 'select'}]} label={"user"}/>

                    <SelectField name={'multi'} control={control}
                                 label={"multi select"} isRequired isMultiple/>
                    <InputDateField control={control} name={'birthday'}/>

                    <InputPhoneField inputName={'phone'} selectName={'prefixPhone'} control={control} label={'Phone'} isRequired/>
              <CheckBoxField control={control} name={'checkbox'} label={'Đang buồn'} />
                </div>
                <Button type="primary" htmlType={'submit'} className={'mt-2'}
                >Submit</Button>
            </div>
        </Form>

    );
}

export default ElementOverview;