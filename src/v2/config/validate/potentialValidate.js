import {commonRegex, commonValidationMsg} from "./commonValidate";
import * as yup from 'yup';
import moment from 'moment';

export const validatePotentialSchema = yup.object().shape({
    name : yup.string().matches(commonRegex.NAME ,"Tên chỉ được nhập chữ ").required( "Tên chỉ được nhập chữ "),
    lastName : yup.string().matches(commonRegex.NAME ,"Họ và Đệm chỉ được nhập chữ ").nullable(),
    vocativeId: yup.string().required("Xưng hô không được để trống"),
    phone: yup.string().required(commonValidationMsg.PHONE_REQUIRE).length(9, commonValidationMsg.PHONE_INVALID),
    officePhone: yup.string().length(9, commonValidationMsg.PHONE_INVALID).nullable(),
    otherPhone  : yup.string().length(9, commonValidationMsg.PHONE_INVALID).nullable(),
    email: yup.string().matches(commonRegex.EMAIL, "Địa chỉ email không hợp lệ").nullable(),
    officeEmail: yup.string().matches(commonRegex.EMAIL, "Địa chỉ email không hợp lệ").nullable(),
    taxCode: yup.string().matches(commonRegex.TAX_CODE, commonValidationMsg.TAX_CODE_INVALID).nullable(),
    customerTaxCode: yup.string().matches(commonRegex.TAX_CODE, commonValidationMsg.TAX_CODE_INVALID).nullable(),
    dateOfBirth: yup.date().max(new moment(), commonValidationMsg.DATE_OF_BIRTH_INVALID).nullable(),
    foundedDate: yup.date().max(new moment(), 'Ngày thành lập không được ngày tương lai').nullable(),
    bankAccount: yup.string().matches(/^[0-9]{10,20}$/, commonValidationMsg.BANK_ACCOUNT_INVALID).nullable(),
    genderId:  yup.string().required("Giới tính không được để trống").nullable(),
    // bank : yup.string().matches(commonRegex.CANCEL_CHART,"Không được nhập kí tự đặc biệt")
});