import * as yup from 'yup';
import { commonRegex, commonValidationMsg } from './commonValidate';

export const validateRoleSchema = yup.object().shape({
    name: yup.string().required(commonValidationMsg.NAME_VALID),
    description : yup.string().required("Mô tả không được để trống"),
});
