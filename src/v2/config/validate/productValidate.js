import * as yup from 'yup';

export const validateProductSchema = yup.object().shape({
    name: yup.string().required('Tên hàng hoá không được để trống'),
    sellPrice: yup.number().positive().moreThan(yup.ref('buyPrice'), 'Đơn giá bán phải lớn hơn đơn giá mua').required("Không được để trống").nullable(),
    sellPrice1: yup.number().positive().moreThan(yup.ref('buyPrice'), 'Đơn giá bán phải lớn hơn đơn giá mua').nullable(),
    sellPrice2: yup.number().positive().moreThan(yup.ref('buyPrice'), 'Đơn giá bán phải lớn hơn đơn giá mua').nullable(),
    permanentPrice: yup.number().positive()
        .moreThan(yup.ref('buyPrice'), 'Đơn giá bán phải lớn hơn đơn giá mua').typeError('Đơn giá không được để trống').required("Không được để trống"),
    buyPrice: yup.number().positive().nullable(),
    vat: yup.number().min(0, "Thuế phải thuộc khoảng 0 - 100").max(100, "Thuế phải thuộc khoảng 0 - 100").required("Không được để trống"),
    costUnitPrice :yup.number().positive().lessThan(yup.ref('buyPrice'), 'Đơn giá bán chi phí phải nhỏ hơn đơn giá mua').nullable(),

});
