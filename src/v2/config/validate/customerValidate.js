import * as yup from 'yup';
import {commonRegex, commonValidationMsg} from './commonValidate';
import moment from 'moment';
import {DATE_FORMAT} from '../date/dateFormat';

export const validateCustomerSchema = yup.object().shape({
    name : yup.string().required("Tên tổ chức Không được để trống ") , 
    email: yup.string().matches(commonRegex.EMAIL, "Địa chỉ email không hợp lệ").required("Địa chỉ Email không được để trống"),
    phone: yup.string().required(commonValidationMsg.PHONE_REQUIRE).length(9, commonValidationMsg.PHONE_INVALID),
    // receiverName: yup.string().required(commonValidationMsg.NAME_REQUIRE),
    // receiverPhone: yup.string().required(commonValidationMsg.PHONE_REQUIRE).length(9, commonValidationMsg.PHONE_INVALID),
    taxCode: yup.string().matches(commonRegex.TAX_CODE, commonValidationMsg.TAX_CODE_INVALID).required("Không được để trống").nullable(),
    // receiverEmail: yup.string().matches(commonRegex.EMAIL, "Địa chỉ email không hợp lệ"),
    bankAccount: yup.string().matches(/^[0-9]{10,20}$/, commonValidationMsg.BANK_ACCOUNT_INVALID).nullable(),
    foundedDate: yup.date().max(new moment().add(1, 'day').format(DATE_FORMAT), 'Không được thực hiện ngày trong tương lai').nullable(),
    customerSince: yup.date().max(new moment().add(1, 'day').format(DATE_FORMAT), 'Không được thực hiện ngày trong tương lai').nullable(),
});