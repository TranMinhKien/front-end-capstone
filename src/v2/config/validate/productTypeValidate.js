import * as yup from 'yup';

export const validateProductTypeSchema = yup.object().shape({
    name: yup.string().required('Tên loại hàng hoá không được để trống')
});