import * as yup from 'yup';
import {commonRegex, commonValidationMsg} from './commonValidate';
import moment from 'moment';

export const validatePersonalSchema = yup.object().shape({
    name: yup.string().matches(commonRegex.NAME,commonValidationMsg.NAME_VALID).nullable(),
    phone: yup.string().nullable(),
    lastName: yup.string() .matches(commonRegex.LAST_NAME, commonValidationMsg.LAST_NAME_VALID).nullable(),
    dateOfBirth: yup.date().max(new moment(), commonValidationMsg.DATE_OF_BIRTH_INVALID).nullable(),
    genderId: yup.string()
});