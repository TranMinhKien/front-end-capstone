import moment from 'moment';
import * as yup from 'yup';
import { commonRegex, commonValidationMsg } from './commonValidate';

export const validateUserSchema = yup.object().shape({
    password : yup.string().matches(commonRegex.PASSWORD , "Phải có ít nhất 1 chữ thường, 1 chữ hoa, 1 số, 1 ký tự đặc biệt").required("Mật Khẩu không được để trống"),
    email: yup.string().matches(commonRegex.EMAIL, "Địa chỉ email không hợp lệ").required("Email không được để trống"),
    username :yup.string().matches(commonRegex.USERNAME , "Không cái gì").required("Tài Khoản không được để trống"),
    phone: yup.string().length(9, commonValidationMsg.PHONE_INVALID).nullable(),
    dateOfBirth: yup.date().max(new moment(), commonValidationMsg.DATE_OF_BIRTH_INVALID).nullable(),
});
