import moment from 'moment';
import * as yup from 'yup';
import { commonRegex, commonValidationMsg } from './commonValidate';

export const validateUser2 = yup.object().shape({
    email: yup.string().matches(commonRegex.EMAIL, "Địa chỉ email không hợp lệ").required("Email không được để trống"),
    username :yup.string().matches(commonRegex.USERNAME , "Không cái gì").required("Tài Khoản không được để trống"),
    phone: yup.string().length(9, commonValidationMsg.PHONE_INVALID).nullable(),
    dateOfBirth: yup.date().max(new moment(), commonValidationMsg.DATE_OF_BIRTH_INVALID).nullable(),
});
