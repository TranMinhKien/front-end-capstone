import * as yup from 'yup';

export const commonValidationMsg = {
    // common
    NAME_REQUIRE: "Tên không được để trống!",
    NAME_VALID: "Tên chỉ chứa ký tự là chữ cái ",
    LAST_NAME_REQUIRE: "Họ và đệm không được để trống",
    LAST_NAME_VALID: "Họ và đệm chỉ chứa ký tự chữ cái và dấu cách",
    TAX_CODE_INVALID: "Mã số thuế chỉ chứa 8-15 ký tự số",
    BANK_ACCOUNT_INVALID: "Số tài khoản ngân hàng chỉ chứa số và trong khoảng 10-20 số",
    DATE_OF_BIRTH_INVALID: "Ngày sinh không được lớn hơn ngày hiện tại",
    PHONE_INVALID: "Số điện thoại không nhiều hơn 9",
    PHONE_REQUIRE: "Điện thoại không được để trống",
    CONTACT_REQUIRE:"Liên hệ không được để trống",
    CUSTOMER_REQUIRE:"Tổ chức không được để trống",
    LIQUIDATION_DEADLINE_REQUIRE:"Hạn thanh toán không được để trống",

};
export const commonRegex = {
    TAX_CODE: /^[0-9]{8,15}$/,
    EMAIL: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{1,}))$/,
    BANK_ACCOUNT: /^[0-9]{10,20}$/,
    // NAME: /[a-zA-Z]+/,
    NAME: /^[a-zA-Z]+$/,
    LAST_NAME: /[a-zA-Z ]+/,
    PASSWORD : /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!\"#$%&'()*+,-.\/:;<=>?@\[\]^_`{|}~\\])(?=\S+$).{8,128}$/,
    USERNAME : /^([a-zA-Z])+([\w]{7,31})+$/ 
};


