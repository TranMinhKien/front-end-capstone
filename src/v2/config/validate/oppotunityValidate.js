import * as yup from 'yup';
import moment from 'moment';

export const validateOpportunitySchema = yup.object().shape({
    name: yup.string().required("Tên cơ hội không được để trống") ,
    opportunityPhaseId : yup.string().required("Giai đoạn không được để trống") ,
    expectedEndDate: yup.date().min(new moment(), 'Ngày kì vọng không được thực hiện trong quá khứ').required("Không được để trống").nullable(),
    sourceId : yup.string().required("Nguồn gốc không được để trống") ,
    contactId : yup.string().required("Liên hệ không được để trống")
})