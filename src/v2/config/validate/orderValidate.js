import * as yup from 'yup';
import {commonValidationMsg} from './commonValidate';
import moment from 'moment';
import {DATE_FORMAT} from '../date/dateFormat';

export const validateOrderSchema = yup.object().shape({
    customerId: yup.number().nullable(),
    liquidationDeadline: yup.date().required(commonValidationMsg.LIQUIDATION_DEADLINE_REQUIRE)
        .typeError("Hạn thanh toán không được để trống").min(new moment().add(-1, 'day'), "Hạn thanh toán không được thực hiện trong quá khứ"),
    receivedMoney: yup.number().positive('Thực thu lơn hơn 0').max(yup.ref('orderValue'), "Thực thu không được lớn hơn giá trị hàng hoá").required("Thực thu không được để trống").typeError('Thực thu là số'),
    // orderValue: yup.number().positive('Giá trị đơn hàng lơn hơn 0').typeError('Giá trị đơn hàng là số'),
    orderDate: yup.date().max(new moment().add(1, 'day').format(DATE_FORMAT), "Ngày đặt hàng không được thực hiện trong tương lai").typeError('Ngày đặt hàng không được để trống').required("Không được để trống"),
    // opportunity : yup.string().required("Tên cơ hội không được để trống") ,
    contactId  : yup.string().required("Liên hệ không được để trống") ,
    deliveryDeadline : yup.date().min(new moment().format(DATE_FORMAT), "Hạn giao hàng không được thực hiện trong quá khư").typeError('Ngày đặt hàng không được để trống').required("Không được để trống")
});