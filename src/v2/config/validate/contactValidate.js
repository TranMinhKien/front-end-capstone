import * as yup from 'yup';
import {commonRegex, commonValidationMsg} from './commonValidate';
import moment from 'moment';
import {DATE_FORMAT} from '../date/dateFormat';

export const validateContactSchema = yup.object().shape({
    name : yup.string().matches(commonRegex.NAME ,"Tên chỉ được nhập chữ ").required( "Tên chỉ được nhập chữ "),
    lastName : yup.string().matches(commonRegex.NAME ,"Họ và Đệm chỉ được nhập chữ ").nullable(),
    vocativeId: yup.string().required("Xưng hô không được để trống"),
    phone: yup.string().required(commonValidationMsg.PHONE_REQUIRE).length(9, commonValidationMsg.PHONE_INVALID),
    email: yup.string().matches(commonRegex.EMAIL, "Địa chỉ email không hợp lệ").required("Email không được để trống"),
    officeEmail: yup.string().matches(commonRegex.EMAIL, "Địa chỉ email không hợp lệ"),
    dateOfBirth: yup.date().max(new moment().add(1,'day').format(DATE_FORMAT), commonValidationMsg.DATE_OF_BIRTH_INVALID).nullable(),
    bankAccount: yup.string().matches(/^[0-9]{10,20}$/, commonValidationMsg.BANK_ACCOUNT_INVALID).nullable(),
    officePhone: yup.string().length(9, commonValidationMsg.PHONE_INVALID),
    otherPhone  : yup.string().length(9, commonValidationMsg.PHONE_INVALID),
});