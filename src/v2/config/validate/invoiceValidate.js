import * as yup from 'yup';
import {commonRegex, commonValidationMsg} from './commonValidate';
import moment from 'moment';
import {DATE_FORMAT} from '../date/dateFormat';


export const validateInvoicerSchema = yup.object().shape({
buyerId  : yup.string().required("Người mua hàng không được để trống") ,
receiverName : yup.string().required(commonValidationMsg.NAME_REQUIRE),
receiverPhone : yup.string().required(commonValidationMsg.PHONE_REQUIRE).length(9, commonValidationMsg.PHONE_INVALID),
orderId : yup.string().required("Đơn hàng không được để trống") ,
bankAccount: yup.string().matches(/^[0-9]{10,20}$/, commonValidationMsg.BANK_ACCOUNT_INVALID).nullable(),
taxCode: yup.string().matches(commonRegex.TAX_CODE, commonValidationMsg.TAX_CODE_INVALID).nullable(),
receiverEmail: yup.string().matches(commonRegex.EMAIL, "Địa chỉ email không hợp lệ").nullable(),

});