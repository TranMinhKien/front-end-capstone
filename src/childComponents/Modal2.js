import React from 'react';
import { Modal} from '@material-ui/core';

export default function Modal2(props) {
    return (
        <div>
            <Modal
                open ={props.open}
                onClose ={props.close}
            >
            {props.body}
            </Modal>
        </div>
    );
}

