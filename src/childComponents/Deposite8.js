import React from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Title from './Title';


function preventDefault(event) {
  event.preventDefault();
}

const useStyles = makeStyles({
  depositContext: {
    flex: 1,
  },
});

export default function Deposits8(props) {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Title>Tỷ lệ chiến thắng cơ hội </Title>
      <br></br>
      <Typography component="p" variant="h4">
        {props.opportunityWinRate}%
      </Typography>
    
      {/* <Typography color="textSecondary" className={classes.depositContext}>
        15 
      </Typography> */}
    </React.Fragment>
  );
}