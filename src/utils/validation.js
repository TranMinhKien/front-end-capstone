class Validation {

    usernameRegex = RegExp(/^([a-zA-Z])+([\w]{7,31})+$/)

    emailRegex = RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

    phoneRegex = RegExp(/^\d{10,15}$/)

    numberRegex = RegExp(/^-?\d+$/)
   
    passwordRegex =  RegExp(/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!\"#$%&'()*+,-.\/:;<=>?@\[\]^_`{|}~\\])(?=\S+$).{8,128}$/)

    value = null

    username = (e) => {
        if(e === null || e === '') return ''
        return this.usernameRegex.test(e) ? '' : 'Không bắt đầu bằng số, không chưa ký tự đặc biệt'
    }

    password = (e) => {
        if(e === null || e === '') return ''
        return this.passwordRegex.test(e) ? '' : 'Phải có ít nhất 1 chữ thường, 1 chữ hoa, 1 số, 1 ký tự đặc biệt'
    }

    notBlank = (e) => {
        if (e === null || e === '')
            return 'không được để trống'
        return ''
    }

    email = (e) => {
        if (e === null || e === '') return ''
        return this.emailRegex.test(e) ? '' : 'phải là email hợp lệ'
    }

    number = (e) => {
        if (e === null || e === '') return ''
        return this.numberRegex.test(e) ? '' : 'phải là số'
    }

    rate = (e) => {
        if (e === null || e === '') return ''
        if (0 <= e && e <= 100)
            return ''
        return 'phải nằm trong đoạn từ 1 đến 100'
    }

    min(value) {
        return (e) => {
            if (e === null || e === '') return ''
            if (e >= value)
                return ''
            return 'phải lớn hơn hoặc bằng ' + value
        }
    }
    max(value) {
        return (e) => {
            if (e === null || e === '') return ''
            if (e <= value)
                return ''
            return 'phải nhỏ hơn hoặc bằng ' + value
        }
    }

    length(min, max) {
        return (e) => {
            if (e === null || e === '') return ''
            if (typeof e === 'string' || e instanceof String) {
                if (e.length < min || e.length > max) {
                    return 'phải có độ dài từ ' + min + ' đến ' + max
                }
            }
            return ''
        }
    }

    phone = (e) => {
        if (e === null || e === '') return ''
        return this.phoneRegex.test(e) ? '' : 'phải chứa từ 10 đến 15 số'
    }

    positive = (e) => {
        if (e === null || e === '') return ''
        return e > 0 ? '' : 'phải là số dương'
    }

    positiveOrZero = (e) => {
        if (e === null || e === '') return ''
        return e >= 0 ? '' : 'không được âm'
    }

    validate(name = '', validators = []) {
        name = name.trim();
        for (let i = 0; i < validators.length; i++) {
            const result = validators[i](this.value)
            if (result) return name + ' ' + result
        }
        return ''
    }

    validate1(name = '', value, validators = []) {
        name = name.trim();
        for (let i = 0; i < validators.length; i++) {
            const result = validators[i](value)
            if (result) return name + ' ' + result
        }
        return ''
    }

}

export default new Validation();