import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { toastSuccess } from './MessageSuccess';

export const ErrorMess = (e, message = "Đã có lỗi xảy ra!") => {
    toast.configure()
    // if (e.response && e.response.data) {
    //     if (e.response.data.errors) {
    //         try {
    //             e.response.data.errors.forEach((error, index) => {
    //                 toast.error(error.field + " " + error.message);
    //               // toast(error.field + " " + error.message);
    //             });
    //         } catch (e1) {
    //             toast.error("An unexpected error has occured");
    //         }
    //     }
    //     if (e.response.data.error) {
    //         // alert
    //     }
    // }
    console.log(e);
    console.log(e?.response?.data);
    toast.error(message, {
        position: toast.POSITION.BOTTOM_LEFT,
        autoClose: 8000,
    });
}