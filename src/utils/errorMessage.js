import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
export const missingPermission = () => {
    toast.configure()
    toast.warning("Bạn không có quyền!", {
        position: toast.POSITION.BOTTOM_LEFT,
        autoClose: 8000,
    });
}