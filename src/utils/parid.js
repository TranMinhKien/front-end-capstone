import axios from 'axios';
import authHeader from '../services/auth_header';
import { ErrorMess } from './MessageErrors';
export const ParidGet = (url, dataAdd) => {
    return new Promise(resolve => {
        axios({
            method: "GET",
            url: url, params : dataAdd,
            headers: {
                'Authorization': authHeader().Authorization
            }
        }).then(response => {
            resolve(response)
        }).catch(err => {
            ErrorMess(err)
        })
    })
}