import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export const toastSuccess = (e) => {
    toast.configure()
    toast.success(e, {
        position: toast.POSITION.BOTTOM_LEFT,
        autoClose: 8000,
    });
    console.log(e);
}