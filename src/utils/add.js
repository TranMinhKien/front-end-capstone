import axios from 'axios';
import authHeader from '../services/auth_header';
import { ErrorMess } from './MessageErrors';
import { toastSuccess } from './MessageSuccess';
export const commonFetchAdd = (url, dataAdd) => {
    return new Promise((resolve, reject) => {
        axios.post(
                      url, dataAdd, { headers: { 'Authorization': authHeader().Authorization}}
                  )
            .then(response => {
                if (response.data.accessToken) {
                    localStorage.setItem("user", JSON.stringify(response.data));
                }
                resolve(response);
            }).catch(e => {
                reject(e);
            })
    })
}
// import axios from 'axios';
// export const commonFetchAdd = (url , dataAdd) => {
//     return new Promise(resolve => {
//         axios.post(url, dataAdd  )
//         .then(response => {
//             resolve(response)
//         }).catch(error => {
//             console.log(error)
//         })
//     })
// }