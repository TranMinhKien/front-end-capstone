import axios from 'axios';
import authHeader from '../services/auth_header';
export const commonFetchDataHandler = (url) => {
    return new Promise(resolve => {
        axios({
            method: "GET",
            url: url,
            headers: {
                'Authorization': authHeader().Authorization
            }
        }).then(response => {
            resolve(response)
            console.log("Minh Kien")
        }).catch(err => {
            throw err
        })
    })
}