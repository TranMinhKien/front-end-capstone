import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import axios from 'axios';
import MaterialTable from 'material-table';
import { Button, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import "../App.css";
import authHeader from '../services/auth_header';
import { Container, Row, Col } from 'react-bootstrap';
import { toastSuccess } from '../utils/MessageSuccess';


const columnas = [
    { field: 'name', title: 'Tên cơ hội' },
    { field: 'contact.name', title: 'Liên hệ' },
    { field: 'moneyAmount', title: 'Số tiền' },
    { field: 'opportunityPhase.name', title: 'Giai đoạn ' },
    { field: 'expectedEndDate', title: 'Ngày kì vọng ' },
    { field: 'createdAt', title: 'Ngày tạo' },
]

const insert = [
    { field: 'productCode', title: 'Mã Hàng Hóa' },
    { field: 'explanation', title: 'Diễn giải' },
    { field: 'unit', title: 'Đơn vị tính' },
    { field: 'amount', title: 'Số lượng' },
    { field: 'price', title: 'Đơn giá ' },
    { field: 'discount', title: 'Thành tiền' },
    { field: 'vat', title: 'Tiền chiết Khấu' },
]

const baseURL = "http://localhost:8080/api/opportunity/";
const baseURLcustomer = "http://localhost:8080/api/customer/";
const baseURLcontact = " http://localhost:8080/api/contact/";
const baseURLsource = " http://localhost:8080/api/source/";
const baseURLphase = "http://localhost:8080/api/opportunityPhase/";


const useStyles = makeStyles((theme) => ({
    modal: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)'
    },
    iconos: {
        cursor: 'pointer'
    },
    inputMaterial: {
        width: '100%'
    }
}));

function InsertOpportunity() {
    const styles = useStyles();
    const [data, setData] = useState([]);
    const [dataproductInfo, setDataProductInfo] = useState([]);
    const [modalInsertar, setModalInsertar] = useState(false);
    const [modalEditar, setModalEditar] = useState(false);
    const [modalEliminar, setModalEliminar] = useState(false);
    const [modalViewInfor, setModalViewInfor] = useState(false);
    const [artistaSeleccionado, setArtistaSeleccionado] = useState({
        customerId: "1",
        contactId: "1",
        name: "",
        moneyAmount: "22",
        opportunityPhaseId: "1",
        successRate: "2",
        expectedEndDate: "2017-05-24",
        expectedTurnOver: "23",
        sourceId: "1",
        productInfos: []
    });

    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [source3, setSource3] = useState({
        selectSource: [],
        id: "",
        name: ""
    });
    const [customer3, setCustomer3] = useState({
        selectCustomer: [],
        id: "",
        name: ""
    });
    const [contact3, setContact3] = useState({
        selectContact: [],
        id: "",
        name: ""
    });
    const [phase3, setPhase3] = useState({
        selectPhase: [],
        id: "",
        name: ""
    });

    // hiện dữ liệu vào Selection từ giai đoạn   
    const getPhase = async () => {
        await axios({
            method: "GET",
            url: baseURLphase,
            headers: {
                'Authorization': authHeader().Authorization
            }
        }).then(response => {
            if (response.data.accessToken) {
                localStorage.setItem("user", JSON.stringify(response.data));
            }
            console.log(response.data);
            var options = response.data;
            var dataOption = options.map(d => ({
                "value": d.id,
                "label": d.name
            }))
            return setPhase3({ selectPhase: dataOption })
        }).catch(error => {
            toastSuccess(error);
        })
    }
    // hiện dữ liệu vào Selection từ liên hệ  
    const getContact = async () => {
        await axios({
            method: "GET",
            url: baseURLcontact,
            headers: {
                'Authorization': authHeader().Authorization
            }
        }).then(response => {
            if (response.data.accessToken) {
                localStorage.setItem("user", JSON.stringify(response.data));
            }
            console.log(response.data);
            var options = response.data;
            var dataOption = options.map(d => ({
                "value": d.id,
                "label": d.name
            }))
            console.log(dataOption)
            return setContact3({ selectContact: dataOption })
        }).catch(error => {
            toastSuccess(error);
        })
    }
    // hiện dữ liệu vào Selection từ khách hàng  
    const getCustomer = async () => {
        await axios({
            method: "GET",
            url: baseURLcustomer,
            headers: {
                'Authorization': authHeader().Authorization
            }
        }).then(response => {
            if (response.data.accessToken) {
                localStorage.setItem("user", JSON.stringify(response.data));
            }
            console.log(response.data);
            var options = response.data;
            var dataOption = options.map(d => ({
                "value": d.id,
                "label": d.name
            }))
            console.log(dataOption)
            return setCustomer3({ selectCustomer: dataOption })
        }).catch(error => {
            toastSuccess(error);
        })
    }

    // hiện dữ liệu vào Selection từ nguồn gốc  
    const getSource = async () => {
        await axios({
            method: "GET",
            url: baseURLsource,
            headers: {
                'Authorization': authHeader().Authorization
            }
        }).then(response => {
            if (response.data.accessToken) {
                localStorage.setItem("user", JSON.stringify(response.data));
            }
            console.log(response.data);
            var options = response.data;
            var dataOption = options.map(d => ({
                "value": d.id,
                "label": d.name
            }))
            console.log(dataOption)
            return setSource3({ selectSource: dataOption })
        }).catch(error => {
            toastSuccess(error);
        })
    }

    // thực hiện hành động input trong textfiels 
    const handleChange = e => {
        const { name, value } = e.target;
        setArtistaSeleccionado(prevState => ({
            ...prevState,
            [name]: value
        }));
        console.log(artistaSeleccionado);
    }


    const peticionGet = async () => {
        console.log(authHeader())
        debugger
        await axios({
            method: "GET",
            url: baseURL,
            headers: {
                'Authorization': authHeader().Authorization
            }
        }).then(response => {
            if (response.data.accessToken) {
                localStorage.setItem("user", JSON.stringify(response.data));
            }
            console.log(response.data);
            return setData(response.data);
        }).catch(error => {
            toastSuccess(error);
        })
    }

    const peticionGetProductInfo = async () => {
        await axios({
            method: "GET",
            url: baseURL + artistaSeleccionado.id + "/product",
            headers: {
                'Authorization': authHeader().Authorization
            }
        }).then(response => {
            if (response.data.accessToken) {
                localStorage.setItem("user", JSON.stringify(response.data));
            }
            return setDataProductInfo(response.data);
        }).catch(error => {
            console.log(error);
        })
    }

    // insert dữ liệu
    const peticionPost = async () => {
        await axios.post(baseURL, artistaSeleccionado)
            .then(response => {
                setData(data.concat(response.data));
                abrirCerrarModalInsertar();
                toastSuccess("Thêm dữ liệu thành công ");
            }).catch(error => {
                toastSuccess(error);
                toastSuccess(error);
            })
    }


    // hiện lên để chỉnh sửa trong database
    const peticionPut = async () => {
        await axios.put(baseURL + artistaSeleccionado.id, artistaSeleccionado)
            .then(response => {
                var dataNueva = data;
                dataNueva.map(id => {
                    if (id.id === artistaSeleccionado.id) {
                        id.customerId = artistaSeleccionado.customerId;
                        id.contactId = artistaSeleccionado.contactId;
                        id.name = artistaSeleccionado.name;
                        id.moneyAmount = artistaSeleccionado.moneyAmount;
                        id.opportunityPhaseId = artistaSeleccionado.opportunityPhaseId;
                        id.successRate = artistaSeleccionado.successRate;
                        id.expectedEndDate = artistaSeleccionado.expectedEndDate;
                        id.expectedTurnOver = artistaSeleccionado.expectedTurnOver;
                        id.sourceId = artistaSeleccionado.sourceId;

                    }
                });
                setData(dataNueva);
                abrirCerrarModalEditar();
                toastSuccess("Edit dữ liệu thành công");
            }).catch(error => {
                console.log(error);
            })
    }
    // xóa dữ liệu
    const peticionDelete = async () => {
        await axios.delete(baseURL + artistaSeleccionado.id)
            .then(response => {
                setData(data.filter(name => name.id !== artistaSeleccionado.id));
                abrirCerrarModalEliminar();
                toastSuccess("Xóa dữ liệu thành công");
            }).catch(error => {
                console.log(error);
            })
    }


    const seleccionarArtista = (id, caso) => {
        setArtistaSeleccionado(id);
        (caso === "Editar") ? abrirCerrarModalEditar()
            :
            abrirCerrarModalEliminar()
    }


    const seleccionarView = (id, caso) => {
        setArtistaSeleccionado(id);
        if (caso === "check") {
            viewInformation();
        }
    }


    // // // thực hiện lệnh tắt và bật  modal
    const abrirCerrarModalInsertar = () => {
        setModalInsertar(!modalInsertar);
    }
    // // cũng để bật tắt
    const abrirCerrarModalEditar = () => {
        setModalEditar(!modalEditar);
    }
    // // cũng để bật tắt 
    const abrirCerrarModalEliminar = () => {
        setModalEliminar(!modalEliminar);
    }

    const viewInformation = () => {
        setModalViewInfor(!modalViewInfor);
    }

    useEffect(() => {
        peticionGet();
        peticionGetProductInfo();
        getPhase();
        getContact();
        getCustomer();
    }, [])

    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-12">
                    <Container>
                        <Row>
                            <Col md={6}>
                                <label> Khách hàng </label>
                                <select className="form-control" name="customerId" onChange={handleChange} >
                                    {customer3.selectCustomer.map((option2 , index) => (
                                        <option value={option2.value} key={index}> {option2.label}</option>
                                    ))}
                                </select>
                            </Col>
                            <Col md={6}>
                                <label> Liên hệ </label>
                                <select className="form-control" name="contactId" onChange={handleChange} >
                                    {contact3.selectContact.map((option2 , index) => (
                                        <option value={option2.value} key={index}> {option2.label}</option>
                                    ))}
                                </select>
                            </Col>
                        </Row>

                        <Row>
                            <Col md={6}>
                                <TextField className={styles.inputMaterial} label=" Tên cơ hội " name="name" onChange={handleChange} />
                                <br />
                            </Col>
                            <Col md={6}>
                                <TextField className={styles.inputMaterial} label=" Số tiền " name="moneyAmount" onChange={handleChange} />
                                <br />
                            </Col>
                        </Row>

                        <Row>
                            <Col md={6}>
                                <label> Giai Đoạn  </label>
                                <select className="form-control" name="opportunityPhaseId" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.opportunityPhaseId} >
                                    {phase3.selectPhase.map((option2 , index) => (
                                        <option value={option2.value} key={index}> {option2.label}</option>
                                    ))}
                                </select>
                            </Col>
                            <Col md={6}>
                                <TextField className={styles.inputMaterial} label=" Tỷ lệ thành công " name="successRate" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.successRate} />
                                <br />
                            </Col>
                        </Row>

                        <Row>
                            <Col md={6}>
                                <TextField
                                    name="expectedEndDate"
                                    label="Ngày kì vọng"
                                    InputLabelProps={{ shrink: true, required: true }}
                                    type="date"
                                    onChange={handleChange}
                                    defaultValue={artistaSeleccionado.expectedEndDate}
                                />
                            </Col>
                            <Col md={6}>
                                <TextField className={styles.inputMaterial} label=" Doanh số kì vọng  " name="expectedTurnOver" onChange={handleChange} />
                                <br />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={6}>
                                <label> Nguồn gốc</label>
                                <select className="form-control" name="sourceId" onChange={handleChange} >
                                    {source3.selectSource.map((option2 , index) => (
                                        <option value={option2.value} key={index}> {option2.label}</option>
                                    ))}
                                </select>
                            </Col>
                            <Col md={6}>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12}>
                                <MaterialTable
                                    title="Hàng Hóa"
                                    columns={insert}
                                    data={dataproductInfo}
                                    options={{
                                        actionsColumnIndex: -1,
                                       
                                        exportButton: true,
                                        filtering: true,
                                    }}
                                    localization={{
                                        header: {
                                           actions: 'Hành động'
                                        }
                                    }}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={10}></Col>
                            <Col md={2}>
                                <Button variant="secondary" onClick={() => handleClose()}>
                                    Close
                                </Button>
                                <Button variant="primary" onClick={() => peticionPost()}>
                                    Save
                                </Button>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </div>
        </div>
    );

};

export default InsertOpportunity;