import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import axios from 'axios';
import MaterialTable from 'material-table';
import { TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import "../App.css";
import { DOMAIN_BE } from '../constant/constant';
import { commonFetchDataHandler } from '../utils/index'
import { commonFetchAdd } from '../utils/add';
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import Select from 'react-select';
import authHeader from '../services/auth_header';
import { Button, Modal, Container, Row, Col } from 'react-bootstrap';
import AuthService from "../services/auth.service";
import { missingPermission } from '../utils/errorMessage';
import validation from '../utils/validation';
import VisibilityIcon from '@material-ui/icons/Visibility'
import { toastSuccess } from '../utils/MessageSuccess';
import { ErrorMess } from '../utils/MessageErrors';


const baseURLproductType = DOMAIN_BE + "/api/productType/";

const baseURLproductType2 = DOMAIN_BE + "/api/productType/getavailable";

const useStyles = makeStyles((theme) => ({
    modal: {
        position: 'absolute',
        width: 1200,

        backgroundColor: theme.palette.background.paper,
        border: '1px solid #000',
        // boxShadow: theme.shadows[5],
        boxShadow: '20px 10px',
        padding: theme.spacing(2, 4, 3),
        // padding: '1px 1px 1px 1px',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)'

    },
    iconos: {
        cursor: 'pointer'
    },
    inputMaterial: {
        width: '100%',
        //  padding: '3px 10px',
        margin: '3px 0',

    }
}));

const validateForm = (errors) => {
    let valid = true;
    if (errors == null)
        return true;
    Object.values(errors).forEach(
        (val) => val.length > 0 && (valid = false)
    );
    return valid;
}

const validEmailRegex = RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
const validPhoneRegex = RegExp(/^\d*$/)

function ProductType() {
    const styles = useStyles();
    const [data, setData] = useState([]);
    const [modalInsertar, setModalInsertar] = useState(false);
    const [modalEditar, setModalEditar] = useState(false);
    const [modalEliminar, setModalEliminar] = useState(false);
    const [modalViewInfor, setModalViewInfor] = useState(false);
    // lấy thông tin USER
    const user = AuthService.getCurrentUser();
    const [artistaSeleccionado, setArtistaSeleccionado] = useState({
        name: "",
        code: "",
        productTypeId: null,
        createdAt: "",
        createdBy: "",
        updatedAt: "",
        updatedBy: "",
        errors: {
            name: '',
            code: '',
        }

    })

    const [chilProductType, setchilProductType] = useState({
        selectProductType: [],
        id: "",
        name: ""
    });

    const clearErrors = () => {
        setArtistaSeleccionado(prevState => ({
            ...prevState,
            errors: {}
        }));
    }

    const handleChange = e => {
        const { name, value } = e.target;

        let errors = artistaSeleccionado.errors || (artistaSeleccionado.errors = {})
        validation.value = value;

        switch (name) {
            case 'name':
                errors.name = validation.validate('Tên', [validation.notBlank])
                break;

            case 'code':
                errors.code = validation.validate('Mã', [validation.notBlank])
                break;
            default:
                break;
        }
        setArtistaSeleccionado(prevState => ({
            ...prevState,
            [name]: value || null
        }));
        console.log(artistaSeleccionado);
    }
    const hasPerm = (user, perm = "") => {
        return user && user.roles && user.roles.includes(perm)
    }

    // hiện dữ liệu lên bảng    
    const getProductType = async () => {
        const response = await commonFetchDataHandler(baseURLproductType)
        response.data.forEach(element => {
            element.productTypeId = element.productType ? element.productType.id : null
        });
        setData(response.data)
    }

    // API cũ insert dropdown
    const getProductTypeChil = async () => {
        const responseProductTypeChil = await commonFetchDataHandler(baseURLproductType)
        const optionsProductTypeChil = responseProductTypeChil.data.map(d => ({
            "value": d.id,
            "label": d.name
        }))
        optionsProductTypeChil.unshift({
            "value": "",
            "label": "-- Không chọn --"
        })
        setchilProductType({ selectProductType: optionsProductTypeChil })
    }



    // insert dữ liệu
    const createProductType = async () => {
        // Check validate khi ấn "Thêm"
        for (const name in artistaSeleccionado) {
            const value = artistaSeleccionado[name]
            handleChange({
                target: { name, value }
            })
        }
        // Nếu chưa valid thì không thêm dữ liệu
        if (!validateForm(artistaSeleccionado.errors))
            return;

       commonFetchAdd(baseURLproductType, artistaSeleccionado).then(responseSource => {
            const d = responseSource.data
            d.productTypeId = d.productType ? d.productType.id : null
            setData(data.concat(d))
            abrirCerrarModalInsertar();
            toastSuccess("Thêm dữ liệu thành công")
            getProductTypeChil();
        }).catch(error => {
            if (error.response && error.response.data && error.response.data.errors && error.response.data.errors[0].type === 'duplicate') {
                let errors = artistaSeleccionado.errors || (artistaSeleccionado.errors = {})
                error.response.data.errors.forEach(e => {
                    errors[e.field] = 'Giá trị đã bị trùng';
                });
                setArtistaSeleccionado(prevState => ({
                    ...prevState,
                    errors: errors,
                }));
                console.log(artistaSeleccionado)
            } else {
                ErrorMess(error);
            }
        })

    }


    // hiện lên để chỉnh sửa trong database
    const editProductType = async () => {
        await axios.put(baseURLproductType + artistaSeleccionado.id, artistaSeleccionado, { headers: { 'Authorization': authHeader().Authorization } })
            .then(response => {
                const dataNueva = data;
                console.log("response.data khi update")
                console.log(response.data)
                dataNueva.map(id => {
                    if (id.id === artistaSeleccionado.id) {
                        id.code = artistaSeleccionado.code;
                        id.name = artistaSeleccionado.name;
                        id.productType = response.data.productType;
                        id.productTypeId = id.productType?.id;
                    }
                });
                setData(dataNueva);
                abrirCerrarModalEditar();
                toastSuccess("Edit thành công")
                // Update lại dropdown
                getProductTypeChil();
            }).catch(error => {
                ErrorMess(error);
            })
    }
    // xóa dữ liệu
    const deleteProductType = async () => {
        const ids = Array.isArray(artistaSeleccionado) ? artistaSeleccionado.map(a => a.id) : [artistaSeleccionado.id]
        await axios.delete(baseURLproductType + ids.join(','), { headers: { 'Authorization': authHeader().Authorization } })
            .then(response => {
                setData(data.filter(d => !ids.includes(d.id)));
                abrirCerrarModalEliminar();
                toastSuccess("Xóa thành công")
            }).catch(error => {
                ErrorMess(error);
            })
    }


    const seleccionarArtista = (id, caso) => {
        setArtistaSeleccionado(id);
        (caso === "Editar") ? abrirCerrarModalEditar()
            :
            // viewInformation
            abrirCerrarModalEliminar()

    }

    const seleccionarView = (id, caso) => {
        setArtistaSeleccionado(id);
        if (caso === "check") {
            viewInformation();
        }
    }

    // // thực hiện lệnh tắt và bật  modal
    const abrirCerrarModalInsertar = () => {
        setModalInsertar(!modalInsertar);
    }
    // // cũng để bật tắt
    const abrirCerrarModalEditar = () => {
        setModalEditar(!modalEditar);
    }
    // // cũng để bật tắt 
    const abrirCerrarModalEliminar = () => {
        setModalEliminar(!modalEliminar);
    }
    // dùng để bật tắt thông tin chi tiết 

    const viewInformation = () => {
        setModalViewInfor(!modalViewInfor);
    }

    useEffect(() => {
        getProductType();
        getProductTypeChil();
    }, [])

    const hasError = (name) => {
        return artistaSeleccionado && artistaSeleccionado.errors && artistaSeleccionado.errors[name] &&
            artistaSeleccionado.errors[name].length > 0 &&
            (<span className='error'>{artistaSeleccionado.errors[name]}</span>)
    }
    const bodyInsert = (
        <Modal size="lg" show={modalInsertar} onHide={() => abrirCerrarModalInsertar()} backdrop="static" keyboard={false}
        >
            <Modal.Header >
                <Modal.Title>Thêm Loại Hàng Hóa</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Container>
                    <Row>
                        <Col md={6}>
                            <label  className ="required">Mã loại hàng hóa</label>
                            <input type="text" class="form-control" name="code" onChange={handleChange}></input>
                            {hasError('code')}
                        </Col>
                        <Col md={6}>
                            <label  className ="required">Tên loại hàng hóa</label>
                            <input type="text" class="form-control" name="name" onChange={handleChange}></input>
                            {hasError('name')}
                        </Col>
                    </Row>
                    <Row>
                        <label>Loại hàng hóa</label>
                        <Col md={6}>
                            <select className="form-control controll" name="productTypeId" onChange={handleChange} value={artistaSeleccionado.productTypeId}>
                                {chilProductType.selectProductType.map((option2, index) => (
                                    <option value={option2.value} key={index}> {option2.label}</option>
                                ))}
                            </select>
                        </Col>
                        <Col md={6}>
                        </Col>
                    </Row>
                </Container>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => { clearErrors(); abrirCerrarModalInsertar() }}>
                    Hủy Bỏ
                </Button>
                <Button variant="primary" onClick={() => createProductType()} disabled={!validateForm(artistaSeleccionado.errors)} > Thêm </Button>
            </Modal.Footer>
        </Modal>
    )

    const bodyEdit = (
        <Modal
            size="lg"
            show={modalEditar}
            onHide={() => abrirCerrarModalEditar()}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header >
                <Modal.Title>Sửa Thông Tin</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Container>
                    <Row>
                        <Col md={6}>
                            <label>Mã loại hàng hóa</label>
                            <input type="text" class="form-control" name="code" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.code}></input>
                            {hasError('code')}
                        </Col>
                        <Col md={6}>
                            <label>Tên loại hàng hóa</label>
                            <input type="text" class="form-control" name="name" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.name}></input>
                            {hasError('name')}
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label> Loại Hàng Hóa  </label>
                            <select className="form-control controll" name="productTypeId" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.productTypeId}>
                                {chilProductType.selectProductType.map((option2, index) => (
                                    <option value={option2.value} key={index}> {option2.label}</option>
                                ))}
                            </select>
                        </Col>
                    </Row>
                </Container>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => { clearErrors(); abrirCerrarModalEditar() }}>
                    Hủy Bỏ
                </Button>
                <Button variant="primary" onClick={() => editProductType()} disabled={!validateForm(artistaSeleccionado.errors)}>Sửa</Button>
            </Modal.Footer>
        </Modal>
    )

    const View = (
        <Modal
            backdrop="static"
            keyboard={false}
            size="lg"
            show={modalViewInfor}
            onHide={() => viewInformation()}
        >
            <Modal.Header >
                <Modal.Title>Xem Thông Tin Chi Tiết</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Container>
                    <Row>
                        <Col md={6}>
                            <label>Mã loại hàng hóa</label>
                            <input type="text" class="form-control" name="code" value={artistaSeleccionado && artistaSeleccionado.code}></input>
                        </Col>
                        <Col md={6}>
                            <label>Tên loại hàng hóa</label>
                            <input type="text" class="form-control" name="name" value={artistaSeleccionado && artistaSeleccionado.name}></input>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label> Loại Hàng Hóa  </label>
                            <select className="form-control controll" name="productTypeId" value={artistaSeleccionado && artistaSeleccionado.productTypeId}>
                                {chilProductType.selectProductType.map((option2, index) => (
                                    <option value={option2.value} key={index}> {option2.label}</option>
                                ))}
                            </select>                        </Col>
                        <Col md={6}>
                            <label > Ngày Tạo </label>
                            <input type="text" class="form-control" name="createdAt" value={artistaSeleccionado && artistaSeleccionado.createdAt}></input>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label>Người sửa </label>
                            <input type="text" class="form-control" name="updatedBy" value={artistaSeleccionado && artistaSeleccionado.updatedBy && artistaSeleccionado.updatedBy.username}></input>
                        </Col>
                        <Col md={6}>
                            <label > Người tạo </label>
                            <input type="text" class="form-control" name="createdBy" value={artistaSeleccionado && artistaSeleccionado.createdBy && artistaSeleccionado.createdBy.username}></input>
                        </Col>

                    </Row>
                    <Row>
                        <Col md={6}>
                            <label > Ngày sửa </label>
                            <input type="text" class="form-control" name="updatedAt" value={artistaSeleccionado && artistaSeleccionado.updatedAt}></input>
                        </Col>
                    </Row>
                </Container>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => viewInformation()}>
                    Hủy Bỏ
                </Button>
            </Modal.Footer>
        </Modal>
    )

    const Delete = (
        <Modal
            show={modalEliminar}
            onHide={() => abrirCerrarModalEliminar()}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header >
                <Modal.Title>Xóa</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p> Bạn có chắc chắn muốn xóa không ? </p>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => abrirCerrarModalEliminar()}>
                    Không
                </Button>
                <Button variant="primary" onClick={() => deleteProductType()}>Có</Button>
            </Modal.Footer>
        </Modal>
    )

    const actions = [
        {
            icon: "add_box",
            tooltip: "Thêm",
            position: "toolbar",
            onClick: () => (hasPerm(user, "ROLE_CREATE_PRODUCT_TYPE") ? abrirCerrarModalInsertar() : missingPermission()),
            position: 'toolbar',
        },
        {
            icon: 'refresh',
            tooltip: 'Refresh',
            position: 'toolbar',
            onClick: () => {
                getProductType()
                getProductTypeChil()
            }
        },
        {
            icon: 'delete',
            tooltip: "Xóa tất cả đang chọn",
            onClick: (event, rowData) => (hasPerm(user, "ROLE_DELETE_PRODUCT_TYPE") ? seleccionarArtista(rowData, "Eliminar") : missingPermission()),
            position: 'toolbarOnSelect'
        },
        {
            icon: 'edit',
            tooltip: 'Sửa thông tin',
            onClick: (event, rowData) => (hasPerm(user, "ROLE_UPDATE_PRODUCT_TYPE") ? seleccionarArtista(rowData, "Editar") : missingPermission()),
            position: 'row'
        },
        {
            icon: 'delete',
            tooltip: 'Xóa thông tin',
            onClick: (event, rowData) => (hasPerm(user, "ROLE_DELETE_PRODUCT_TYPE") ? seleccionarArtista(rowData, "Eliminar") : missingPermission()),
            position: 'row'
        },
        {
            icon: () => <VisibilityIcon />,
            tooltip: 'Xem chi tiết',
            onClick: (event, rowData) => (hasPerm(user, "ROLE_READ_PRODUCT_TYPE") ? seleccionarView(rowData, "check") : missingPermission()),
            position: 'row'
        },
    ]

    return (

        <div className="container-fluid">

            <div className="row">
                <div className="col-md-12">
                    <MaterialTable
                        title="Loại Hàng Hóa"
                        columns={
                            [
                                {
                                    field: 'code', title: 'Mã Loại Hàng Hóa ', headerStyle: {
                                        backgroundColor: '#696969',
                                        color: '#FFF'
                                    },
                                },
                                {
                                    field: 'name', title: ' Tên Loại Hàng Hóa ', headerStyle: {
                                        backgroundColor: '#696969',
                                        color: '#FFF'
                                    },
                                },
                                {
                                    field: 'productType.name', title: 'Loại Hàng Hóa', headerStyle: {
                                        backgroundColor: '#696969',
                                        color: '#FFF'
                                    },
                                }
                            ]
                        }
                        data={data}
                        actions={actions}
                        options={{
                            headerStyle: {
                                backgroundColor: '#696969',
                                color: '#FFF'
                            },
                            rowStyle: {
                                backgroundColor: '#EEE',
                            },
                            actionsColumnIndex: -1,
                            pageSize: 10,
                            pageSizeOptions: [5, 10, 20, 30, 50, 75, 100],
                            selection: true,
                            exportButton: { csv: true },
                            filtering: true,
                        }}
                        parentChildData={(row, rows) => rows.find(a => a.id === row.parentId)}
                        localization={
                            {
                                header: {
                                    actions: 'Hành động'
                                },
                                body: {
                                    editRow: {
                                        deleteText: "Bạn có chắc chắn muốn xóa?"
                                    },
                                    emptyDataSourceMessage: 'Không có dữ liệu để hiển thị ',
                                }
                            }
                        }
                    />
                    {bodyInsert}
                    {View}
                    {bodyEdit}
                    {Delete}
                </div>
            </div>

        </div>
    );
}

export default ProductType;

