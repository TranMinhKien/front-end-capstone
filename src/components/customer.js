import React, {useEffect, useState} from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/js/bootstrap.js";
import axios from "axios";
import MaterialTable from "material-table";
import {makeStyles} from "@material-ui/core/styles";
import "../App.css";
import {DOMAIN_BE} from "../constant/constant";
import {commonFetchDataHandler} from "../utils/index";
import {commonFetchAdd} from "../utils/add";
import {Button, Col, Container, Modal, Row} from "react-bootstrap";
import Select from "react-select";
import authHeader from "../services/auth_header";
import {ErrorMess} from "../utils/MessageErrors";
import {missingPermission} from "../utils/errorMessage";
import AuthService from "../services/auth.service";
import validation from "../utils/validation";
import VisibilityIcon from "@material-ui/icons/Visibility";
import {toastSuccess} from "../utils/MessageSuccess";
import Tableb from "v2/components/table";
import {useHistory} from "react-router";
import {CloseOutlined, EditOutlined, EyeOutlined} from "@ant-design/icons";
import {convertColumn, convertListname} from "v2/components/columns";

// API
const baseURL = DOMAIN_BE + "/api/customer/";
const baseURLsource = DOMAIN_BE + "/api/source/name";
const baseURLcareers = DOMAIN_BE + "/api/career/name";
const baseURLclassification = DOMAIN_BE + "/api/classification/name";
const baseURLtype = DOMAIN_BE + "/api/type/name";
const baseURLfield = DOMAIN_BE + "/api/field/name";

const useStyles = makeStyles((theme) => ({
    modal: {
        position: "absolute",
        width: 1200,
        backgroundColor: theme.palette.background.paper,
        border: "1px solid #000",
        boxShadow: "20px 10px",
        padding: "1px 1px 1px 1px",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)"
    },
    iconos: {
        cursor: "pointer"
    },
    inputMaterial: {
        width: "100%",
        //  padding: '3px 10px',
        margin: "3px 0"
    },
    inputMaterial2: {
        color: "blue"
    }
}));

const validateForm = (errors) => {
    let valid = true;
    if (errors == null) return true;
    Object.values(errors).forEach((val) => val.length > 0 && (valid = false));
    return valid;
};

const validEmailRegex = RegExp(
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
);
const validPhoneRegex = RegExp(/^\d*$/);

function Customer() {
    const styles = useStyles();
    const [data, setData] = useState([]);
    const [modalInsertar, setModalInsertar] = useState(false);
    const [modalEditar, setModalEditar] = useState(false);
    const [modalEliminar, setModalEliminar] = useState(false);
    const [modalViewInfor, setModalViewInfor] = useState(false);
    const [selectedRow, setSelectedRow] = useState(null);
    const history = useHistory();

    // lấy thông tin USER
    const user = AuthService.getCurrentUser();

    const [artistaSeleccionado, setArtistaSeleccionado] = useState({
        code: null,
        name: null,
        taxCode: null,
        phone: null,
        email: null,
        shortName: null,
        sourceId: null,
        classificationIds: [],
        fieldIds: [],
        typeId: null,
        careerIds: [],
        address: null,
        errors: {
            code: "",
            name: "",
            taxCode: "",
            phone: "",
            email: "",
            address: ""
        }
    });

    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const hasPerm = (user, perm = "") => {
        return user && user.roles && user.roles.includes(perm);
    };

    const [source3, setSource3] = useState({
        selectSource: [],
        id: "",
        name: ""
    });

    const [career3, setCareer3] = useState({
        selectCareer: [],
        id: "",
        name: ""
    });

    const [type3, setType3] = useState({
        selectType: [],
        id: "",
        name: ""
    });

    const [fields3, SetFields3] = useState({
        selectFields: [],
        id: "",
        name: ""
    });

    const [classifications3, setClassifications3] = useState({
        selectClassification: [],
        id: "",
        name: ""
    });

    const clearErrors = () => {
        setArtistaSeleccionado((prevState) => ({
            ...prevState,
            errors: {}
        }));
    };

    const handleChange = (e) => {
        const {name, value} = e.target;
        let errors =
            artistaSeleccionado.errors || (artistaSeleccionado.errors = {});
        validation.value = value;

        switch (name) {
            case "name":
                errors.name = validation.validate("Tên", [validation.notBlank]);
                break;
            case "email":
                errors.email = validation.validate("Địa chỉ email", [
                    validation.email,
                    validation.notBlank
                ]);
                break;
            case "taxCode":
                errors.taxCode = validation.validate("Mã số thuế", [
                    validation.number,
                    validation.notBlank
                ]);
                break;
            case "phone":
                errors.phone = validation.validate("Số điện thoại", [validation.phone]);
                break;
            case "address":
                break;
            case "code":
                errors.code = validation.validate("Mã ", [validation.notBlank]);
                break;
            default:
                break;
        }

        setArtistaSeleccionado((prevState) => ({
            ...prevState,
            [name]: value || null
        }));
    };

    const handleChange2 = (e) => {
        artistaSeleccionado.classificationIds = Array.isArray(e)
            ? e.map((x) => x.value)
            : [];
        artistaSeleccionado.classifications = Array.isArray(e)
            ? e.map((x) => x)
            : [];
    };

    const handleChangefields = (e) => {
        artistaSeleccionado.fieldIds = Array.isArray(e)
            ? e.map((x) => x.value)
            : [];
        artistaSeleccionado.fields = Array.isArray(e) ? e.map((x) => x) : [];
    };

    const handleChangecareer = (e) => {
        artistaSeleccionado.careerIds = Array.isArray(e)
            ? e.map((x) => x.value)
            : [];
        artistaSeleccionado.careers = Array.isArray(e) ? e.map((x) => x) : [];
    };
    const viewInformation = () => {
        setModalViewInfor(!modalViewInfor);
    };

    const createCustomer = async () => {
        // Check validate khi ấn "Thêm"
        for (const name in artistaSeleccionado) {
            const value = artistaSeleccionado[name];
            handleChange({
                target: {name, value}
            });
        }
        // Nếu chưa valid thì không thêm dữ liệu
        if (!validateForm(artistaSeleccionado.errors)) return;
        commonFetchAdd(baseURL, artistaSeleccionado)
            .then((responseSource) => {
                const d = responseSource.data;
                d.sourceId = d.source ? d.source.id : null;
                d.typeId = d.type ? d.type.id : null;
                d.classificationIds = d.classification ? d.classification.id : null;
                d.fieldIds = d.field ? d.field.id : null;
                d.careerIds = d.career ? d.career.id : null;
                setData(data.concat(d));
                abrirCerrarModalInsertar();
                toastSuccess("Thêm dữ liệu thành công");
            })
            .catch((error) => {
                if (
                    error.response &&
                    error.response.data &&
                    error.response.data.errors &&
                    error.response.data.errors[0].type === "duplicate"
                ) {
                    let errors =
                        artistaSeleccionado.errors || (artistaSeleccionado.errors = {});
                    error.response.data.errors.forEach((e) => {
                        errors[e.field] = "Giá trị đã bị trùng";
                    });
                    setArtistaSeleccionado((prevState) => ({
                        ...prevState,
                        errors: errors
                    }));
                    console.log(artistaSeleccionado);
                } else {
                    ErrorMess(error);
                }
            });
    };

    const editCustomer = async () => {
        await axios
            .put(baseURL + artistaSeleccionado.id, artistaSeleccionado, {
                headers: {Authorization: authHeader().Authorization}
            })
            .then((response) => {
                const dataNueva = data;
                dataNueva.map((member) => {
                    if (member.id === artistaSeleccionado.id) {
                        member.phone = artistaSeleccionado.phone;
                        member.taxCode = artistaSeleccionado.taxCode;
                        member.email = artistaSeleccionado.email;
                        member.taxCode = artistaSeleccionado.taxCode;
                        member.address = artistaSeleccionado.address;
                        // TODO 2
                        member.source = response.data.source;
                        member.sourceId = member.source?.id;
                        member.type = response.data.type;
                        member.typeId = member.type?.id;

                        member.careerIds = artistaSeleccionado.careerIds;
                        member.classificationIds = artistaSeleccionado.classificationIds;
                        member.fieldIds = artistaSeleccionado.fieldIds;
                        member.code = artistaSeleccionado.code;
                        member.name = artistaSeleccionado.name;
                    }
                });
                setData(dataNueva);
                abrirCerrarModalEditar();
                toastSuccess("Edit dữ liệu thành công");
            })
            .catch((error) => {
                ErrorMess(error);
            });
    };

    const deleteCustomer = async () => {
        const ids = Array.isArray(artistaSeleccionado)
            ? artistaSeleccionado.map((a) => a.id)
            : [artistaSeleccionado.id];
        await axios
            .delete(baseURL + ids.join(","), {
                headers: {Authorization: authHeader().Authorization}
            })
            .then((response) => {
                toastSuccess("Xóa dữ liệu thành công");
                const newData = data;
                setData(data.filter((d) => !ids.includes(d.id)));
                abrirCerrarModalEliminar();
            })
            .catch((error) => {
                ErrorMess(error);
            });
    };

    const settingModalCustomer = (id, caso) => {
        setArtistaSeleccionado(id);
        caso === "Editar" ? abrirCerrarModalEditar() : abrirCerrarModalEliminar();
    };

    const getCustomer = async () => {
        // TODO 3

        const response = await commonFetchDataHandler(baseURL);
        response.data.forEach((element) => {
            element.sourceId = element.source ? element.source.id : null;
            element.typeId = element.type ? element.type.id : null;
            // element.classificationIds = element.classification ? element.classification.id : null
            // element.fieldIds = element.field ? element.field.id : null
            // element.careerIds = element.career ? element.career.id : null
            element.classificationIds = element.classifications
                ? element.classifications.map((i) => i?.id)
                : [];
            element.fieldIds = element.fields ? element.fields.map((i) => i?.id) : [];
            element.careerIds = element.careers
                ? element.careers.map((i) => i?.id)
                : [];
        });
        setData(response.data);
    };

    const getCareer = async () => {
        const responseCareer = await commonFetchDataHandler(baseURLcareers);
        const optionsCareer = responseCareer.data.map((d) => ({
            value: d.id,
            label: d.name
        }));
        setCareer3({selectCareer: optionsCareer});
    };

    const getType = async () => {
        const responseType = await commonFetchDataHandler(baseURLtype);
        const optionsType = responseType.data.map((d) => ({
            value: d.id,
            label: d.name
        }));
        // TODO 4
        optionsType.unshift({
            value: "",
            label: "-- Không chọn --"
        });
        setType3({selectType: optionsType});
    };
    const getSource = async () => {
        const responseSource = await commonFetchDataHandler(baseURLsource);
        const optionsSource = responseSource.data.map((d) => ({
            value: d.id,
            label: d.name
        }));
        optionsSource.unshift({
            value: "",
            label: "-- Không chọn --"
        });
        setSource3({selectSource: optionsSource});
    };

    const getClasstification = async () => {
        const responseClasstification = await commonFetchDataHandler(
            baseURLclassification
        );
        const optionsClasstification = responseClasstification.data.map((d) => ({
            value: d.id,
            label: d.name
        }));
        setClassifications3({selectClassification: optionsClasstification});
    };

    const getFields = async () => {
        const responseFiels = await commonFetchDataHandler(baseURLfield);
        const optionsFiels = responseFiels.data.map((d) => ({
            value: d.id,
            label: d.name
        }));
        SetFields3({selectFields: optionsFiels});
    };

    useEffect(() => {
        // const fetchData = async () => {
        getCustomer();
        getCareer();
        getFields();
        getType();
        getClasstification();
        getSource();
        // }
        // fetchData()
    }, []);

    const seleccionarView = (id, caso) => {
        setArtistaSeleccionado(id);
        if (caso === "check") {
            viewInformation();
        }
    };
    // cái này insert
    const abrirCerrarModalInsertar = () => {
        setModalInsertar(!modalInsertar);
    };
    // cái này là edit
    const abrirCerrarModalEditar = () => {
        setModalEditar(!modalEditar);
    };
    // cái này là xóa
    const abrirCerrarModalEliminar = () => {
        setModalEliminar(!modalEliminar);
    };

    const ListCareers = (data) => {
        let a = [];
        data?.map((o) => a.push(o.name));
        return a;
    };

    const hasError = (name) => {
        return (
            artistaSeleccionado &&
            artistaSeleccionado.errors &&
            artistaSeleccionado.errors[name] &&
            artistaSeleccionado.errors[name].length > 0 && (
                <span className="error">{artistaSeleccionado.errors[name]}</span>
            )
        );
    };

    const bodyInsert = (
        <Modal
            size="lg"
            show={modalInsertar}
            onHide={() => abrirCerrarModalInsertar()}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header>
                <Modal.Title>Thêm Khách Hàng </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Container>
                    <Row>
                        <Col md={6}>
                            <label className="required">Mã khách hàng</label>
                            <input
                                type="text"
                                class="form-control"
                                name="code"
                                onChange={handleChange}
                            ></input>
                            {hasError("code")}
                        </Col>
                        <Col md={6}>
                            <label className="required">Tên Khách Hàng</label>
                            <input
                                type="text"
                                class="form-control"
                                name="name"
                                onChange={handleChange}
                            ></input>
                            {hasError("name")}
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label>Điện thoại</label>
                            <input
                                type="text"
                                class="form-control"
                                name="phone"
                                onChange={handleChange}
                            ></input>
                            {hasError("phone")}
                        </Col>
                        <Col md={6}>
                            <label className="required">Mã số thuế</label>
                            <input
                                type="text"
                                class="form-control"
                                name="taxCode"
                                onChange={handleChange}
                            ></input>
                            {hasError("taxCode")}
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label className="required">Email</label>
                            <input
                                type="text"
                                class="form-control"
                                name="email"
                                onChange={handleChange}
                            ></input>
                            {hasError("email")}
                        </Col>
                        <Col md={6}>
                            <label>Địa chỉ</label>
                            <input
                                type="text"
                                class="form-control"
                                name="address"
                                onChange={handleChange}
                            ></input>
                            {hasError("address")}
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label>Nguồn gốc </label>
                            <select
                                className="form-control controll formsingle"
                                placeholder="hhhh"
                                name="sourceId"
                                onChange={handleChange}
                            >
                                {source3.selectSource.map((option2, index) => (
                                    <option value={option2.value} key={index}>
                                        {" "}
                                        {option2.label}
                                    </option>
                                ))}
                            </select>
                        </Col>
                        <Col md={6}>
                            <label>Nghề Nghiệp </label>
                            <Select
                                className="dropdown controll1 formselect"
                                placeholder="Nghề Nghiệp"
                                isMulti
                                options={career3.selectCareer}
                                onChange={handleChangecareer}
                            ></Select>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label>Loại Hình</label>
                            <select
                                className="form-control controll formsingle"
                                name="typeId"
                                onChange={handleChange}
                            >
                                {type3.selectType.map((option2, index) => (
                                    <option value={option2.value} key={index}>
                                        {" "}
                                        {option2.label}
                                    </option>
                                ))}
                            </select>
                        </Col>
                        <Col md={6}>
                            <label>Lĩnh Vực </label>
                            <Select
                                className="dropdown controll1 formselect"
                                placeholder="Lĩnh Vực"
                                isMulti
                                options={fields3.selectFields}
                                onChange={handleChangefields}
                                defaultValue={fields3.selectFields[1]}
                            ></Select>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label>Phân Loại Khách Hàng </label>
                            <Select
                                className="dropdown controll1 formselect"
                                placeholder="Phân Loại Khách Hàng"
                                isMulti
                                options={classifications3.selectClassification}
                                onChange={handleChange2}
                                defaultValue={classifications3.selectClassification[1]}
                            ></Select>
                        </Col>
                    </Row>
                </Container>
            </Modal.Body>
            <Modal.Footer>
                <Button
                    variant="secondary"
                    onClick={() => {
                        clearErrors();
                        abrirCerrarModalInsertar();
                    }}
                >
                    Hủy Bỏ
                </Button>
                <Button
                    variant="primary"
                    onClick={() => createCustomer()}
                    disabled={!validateForm(artistaSeleccionado.errors)}
                >
                    {" "}
                    Thêm{" "}
                </Button>
            </Modal.Footer>
        </Modal>
    );

    const View = (
        <Modal
            size="lg"
            show={modalViewInfor}
            onHide={() => viewInformation()}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header>
                <Modal.Title>Xem Thông Tin Chi Tiết</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Container>
                    <Row>
                        <Col md={6}>
                            <label className="required">Mã khách hàng</label>
                            <input
                                type="text"
                                class="form-control"
                                name="code"
                                value={artistaSeleccionado && artistaSeleccionado.code}
                            ></input>
                        </Col>
                        <Col md={6}>
                            <label className="required">Tên Khách Hàng</label>
                            <input
                                type="text"
                                class="form-control"
                                name="name"
                                value={artistaSeleccionado && artistaSeleccionado.name}
                            ></input>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label className="required">Điện thoại</label>
                            <input
                                type="text"
                                class="form-control"
                                name="phone"
                                value={artistaSeleccionado && artistaSeleccionado.phone}
                            ></input>
                        </Col>
                        <Col md={6}>
                            <label>Mã số thuế</label>
                            <input
                                type="text"
                                class="form-control"
                                name="taxCode"
                                value={artistaSeleccionado && artistaSeleccionado.taxCode}
                            ></input>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label className="required">Email</label>
                            <input
                                type="text"
                                class="form-control"
                                name="email"
                                value={artistaSeleccionado && artistaSeleccionado.email}
                            ></input>
                        </Col>
                        <Col md={6}>
                            <label>Địa chỉ</label>
                            <input
                                type="text"
                                class="form-control"
                                name="address"
                                value={artistaSeleccionado && artistaSeleccionado.address}
                            ></input>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label>Nguồn gốc </label>
                            <select
                                className="form-control controll formsingle"
                                name="sourceId"
                                value={artistaSeleccionado && artistaSeleccionado.sourceId}
                            >
                                {source3.selectSource.map((option2, index) => (
                                    <option value={option2.value} key={index}>
                                        {" "}
                                        {option2.label}
                                    </option>
                                ))}
                            </select>
                        </Col>
                        <Col md={6}>
                            <label>Nghề Nghiệp</label>
                            <Select
                                className="dropdown controll1 formselect"
                                placeholder="Nghề Nghiệp"
                                isMulti
                                options={career3.selectCareer}
                                defaultValue={
                                    artistaSeleccionado &&
                                    artistaSeleccionado.careers &&
                                    artistaSeleccionado.careers.map((d) => ({
                                        value: d.id ? d.id : d.value,
                                        label: d.id ? d.name : d.label
                                    }))
                                }
                                // value = {artistaSeleccionado.careers}
                            ></Select>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label>Loại Hình</label>
                            <select
                                className="form-control controll formsingle"
                                name="typeId"
                                value={artistaSeleccionado && artistaSeleccionado.typeId}
                            >
                                {type3.selectType.map((option2, index) => (
                                    <option value={option2.value} key={index}>
                                        {" "}
                                        {option2.label}
                                    </option>
                                ))}
                            </select>
                        </Col>
                        <Col md={6}>
                            <label>Lĩnh Vực</label>
                            <Select
                                className="dropdown controll1 formselect"
                                placeholder="Lĩnh Vực"
                                isMulti
                                options={fields3.selectFields}
                                defaultValue={
                                    artistaSeleccionado &&
                                    artistaSeleccionado.fields &&
                                    artistaSeleccionado.fields.map((d) => ({
                                        value: d.id ? d.id : d.value,
                                        label: d.id ? d.name : d.label
                                    }))
                                }
                            ></Select>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label>Phân Loại Khách Hàng</label>
                            <Select
                                className="dropdown controll1 formselect"
                                placeholder="Phân Loại Khách Hàng"
                                isMulti
                                options={classifications3.selectClassification}
                                defaultValue={
                                    artistaSeleccionado &&
                                    artistaSeleccionado.classifications &&
                                    artistaSeleccionado.classifications.map((d) => ({
                                        value: d.id ? d.id : d.value,
                                        label: d.id ? d.name : d.label
                                    }))
                                }
                            ></Select>
                        </Col>
                        <Col md={6}>
                            <label>Người sửa </label>
                            <input
                                type="text"
                                class="form-control"
                                name="updatedBy"
                                value={
                                    artistaSeleccionado &&
                                    artistaSeleccionado.updatedBy &&
                                    artistaSeleccionado.updatedBy.username
                                }
                            ></input>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label> Ngày Tạo </label>
                            <input
                                type="text"
                                class="form-control"
                                name="createdAt"
                                value={artistaSeleccionado && artistaSeleccionado.createdAt}
                            ></input>
                        </Col>
                        <Col md={6}>
                            <label> Ngày sửa </label>
                            <input
                                type="text"
                                class="form-control"
                                name="updatedAt"
                                value={artistaSeleccionado && artistaSeleccionado.updatedAt}
                            ></input>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label> Người tạo </label>
                            <input
                                type="text"
                                class="form-control"
                                name="createdBy"
                                value={
                                    artistaSeleccionado &&
                                    artistaSeleccionado.createdBy &&
                                    artistaSeleccionado.createdBy.username
                                }
                            ></input>
                        </Col>
                        <Col md={6}></Col>
                    </Row>
                </Container>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => viewInformation()}>
                    Hủy Bỏ
                </Button>
            </Modal.Footer>
        </Modal>
    );

    const bodyEdit = (
        <Modal
            size="lg"
            show={modalEditar}
            onHide={() => abrirCerrarModalEditar()}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header>
                <Modal.Title>Sửa Thông Tin</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Container>
                    <Row>
                        <Col md={6}>
                            <label>Mã khách hàng</label>
                            <input
                                type="text"
                                class="form-control"
                                name="code"
                                onChange={handleChange}
                                value={artistaSeleccionado && artistaSeleccionado.code}
                            ></input>
                            {hasError("code")}
                        </Col>
                        <Col md={6}>
                            <label>Tên Khách Hàng</label>
                            <input
                                type="text"
                                class="form-control"
                                name="name"
                                onChange={handleChange}
                                value={artistaSeleccionado && artistaSeleccionado.name}
                            ></input>
                            {hasError("name")}
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label>Điện thoại</label>
                            <input
                                type="text"
                                class="form-control"
                                name="phone"
                                onChange={handleChange}
                                value={artistaSeleccionado && artistaSeleccionado.phone}
                            ></input>
                            {hasError("phone")}
                        </Col>
                        <Col md={6}>
                            <label>Mã số thuế</label>
                            <input
                                type="text"
                                class="form-control"
                                name="taxCode"
                                onChange={handleChange}
                                value={artistaSeleccionado && artistaSeleccionado.taxCode}
                            ></input>
                            {hasError("taxCode")}
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label>Email</label>
                            <input
                                type="text"
                                class="form-control"
                                name="email"
                                onChange={handleChange}
                                value={artistaSeleccionado && artistaSeleccionado.email}
                            ></input>
                            {hasError("email")}
                        </Col>
                        <Col md={6}>
                            <label>Địa chỉ</label>
                            <input
                                type="text"
                                class="form-control"
                                name="address"
                                onChange={handleChange}
                                value={artistaSeleccionado && artistaSeleccionado.address}
                            ></input>
                            {hasError("address")}
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label>Nguồn gốc </label>
                            <select
                                className="form-control controll1 formsingle"
                                name="sourceId"
                                onChange={handleChange}
                                value={artistaSeleccionado && artistaSeleccionado.sourceId}
                            >
                                {/* <option value= {null}> -- Không chọn -- </option> */}
                                {source3.selectSource.map((option2, index) => (
                                    <option value={option2.value} key={index}>
                                        {" "}
                                        {option2.label}
                                    </option>
                                ))}
                            </select>
                        </Col>
                        <Col md={6}>
                            <label>Nghề Nghiệp</label>
                            <Select
                                className="dropdown controll1 formselect"
                                placeholder="Nghề Nghiệp"
                                isMulti
                                options={career3.selectCareer}
                                onChange={handleChangecareer}
                                defaultValue={
                                    artistaSeleccionado &&
                                    artistaSeleccionado.careers &&
                                    artistaSeleccionado.careers.map((d) => ({
                                        value: d.id ? d.id : d.value,
                                        label: d.id ? d.name : d.label
                                    }))
                                }
                                // value = {artistaSeleccionado.careers}
                            ></Select>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label>Loại Hình</label>
                            <select
                                className="form-control controll1 formsingle"
                                name="typeId"
                                onChange={handleChange}
                                value={artistaSeleccionado && artistaSeleccionado.typeId}
                            >
                                {type3.selectType.map((option2, index) => (
                                    <option value={option2.value} key={index}>
                                        {" "}
                                        {option2.label}
                                    </option>
                                ))}
                            </select>
                        </Col>
                        <Col md={6}>
                            <label>Lĩnh Vực</label>
                            <Select
                                className="dropdown controll1 formselect"
                                placeholder="Lĩnh Vực"
                                isMulti
                                options={fields3.selectFields}
                                onChange={handleChangefields}
                                defaultValue={
                                    artistaSeleccionado &&
                                    artistaSeleccionado.fields &&
                                    artistaSeleccionado.fields.map((d) => ({
                                        value: d.id ? d.id : d.value,
                                        label: d.id ? d.name : d.label
                                    }))
                                }
                            ></Select>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label>Phân Loại Khách Hàng</label>
                            <Select
                                className="dropdown controll1 formselect"
                                placeholder="Phân Loại Khách Hàng"
                                isMulti
                                options={classifications3.selectClassification}
                                onChange={handleChange2}
                                defaultValue={
                                    artistaSeleccionado &&
                                    artistaSeleccionado.classifications &&
                                    artistaSeleccionado.classifications.map((d) => ({
                                        value: d.id ? d.id : d.value,
                                        label: d.id ? d.name : d.label
                                    }))
                                }
                            ></Select>
                        </Col>
                        <Col md={6}></Col>
                    </Row>
                </Container>
            </Modal.Body>
            <Modal.Footer>
                <Button
                    variant="secondary"
                    onClick={() => {
                        clearErrors();
                        abrirCerrarModalEditar();
                    }}
                >
                    Hủy Bỏ
                </Button>
                <Button
                    variant="primary"
                    onClick={() => editCustomer()}
                    disabled={!validateForm(artistaSeleccionado.errors)}
                >
                    Sửa
                </Button>
            </Modal.Footer>
        </Modal>
    );

    const Delete = (
        <Modal
            show={modalEliminar}
            onHide={() => abrirCerrarModalEliminar()}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header>
                <Modal.Title>Xóa</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p> Bạn có chắc chắn muốn xóa không ? </p>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => abrirCerrarModalEliminar()}>
                    Không
                </Button>
                <Button variant="primary" onClick={() => deleteCustomer()}>
                    Có
                </Button>
            </Modal.Footer>
        </Modal>
    );
    const lookUp = (e) =>
        e
            ?.map((item) => ({[item.value]: item.label}))
            .reduce(function (result, item) {
                var key = Object.keys(item)[0];
                result[key] = item[key];
                return result;
            }, {});

    const TableCustomer = (
        <MaterialTable
            title="Khách Hàng"
            columns={[
                {
                    field: "code",
                    title: "Mã Khách Hàng",
                    headerStyle: {
                        backgroundColor: "#696969",
                        color: "#FFF"
                    }
                },

                {
                    field: "name",
                    title: "Tên Khách Hàng",
                    headerStyle: {
                        backgroundColor: "#696969",
                        color: "#FFF"
                    }
                },

                {
                    field: "taxCode",
                    title: "Mã số thuế",
                    headerStyle: {
                        backgroundColor: "#696969",
                        color: "#FFF"
                    }
                },

                {
                    field: "phone",
                    title: "Điện Thoại",
                    headerStyle: {
                        backgroundColor: "#696969",
                        color: "#FFF"
                    }
                },

                {
                    field: "email",
                    title: "Email",
                    headerStyle: {
                        backgroundColor: "#696969",
                        color: "#FFF"
                    }
                },
                {
                    field: "address",
                    title: "Địa điểm",
                    headerStyle: {
                        backgroundColor: "#696969",
                        color: "#FFF"
                    }
                },
                {
                    field: "type.id",
                    title: "Loại Hình",
                    headerStyle: {
                        backgroundColor: "#696969",
                        color: "#FFF"
                    },
                    lookup: lookUp(type3?.selectType)
                },
                {
                    field: "source.id",
                    title: "Nguồn gốc",
                    headerStyle: {
                        backgroundColor: "#696969",
                        color: "#FFF"
                    },
                    lookup: lookUp(source3?.selectSource)
                }
            ]}
            data={data}
            actions={[
                {
                    icon: "edit",
                    tooltip: "Sửa thông tin",
                    position: "row",
                    onClick: (event, rowData) =>
                        hasPerm(user, "ROLE_UPDATE_CUSTOMER")
                            ? settingModalCustomer(rowData, "Editar")
                            : missingPermission()
                },
                {
                    icon: "delete",
                    tooltip: "Xóa thông tin",
                    onClick: (event, rowData) =>
                        hasPerm(user, "ROLE_DELETE_CUSTOMER")
                            ? settingModalCustomer(rowData, "Eliminar")
                            : missingPermission(),
                    position: "row"
                },
                {
                    icon: () => <VisibilityIcon/>,
                    tooltip: "Xem chi tiết",
                    onClick: (event, rowData) =>
                        hasPerm(user, "ROLE_READ_CUSTOMER")
                            ? seleccionarView(rowData, "check")
                            : missingPermission(),
                    position: "row"
                },
                {
                    icon: "add_box",
                    tooltip: "Thêm",
                    position: "toolbar",
                    onClick: () =>
                        hasPerm(user, "ROLE_CREATE_CUSTOMER")
                            ? abrirCerrarModalInsertar()
                            : missingPermission()
                },
                {
                    icon: "delete",
                    tooltip: "Xóa tất cả đang chọn",
                    onClick: (event, rowData) =>
                        hasPerm(user, "ROLE_DELETE_CUSTOMER")
                            ? settingModalCustomer(rowData, "Eliminar")
                            : missingPermission(),
                    position: "toolbarOnSelect"
                }
            ]}
            options={{
                rowStyle: (rowData) => ({
                    backgroundColor:
                        selectedRow === rowData.tableData.id ? "#FFFF00" : "#008080"
                }),
                headerStyle: {
                    backgroundColor: "#696969",
                    color: "#FFF"
                },

                actionsColumnIndex: -1,
                pageSize: 10,
                pageSizeOptions: [5, 10, 20, 30, 50, 75, 100],
                exportButton: {csv: true},
                filtering: true,
                selection: true,
                selectionProps: (rowData) => ({
                    disabled: rowData.deleteable === false,
                    color: "primary"
                })
            }}
            onRowClick={(evt, selectedRow) =>
                setSelectedRow(selectedRow.tableData.id)
            }
            localization={{
                header: {
                    actions: "Hành động"
                },
                body: {
                    editRow: {
                        deleteText: "Bạn có chắc chắn muốn xóa?"
                    },
                    emptyDataSourceMessage: "Không có dữ liệu để hiển thị "
                }
            }}
        />
    );
    const listColumn = [
        {
            title: "Mã tổ chức",
            dataIndex: "code",
            // sorter:(a,b) => a.code > b.code
        },
        {
            title: "Tên viết tắt",
            dataIndex: "shortName"
        },
        {
            title: "Tên tổ chức",
            dataIndex: "name"
        },
        {
            title: "Mã số thuế",
            dataIndex: "taxCode"
        },
        {
            title: "Điện thoại",
            dataIndex: "phone"
        },
        {
            title: "Email",
            dataIndex: "email",
            width: 200
        },
        {
            title: "Địa chỉ",
            dataIndex: "address",
            width: 300
        },
        {
            title: "Loại hình",
            dataIndex: "type"
        },
        {
            title: "Ngành nghề",
            dataIndex: "careers",
            width: 250
        },
        {
            title: "Lĩnh vực",
            dataIndex: "field"
        },
        {
            title: "Nguồn gốc",
            dataIndex: "source",
            width: 300
        },
        {
            title: "Hành động",
            key: "operation",
            fixed: "right",
            width: "150px",
            dataIndex: "action"
        }
    ];
    const handleMultiDelete = (selectedRowKeys) => {
        setArtistaSeleccionado(selectedRowKeys);
        abrirCerrarModalEliminar();
    };
    const handlDelete = (data) => {
        setArtistaSeleccionado(data);
        abrirCerrarModalEliminar();
    };
    const onEdit = (id) => {
        history.push(`/customer/edit/${id}`);
    };
    const onView = (id) => {
        history.push(`/customer/view/${id}`);
    };
    const onAddNew = () => {
        hasPerm(user , "ROLE_CREATE_CUSTOMER") ? 
        history.push(`/customer/add`) : missingPermission() ;
        
    };
    const getDataTable = () => {
        let cloneData = data.map((item, index) => {
            return {
                ...item,
                key: index,
                source: item?.source?.name,
                field: convertListname(item?.fields),
                careers: convertListname(item?.careers),
                action: (
                    <div className="d-flex align-items-center p-0 justify-content-between">
                         <EditOutlined
                            className=" btnIcon"
                            onClick={() => {
                                hasPerm(user, "ROLE_UPDATE_CUSTOMER")
                                    ? onEdit(item?.id)
                                    : missingPermission()
                            }}
                            style={{ color: "#C8DF52" }}
                        />
                        <CloseOutlined
                            className="btnIcon"
                            onClick={() => {
                                hasPerm(user, "ROLE_DELETE_CUSTOMER")
                                    ? handlDelete(item)
                                    : missingPermission();
                            }}
                            style={{ color: "#D2042D" }}
                        />
                        <EyeOutlined
                            className=" btnIcon"
                            onClick={() => {
                                hasPerm(user, "ROLE_READ_CUSTOMER")
                                    ? onView(item?.id)
                                    : missingPermission()
                            }}
                            style={{ color: "#B3CDE0" }} />
                    </div>
                )
            };
        });
        return cloneData;
    };
    function convertToOpportunity(rowSelected) {
        if(Array.isArray(rowSelected) && rowSelected.length===1){
            history.push('/opportunity/convert?customerId='+rowSelected[0]?.id)
        }
    }
    return (
        <div className="container-fluid">

            <Tableb
                columns={convertColumn(listColumn)}
                handleMultiDelete={handleMultiDelete}
                data={getDataTable()}
                onAddNew={onAddNew}
                handleConvert={convertToOpportunity}
                titlechucnang = "Tổ chức"
                hasConvert
            />
            {/*{bodyInsert}*/}
            {/*{bodyEdit}*/}
            {/*{View}*/}
            {Delete}
        </div>
    );
}

export default Customer;
