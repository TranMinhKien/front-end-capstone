import React, { Component } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { isEmail } from "validator";
import AuthService from "../services/auth.service";
import "../register.css"
import validation from "../utils/validation";

const required = value => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        Không được bỏ trống!
      </div>
    );
  }
};

const email = value => {
  validation.value = value
  if (validation.validate('Email', [validation.notBlank, validation.email]) !== '') {
    return (
      <div className="alert alert-danger" role="alert">
        Không đúng định dạng email !
      </div>
    );
  }
};

const vusername = value => {
  validation.value = value
  var errors = validation.validate('Tài khoản', [validation.notBlank, validation.length(8, 32), validation.username])
  if (errors !== '') {
    return (
      <div className="alert alert-danger" role="alert">
        {errors}
      </div>
    );
  }
};

const vpassword = value => {
  if (value.length < 8 || value.length > 40) {
    return (
      <div className="alert alert-danger" role="alert">
        Mật Khẩu ít nhất 8 kí tự, nhiều nhất 40 kí tự
      </div>
    );
  }
};

const vpassword1 = value => {
  validation.value = value
  var errors = validation.validate('Mật khẩu', [validation.notBlank, validation.length(8, 40), validation.password])
  if (errors !== '') {
    return (
      <div className="alert alert-danger" role="alert">
        {errors}
      </div>
    );
  }
};

export default class Register extends Component {
  constructor(props) {
    super(props);
    this.handleRegister = this.handleRegister.bind(this);
    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);

    this.state = {
      username: "",
      email: "",
      password: "",
      successful: false,
      message: ""
    };
  }

  onChangeUsername(e) {
    this.setState({
      username: e.target.value
    });
  }

  onChangeEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value
    });
  }

  handleRegister(e) {
    e.preventDefault();

    this.setState({
      message: "",
      successful: false
    });

    this.form.validateAll();

    if (this.checkBtn.context._errors.length === 0) {
      AuthService.register(
        this.state.username,
        this.state.email,
        this.state.password
      ).then(
        response => {
          this.setState({
            message: response.data.message,
            successful: true
          });
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          this.setState({
            successful: false,
            message: resMessage
          });
        }
      );
    }
  }

  render() {
    return (
      <div>
        <section className="vh-100 bg-image" style={{ backgroundImage: 'url("https://mdbcdn.b-cdn.net/img/Photos/new-templates/search-box/img4.jpg")' }}>
          <div className="mask d-flex align-items-center h-500 gradient-custom-3">
            <div className="container h-100">
              <div className="row d-flex justify-content-center align-items-center h-100">
                <div className="col-12 col-md-9 col-lg-7 col-xl-6">
                  <div className="card" style={{ borderRadius: '15px' }}>
                    <div className="card-body p-5">
                      <h2 className="text-uppercase text-center mb-5">Đăng ký</h2>
                      <Form
                        onSubmit={this.handleRegister}
                        ref={c => {
                          this.form = c;
                        }}
                      >
                        {!this.state.successful && (
                          <div>
                            <div className="form-group">
                              <label htmlFor="username">Tài Khoản</label>
                              <Input
                                type="text"
                                className="form-control"
                                name="username"
                                value={this.state.username}
                                onChange={this.onChangeUsername}
                                validations={[required, vusername]}
                              />
                            </div>

                            <div className="form-group">
                              <label htmlFor="email">Email</label>
                              <Input
                                type="text"
                                className="form-control"
                                name="email"
                                value={this.state.email}
                                onChange={this.onChangeEmail}
                                validations={[required, email]}
                              />
                            </div>

                            <div className="form-group">
                              <label htmlFor="password">Mật khẩu </label>
                              <Input
                                type="password"
                                className="form-control"
                                name="password"
                                value={this.state.password}
                                onChange={this.onChangePassword}
                                validations={[required, vpassword, vpassword1]}
                              />
                            </div>

                            <div className="form-group register-form">
                              <button className="btn btn-primary btn-block"> Đăng ký </button>
                            </div>


                          </div>
                        )}

                        {this.state.message && (
                          <div className="form-group">
                            <div
                              className={
                                this.state.successful
                                  ? "alert alert-success"
                                  : "alert alert-danger"
                              }
                              role="alert"
                            >
                              {this.state.message}
                            </div>
                          </div>
                        )}
                        <CheckButton
                          style={{ display: "none" }}
                          ref={c => {
                            this.checkBtn = c;
                          }}
                        />
                      </Form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* <div className="main-w3layouts wrapper">
          <h1> Register </h1>
          <div className="main-agileinfo">
            <div className="agileits-top">
              <Form
                onSubmit={this.handleRegister}
                ref={c => {
                  this.form = c;
                }}
              >
                {!this.state.successful && (
                  <div>
                    <div className="form-group">
                      <label htmlFor="username">Username</label>
                      <Input
                        type="text"
                        className="form-control"
                        name="username"
                        value={this.state.username}
                        onChange={this.onChangeUsername}
                        validations={[required, vusername]}
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="email">Email</label>
                      <Input
                        type="text"
                        className="form-control"
                        name="email"
                        value={this.state.email}
                        onChange={this.onChangeEmail}
                        validations={[required, email]}
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="password">Password</label>
                      <Input
                        type="password"
                        className="form-control"
                        name="password"
                        value={this.state.password}
                        onChange={this.onChangePassword}
                        validations={[required, vpassword]}
                      />
                    </div>

                    <div className="form-group">
                      <button className="btn btn-primary btn-block"> Đăng ký </button>
                    </div>
                  </div>
                )}

                {this.state.message && (
                  <div className="form-group">
                    <div
                      className={
                        this.state.successful
                          ? "alert alert-success"
                          : "alert alert-danger"
                      }
                      role="alert"
                    >
                      {this.state.message}
                    </div>
                  </div>
                )}
                <CheckButton
                  style={{ display: "none" }}
                  ref={c => {
                    this.checkBtn = c;
                  }}
                />
              </Form>
            </div>
          </div>
        </div> */}
      </div>
    );
  }
}