import { CloseOutlined, EditOutlined, EyeOutlined } from "@ant-design/icons";
import { Tooltip } from "antd";
import { makeStyles } from "@material-ui/core/styles";
import VisibilityIcon from "@material-ui/icons/Visibility";
import "antd/dist/antd.css";
import axios from "axios";
import "bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.js";
import React, { useEffect, useState } from "react";
import { Button, Col, Container, Modal, Row } from "react-bootstrap";
import Tableb from "v2/components/table";
import "../App.css";
import { DOMAIN_BE } from "../constant/constant";
import AuthService from "../services/auth.service";
import authHeader from "../services/auth_header";
import { commonFetchAdd } from "../utils/add";
import { missingPermission } from "../utils/errorMessage";
import { commonFetchDataHandler } from "../utils/index";
import { ErrorMess } from "../utils/MessageErrors";
import { toastSuccess } from "../utils/MessageSuccess";
import validation from "../utils/validation";
import { useHistory } from "react-router-dom";
import { convertColumn } from "v2/components/columns";
import { toast } from "react-toastify";
import { potentialSingleConvert } from "v2/api/convertAPI";

const baseURL = DOMAIN_BE + "/api/potential/";
const baseURLsource = DOMAIN_BE + "/api/source/name";
const baseURLcustomer = DOMAIN_BE + "/api/customer/name";

const useStyles = makeStyles((theme) => ({
  modal: {
    position: "absolute",
    width: 1200,
    backgroundColor: theme.palette.background.paper,
    border: "1px solid #000",
    // boxShadow: theme.shadows[5],
    boxShadow: "20px 10px",
    padding: theme.spacing(2, 4, 3),
    // padding: '1px 1px 1px 1px',
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
  iconos: {
    cursor: "pointer",
  },
  inputMaterial: {
    width: "100%",
    //padding: '3px 10px',
    margin: "3px 0",
  },
  inputMaterial2: {
    color: "blue",
  },
}));

const validateForm = (errors) => {
  let valid = true;
  if (errors == null) return true;
  Object.values(errors).forEach((val) => val.length > 0 && (valid = false));
  return valid;
};

const validEmailRegex = RegExp(
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
);
const validPhoneRegex = RegExp(/^\d*$/);

function Potential() {
  const styles = useStyles();
  const [data, setData] = useState([]);
  const [modalInsertar, setModalInsertar] = useState(false);
  const [modalEditar, setModalEditar] = useState(false);
  const [modalEliminar, setModalEliminar] = useState(false);
  const [modalViewInfor, setModalViewInfor] = useState(false);
  const user = AuthService.getCurrentUser();
  const history = useHistory();

  const [artistaSeleccionado, setArtistaSeleccionado] = useState({
    vocative: null,
    lastName: null,
    name: null,
    department: null,
    position: null,
    phone: null,
    customerId: null,
    officePhone: null,
    otherPhone: null,
    sourceId: null,
    email: null,
    taxCode: null,
    address: null,
    errors: {
      lastName: "",
      name: "",
      phone: "",
      officePhone: "",
      otherPhone: "",
      email: "",
      taxCode: "",
      address: "",
    },
  });

  const clearErrors = () => {
    setArtistaSeleccionado((prevState) => ({
      ...prevState,
      errors: {},
    }));
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    let errors =
      artistaSeleccionado.errors || (artistaSeleccionado.errors = {});

    validation.value = value;
    switch (name) {
      case "name":
        errors.name = validation.validate("Tên", [validation.notBlank]);
        break;
      case "email":
        errors.email = validation.validate("Địa chỉ email", [
          validation.notBlank,
          validation.email,
        ]);
        break;
      case "taxCode":
        errors.taxCode = validation.validate("Mã số thuế", [validation.number]);
        break;
      case "phone":
        errors.phone = validation.validate("Số điện thoại", [
          validation.number,
          validation.phone,
        ]);
        break;
      case "officePhone":
        errors.officePhone = validation.validate("Điện thoại cơ quan", [
          validation.number,
          validation.phone,
        ]);
        break;
      case "otherPhone":
        errors.otherPhone = validation.validate("Điện thoại khác", [
          validation.number,
          validation.phone,
        ]);
        break;
      case "address":
        break;
      case "lastName":
        errors.lastName = validation.validate("Họ và Đệm", []);
        break;
      default:
        break;
    }

    setArtistaSeleccionado((prevState) => ({
      ...prevState,
      [name]: value || null,
    }));
    console.log(artistaSeleccionado);
  };

  const vocative3 = ["Ông", "Bà", "Anh", "Chị"].map((d) => ({
    value: d,
    label: d,
  }));
  vocative3.unshift({
    value: "",
    label: "-- Không chọn --",
  });

  const department3 = [
    "Phòng nhân sự",
    "Ban giám đốc",
    "Phòng tài chính",
    "Phòng marketing",
    "Phòng chăm sóc khách hàng",
    "Phòng hàng chính tổng hợp",
    "Phòng kinh doanh",
  ].map((d) => ({
    value: d,
    label: d,
  }));
  department3.unshift({
    value: "",
    label: "-- Không chọn --",
  });

  const position3 = [
    "Trưởng phòng",
    "Chủ tịch",
    "Phó chủ tịch",
    "Tổng giám đốc",
    "Phó tổng giám đốc",
    "Giám đốc",
    "Kế toán trưởng",
    "Trợ lý",
    "Nhân viên",
  ].map((d) => ({
    value: d,
    label: d,
  }));
  position3.unshift({
    value: "",
    label: "-- Không chọn --",
  });

  const [customer3, setCustomer3] = useState({
    selectCustomer: [],
    id: "",
    name: "",
  });
  const getCustomer = async () => {
    // TODO 1
    const responseCustomer = await commonFetchDataHandler(baseURLcustomer);
    const optionsCustomer = responseCustomer.data.map((d) => ({
      value: d.id,
      label: d.name,
    }));
    optionsCustomer.unshift({
      value: "",
      label: "-- Không chọn --",
    });
    setCustomer3({ selectCustomer: optionsCustomer });
  };

  const [source3, setSource3] = useState({
    selectSource: [],
    id: "",
    name: "",
  });

  const getSource = async () => {
    const responseSource = await commonFetchDataHandler(baseURLsource);
    const optionsSource = responseSource.data.map((d) => ({
      value: d.id,
      label: d.name,
    }));
    optionsSource.unshift({
      value: "",
      label: "-- Không chọn --",
    });
    setSource3({ selectSource: optionsSource });
  };

  const getPotential = async () => {
    const response = await commonFetchDataHandler(baseURL);
    response.data.forEach((element) => {
      element.sourceId = element.source ? element.source.id : null;
      element.customerId = element.customer ? element.customer.id : null;
    });
    setData(response.data);
  };

  // insert dữ liệu
  const createPotential = async () => {
    // Check validate khi ấn "Thêm"
    for (const name in artistaSeleccionado) {
      const value = artistaSeleccionado[name];
      handleChange({
        target: { name, value },
      });
    }
    // Nếu chưa valid thì không thêm dữ liệu
    if (!validateForm(artistaSeleccionado.errors)) return;
    // Thêm
    try {
      const responseSource = await commonFetchAdd(baseURL, artistaSeleccionado);
      const d = responseSource.data;
      d.sourceId = d.source ? d.source.id : null;
      d.customerId = d.customer ? d.customer.id : null;
      setData(data.concat(responseSource.data));
      abrirCerrarModalInsertar();
      toastSuccess("Thêm dữ liệu thành công");
    } catch (error) {
      if (
        error &&
        error.response &&
        error.response.data &&
        error.response.data.errors &&
        error.response.data.errors[0].type === "duplicate"
      ) {
        let errors =
          artistaSeleccionado.errors || (artistaSeleccionado.errors = {});
        error.response.data.errors.forEach((e) => {
          errors[e.field] = "Giá trị đã bị trùng";
        });
      } else {
        ErrorMess(error);
      }
    }
  };

  // hiện lên để chỉnh sửa trong database
  const editPotential = async () => {
    await axios
      .put(baseURL + artistaSeleccionado.id, artistaSeleccionado, {
        headers: { Authorization: authHeader().Authorization },
      })
      .then((response) => {
        const dataNueva = data;
        dataNueva.map((id) => {
          if (id.id === artistaSeleccionado.id) {
            id.vocative = artistaSeleccionado.vocative;
            id.lastName = artistaSeleccionado.lastName;
            id.position = artistaSeleccionado.position;
            id.department = artistaSeleccionado.department;
            id.source = response.data.source;
            id.sourceId = id.source?.id;
            id.customer = response.data.customer;
            id.customerId = id.customer?.id;
            id.name = artistaSeleccionado.name;
            id.phone = artistaSeleccionado.phone;
            id.officePhone = artistaSeleccionado.officePhone;
            id.otherPhone = artistaSeleccionado.otherPhone;
            id.address = artistaSeleccionado.address;
            id.email = artistaSeleccionado.email;
            id.taxCode = artistaSeleccionado.taxCode;
          }
        });
        setData(dataNueva);
        abrirCerrarModalEditar();
        toastSuccess("Edit dữ liệu thành công");
      })
      .catch((error) => {
        ErrorMess(error);
      });
  };

  // xóa dữ liệu
  const deletePoyential = async () => {
    const ids = Array.isArray(artistaSeleccionado)
      ? artistaSeleccionado.map((a) => a.id)
      : [artistaSeleccionado.id];
    await axios
      .delete(baseURL + ids.join(","), {
        headers: { Authorization: authHeader().Authorization },
      })
      .then((response) => {
        setData(data.filter((d) => !ids.includes(d.id)));
        abrirCerrarModalEliminar();
        toastSuccess("Xóa dữ liệu thành công");
      })
      .catch((error) => {
        // alert(JSON.stringify(error))
        ErrorMess(error);
      });
  };

  const seleccionarArtista = (id, caso) => {
    setArtistaSeleccionado(id);
    caso === "Editar" ? abrirCerrarModalEditar() : abrirCerrarModalEliminar();
  };
  const seleccionarView = (id, caso) => {
    setArtistaSeleccionado(id);
    if (caso === "check") {
      viewInformation();
    }
  };

  const abrirCerrarModalInsertar = () => {
    setModalInsertar(!modalInsertar);
  };
  const abrirCerrarModalEditar = () => {
    setModalEditar(!modalEditar);
  };
  const abrirCerrarModalEliminar = () => {
    setModalEliminar(!modalEliminar);
  };
  const viewInformation = () => {
    setModalViewInfor(!modalViewInfor);
  };

  useEffect(() => {
    getPotential();
    // getSource();
    // getCustomer();
  }, []);

  const hasError = (name) => {
    return (
      artistaSeleccionado &&
      artistaSeleccionado.errors &&
      artistaSeleccionado.errors[name] &&
      artistaSeleccionado.errors[name].length > 0 && (
        <span className="error">{artistaSeleccionado.errors[name]}</span>
      )
    );
  };

  const bodyInsert = (
    <Modal
      size="lg"
      show={modalInsertar}
      onHide={() => abrirCerrarModalInsertar()}
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header>
        <Modal.Title>Thêm Tiềm Năng</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Container>
          <Row>
            <Col md={6}>
              <label>Họ và Đệm</label>
              <input
                type="text"
                class="form-control"
                name="lastName"
                onChange={handleChange}
              ></input>
              {hasError("lastName")}
            </Col>
            <Col md={6}>
              <label className="required">Tên</label>
              <input
                type="text"
                class="form-control"
                name="name"
                onChange={handleChange}
              ></input>
              {hasError("name")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Mã số thuế</label>
              <input
                type="text"
                class="form-control"
                name="taxCode"
                onChange={handleChange}
              ></input>
              {hasError("taxCode")}
            </Col>
            <Col md={6}>
              <label>Điện thoại</label>
              <input
                type="text"
                class="form-control controll"
                name="phone"
                onChange={handleChange}
              ></input>
              {hasError("phone")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Điện thoại cơ quan</label>
              <input
                type="text"
                class="form-control"
                name="officePhone"
                onChange={handleChange}
              ></input>
              {hasError("officePhone")}
            </Col>
            <Col md={6}>
              <label>Điện thoại khác</label>
              <input
                type="text"
                class="form-control controll"
                name="otherPhone"
                onChange={handleChange}
              ></input>
              {hasError("otherPhone")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label className="required">Email</label>
              <input
                type="email"
                class="form-control"
                name="email"
                onChange={handleChange}
              ></input>
              {hasError("email")}
            </Col>
            <Col md={6}>
              <label>Địa chỉ </label>
              <input
                maxLength="255"
                type="text"
                class="form-control"
                name="address"
                onChange={handleChange}
              ></input>
              {hasError("address")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Xưng hô </label>
              <select
                className="form-control controll"
                name="vocative"
                onChange={handleChange}
              >
                {vocative3.map((option2, index) => (
                  <option value={option2.value} key={index}>
                    {" "}
                    {option2.label}
                  </option>
                ))}
              </select>
            </Col>

            <Col md={6}>
              <label>Nguồn gốc </label>
              <select
                className="form-control controll formsingle"
                name="sourceId"
                onChange={handleChange}
              >
                {source3.selectSource.map((option2, index) => (
                  <option value={option2.value} key={index}>
                    {" "}
                    {option2.label}
                  </option>
                ))}
              </select>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Phòng Ban </label>
              <select
                className="form-control controll formsingle"
                name="department"
                onChange={handleChange}
              >
                {department3.map((option2, index) => (
                  <option value={option2.value} key={index}>
                    {" "}
                    {option2.label}
                  </option>
                ))}
              </select>
            </Col>
            <Col md={6}>
              <label> Chức Danh </label>
              <select
                className="form-control controll formsingle"
                value={artistaSeleccionado.position}
                name="position"
                onChange={handleChange}
              >
                {position3.map((option2, index) => (
                  <option value={option2.value} key={index}>
                    {" "}
                    {option2.label}
                  </option>
                ))}
              </select>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Tổ chức </label>
              <select
                className="form-control controll formsingle"
                name="customerId"
                onChange={handleChange}
              >
                {customer3.selectCustomer.map((option2, index) => (
                  <option value={option2.value} key={index}>
                    {" "}
                    {option2.label}
                  </option>
                ))}
              </select>
            </Col>
          </Row>
        </Container>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={() => {
            clearErrors();
            abrirCerrarModalInsertar();
          }}
        >
          Hủy Bỏ
        </Button>
        <Button
          variant="primary"
          onClick={() => createPotential()}
          disabled={!validateForm(artistaSeleccionado.errors)}
        >
          {" "}
          Thêm{" "}
        </Button>
      </Modal.Footer>
    </Modal>
  );
  const bodyEdit = (
    <Modal
      size="lg"
      show={modalEditar}
      onHide={() => abrirCerrarModalEditar()}
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header>
        <Modal.Title>Sửa Thông Tin</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Container>
          <Row>
            <Col md={6}>
              <label>Họ và Đệm</label>
              <input
                type="text"
                class="form-control"
                name="lastName"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.lastName}
              ></input>
              {hasError("lastName")}
            </Col>
            <Col md={6}>
              <label>Mã số thuế</label>
              <input
                type="text"
                class="form-control"
                name="taxCode"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.taxCode}
              ></input>
              {hasError("taxCode")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label className="required">Tên</label>
              <input
                type="text"
                class="form-control"
                name="name"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.name}
              ></input>
              {hasError("name")}
            </Col>
            <Col md={6}>
              <label>Điện thoại</label>
              <input
                type="text"
                class="form-control"
                name="phone"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.phone}
              ></input>
              {hasError("phone")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Điện thoại cơ quan</label>
              <input
                type="text"
                class="form-control"
                name="officePhone"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.officePhone}
              ></input>
              {hasError("officePhone")}
            </Col>
            <Col md={6}>
              <label>Điện thoại khác</label>
              <input
                type="text"
                class="form-control controll"
                name="otherPhone"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.otherPhone}
              ></input>
              {hasError("otherPhone")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label className="required">Email</label>
              <input
                type="text"
                class="form-control"
                name="email"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.email}
              ></input>
              {hasError("email")}
            </Col>
            <Col md={6}>
              <label>Địa chỉ </label>
              <input
                type="text"
                class="form-control"
                name="address"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.address}
              ></input>
              {hasError("address")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Xưng hô </label>
              <select
                className="form-control controll"
                name="vocative"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.vocative}
              >
                <option value="">-- Không chọn --</option>
                <option value="Ông"> Ông</option>
                <option value="Bà"> Bà</option>
                <option value="Anh"> Anh</option>
                <option value="Chị"> Chị</option>
              </select>
            </Col>
            <Col md={6}>
              <label>Nguồn gốc </label>
              <select
                className="form-control formsingle controll"
                name="sourceId"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.sourceId}
              >
                {source3.selectSource.map((option2, index) => (
                  <option value={option2.value} key={index}>
                    {" "}
                    {option2.label}
                  </option>
                ))}
              </select>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Phòng Ban </label>
              <select
                className="form-control formsingle controll"
                name="department"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.department}
              >
                <option value="">-- Không chọn --</option>
                <option value="Phòng nhân sự"> Phòng nhân sự</option>
                <option value="Ban giám đốc">Ban Giám Đốc</option>
                <option value="Phòng tài chính">Phòng tài chính</option>
                <option value="Phòng marketing">Phòng Marketing</option>
                <option value="Phòng chăm sóc khách hàng">
                  Phòng chăm sóc khách hàng
                </option>
                <option value="Phòng hàng chính tổng hợp">
                  Phòng hàng chính tổng hợp
                </option>
                <option value="Phòng kinh doanh">Phòng Kinh Doanh</option>
              </select>
            </Col>
            <Col md={6}>
              <label> Chức Danh </label>
              <select
                className="form-control formsingle controll"
                value={artistaSeleccionado.position}
                name="position"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.position}
              >
                <option value="">-- Không chọn --</option>
                <option value="Trưởng phòng"> Trưởng Phòng</option>
                <option value="Chủ tịch">Chủ tịch</option>
                <option value="Phó chủ tịch">Phó Chủ Tịch</option>
                <option value="Tổng giám đốc">Tổng Giám Đốc</option>
                <option value="Phó tổng giám đốc">Phó Tổng Giám Đốc</option>
                <option value="Giám đốc">Giám Đốc</option>
                <option value="Kế toán trưởng">Kế Toán Trưởng</option>
                <option value="Trợ lý">Trợ Lý</option>
                <option value="Nhân viên"> Nhân viên</option>
              </select>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Tổ chức </label>
              <select
                className="form-control controll formsingle"
                name="customerId"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.customerId}
              >
                {customer3.selectCustomer.map((option2, index) => (
                  <option value={option2.value} key={index}>
                    {" "}
                    {option2.label}
                  </option>
                ))}
              </select>
            </Col>
          </Row>
        </Container>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={() => {
            clearErrors();
            abrirCerrarModalEditar();
          }}
        >
          Hủy Bỏ
        </Button>
        <Button
          variant="primary"
          onClick={() => editPotential()}
          disabled={!validateForm(artistaSeleccionado.errors)}
        >
          Sửa
        </Button>
      </Modal.Footer>
    </Modal>
  );
  const View = (
    <Modal size="lg" show={modalViewInfor} onHide={() => viewInformation()}>
      <Modal.Header>
        <Modal.Title>Xem Thông Tin Chi Tiết</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Container>
          <Row>
            <Col md={6}>
              <label>Họ và Đệm</label>
              <input
                type="text"
                class="form-control"
                name="lastName"
                value={artistaSeleccionado && artistaSeleccionado.lastName}
              ></input>
            </Col>
            <Col md={6}>
              <label>Mã số thuế</label>
              <input
                type="text"
                class="form-control"
                name="taxCode"
                value={artistaSeleccionado && artistaSeleccionado.taxCode}
              ></input>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Tên</label>
              <input
                type="text"
                class="form-control"
                name="name"
                value={artistaSeleccionado && artistaSeleccionado.name}
              ></input>
            </Col>
            <Col md={6}>
              <label>Điện thoại</label>
              <input
                type="text"
                class="form-control"
                name="phone"
                value={artistaSeleccionado && artistaSeleccionado.phone}
              ></input>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Điện thoại cơ quan</label>
              <input
                type="text"
                class="form-control"
                name="officePhone"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.officePhone}
              ></input>
              {hasError("officePhone")}
            </Col>
            <Col md={6}>
              <label>Điện thoại khác</label>
              <input
                type="text"
                class="form-control controll"
                name="otherPhone"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.otherPhone}
              ></input>
              {hasError("otherPhone")}
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label>Email</label>
              <input
                type="text"
                class="form-control"
                name="email"
                value={artistaSeleccionado && artistaSeleccionado.email}
              ></input>
            </Col>
            <Col md={6}>
              <label>Địa chỉ </label>
              <input
                type="text"
                class="form-control"
                name="address"
                value={artistaSeleccionado && artistaSeleccionado.address}
              ></input>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Xưng hô </label>
              <select
                className="form-control controll"
                name="vocative"
                value={artistaSeleccionado && artistaSeleccionado.vocative}
              >
                <option value="">-- Không chọn --</option>
                <option value="Ông"> Ông</option>
                <option value="Bà"> Bà</option>
                <option value="Anh"> Anh</option>
                <option value="Chị"> Chị</option>
              </select>
            </Col>

            <Col md={6}>
              <label>Nguồn gốc </label>
              <select
                className="form-control formsingle controll"
                name="sourceId"
                value={artistaSeleccionado && artistaSeleccionado.sourceId}
              >
                {source3.selectSource.map((option2, index) => (
                  <option value={option2.value} key={index}>
                    {" "}
                    {option2.label}
                  </option>
                ))}
              </select>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Phòng Ban </label>
              <select
                className="form-control formsingle controll"
                name="department"
                value={artistaSeleccionado && artistaSeleccionado.department}
              >
                <option value="">-- Không chọn --</option>
                <option value="Phòng nhân sự"> Phòng nhân sự</option>
                <option value="Ban giám đốc">Ban Giám Đốc</option>
                <option value="Phòng tài chính">Phòng tài chính</option>
                <option value="Phòng marketing">Phòng Marketing</option>
                <option value="Phòng chăm sóc khách hàng">
                  Phòng chăm sóc khách hàng
                </option>
                <option value="Phòng hàng chính tổng hợp">
                  Phòng hàng chính tổng hợp
                </option>
                <option value="Phòng kinh doanh">Phòng Kinh Doanh</option>
              </select>
            </Col>
            <Col md={6}>
              <label> Chức Danh </label>
              <select
                className="form-control formsingle controll"
                value={artistaSeleccionado.position}
                name="position"
                value={artistaSeleccionado && artistaSeleccionado.position}
              >
                <option value="">-- Không chọn --</option>
                <option value="Trưởng phòng"> Trưởng Phòng</option>
                <option value="Chủ tịch">Chủ tịch</option>
                <option value="Phó chủ tịch">Phó Chủ Tịch</option>
                <option value="Tổng giám đốc">Tổng Giám Đốc</option>
                <option value="Phó tổng giám đốc">Phó Tổng Giám Đốc</option>
                <option value="Giám đốc">Giám Đốc</option>
                <option value="Kế toán trưởng">Kế Toán Trưởng</option>
                <option value="Trợ lý">Trợ Lý</option>
                <option value="Nhân viên"> Nhân viên</option>
              </select>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Tổ chức </label>
              <select
                className="form-control controll formsingle"
                name="customerId"
                onChange={handleChange}
                value={artistaSeleccionado && artistaSeleccionado.customerId}
              >
                {customer3.selectCustomer.map((option2, index) => (
                  <option value={option2.value} key={index}>
                    {" "}
                    {option2.label}
                  </option>
                ))}
              </select>
            </Col>
            <Col md={6}>
              <label> Ngày Tạo </label>
              <input
                type="text"
                class="form-control"
                name="createdAt"
                value={artistaSeleccionado && artistaSeleccionado.createdAt}
              ></input>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Người tạo </label>
              <input
                type="text"
                class="form-control"
                name="createdBy"
                value={
                  artistaSeleccionado &&
                  artistaSeleccionado.createdBy &&
                  artistaSeleccionado.createdBy.username
                }
              ></input>
            </Col>
            <Col md={6}>
              <label> Ngày sửa </label>
              <input
                type="text"
                class="form-control"
                name="updatedAt"
                value={artistaSeleccionado && artistaSeleccionado.updatedAt}
              ></input>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <label> Người Sửa </label>
              <input
                type="text"
                class="form-control"
                name="updatedBy"
                value={
                  artistaSeleccionado &&
                  artistaSeleccionado.updatedBy &&
                  artistaSeleccionado.updatedBy.username
                }
              ></input>
            </Col>
          </Row>
        </Container>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={() => viewInformation()}>
          Hủy Bỏ
        </Button>
      </Modal.Footer>
    </Modal>
  );
  const Delete = (
    <Modal
      show={modalEliminar}
      onHide={() => abrirCerrarModalEliminar()}
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header>
        <Modal.Title>Xóa</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p> Bạn có chắc chắn muốn xóa không ? </p>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={() => abrirCerrarModalEliminar()}>
          Không
        </Button>
        <Button variant="primary" onClick={() => deletePoyential()}>
          Có
        </Button>
      </Modal.Footer>
    </Modal>
  );

  const hasPerm = (user, perm = "") => {
    return user && user.roles && user.roles.includes(perm);
  };

  const actions = [
    {
      icon: "add_box",
      tooltip: "Thêm",
      position: "toolbar",
      onClick: () =>
        hasPerm(user, "ROLE_CREATE_POTENTIAL")
          ? abrirCerrarModalInsertar()
          : missingPermission(),
    },
    {
      icon: "refresh",
      tooltip: "Refresh",
      position: "toolbar",
      onClick: () => {
        getPotential();
        getSource();
        getCustomer();
      },
    },
    {
      icon: "delete",
      tooltip: "Xóa tất cả đang chọn",
      onClick: (event, rowData) =>
        hasPerm(user, "ROLE_DELETE_POTENTIAL")
          ? seleccionarArtista(rowData, "Eliminar")
          : missingPermission(),
      position: "toolbarOnSelect",
    },
    {
      icon: "edit",
      tooltip: "Sửa thông tin",
      onClick: (event, rowData) =>
        hasPerm(user, "ROLE_UPDATE_POTENTIAL")
          ? seleccionarArtista(rowData, "Editar")
          : missingPermission(),
      position: "row",
    },
    {
      icon: "delete",
      tooltip: "Xóa thông tin",
      onClick: (event, rowData) =>
        hasPerm(user, "ROLE_DELETE_POTENTIAL")
          ? seleccionarArtista(rowData, "Eliminar")
          : missingPermission(),
      position: "row",
    },
    {
      icon: () => <VisibilityIcon />,
      tooltip: "Xem chi tiết",
      onClick: (event, rowData) =>
        hasPerm(user, "ROLE_READ_POTENTIAL")
          ? seleccionarView(rowData, "check")
          : missingPermission(),
      position: "row",
    },
  ];

  const listColumn = [
    {
      title: "Xưng hô",
      dataIndex: "vocative",
      width: 100,
      sorter: (a, b) => a.vocative > b.vocative,
    },
    {
      title: "Tên",
      dataIndex: "name",
    },
    {
      title: "Chức danh",
      dataIndex: "position",
    },
    {
      title: "Điện thoại",
      dataIndex: "phone",
    },
    {
      title: "Điện thoại cơ quan",
      dataIndex: "officePhone",
    },
    {
      title: "Email",
      dataIndex: "email",
      width: 200,
    },
    {
      title: "Phòng ban",
      dataIndex: "department",
    },
    {
      title: "Nguồn gốc",
      dataIndex: "source",
      width: 200,
    },
    {
      title: "Tổ chức",
      dataIndex: "customer",
      width: 300,
    },
    {
      title: "Hành động",
      key: "operation",
      fixed: "right",
      width: "150px",
      dataIndex: "action",
    },
  ];

  const handleMultiDelete = (selectedRowKeys) => {
    setArtistaSeleccionado(selectedRowKeys);
    abrirCerrarModalEliminar();
  };
  const handlDelete = (data) => {
    setArtistaSeleccionado(data);
    abrirCerrarModalEliminar();
  };
  const onEdit = (id) => {
    history.push(`/potential/edit/${id}`);
  };
  const onView = (id) => {
    history.push(`/potential/view/${id}`)
  }
  const onAddNew = () => {
    hasPerm(user , "ROLE_CREATE_POTENTIAL") ? 
    history.push(`/potential/add`) : missingPermission() ;
  };
  const handleConvert = (selectedRows) => {
    console.log("selectedRows");
    console.log(selectedRows.length);
    if (selectedRows.length > 1) {
      toastSuccess("Tính năng đang bảo trì");
    } else history.push(`/potential/singleConvert/${selectedRows[0]?.id}`);
  };

  const getDataTable = () => {
    let cloneData = data.map((item, index) => {
      return {
        ...item,
        key: index,
        source: item?.source?.name,
        vocative: item?.vocative?.name,
        position: item?.position?.name,
        department: item?.department?.name,
        action: (
          <div className="d-flex align-items-center p-0 justify-content-between">
           <EditOutlined
              className=" btnIcon"
              onClick=
              { () => {
              hasPerm(user , "ROLE_UPDATE_POTENTIAL") 
              ? onEdit(item.id) 
              : missingPermission()
              }}
              style={{ color: "#C8DF52" }}
            />
            <CloseOutlined
              className="btnIcon"
              onClick={() => {
                hasPerm(user, "ROLE_DELETE_POTENTIAL")
                  ? handlDelete(item)
                  : missingPermission();
              }}
              style={{ color: "#D2042D" }}
            />
            <EyeOutlined
              className=" btnIcon"
              onClick={ () => {
                hasPerm(user , "ROLE_READ_POTENTIAL") 
                ?   onView(item.id)
                :   missingPermission();
              }}
              style={{ color: "#B3CDE0" }}
            />
          </div>
        ),
      };
    });
    return cloneData;
  };

  return (
    <div className="">
      <Tableb
        columns={convertColumn(listColumn)}
        handleMultiDelete={handleMultiDelete}
        data={getDataTable()}
        onAddNew={onAddNew}
        handleConvert={handleConvert}
        titlechucnang="Khách hàng Tiềm Năng"
        hasConvert
      ></Tableb>
      {Delete}
      {/* {bodyInsert}
      {bodyEdit}
      {View}
      {Delete} */}
    </div>
  );
}

export default Potential;
