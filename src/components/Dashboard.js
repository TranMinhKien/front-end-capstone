import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import { Bar } from 'react-chartjs-2';
import Deposits from '../childComponents/Deposite';
import Deposits2 from '../childComponents/Deposite2';
import Deposits3 from '../childComponents/Deposite3';
import Deposite4 from '../childComponents/Deposite4';
import Deposite5 from '../childComponents/Deposite5';
import Deposite6 from '../childComponents/Deposite6';
import Deposite7 from '../childComponents/Deposite7';
import Deposite8 from '../childComponents/Deposite8';
import "../deposite.css"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCog } from "@fortawesome/free-solid-svg-icons";
import { Button } from 'reactstrap';
import { Modal } from 'react-bootstrap';
import { TextField } from '@material-ui/core';
import { DOMAIN_BE } from '../constant/constant';
import { formatDate } from '../utils/date';
import { ParidGet } from '../utils/parid';
import { convertToVND } from '../utils/convert';
import { commonFetchDataHandler } from '../utils';

// API
const baseURL = DOMAIN_BE + "/api/main/overview/order"
const baseURL2 = DOMAIN_BE + "/api/main/overview/opportunity"
const baseURL3 = DOMAIN_BE + "/api/main/revenueByYear"


const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    // background: 'linear-gradient(45deg, #e0e0e0 30%, #e0e0e0 90%)',
    height: 480
  },
  Depositeee: {
    color: "red"
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed

  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '137vh',
    overflow: 'auto',
  },
  container: {
    background: 'linear-gradient(45deg, #fafafa 30%, #fafafa 90%)',
    paddingTop: theme.spacing(0),
    paddingBottom: theme.spacing(4),
    height: 240,
  },
  container1: {
    background: 'linear-gradient(45deg, #fafafa 30%, #fafafa 90%)',
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(4),
    height: 240,
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
    background: 'linear-gradient(45deg, #a5d6a7 30%, #e6ee9c 90%)',
  },
  fixedHeight: {
    height: 150,
  },
  paper2: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
    background: 'linear-gradient(45deg, #bbdefb 30%, #c5cae9 90%)',
  },
  fixedHeight2: {
    height: 150,
  },
  paper3: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
    background: 'linear-gradient(45deg, #84ffff 30%, #a7ffeb 90%)',
  },
  fixedHeight3: {
    height: 150,
  },
  paper4: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
    background: 'linear-gradient(45deg, #a5d6a7 30%, #e6ee9c 90%)',
  },
  fixedHeight4: {
    height: 150,
  },
}));





// BarChart
const stateBanr = {
  labels: ['January', 'February', 'March',
    'April', 'May', 'November', 'Novenber'],
  datasets: [
    {
      label: 'Rainfall',
      backgroundColor: 'rgba(75,192,192,1)',
      borderColor: 'rgba(0,0,0,1)',
      borderWidth: 2,
      data: [65, 59, 80, 81, 56, 70, 80]
    }
  ]
}


// Data Hiện Tại
const dateNow = new Date();
// const materialDateInput = formatDate(dateNow)
const firstDayOfYear = formatDate(new Date(dateNow.getFullYear(), 0, 1))
const lastDayOfYear = formatDate(new Date(dateNow.getFullYear(), 11, 31));

export default function Dashboard() {
  const classes = useStyles();
  // state
  const [data, setData] = useState([]);
  const [data2, setData2] = useState([]);
  const [data3, setData3] = useState([]);

  const [artistaSeleccionado, setArtistaSeleccionado] = useState({
    from: firstDayOfYear,
    to: lastDayOfYear
  })



  // HighChart
  const options = {
    chart: {
      type: "spline"
    },
    title: {
      text: " Thống kê dữ liệu"
    },
    series: [
      {
        data: data3

      }
    ]
  };

  // modal 1 
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  // modal cơ hội
  const [show2, setShow2] = useState(false);
  const handleClose2 = () => setShow2(false);
  const handleShow2 = () => setShow2(true);



  const handleChange = e => {
    const { name, value } = e.target;
    setArtistaSeleccionado(prevState => ({
      ...prevState,
      [name]: value
    }));
    console.log(artistaSeleccionado);
  }

  
 


  const getParid = async () => {
    console.log(artistaSeleccionado.from)
    const response = await ParidGet(baseURL, { from: artistaSeleccionado.from, to: artistaSeleccionado.to })
    setData(response.data)
  }

  const getParid2 = async () => {
    console.log(artistaSeleccionado.from)
    const response = await ParidGet(baseURL2, { from: artistaSeleccionado.from, to: artistaSeleccionado.to })
    setData2(response.data)
  }

  const getParid3 = async () => {
    const response = await ParidGet(baseURL3, { year: 2021})
    setData3(response.data)
  }

  // Hight chart




  useEffect(() => {
    const fetchData = async () => {
      getParid();
      getParid2();
      getParid3();
    }
    fetchData()
  }, [])



  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
  const fixedHeightPaper2 = clsx(classes.paper2, classes.fixedHeight2);
  const fixedHeightPaper3 = clsx(classes.paper3, classes.fixedHeight3);
  const fixedHeightPaper4 = clsx(classes.paper4, classes.fixedHeight4);

  const Modaldate = (
    <Modal
      show={show}
      backdrop="static"
      keyboard={false} 
      onHide={handleClose}
      >
      <Modal.Header >
        <Modal.Title>Tham số báo cáo </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div class="container">
          <div class="row">
            <div class="col-md 4">
              <TextField
                name="from"
                label="Từ Ngày "
                // InputLabelProps={{ shrink: true, required: true }}
                type="date"
                defaultValue={firstDayOfYear}
                onChange={handleChange}
              />
            </div>
            <div class="col-md-6">
              <TextField
                name="to"
                label="Đến Ngày"
                // InputLabelProps={{ shrink: true, required: true }}
                type="date"
                defaultValue={lastDayOfYear}
                onChange={handleChange}
              />
            </div>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
        <Button variant="primary" onClick={() => getParid()}>
          Save Changes
        </Button>
      </Modal.Footer>
    </Modal>
  )
  const Modaldate2 = (
    <Modal
      show={show2}
      backdrop="static"
      keyboard={false} 
      onHide={handleClose2}
      >
      <Modal.Header >
        <Modal.Title>Tham số báo cáo cơ hội </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div class="container">
          <div class="row">
            <div class="col-md 4">
              <TextField
                name="from"
                label="Từ Ngày "
                type="date"
                defaultValue={firstDayOfYear}
                onChange={handleChange}
              />
            </div>
            <div class="col-md-6">
              <TextField
                name="to"
                label="Đến Ngày"
                type="date"
                defaultValue={lastDayOfYear}
                onChange={handleChange}
              />
            </div>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose2}>
          Close
        </Button>
        <Button variant="primary" onClick={() => getParid2()}>
          Save Changes
        </Button>
      </Modal.Footer>
    </Modal>
  )
 

  return (
    <div className={classes.root}>
      <CssBaseline />
      {/* // bao quanh nội dung */}
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        {/* container này màu trắng bao quanh cái kia rồi */}
        <Container maxWidth="false" className={classes.container}>

          <label className="setting-icon">Tổng quan</label>
        
          <Button onClick={handleShow} className="facebookbutton2">
            <FontAwesomeIcon icon={faCog} />
          </Button>

          <label className="setting-date">{data.from} - </label>
          <label className="setting-date">{data.to}</label>

          <Grid container spacing={3}>
            <Grid item xs={12} md={3} lg={3}>
              <Paper className={fixedHeightPaper}>
                <Deposits quantity={data.quantity} />
              </Paper>
            </Grid>
            <Grid item xs={12} md={3} lg={3}>
              <Paper className={fixedHeightPaper2}>
                <Deposits2 turnOver={convertToVND(data.turnOver)} />
              </Paper>
            </Grid>
            <Grid item xs={12} md={3} lg={3}>
              <Paper className={fixedHeightPaper3}>
                <Deposits3 recordedQuantity={data.recordedQuantity} />
              </Paper>
            </Grid>
            <Grid item xs={12} md={3} lg={3}>
              <Paper className={fixedHeightPaper4}>
                <Deposite4 recordedTurnOver={convertToVND(data.recordedTurnOver)} />
              </Paper>
            </Grid>
          </Grid>
        </Container>

        <Container maxWidth="false" className={classes.container1}>
          <label className="setting-icon">Tổng quan</label>

          <Button onClick={handleShow2} className="facebookbutton2">
            <FontAwesomeIcon icon={faCog} />
          </Button>

          <label className="setting-date">{data2.from} - </label>
          <label className="setting-date">{data2.to}</label>


          <Grid container spacing={3}>
            <Grid item xs={12} md={3} lg={3}>
              <Paper className={fixedHeightPaper}>
                <Deposite5 numberOfOportunitiesInProgress={data2.numberOfOportunitiesInProgress} />
              </Paper>
            </Grid>
            <Grid item xs={12} md={3} lg={3}>
              <Paper className={fixedHeightPaper2}>
                <Deposite6 doneTurnOver={convertToVND(data2.doneTurnOver)} />
              </Paper>
            </Grid>
            <Grid item xs={12} md={3} lg={3}>
              <Paper className={fixedHeightPaper3}>
                <Deposite7 expectedTurnOver={convertToVND(data2.expectedTurnOver)} />
              </Paper>
            </Grid>
            <Grid item xs={12} md={3} lg={3}>
              <Paper className={fixedHeightPaper4}>
                <Deposite8 opportunityWinRate={data2.opportunityWinRate} />
              </Paper>
            </Grid>
          </Grid>
        </Container>

        <Container maxWidth="false" className={classes.container1}>
          {/* <Grid container spacing={6}>
            <Grid item xs={12} md={6} >
              <Paper className={fixedHeightPaper}>
                 <HighchartsReact highcharts={Highcharts} options={options} />
              </Paper>
            </Grid> */}
          <label className="setting-icon1">Tình hình thực hiện mục tiêu doanh số theo năm</label>
          <Grid container spacing={6}>
            <Grid item xs={12} md={12} >
              <Paper >
                <HighchartsReact highcharts={Highcharts} options={options} />
              </Paper>
            </Grid>
            {/* <Grid item xs={12} md={6} >
              <Paper >
                <Bar
                  data={stateBanr}
                  options={{
                    title: {
                      display: true,
                      text: 'Average Rainfall per month',
                      fontSize: 20
                    },
                    legend: {
                      display: true,
                      position: 'right'
                    }
                  }}
                />
              </Paper>
            </Grid> */}
          </Grid>
        </Container>
      </main>
      {Modaldate}
      {Modaldate2}
    </div>
  );
}