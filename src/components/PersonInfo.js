import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import axios from 'axios';
import authHeader from '../services/auth_header';
import { Button, Modal, Container, Row, Col } from 'react-bootstrap';
import { DOMAIN_BE } from '../constant/constant';
import { commonFetchDataHandler } from '../utils/index'
import AuthService from "../services/auth.service";
import validation from '../utils/validation';
import "../person.css"
import { dateFormat } from 'highcharts';
import { toastSuccess } from '../utils/MessageSuccess';
import { ErrorMess } from '../utils/MessageErrors';

// API
const baseURL = DOMAIN_BE + "/api/profile";



const validateForm = (errors) => {
    let valid = true;
    if (errors == null)
        return true;
    Object.values(errors).forEach(
        (val) => val.length > 0 && (valid = false)
    );
    return valid;
}

const validEmailRegex = RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
const validPhoneRegex = RegExp(/^\d*$/)
function PersonInfo() {
    const [data, setData] = useState([]);
    // lấy thông tin USER
    const user = AuthService.getCurrentUser();
    const [artistaSeleccionado, setArtistaSeleccionado] = useState({
        lastName: null,
        name: null,
        phone: null,
        dateOfBirth: null,
        gender: null,
        address: null,
        email: null,
        username : null,
        errors: {
            lastName: '',
            name: '',
            phone: '',
            dateOfBirth: '',
            gender: '',
            address: ''
        }
    });

   
    const handleChange = e => {
        const { name, value } = e.target;
        let errors = artistaSeleccionado.errors || (artistaSeleccionado.errors = {})

        validation.value = value;
        switch (name) {
            case 'lastName':
                errors.lastName = validation.validate('Họ và Đệm ', [validation.notBlank])
                break;
            case 'name':
                errors.name = validation.validate('Tên', [validation.notBlank])
                break;
            case 'phone':
                errors.phone = validation.validate('Điện thoại', [validation.number, validation.phone])
                break;
            case 'dateOfBirth':
                break;
            case 'gender':
                break;
            case 'address':
                errors.address = validation.validate('Địa chỉ ', [validation.notBlank])
                break;
            default:
                break;
        }
        setArtistaSeleccionado(prevState => ({
            ...prevState,
            [name]: value || null
        }));
        console.log(artistaSeleccionado);
    }

    // lấy value từ API
    const getPersonInfor = async () => {
        const response = await commonFetchDataHandler(baseURL)
        if (response.data) {
            setData(response.data)
            const newData = response.data
            newData.gender = response.data.gender ? response.data.gender.id : null
            newData.errors = artistaSeleccionado.errors
            setArtistaSeleccionado(newData)
        }
    }

    const hasError = (name) => {
        return artistaSeleccionado && artistaSeleccionado.errors && artistaSeleccionado.errors[name] &&
            artistaSeleccionado.errors[name].length > 0 &&
            (<span className='error'>{artistaSeleccionado.errors[name]}</span>)
    }

    // Chỉnh sửa dữ liệu từ API 
    const editPersonInfor = async () => {
        await axios.put(baseURL, artistaSeleccionado, { headers: { 'Authorization': authHeader().Authorization } })
            .then(response => {
                const id = response.data;
                if (id && id.username) {
                    id.lastName = data.lastName;
                    id.name = data.name;
                    id.email = data.email;
                    id.address = data.address;
                    id.dateOfBirth = data.dateOfBirth;
                    id.gender = data.gender;
                    id.phone = data.phone;
                    id.username = data.username;
                }
                setData(id);
                toastSuccess("Edit dữ liệu thành công");
            }).catch(error => {
                ErrorMess(error);
            })
    }
    useEffect(() => {
        getPersonInfor();
    }, [])
    return (
        <div className ="backgroundPerson1">
        <Container className="background">
            <Row>
                <Col xs={12} md={3}  >
                </Col>
                <Col xs={12} md={7} >
                    <h3> Thông tin cá nhân  </h3>
                </Col>
                <Col xs={12} md={2}>
                </Col>
            </Row>
            <Row>
                <Col xs={12} md={2} >
                    <label className="labelPerson"> Họ và Đệm </label>
                </Col>
                <Col className="distance" xs={12} md={8} >
                    <input type="text" class="form-control" name="lastName" value={artistaSeleccionado.lastName} onChange={handleChange}></input>
                    {hasError("lastName")}
                </Col>
            </Row>
            <Row>
                <Col className="inputperson" xs={12} md={2}  >
                    <label className="labelPerson">  Tên </label>
                </Col>
                <Col className="distance" xs={12} md={8}  >
                    <input type="text" class="form-control" name="name" value={artistaSeleccionado.name} onChange={handleChange}></input>
                    {hasError("name")}
                </Col>
            </Row>
            <Row>
                <Col xs={12} md={2}  >
                    <label className="labelPerson" > Điện thoại </label>
                </Col>
                <Col className="distance" xs={12} md={8}  >
                    <input type="text" class="form-control" name="phone" value={artistaSeleccionado.phone} onChange={handleChange}></input>
                    {hasError("phone")}
                </Col>
            </Row>
            <Row>
                <Col xs={12} md={2}  >
                    <label className="labelPerson" > Username </label>
                </Col>
                <Col className="distance" xs={12} md={8}  >
                    <input disabled type="text" class="form-control" name="username" value={artistaSeleccionado.username} onChange={handleChange}></input>
                </Col>
            </Row>
            <Row>
                <Col xs={12} md={2}  >
                    <label className="labelPerson" > Email </label>
                </Col>
                <Col className="distance" xs={12} md={8}  >
                    <input disabled type="text" class="form-control" name="email" value={artistaSeleccionado.email} onChange={handleChange}></input>
                </Col>
            </Row>
            <Row>
                <Col xs={12} md={2}  >
                    <label className="labelPerson" > Địa chỉ </label>
                </Col>
                <Col className="distance" xs={12} md={8}  >
                    <input type="text" class="form-control" name="address" value={artistaSeleccionado.address} onChange={handleChange}></input>
                    {hasError("address")}
                </Col>
            </Row>
            <Row>
                <Col xs={12} md={2} >
                    <label className="labelPerson"> Ngày sinh </label>
                </Col>
                <Col className="distance" xs={12} md={4}  >
                    <input type="date" class="form-control" name="dateOfBirth" value={artistaSeleccionado.dateOfBirth} onChange={handleChange}></input>
                </Col>
            </Row>

            <Row>
                <Col xs={12} md={2}  >
                    <label className="labelPerson"> Giới tính </label>
                </Col>
                <Col className="distance" xs={12} md={4} >
                    <select className="form-control" value={artistaSeleccionado.gender} name="gender" onChange={handleChange} >
                        <option value=""> -- Không chọn -- </option>
                        <option value="MALE"> Nam </option>
                        <option value="FEMALE"> Nữ  </option>
                        <option value="OTHER"> Khác </option>
                    </select>
                </Col>
            </Row>
            <Row className="backgroundPerson">
                <Col xs={12} md={3}></Col>
                <Col xs={12} md={9}>
                    <Button className="buttonPro" onClick={() => editPersonInfor()} disabled={!validateForm(artistaSeleccionado.errors)}> Sửa </Button>
                </Col>
            </Row>
        </Container>
        <Container  >
            <Row>
            <Col xs={12} md={12}>
            <h6 className ="text-person">Design by F-CRM</h6>
            </Col>
            </Row>
        </Container>
    </div>
    );
}

export default PersonInfo;

