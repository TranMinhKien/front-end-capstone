import React, { Component, useState } from "react";
import { NavLink, Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import AuthService from "../services/auth.service";
import AppRouter from "../services/AppRouter";
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import'bootstrap/dist/js/bootstrap.bundle.js';
import 'bootstrap';
import { Nav, NavDropdown, Navbar } from 'react-bootstrap';
import "../App.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faHome, faBell, faUsersCog, faQuestionCircle, faCog, faCalendar } from "@fortawesome/free-solid-svg-icons";
import { faFacebook } from "@fortawesome/free-brands-svg-icons";
import { Button } from 'reactstrap';
import Avatar from '@material-ui/core/Avatar';
import { missingPermission } from "../utils/errorMessage";
import { Drawer } from 'antd';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Fade from '@material-ui/core/Fade';
import 'v2/assets/css/navbar.css'
import {UserOutlined

} from '@ant-design/icons';
const hasPerm = (user, perm = "") => {
  return user && user.roles && user.roles.includes(perm)
}

class App1 extends Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);
    this.state = {
      show: false,
      isOpen: false,
      currentUser: undefined
    };
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
  }

  showModal = () => {
    this.setState({ show: true });
  };

  hideModal = () => {
    this.setState({ show: false });
  }


  componentDidMount() {
    const user = AuthService.getCurrentUser();

    if (user) {
      this.setState({
        currentUser: user,
        showModeratorBoard: user.roles.includes("ROLE_MODERATOR"),
        showAdminBoard: user.roles.includes("ROLE_ADMIN"),
      });
    }
  }

  logOut() {
    AuthService.logout();
    this.setState({
      showModeratorBoard: false,
      showAdminBoard: false,
      currentUser: undefined,
    });
  }

  preventClick = (e) => {
    e.preventDefault()
    missingPermission()
  }

  doNothing = (e) => { }

  render() {
    const { currentUser, showModeratorBoard, showAdminBoard } = this.state;
    return (

      <div style={{height:'100vh'}} >
          <Navbar className="height-narbar navbar_common position-fixed w-100"  sticky="top" expand="lg" collapseOnSelect>
          {currentUser && (
            <Navbar.Brand className="mx-4 nav_logo" as={NavLink} to="/dashboard">
              <FontAwesomeIcon icon={faHome} />
            </Navbar.Brand>
          )}
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">

              {currentUser && (
                <Nav.Link  as={NavLink} to="/potential" onClick={hasPerm(currentUser, "ROLE_READ_POTENTIAL") ? this.doNothing : this.preventClick} > Tiềm Năng </Nav.Link>
              )}

              {currentUser && (
                <Nav.Link as={NavLink} to="/customer" onClick={hasPerm(currentUser, "ROLE_READ_CUSTOMER") ? this.doNothing : this.preventClick} > Tổ Chức </Nav.Link>
              )}


              {currentUser && (
                <Nav.Link as={NavLink} to="/contact" onClick={hasPerm(currentUser, "ROLE_READ_CONTACT") ? this.doNothing : this.preventClick} >Liên Hệ </Nav.Link>
              )}

             
              {currentUser && (
                <Nav.Link as={NavLink} to="/productType" onClick={hasPerm(currentUser, "ROLE_READ_PRODUCT_TYPE") ? this.doNothing : this.preventClick}  >Loại Hàng Hóa </Nav.Link>
              )}

              {currentUser && (
                <Nav.Link as={NavLink} to="/product" onClick={hasPerm(currentUser, "ROLE_READ_PRODUCT") ? this.doNothing : this.preventClick}>Hàng Hóa</Nav.Link>
              )}
              {currentUser ? (
                <Nav.Link as={NavLink} to="/opportunity" onClick={hasPerm(currentUser, "ROLE_READ_OPPORTUNITY") ? this.doNothing : this.preventClick} >Cơ hội</Nav.Link>
              ) : (
                <Nav.Link as={NavLink} to="/login"> Đăng nhập</Nav.Link>
              )}

              {currentUser && (
                <Nav.Link as={NavLink} to="/order" onClick={hasPerm(currentUser, "ROLE_READ_ORDER") ? this.doNothing : this.preventClick}>Đơn hàng </Nav.Link>
              )}
              {currentUser && (
                <Nav.Link as={NavLink} to="/invoice" onClick={hasPerm(currentUser, "ROLE_READ_INVOICE") ? this.doNothing : this.preventClick} >Hóa Đơn </Nav.Link>
              )}            
              {currentUser && (
                <Nav.Link as={NavLink} to="/user" onClick={hasPerm(currentUser, "ROLE_READ_USER") ? this.doNothing : this.preventClick}> Người dùng </Nav.Link>
              )}

              {currentUser ? (
                <Nav.Link as={NavLink} to="/role" onClick={hasPerm(currentUser, "ROLE_READ_ROLE") ? this.doNothing : this.preventClick} >Vai trò</Nav.Link>
              ) : (
                <Nav.Link as={NavLink} to="/register" > Đăng ký </Nav.Link>
              )}
            </Nav>

          </Navbar.Collapse>
              <div className="dropdown px-5">
                  <UserOutlined   style={{ fontSize: '24px', color: '#08c', cursor:'pointer' }} data-bs-toggle="dropdown" aria-expanded="false"/>

                  <ul className="dropdown-menu dropdown-user" style={{ left:"-80px",top:"35px" }} aria-labelledby="dropdownMenuLink">
                      <li><Nav.Link onClick={this.hideModal} as={NavLink} to="/personinfo">
                          Thông tin cá nhân
                      </Nav.Link></li>
                      <li><Nav.Link as={NavLink} onClick={this.hideModal} to="/changePassword" > Thay đổi mật khẩu </Nav.Link></li>
                      <li><Nav.Link href="/login" onClick={this.logOut}>Đăng xuất</Nav.Link></li>
                  </ul>
              </div>
        </Navbar>
        <div style={{marginTop:'67px'}}>
          <AppRouter />
        </div>

      </div>
    );
  }
}
export default App1;

