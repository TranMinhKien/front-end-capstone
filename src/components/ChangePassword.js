import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import axios from 'axios';
import authHeader from '../services/auth_header';
import { Button, Modal, Container, Row, Col } from 'react-bootstrap';
import { DOMAIN_BE } from '../constant/constant';
import { commonFetchDataHandler } from '../utils/index'
import AuthService from "../services/auth.service";
import validation from '../utils/validation';
import "../person.css"
import { dateFormat } from 'highcharts';
import { toastSuccess } from '../utils/MessageSuccess';
import { ErrorMess } from '../utils/MessageErrors';

// API
const baseURL = DOMAIN_BE + "/api/security/changePassword";



const validateForm = (errors) => {
    let valid = true;
    if (errors == null)
        return true;
    Object.values(errors).forEach(
        (val) => val.length > 0 && (valid = false)
    );
    return valid;
}

const validEmailRegex = RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
const validPhoneRegex = RegExp(/^\d*$/)

function ChangePassword() {
    const [data, setData] = useState([]);
    // lấy thông tin USER
    const user = AuthService.getCurrentUser();
    const [artistaSeleccionado, setArtistaSeleccionado] = useState({
        oldPassword: null,
        newPassword2: null,
        newPassword: null,
        errors: {
            oldPassword: '',
            newPassword: '',
            newPassword2: '',

        }
    });

   
    const handleChange = e => {
        const { name, value } = e.target;
        let errors = artistaSeleccionado.errors || (artistaSeleccionado.errors = {})

        validation.value = value;
        switch (name) {
            case 'oldPassword':
                errors.oldPassword = validation.validate('Mật khẩu cũ', [validation.notBlank , validation.password])
                break;
            case 'newPassword':
                errors.newPassword = validation.validate('Mật khẩu mới', [validation.notBlank , validation.password])
                break;
            case 'newPassword2':
                errors.newPassword2 = validation.validate('Xác nhận mật khẩu', [validation.notBlank , validation.password, (e) => {
                    if (e === null || e === '') return '';
                    return e === artistaSeleccionado.newPassword ? '' : 'Xác nhận mật khẩu phải khớp với mật khẩu mới'
                }]);
                break;
            default : 
            break ;
        }
        setArtistaSeleccionado(prevState => ({
            ...prevState,
            [name]: value || null
        }));
    }

    

    const hasError = (name) => {
        return artistaSeleccionado && artistaSeleccionado.errors && artistaSeleccionado.errors[name] &&
            artistaSeleccionado.errors[name].length > 0 &&
            (<span className='error'>{artistaSeleccionado.errors[name]}</span>)
    }

    // Chỉnh sửa dữ liệu từ API 
    const peticionPut = async () => {
        // Check validate khi ấn "Thêm"
        for (const name in artistaSeleccionado) {
            const value = artistaSeleccionado[name]
            handleChange({
                target: { name, value }
            })
        }
        // Nếu chưa valid thì không thêm dữ liệu
        if (!validateForm(artistaSeleccionado.errors))
            return;
        console.log(artistaSeleccionado);
        await axios.put(baseURL, artistaSeleccionado, { headers: { 'Authorization': authHeader().Authorization } })
            .then(response => {
                toastSuccess("Đổi mật khẩu thành công");
                setArtistaSeleccionado({errors: {}})
            }).catch(error => {
                let errors = artistaSeleccionado.errors || (artistaSeleccionado.errors = {})
                errors.oldPassword = "Mật khẩu cũ không đúng";
                ErrorMess(error, errors.oldPassword);
                setArtistaSeleccionado(prevState => ({
                    ...prevState,
                }));
            })
    }

    return (
        <div className="backgroundPerson1">
            <Container className="background">
                <Row>
                    <Col xs={12} md={3}  >
                    </Col>
                    <Col xs={12} md={7} >
                        <h3> Thay đổi mật khẩu </h3>
                    </Col>
                    <Col xs={12} md={2}>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} md={2} >
                        <label className="labelPerson"> Mật khẩu cũ  </label>
                    </Col>
                    <Col className="distance" xs={12} md={8} >
                        <input type="password" class="form-control" name="oldPassword" value={artistaSeleccionado.oldPassword} onChange={handleChange}></input>
                        {hasError("oldPassword")}
                    </Col>
                </Row>
                <Row>
                    <Col className="inputperson" xs={12} md={2}  >
                        <label className="labelPerson">  Mật khẩu mới </label>
                    </Col>
                    <Col className="distance" xs={12} md={8}  >
                        <input type="password" class="form-control" name="newPassword" value={artistaSeleccionado.newPassword} onChange={handleChange}></input>
                        {hasError("newPassword")}
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} md={2}  >
                        <label className="labelPerson" > Xác nhận mật khẩu   </label>
                    </Col>
                    <Col className="distance" xs={12} md={8}  >
                        <input type="password" class="form-control" name="newPassword2" value={artistaSeleccionado.newPassword2} onChange={handleChange}></input>
                        {hasError("newPassword2")}
                    </Col>
                </Row>
                <Row className="backgroundPerson">
                    <Col xs={12} md={3}></Col>
                    <Col xs={12} md={9}>
                        <Button className="buttonPro" onClick={() => peticionPut()} disabled={!validateForm(artistaSeleccionado.errors)}> Sửa </Button>
                    </Col>
                </Row>
            </Container>
            <Container  >
                <Row>
                    <Col xs={12} md={12}>
                        <h6 className="text-person-changepassword">Design by F-CRM</h6>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default ChangePassword;

