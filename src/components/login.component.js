import React, { Component } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import AuthService from "../services/auth.service";
import "../login.css"
import validation from "../utils/validation";


// hàm thông báo không được để trống
const required = value => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert"> 
            Không được để trống ! 
      </div>
    );
  }
};

const vusername = value => {
  validation.value = value
  var errors = validation.validate('Tài khoản', [validation.notBlank, validation.length(8, 32), validation.username])
  if (errors !== '') {
    return (
      <div className="alert alert-danger" role="alert">
        {errors}
      </div>
    );
  }
};

const vpassword1 = value => {
  validation.value = value
  var errors = validation.validate('Mật khẩu', [validation.notBlank, validation.length(8, 40), validation.password])
  if (errors !== '') {
    return (
      <div className="alert alert-danger" role="alert">
        {errors}
      </div>
    );
  }
};

const vpassword = value => {
  if (value.length < 8 || value.length > 40) {
    return (
      <div className="alert alert-danger" role="alert">
        Mật Khẩu ít nhất 8 kí tự, nhiều nhất 40 kí tự
      </div>
    );
  }
};

export default class Login extends Component {
  // ban đầu hiện lên thông tin gì 
  constructor(props) {
    // các nút login
    super(props);
    this.handleLogin = this.handleLogin.bind(this);
    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
 
    // ban đầu không có gì 
    this.state = {
      username: "",
      password: "",
      loading: false,
      message: ""
    };
  }

  onChangeUsername(e) {
    this.setState({
      username: e.target.value
    });
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value
    });
  }

  handleLogin(e) {
    e.preventDefault();

    this.setState({
      message: "",
      loading: true
    });

    this.form.validateAll();

    if (this.checkBtn.context._errors.length === 0) {
      AuthService.login(this.state.username, this.state.password).then(
        () => {
          this.props.history.push("/dashboard");
          window.location.reload();
        },
        // phía trên là đăng nhập được 
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          this.setState({
            loading: false,
            message: resMessage
          });
        }
      );
    } else {
      this.setState({
        loading: false
      });
    }
  }

  render() {
    return (
      <div className="container h-100">
      <div className="d-flex justify-content-center h-100">
        <div className="user_card">
          <div className="d-flex justify-content-center">
            <div className="brand_logo_container">
              <img src="fcrm.png" className="brand_logo" alt="Logo" />
            </div>
          </div>
          <div className="d-flex justify-content-center form_container">
            <Form 
             onSubmit={this.handleLogin} // onSubmit bằng form 
              ref={c => {
              this.form = c;
              }}
            >
              <div className="input-group mb-3">
                <div className="input-group-append">
                  <span>
                      <img src="Screenshot1.png"/>
                  </span>
                </div>
                <Input
                type="text"
                className="form-control"
                name="username"
                value={this.state.username}
                onChange={this.onChangeUsername}
                validations={[required ,vusername ]}
                 />
              </div>
              <div className="input-group mb-2">
                <div className="input-group-append">
                <span>
                      <img src="Screenshot2.png"/>
                  </span>
                </div>
                <Input
                type="password"
                className="form-control"
                name="password"
                value={this.state.password}
                onChange={this.onChangePassword}
                validations={[required , vpassword , vpassword1]}
              />
              </div>
              <div className="d-flex justify-content-center mt-3 login_container">
              <button
                className="btn btn-primary btn-block"
                disabled={this.state.loading}
              >
                {this.state.loading && (
                  <span className="spinner-border spinner-border-sm"></span>
                )}
                <span>Đăng nhập </span>
              </button>
              </div>
              {this.state.message && (
               <div className="form-group">
                <div className="alert alert-danger" role="alert">
                 {this.state.message}
               </div>
              </div>
            )}
            <CheckButton
             style={{ display: "none" }}
              ref={c => {
                this.checkBtn = c;
              }}
            />
              </Form>
          </div>
        </div>
      </div>
    </div>
    );
  }
}