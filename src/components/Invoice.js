import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import axios from 'axios';
import MaterialTable from 'material-table';
import { TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import "../App.css";
import { Button, Modal, Container, Row, Col } from 'react-bootstrap';
import { DOMAIN_BE } from '../constant/constant';
import { commonFetchDataHandler } from '../utils/index'
import { commonFetchAdd } from '../utils/add';
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import Select from 'react-select';
import authHeader from '../services/auth_header';
import { missingPermission } from '../utils/errorMessage';
import AuthService from "../services/auth.service";
import validation from '../utils/validation';
import VisibilityIcon from '@material-ui/icons/Visibility'
import { Grid, TablePagination, Typography } from '@material-ui/core';
import { toastSuccess } from '../utils/MessageSuccess';
import { ErrorMess } from '../utils/MessageErrors';

/// API
const baseURLcustomer = DOMAIN_BE + "/api/customer/name";
const baseURLcontact = DOMAIN_BE + "/api/contact/name";
const baseURLorder = DOMAIN_BE + "/api/order/name"
const baseURL = DOMAIN_BE + "/api/invoice/";
const baseURLproduct = DOMAIN_BE + "/api/product/name";


const useStyles = makeStyles((theme) => ({
    modal: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)'
    },
    iconos: {
        cursor: 'pointer'
    },
    inputMaterial: {
        width: '100%'
    }
}));

const validateForm = (errors) => {
    let valid = true;
    if (errors == null)
        return true;
    Object.values(errors).forEach(
        (val) => val.length > 0 && (valid = false)
    );
    return valid;
}

const validEmailRegex = RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
const validPhoneRegex = RegExp(/^\d*$/)
function Invoice() {
    const styles = useStyles();
    const [data, setData] = useState([]);
    const [dataproductInfo, setDataProductInfo] = useState([]);
    const [modalInsertar, setModalInsertar] = useState(false);
    const [modalEditar, setModalEditar] = useState(false);
    const [modalEliminar, setModalEliminar] = useState(false);
    const [modalViewInfor, setModalViewInfor] = useState(false);
    const [deleteChild, setDeleteChild] = useState(false);
    const [EditChild, setEditChild] = useState(false);

    // lấy thông tin USER
    const user = AuthService.getCurrentUser();

    const [artistaSeleccionado, setArtistaSeleccionado] = useState({
        id: null,
        customerId: null,
        address: null,
        bankAccount: null,
        bank: null,
        taxCode: null,
        buyerId: null,
        receiverName: null,
        receiverEmail: null,
        receiverPhone: null,
        orderId: null,
        code: null,
        errors: {
            address: '',
            bank: '',
            bankAccount: '',
            taxCode: '',
            receiverEmail: '',
            receiverPhone: '',
            receiverName: '',
            code: ''
        }
    });


    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [show2, setshow2] = useState(false);
    const handleClose2 = () => setshow2(false);
    const handleShow2 = () => setshow2(true);

    const hasPerm = (user, perm = "") => {
        return user && user.roles && user.roles.includes(perm)
    }

    const [order3, SetOrder3] = useState({
        selectOrder3: [],
        id: "",
        name: ""
    });

    const [customer3, setCustomer3] = useState({
        selectCustomer: [],
        id: "",
        name: ""
    });

    const [contact3, setContact3] = useState({
        selectContact: [],
        id: "",
        name: ""
    });

    const [product3, setProduct3] = useState({
        selectProduct: [],
        id: "",
        name: ""
    });

    const getOrder = async () => {
        const responseOrder = await commonFetchDataHandler(baseURLorder)
        const optionsOrder = responseOrder.data.map(d => ({
            "value": d.id,
            "label": d.name
        }))
        optionsOrder.unshift({
            "value": "",
            "label": "-- Không chọn --"
        })
        SetOrder3({ selectOrder3: optionsOrder })
    }

    const getContact = async () => {
        const responseContact = await commonFetchDataHandler(baseURLcontact)
        const optionsContact = responseContact.data.map(d => ({
            "value": d.id,
            "label": d.name
        }))
        optionsContact.unshift({
            "value": "",
            "label": "-- Không chọn --"
        })
        setContact3({ selectContact: optionsContact })
    }

    const getCustomer = async () => {
        const responseCustomer = await commonFetchDataHandler(baseURLcustomer)
        const optionsCustomer = responseCustomer.data.map(d => ({
            "value": d.id,
            "label": d.name
        }))
        optionsCustomer.unshift({
            "value": "",
            "label": "-- Không chọn --"
        })
        setCustomer3({ selectCustomer: optionsCustomer })
    }

    const clearErrors = () => {
        setArtistaSeleccionado(prevState => ({
            ...prevState,
            errors: {}
        }));
    }
    const handleChange = e => {
        const { name, value } = e.target;
        let errors = artistaSeleccionado.errors || (artistaSeleccionado.errors = {})
        validation.value = value;

        switch (name) {
            case 'bank':
                break;
            case 'bankAccount':
                errors.bankAccount = validation.validate('Số tài khoản ngân hàng', [validation.number])
                break;
            case 'taxCode':
                errors.taxCode = validation.validate('Mã số thuế', [validation.number])
                break;
            case 'code':
                errors.code = validation.validate('Mã', [validation.notBlank])
                break;
            case 'receiverEmail':
                errors.receiverEmail = validation.validate('Địa chỉ email', [validation.notBlank, validation.email])
                break;
            case 'receiverName':
                errors.receiverName = validation.validate('Tên người nhận', [validation.notBlank])
                break;
            case 'receiverPhone':
                errors.receiverPhone = validation.validate('Số điện thoại người nhận ', [validation.notBlank, validation.phone])
                break;
            case 'address':
                break;
            default:
                break;
        }
        setArtistaSeleccionado(prevState => ({
            ...prevState,
            [name]: value || null
        }));
    }


    const getInvoice = async () => {
        const response = await commonFetchDataHandler(baseURL)
        response.data.forEach(element => {
            // TODO 3
            element.customerId = element.customer ? element.customer.id : null
            element.buyerId = element.buyer ? element.buyer.id : null
            element.orderId = element.order ? element.order.id : null
        });
        setData(response.data)
    }

    const createInvoice = async () => {
        // Check validate khi ấn "Thêm"
        for (const name in artistaSeleccionado) {
            const value = artistaSeleccionado[name]
            handleChange({
                target: { name, value }
            })
        }
        // Nếu chưa valid thì không thêm dữ liệu
        if (!validateForm(artistaSeleccionado.errors))
            return;
        commonFetchAdd(baseURL, artistaSeleccionado).then(responseSource => {
            const d = responseSource.data
            // TODO 1
            d.customerId = d.customer ? d.customer.id : null
            d.buyerId = d.buyer ? d.buyer.id : null
            d.orderId = d.order ? d.order.id : null
            setData(data.concat(d))
            handleClose();
            toastSuccess("Thêm dữ liệu thành công")
        }).catch(error => {
            if (error.response && error.response.data && error.response.data.errors && error.response.data.errors[0].type === 'duplicate') {
                let errors = artistaSeleccionado.errors || (artistaSeleccionado.errors = {})
                error.response.data.errors.forEach(e => {
                    errors[e.field] = 'Giá trị đã bị trùng';
                });
                setArtistaSeleccionado(prevState => ({
                    ...prevState,
                    errors: errors,
                }));
                console.log(artistaSeleccionado)
            } else {
                ErrorMess(error);
            }
        })

    }

   

    const peticionPostChild = async () => {
        await axios.post(baseURL + artistaSeleccionado.id + "/product/" + artistaSeleccionado.productId, artistaSeleccionado, { headers: { 'Authorization': authHeader().Authorization } })
            .then(response => {
                const d = response.data;
                d.productId = d.product ? d.product.id : null;
                // thêm vào bảng cũ
                setDataProductInfo(dataproductInfo.concat(d));
                // Thêm vào bảng mới
                const newData = data;
                newData.forEach(n => {if (n && n.id === d.invoiceId) {
                    if (n.productInfos);
                    else n.productInfos = [];
                    n.productInfos.push(d);
                }});
                setData(newData);
                // sửa cái 1 
                handleClose2();
                toastSuccess("Thêm dữ liệu thành công ");
            }).catch(error => {

                ErrorMess(error);
            })
    }

    const getProduct = async () => {
        const responseProduct = await commonFetchDataHandler(baseURLproduct)
        const optionsProduct = responseProduct.data.map(d => ({
            "value": d.id,
            "label": d.name
        }))
        setProduct3({ selectProduct: optionsProduct })
    }


    const editInvoice = async () => {
        await axios.put(baseURL + artistaSeleccionado.id, artistaSeleccionado, { headers: { 'Authorization': authHeader().Authorization } })
            .then(response => {
                const dataNueva = data;
                dataNueva.map(id => {
                    if (id.id === artistaSeleccionado.id) {
                        //TODO 2
                        id.code = artistaSeleccionado.code;

                        id.customer = response.data.customer;
                        id.customerId = id.customer?.id


                        id.address = artistaSeleccionado.address;
                        id.bankAccount = artistaSeleccionado.bankAccount;
                        id.bank = artistaSeleccionado.bank;
                        id.taxCode = artistaSeleccionado.taxCode;

                        id.buyer = response.data.buyer;
                        id.buyerId = id.buyer?.id

                        id.receiverName = artistaSeleccionado.receiverName;
                        id.receiverEmail = artistaSeleccionado.receiverEmail;
                        id.receiverPhone = artistaSeleccionado.receiverPhone;

                        id.order = response.data.order;
                        id.orderId = id.order?.id

                        id.amount = artistaSeleccionado.amount;
                        id.productCode = artistaSeleccionado.productCode;
                        id.vat = artistaSeleccionado.vat;
                        id.explanation = artistaSeleccionado.explanation;
                        id.totalPrice = artistaSeleccionado.totalPrice;
                        id.unit = artistaSeleccionado.unit;
                        id.price = artistaSeleccionado.price;
                        id.discountMoney = artistaSeleccionado.discountMoney ;
                    }
                });
                setData(dataNueva);
                abrirCerrarModalEditar();
                toastSuccess("Edit dữ liệu thành công")
            }).catch(error => {
                ErrorMess(error);
            })
    }
    const peticionUpdateProductInfo = async () => {
        await axios.put(baseURL + artistaSeleccionado.invoiceId + "/product/" + artistaSeleccionado.id, artistaSeleccionado, { headers: { 'Authorization': authHeader().Authorization } })
            .then(response => {
                const dataNueva = data;
                dataNueva.map(id => {
                    if (id.id === artistaSeleccionado.id) {
                        id.amount = artistaSeleccionado.amount;
                        id.productCode = artistaSeleccionado.productCode;
                        id.vat = artistaSeleccionado.vat;
                        id.explanation = artistaSeleccionado.explanation;
                        id.price = artistaSeleccionado.price;
                        id.unit = artistaSeleccionado.unit;
                        id.discountMoney = artistaSeleccionado.discountMoney;
                    }
                });
                setData(dataNueva);
                abrirCerrarModalEditar();
                toastSuccess("Edit dữ liệu thành công")
            }).catch(error => {
                ErrorMess(error);
            })
    }
    const peticionDeleteChild = async () => {
        await axios.delete(baseURL + artistaSeleccionado.invoiceId + "/product/" + artistaSeleccionado.id, { headers: { 'Authorization': authHeader().Authorization } })
            .then(response => {
                const newData = data
                setData(newData.filter(name => name.id !== artistaSeleccionado.id));
                abrirCerrarModalEliminar();
                toastSuccess("Xóa dữ liệu thành công")
            }).catch(error => {
                ErrorMess(error);
            })
    }
    // xóa dữ liệu
    const deleteInvoice = async () => {
        await axios.delete(baseURL + artistaSeleccionado.id, { headers: { 'Authorization': authHeader().Authorization } })
            .then(response => {
                setData(data.filter(name => name.id !== artistaSeleccionado.id));
                abrirCerrarModalEliminar();
                toastSuccess("Xóa dữ liệu thành công")
            }).catch(error => {
                ErrorMess(error);
            })
    }
    const peticionGetProductInfo = async (id) => {
        await axios({
            method: "GET",
            url: baseURL + id.id + "/product",
            headers: {
                'Authorization': authHeader().Authorization
            }
        }).then(response => {
            setDataProductInfo(response.data);
        }).catch(error => {
            ErrorMess(error);
        })
    }


    const seleccionarArtista = (id, caso) => {
        setArtistaSeleccionado(id);
        peticionGetProductInfo(id);
        (caso === "Editar") ? abrirCerrarModalEditar()
            :
            abrirCerrarModalEliminar()
    }

    // const selectModal = (id, caso) => {
    //     setArtistaSeleccionado(id);
    //     (caso === "Edit") ? SettingModalEdit() : SettingModalDeleteChild()

    // }
    const selectModal = (id, caso) => {
        setArtistaSeleccionado(id);
        (caso === "them") ?  handleShow2() : ((caso === "sua") ? SettingModalEdit() : SettingModalDeleteChild())

    }


    const seleccionarView = (id, caso) => {
        if (caso === "check") {
            viewInformation();
        }
    }


    const abrirCerrarModalInsertar = () => {
        setModalInsertar(!modalInsertar);
    }

    const abrirCerrarModalEditar = () => {
        setModalEditar(!modalEditar);
    }

    const abrirCerrarModalEliminar = () => {
        setModalEliminar(!modalEliminar);
    }

    const viewInformation = () => {
        setModalViewInfor(!modalViewInfor);
    }
    const SettingModalEdit = () => {
        setEditChild(!EditChild);
    }

    const SettingModalDeleteChild = () => {
        setDeleteChild(!deleteChild);
    }

    useEffect(() => {
        getInvoice();
        getContact();
        getCustomer();
        getOrder();
        getProduct();
    }, [])

    const ModalDeleteChild = (
        <Modal size="lg" show={deleteChild} onHide={() => SettingModalDeleteChild()}>
            <Modal.Header>
                <Modal.Title>Xóa</Modal.Title>
            </Modal.Header>
            <Modal.Body className="show-grid">
                <p> Bạn có chắc chắn muốn xóa   không ? </p>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => SettingModalDeleteChild()}>
                    Không
                </Button>
                <Button variant="primary" onClick={() => peticionDeleteChild()}>
                    Xóa
                </Button>
            </Modal.Footer>
        </Modal>
    )
    const ModalEditChild = (
        <Modal backdrop="static"
            keyboard={false} size="lg" show={EditChild} onHide={() => SettingModalEdit()}>
            <Modal.Header >
                <Modal.Title> Chỉnh Sửa Hàng Hóa  </Modal.Title>
            </Modal.Header>
            <Modal.Body className="show-grid">
                <Container>
                    <Container>
                        <Row>
                            <Col md={6}>
                                <label>Mã  Hàng Hóa</label>
                                <input type="text" class="form-control" name="productCode" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.productCode}></input>
                            </Col>
                            <Col md={6}>
                                <label>Diễn giải khi bán</label>
                                <input type="text" class="form-control" name="explanation" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.explanation}></input>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={6}>
                                <label>Đơn giá bán</label>
                                <input type="text" class="form-control" name="price" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.price}></input>
                            </Col>
                            <Col md={6}>
                                <label> Đơn vị tính chính  </label>
                                <select className="form-control controll" name="unit" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.unit} >
                                    <option value=""> - Không chọn - </option>
                                    <option value="Bao">Bao</option>
                                    <option value="Bình">Bình</option>
                                    <option value="Bộ">Bộ</option>
                                    <option value="Cái">Cái</option>
                                    <option value="Cây">Cây</option>
                                    <option value="Chai">Chai</option>
                                    <option value="Chiếc">Chiếc</option>
                                    <option value="Cuốn">Cuốn</option>
                                    <option value="Điếu">Điếu</option>
                                    <option value="Gói">Gói</option>
                                    <option value="Hộp">Hộp</option>
                                    <option value="két">Két</option>
                                    <option value="kilogam">Kilogam</option>
                                    <option value="két">Quả</option>
                                    <option value="Tấn">Tấn </option>
                                    <option value="Tạ">Tạ </option>
                                    <option value="Yến">Yến</option>
                                </select>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={6}>
                                <label>Số lượng</label>
                                <input type="text" class="form-control" name="amount" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.amount}></input>
                            </Col>
                            <Col md={6}>
                                <label>Thuế (%)</label>
                                <input type="text" class="form-control" name="vat" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.vat}></input>
                            </Col>
                        </Row>
                    </Container>
                </Container>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => SettingModalEdit()}>
                    Hủy bỏ
                </Button>
                <Button variant="primary" onClick={() => peticionUpdateProductInfo()}>
                    Sửa
                </Button>
            </Modal.Footer>
        </Modal>
    )

    const ModalInsertChild = (
        <Modal backdrop="static"
            keyboard={false} size="lg" show={show2} onHide={() => handleClose2()}>
            <Modal.Header >
                <Modal.Title> Chọn Hàng Hóa </Modal.Title>
            </Modal.Header>
            <Modal.Body className="show-grid">
                <Container>
                    <Row>
                        <label> Chọn Hàng Hóa  </label>
                        <select className="form-control" name="productId" onChange={handleChange}  >
                            {product3.selectProduct.map((option2, index) => (
                                <option value={option2.value} key={index}> {option2.label}</option>
                            ))}
                        </select>
                    </Row>
                </Container>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => handleClose2()}  >
                    Hủy bỏ
                </Button>
                <Button variant="primary" onClick={() => peticionPostChild()} >
                    Thêm
                </Button>
            </Modal.Footer>
        </Modal>
    )
    const TableInvoice = (
        <MaterialTable
            title="Hóa Đơn"
            columns={[
                {
                    field: 'code', title: 'Mã', headerStyle: {
                        backgroundColor: '#696969',
                        color: '#FFF'
                    },
                },
                {
                    field: 'customer.name', title: 'Khách Hàng', headerStyle: {
                        backgroundColor: '#696969',
                        color: '#FFF'
                    },
                },
                {
                    field: 'bank', title: 'Ngân Hàng', headerStyle: {
                        backgroundColor: '#696969',
                        color: '#FFF'
                    },
                },
                {
                    field: 'taxCode', title: 'Mã số thuế', headerStyle: {
                        backgroundColor: '#696969',
                        color: '#FFF'
                    },
                },
                {
                    field: 'buyer.name', title: 'Người Mua', headerStyle: {
                        backgroundColor: '#696969',
                        color: '#FFF'
                    },
                },
                {
                    field: 'receiverEmail', title: 'Email người nhận', headerStyle: {
                        backgroundColor: '#696969',
                        color: '#FFF'
                    },
                },
                {
                    field: 'order.name', title: 'Đơn hàng', headerStyle: {
                        backgroundColor: '#696969',
                        color: '#FFF'
                    },
                },
            ]}
            data={data}
            actions={[
                {
                    icon: 'edit',
                    tooltip: 'Sửa thông tin',
                    onClick: (event, rowData) => (hasPerm(user, "ROLE_UPDATE_INVOICE") ? seleccionarArtista(rowData, "Editar") : missingPermission())
                },
                {
                    icon: 'delete',
                    tooltip: 'Xóa thông tin',
                    onClick: (event, rowData) => (hasPerm(user, "ROLE_DELETE_INVOICE") ? seleccionarArtista(rowData, "Eliminar") : missingPermission())
                },
                {
                    icon: () => <VisibilityIcon />,
                    tooltip: 'Xem chi tiết',
                    onClick: (event, rowData) => (hasPerm(user, "ROLE_READ_INVOICE") ? seleccionarView(rowData, "check") : missingPermission())
                },
                {
                    icon: "add_box",
                    tooltip: "Thêm",
                    position: "toolbar",
                    onClick: () => (hasPerm(user, "ROLE_CREATE_INVOICE") ? handleShow() : missingPermission())
                }
            ]}
            options={{
                headerStyle: {
                    backgroundColor: '#696969',
                    color: '#FFF'
                }, rowStyle: {
                    backgroundColor: '#EEE',
                },
                actionsColumnIndex: -1,
                pageSize: 10,
                pageSizeOptions: [5, 10, 20, 30, 50, 75, 100],
                exportButton: { csv: true },
                filtering: true,
            }}

            localization={
                {
                    header: {
                        actions: 'Hành động'
                    },
                    body: {
                        editRow: {
                            deleteText: "Bạn có chắc chắn muốn xóa?"
                        },
                        emptyDataSourceMessage: 'Không có dữ liệu để hiển thị ',
                    }
                }
            }
            onRowClick={(event, rowData, togglePanel) => togglePanel()}

            detailPanel={(rowData) => {
                return (<MaterialTable
                    title="Hàng Hóa"
                    localization={{
                        header: {
                            actions: 'Hành động'
                        },
                        body: {
                            editRow: { deleteText: 'Bạn có chắc chắn muốn xóa?' },
                            emptyDataSourceMessage: 'Không có dữ liệu để hiển thị ',
                        }
                    }}
                    columns={
                        [
                            {
                                field: 'productCode', title: 'Mã Hàng Hóa', editable: 'never',
                                // width: 120,
                                validate: rowData => validation.validate1("Mã hàng hóa", rowData.productCode, [validation.notBlank]),
                            },
                            {
                                field: 'explanation', title: 'Diễn giải khi bán',
                                // width: 300,
                            },
                            {
                                field: 'unit', title: 'Đơn vị tính'
                                // width: 100,
                            },
                            {
                                field: 'amount', title: 'Số lượng', type: 'numeric',
                                // width: 100,
                                validate: rowData => validation.validate1("Số lượng", rowData.amount, [validation.notBlank, validation.number]),
                            },
                            {
                                field: 'price', title: 'Đơn giá ', type: 'numeric',
                                // width: 100,
                                validate: rowData => validation.validate1("Đơn giá", rowData.price, [validation.notBlank, validation.number, validation.positive]),
                            },
                            {
                                field: 'totalPrice', title: 'Thành Tiền', editable: 'never', type: 'numeric',
                                // width: 100,
                            },
                            {
                                field: 'discount', title: 'Chiết khấu (%)', type: 'numeric',
                                // width: 100,
                                validate: rowData => validation.validate1("Chiết khấu", rowData.discount, [validation.notBlank, validation.number, validation.rate]),
                            },
                            {
                                field: 'discountMoney', title: 'Tiền chiết khấu', editable: 'never', type: 'numeric',
                            },
                            {
                                field: 'vat', title: 'Thuế', type: 'numeric',
                                // width: 100,
                                validate: rowData => validation.validate1("Chiết khấu", rowData.vat, [validation.notBlank, validation.number, validation.rate]),
                            },
                            {
                                field: 'vatMoney', title: ' Tiền Thuế ', editable: 'never', type: 'numeric',
                                // width: 100,
                            },
                            {
                                field: 'totalMoney', title: 'Tổng tiền', editable: 'never', type: 'numeric',
                                // width: 100,
                            }
                        ]
                    }

                    data={rowData.productInfos ? rowData.productInfos : rowData.productInfos = []}

                    actions={[
                        {
                            icon: 'add_box',
                            tooltip: 'Thêm',
                            onClick: () => (hasPerm(user, "ROLE_UPDATE_INVOICE") ? selectModal(rowData, "them") : missingPermission()),
                            position: 'toolbar'
                        },
                        {
                            icon: 'edit',
                            tooltip: 'Chỉnh sửa ',
                            onClick: (event, rowData) => (hasPerm(user, "ROLE_UPDATE_INVOICE") ? selectModal(rowData, "sua") : missingPermission()),
                        },
                        {
                            icon: 'delete',
                            tooltip: 'xóa',
                            position: 'toolbarOnSelect',
                            onClick: async (event, oldData) => {
                                if (hasPerm(user, 'ROLE_UPDATE_INVOICE')) {
                                    let ids = oldData;
                                    ids = Array.isArray(ids) ? ids.map(a => a.id) : [ids.id];
                                    try {
                                        const response = await axios.delete(baseURL + rowData.id + "/product/" + ids.join(','), { headers: { 'Authorization': authHeader().Authorization } });
                                        const dataDelete = [...rowData.productInfos].filter(d => !ids.includes(d.id));
                                        rowData.productInfos = dataDelete;
                                        setData(prevState => ({
                                            ...prevState,
                                            productInfos: dataDelete.filter(d_1 => !ids.includes(d_1.id)),
                                        }));
                                        console.log(data.productInfos);
                                    } catch (error) {
                                        ErrorMess(error);
                                    }
                                } else missingPermission();
                            },
                        }
                    ]}
                    editable={{
                        onRowUpdate: (newData, oldData) => axios.put(baseURL + oldData.invoiceId + "/product/" + oldData.id, newData, { headers: { 'Authorization': authHeader().Authorization } })
                            .then(response => {
                                const dataUpdate = [...rowData.productInfos];
                                dataUpdate.map(d => {
                                    if (d.id === oldData.id) Object.assign(d, response.data)
                                });
                                const newData = data;
                                newData.forEach(n => {
                                    if (n && n.id === rowData.id) {
                                        n.productInfos = dataUpdate;
                                    }
                                });
                                setData(newData);
                            }).catch(error => {
                                ErrorMess(error);
                            }),
                        onRowDelete: async (oldData) => {
                            let ids = oldData
                            ids = Array.isArray(ids) ? ids.map(a => a.id) : [ids.id]
                            try {
                                const response = await axios.delete(baseURL + oldData.invoiceId + "/product/" + ids.join(','), { headers: { 'Authorization': authHeader().Authorization } });
                                const dataDelete = rowData.productInfos.filter(d => !ids.includes(d.id));
                                setData(prevState => {
                                    return prevState.map(p => {
                                        if (p?.id === rowData?.id) {
                                            p.productInfos = dataDelete;
                                        }
                                        return p;
                                    })
                                });
                            } catch (error) {
                                ErrorMess(error);
                            }
                        },
                    }}
                    options={{
                        actionsColumnIndex: -1,
                        selection: true,
                        pageSize: 5,
                        pageSizeOptions: [5, 10, 20, 30, 50, 75, 100],
                        exportButton: { csv: true },
                        filtering: true,
                        headerStyle: {
                            backgroundColor: '#696969',
                            color: '#FFF',

                        },
                        rowStyle: {
                            backgroundColor: '#EEE',
                        },
                    }}
                    components={{
                        Pagination: (props) => <div>
                            <Grid container style={{ padding: 15, background: "f5f5f5" }}>
                                <Grid sm={1.95} item><Typography variant="subtitle2">Tổng cộng</Typography></Grid>
                                <Grid sm={2} item align="center"><Typography variant="subtitle2">Số lượng: {rowData.productInfos?.reduce((accumulator, i) => accumulator + i.amount, 0)}</Typography></Grid>
                                <Grid sm={2} item align="center"><Typography variant="subtitle2">Thành tiền: {rowData.productInfos?.reduce((accumulator, i) => accumulator + i.totalPrice, 0)}</Typography></Grid>
                                <Grid sm={2} item align="center"><Typography variant="subtitle2">Tiền chiết khấu: {rowData.productInfos?.reduce((accumulator, i) => accumulator + i.discountMoney, 0)}</Typography></Grid>
                                <Grid sm={2} item align="center"><Typography variant="subtitle2">Tiền thuế: {rowData.productInfos?.reduce((accumulator, i) => accumulator + i.vatMoney, 0)}</Typography></Grid>
                                <Grid sm={2} item align="center"><Typography variant="subtitle2">Tổng tiền: {rowData.productInfos?.reduce((accumulator, i) => accumulator + i.totalMoney, 0)}</Typography></Grid>
                            </Grid>
                            <TablePagination {...props} />
                        </div>
                    }}
                />)
            }}

        />
    )
    const hasError = (name) => {
        return artistaSeleccionado && artistaSeleccionado.errors && artistaSeleccionado.errors[name] &&
            artistaSeleccionado.errors[name].length > 0 &&
            (<span className='error'>{artistaSeleccionado.errors[name]}</span>)
    }
    const ModalInsert = (
        <Modal backdrop="static" keyboard={false} size="lg" scrollable={true} show={show} onHide={() => handleClose()}>
            <Modal.Header>
                <Modal.Title>Thêm Hóa Đơn</Modal.Title>
            </Modal.Header>
            <Modal.Body className="show-grid">
                <Container>
                    <Row>
                        <Col md={6}>
                            <label  className ="required">Mã hóa đơn</label>
                            <input type="text" class="form-control" name="code" onChange={handleChange}></input>
                            {hasError('code')}
                        </Col>
                        <Col md={6}>
                            <label> Người Mua Hàng  </label>
                            <select className="form-control controll" name="buyerId" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.buyerId} >
                                {contact3.selectContact.map((option2, index) => (
                                    <option value={option2.value} key={index}> {option2.label}</option>
                                ))}
                            </select>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label>Tài Khoản Ngân Hàng</label>
                            <input type="text" class="form-control" name="bankAccount" onChange={handleChange}></input>
                            {hasError('bankAccount')}
                        </Col>
                        <Col md={6}>
                            <label>Mở tại Ngân Hàng</label>
                            <input type="text" class="form-control" name="bank" onChange={handleChange}></input>
                            {hasError('bank')}
                        </Col>

                    </Row>
                    <Row>
                        <Col md={6}>
                            <label>Địa chỉ </label>
                            <input type="text" class="form-control" name="address" onChange={handleChange}></input>
                            {hasError('address')}

                        </Col>
                        <Col md={6}>
                            <label>Mã số thuế </label>
                            <input type="text" class="form-control" name="taxCode" onChange={handleChange}></input>
                            {hasError('taxCode')}
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>

                            <label  className ="required">Email người nhận</label>
                            <input type="text" class="form-control" name="receiverEmail" onChange={handleChange}></input>
                            {hasError('receiverEmail')}
                        </Col>
                        <Col md={6}>

                            <label  className ="required">Tên người nhận</label>
                            <input type="text" class="form-control" name="receiverName" onChange={handleChange}></input>
                            {hasError('receiverName')}
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label  className ="required">Điện thoại người nhận</label>
                            <input type="text" class="form-control" name="receiverPhone" onChange={handleChange}></input>
                            {hasError('receiverPhone')}
                        </Col>
                        <Col md={6}>
                            <label> Đơn Hàng  </label>
                            <select className="form-control controll " name="orderId" onChange={handleChange} >
                                {order3.selectOrder3.map((option2, index) => (
                                    <option value={option2.value} key={index}> {option2.label}</option>
                                ))}
                            </select>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label> Khách Hàng  </label>
                            <select className="form-control controll" name="customerId" onChange={handleChange}  >
                                {customer3.selectCustomer.map((option2, index) => (
                                    <option value={option2.value} key={index}> {option2.label}</option>
                                ))}
                            </select>
                        </Col>
                    </Row>
                </Container>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => { clearErrors(); handleClose() }}>
                    Hủy bỏ
                </Button>
                <Button variant="primary" onClick={() => createInvoice()} disabled={!validateForm(artistaSeleccionado.errors)}>
                    Thêm
                </Button>
            </Modal.Footer>
        </Modal>
    )

    const ModalEdit = (
        <Modal backdrop="static" keyboard={false} size="lg" scrollable={true} show={modalEditar} onHide={() => abrirCerrarModalEditar()}>
            <Modal.Header>
                <Modal.Title>Sửa Thông Tin </Modal.Title>
            </Modal.Header>
            <Modal.Body className="show-grid">
                <Container>
                    <Row>
                        <Col md={6}>
                            <label  className ="required">Mã hóa đơn</label>
                            <input type="text" class="form-control" name="code" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.code}></input>
                            {hasError('code')}
                        </Col>
                        <Col md={6}>
                            <label> Người Mua Hàng  </label>
                            <select className="form-control controll1" name="buyerId" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.buyerId} >
                                {contact3.selectContact.map((option2, index) => (
                                    <option value={option2.value} key={index}> {option2.label}</option>
                                ))}
                            </select>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label>Tài Khoản Ngân Hàng</label>
                            <input type="text" class="form-control" name="bankAccount" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.bankAccount}></input>
                            {hasError('bankAccount')}
                        </Col>
                        <Col md={6}>
                            <label>Mở tại Ngân Hàng</label>
                            <input type="text" class="form-control" name="bank" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.bank}></input>
                            {hasError('bank')}
                        </Col>

                    </Row>
                    <Row>
                        <Col md={6}>
                            <label>Địa chỉ </label>
                            <input type="text" class="form-control" name="address" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.address}></input>
                            {hasError('address')}
                        </Col>
                        <Col md={6}>
                            <label>Mã số thuế </label>
                            <input type="text" class="form-control" name="taxCode" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.taxCode}></input>
                            {hasError('taxCode')}
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>

                            <label  className ="required">Email người nhận</label>
                            <input type="text" class="form-control" name="receiverEmail" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.receiverEmail}></input>
                            {hasError('receiverEmail')}
                        </Col>
                        <Col md={6}>

                            <label  className ="required">Tên người nhận</label>
                            <input type="text" class="form-control" name="receiverName" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.receiverName}></input>
                            {hasError('receiverName')}
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label  className ="required">Điện thoại người nhận</label>
                            <input type="text" class="form-control" name="receiverPhone" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.receiverPhone}></input>
                            {hasError('receiverPhone')}
                        </Col>
                        <Col md={6}>
                            <label> Đơn Hàng  </label>
                            <select className="form-control controll1" name="orderId" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.orderId} >
                                {order3.selectOrder3.map((option2, index) => (
                                    <option value={option2.value} key={index}> {option2.label}</option>
                                ))}
                            </select>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label> Khách Hàng  </label>
                            <select className="form-control controll1" name="customerId" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.customerId}  >
                                {customer3.selectCustomer.map((option2, index) => (
                                    <option value={option2.value} key={index}> {option2.label}</option>
                                ))}
                            </select>
                        </Col>
                    </Row>
                    {/* <Row>
                        <Col md={12}>
                            <MaterialTable
                                title="Hàng Hóa"
                                columns={
                                    [
                                        {
                                            field: 'productCode', title: 'Mã Hàng Hóa', headerStyle: {
                                                backgroundColor: '#383c36',
                                                color: '#FFF'
                                            }
                                        },
                                        {
                                            field: 'explanation', title: 'Diễn giải', headerStyle: {
                                                backgroundColor: '#383c36',
                                                color: '#FFF'
                                            }
                                        },
                                        {
                                            field: 'unit', title: 'Đơn vị tính', headerStyle: {
                                                backgroundColor: '#383c36',
                                                color: '#FFF'
                                            }
                                        },
                                        {
                                            field: 'amount', title: 'Số lượng', headerStyle: {
                                                backgroundColor: '#383c36',
                                                color: '#FFF'
                                            },
                                            type: 'numeric'
                                        },
                                        {
                                            field: 'price', title: 'Đơn giá ', headerStyle: {
                                                backgroundColor: '#383c36',
                                                color: '#FFF'
                                            },
                                            type: 'numeric'
                                        },
                                        {
                                            field: 'totalPrice', title: 'Thành Tiền', headerStyle: {
                                                backgroundColor: '#383c36',
                                                color: '#FFF'
                                            },
                                            type: 'numeric'
                                        },
                                        {
                                            field: 'discount', title: 'Triết khấu', headerStyle: {
                                                backgroundColor: '#383c36',
                                                color: '#FFF'
                                            },
                                            type: 'numeric'
                                        },
                                        {
                                            field: 'vat', title: 'Thuế', headerStyle: {
                                                backgroundColor: '#383c36',
                                                color: '#FFF'
                                            },
                                            type: 'numeric'
                                        },
                                        {
                                            field: 'vatMoney', title: ' Tiền Thuế ', headerStyle: {
                                                backgroundColor: '#383c36',
                                                color: '#FFF'
                                            },
                                            type: 'numeric'
                                        },
                                        {
                                            field: 'totalMoney', title: 'Tổng tiền', headerStyle: {
                                                backgroundColor: '#383c36',
                                                color: '#FFF'
                                            },
                                            type: 'numeric'
                                        }
                                    ]
                                }
                                data={dataproductInfo}
                                actions={[
                                    {
                                        icon: 'edit',
                                        tooltip: 'Chỉnh sửa ',
                                        onClick: (event, rowData) => selectModal(rowData, "Edit")
                                    },
                                    {
                                        icon: 'delete',
                                        tooltip: 'xóa',
                                        onClick: (event, rowData) => selectModal(rowData, "Editer")
                                    }
                                ]}
                                options={{
                                    actionsColumnIndex: -1,
                                    exportButton: true,
                                    filtering: true,
                                }}

                                localization={
                                    {
                                        header: {
                                            actions: 'Hành động'
                                        },
                                        body: {
                                            editRow: {
                                                deleteText: "Bạn có chắc chắn muốn xóa?"
                                            },
                                            emptyDataSourceMessage: 'Không có dữ liệu để hiển thị ',
                                        }
                                    }
                                }
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={12}>

                        </Col>
                    </Row>
                    <Row>
                        <Col md={12}>
                            <Button variant="primary" onClick={() => handleShow2()}>
                                Thêm Hàng Hóa
                            </Button>
                        </Col>
                    </Row> */}
                </Container>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => { clearErrors(); abrirCerrarModalEditar() }}>
                    Hủy bỏ
                </Button>
                <Button variant="primary" onClick={() => editInvoice()} disabled={!validateForm(artistaSeleccionado.errors)}>
                    Sửa
                </Button>
            </Modal.Footer>
        </Modal>
    )

    const ModalView = (
        <Modal backdrop="static" keyboard={false} size="lg" scrollable={true} show={modalViewInfor} onHide={() => viewInformation()}>
            <Modal.Header>
                <Modal.Title> Xem Thông Tin Chi Tiết </Modal.Title>
            </Modal.Header>
            <Modal.Body className="show-grid">
                <Container>
                    <Row>
                        <Col md={6}>
                            <label>Mã hóa đơn </label>
                            <input type="text" class="form-control" name="code" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.code}></input>
                        </Col>
                        <Col md={6}>
                            <label> Người Mua Hàng  </label>
                            <select className="form-control controll1" name="buyerId" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.buyerId} >
                                {contact3.selectContact.map((option2, index) => (
                                    <option value={option2.value} key={index}> {option2.label}</option>
                                ))}
                            </select>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label>Tài Khoản Ngân Hàng</label>
                            <input type="text" class="form-control" name="bankAccount" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.bankAccount}></input>
                        </Col>
                        <Col md={6}>
                            <label>Mở tại Ngân Hàng</label>
                            <input type="text" class="form-control" name="bank" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.bank}></input>
                        </Col>

                    </Row>
                    <Row>
                        <Col md={6}>
                            <label>Địa chỉ </label>
                            <input type="text" class="form-control" name="address" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.address}></input>
                        </Col>
                        <Col md={6}>
                            <label>Mã số thuế </label>
                            <input type="text" class="form-control" name="taxCode" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.taxCode}></input>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>

                            <label>Email người nhận</label>
                            <input type="text" class="form-control" name="receiverEmail" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.receiverEmail}></input>

                        </Col>
                        <Col md={6}>

                            <label>Tên người nhận</label>
                            <input type="text" class="form-control" name="receiverName" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.receiverName}></input>

                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label>Điện thoại người nhận</label>
                            <input type="text" class="form-control" name="receiverPhone" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.receiverPhone}></input>
                        </Col>
                        <Col md={6}>
                            <label> Đơn Hàng  </label>
                            <select className="form-control controll1" name="orderId" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.orderId} >
                                {order3.selectOrder3.map((option2, index) => (
                                    <option value={option2.value} key={index}> {option2.label}</option>
                                ))}
                            </select>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label> Khách Hàng  </label>
                            <select className="form-control controll1" name="customerId" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.customerId}  >
                                {customer3.selectCustomer.map((option2, index) => (
                                    <option value={option2.value} key={index}> {option2.label}</option>
                                ))}
                            </select>
                        </Col>
                        <Col md={6}>
                            <label > Ngày Tạo </label>
                            <input type="text" class="form-control" name="createdAt" value={artistaSeleccionado && artistaSeleccionado.createdAt}></input>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label > Ngày sửa </label>
                            <input type="text" class="form-control" name="updatedAt" value={artistaSeleccionado && artistaSeleccionado.updatedAt}></input>
                        </Col>
                        <Col md={6}>
                            <label > Người tạo </label>
                            <input type="text" class="form-control" name="createdBy" value={artistaSeleccionado && artistaSeleccionado.createdBy && artistaSeleccionado.createdBy.username}></input>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label>Người sửa </label>
                            <input type="text" class="form-control" name="updatedBy" value={artistaSeleccionado && artistaSeleccionado.updatedBy && artistaSeleccionado.updatedBy.username}></input>
                        </Col>
                        <Col md={6}>

                        </Col>
                    </Row>
                </Container>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => viewInformation()}>
                    Hủy Bỏ
                </Button>
            </Modal.Footer>
        </Modal>
    )

    const ModalDelete = (
        <Modal backdrop="static" keyboard={false} show={modalEliminar} onHide={abrirCerrarModalEliminar}>
            <Modal.Header>
                <Modal.Title>Xóa</Modal.Title>
            </Modal.Header>
            <Modal.Body className="show-grid">
                <p> Bạn có chắc chắn muốn xóa   không ? </p>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => abrirCerrarModalEliminar()}>
                    Không
                </Button>
                <Button variant="primary" onClick={() => deleteInvoice()}>
                    Có
                </Button>
            </Modal.Footer>
        </Modal>
    )

    return (

        <div className="container-fluid">

            <div className="row">
                <div className="col-md-12">
                    {TableInvoice}
                    {ModalInsert}
                    {ModalView}
                    {ModalEdit}
                    {ModalDelete}
                    {ModalEditChild}
                    {ModalInsertChild}
                    {ModalDeleteChild}
                </div>
            </div>
        </div>

    );
}

export default Invoice;

