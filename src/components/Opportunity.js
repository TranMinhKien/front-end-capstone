import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import axios from 'axios';
import MaterialTable, { MTablePagination } from 'material-table';
import { makeStyles } from '@material-ui/core/styles';
import "../App.css";
import authHeader from '../services/auth_header';
import { Button, Modal, Container, Row, Col } from 'react-bootstrap';
import { DOMAIN_BE } from '../constant/constant';
import { commonFetchDataHandler } from '../utils/index'
import { commonFetchAdd } from '../utils/add';
import { event } from 'jquery';
import { missingPermission } from '../utils/errorMessage';
import AuthService from "../services/auth.service";
import validation from '../utils/validation';
import VisibilityIcon from '@material-ui/icons/Visibility'
import { Grid, TablePagination, Typography } from '@material-ui/core';
import { toastSuccess } from '../utils/MessageSuccess';
import { ErrorMess } from '../utils/MessageErrors';


// API

const baseURL = DOMAIN_BE + "/api/opportunity/";
const baseURLcustomer = DOMAIN_BE + "/api/customer/name";
const baseURLcontact = DOMAIN_BE + "/api/contact/name";
const baseURLsource = DOMAIN_BE + "/api/source/name";
const baseURLphase = DOMAIN_BE + "/api/opportunityPhase/name";
const baseURLproduct = DOMAIN_BE + "/api/product/name";

const useStyles = makeStyles((theme) => ({
    modal: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)'
    },
    iconos: {
        cursor: 'pointer'
    },
    inputMaterial: {
        width: '100%'
    }
}));


const validateForm = (errors) => {
    let valid = true;
    if (errors == null)
        return true;
    Object.values(errors).forEach(
        (val) => val.length > 0 && (valid = false)
    );
    return valid;
}

const validEmailRegex = RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
const validPhoneRegex = RegExp(/^\d*$/)
function Opportunity() {
    const styles = useStyles();
    const [data, setData] = useState([]);
    const [dataproductInfo, setDataProductInfo] = useState([]);
    const [modalInsertar, setModalInsertar] = useState(false);
    const [modalEditar, setModalEditar] = useState(false);
    const [modalEliminar, setModalEliminar] = useState(false);
    const [modalViewInfor, setModalViewInfor] = useState(false);
    const [deleteChild, setDeleteChild] = useState(false);
    const [EditChild, setEditChild] = useState(false);
    // lấy thông tin USER
    const user = AuthService.getCurrentUser();
    const [artistaSeleccionado, setArtistaSeleccionado] = useState({
        id: null,
        customerId: null,
        contactId: null,
        name: null,
        moneyAmount: null,
        opportunityPhaseId: null,
        successRate: null,
        expectedEndDate: null,
        expectedTurnOver: null,
        sourceId: null,
        productInfoDtos: [],
        errors: {
            name: '',
            successRate: '',
            opportunityPhaseId: '',
            expectedEndDate: ''
        }
    });



    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [show2, setshow2] = useState(false);
    const handleClose2 = () => setshow2(false);
    const handleShow2 = () => setshow2(true);

    const hasPerm = (user, perm = "") => {
        return user && user.roles && user.roles.includes(perm)
    }

    const [source3, setSource3] = useState({
        selectSource: [],
        id: "",
        name: ""
    });

    const [product3, setProduct3] = useState({
        selectProduct: [],
        id: "",
        name: ""
    });

    const [customer3, setCustomer3] = useState({
        selectCustomer: [],
        id: "",
        name: ""
    });

    const [contact3, setContact3] = useState({
        selectContact: [],
        id: "",
        name: ""
    });

    const [phase3, setPhase3] = useState({
        selectPhase: [],
        id: "",
        name: ""
    });

    const clearErrors = () => {
        setArtistaSeleccionado(prevState => ({
            ...prevState,
            errors: {}
        }));
    }

    // thực hiện hành động input trong textfiels 
    const handleChange = e => {
        const { name, value } = e.target;
        let errors = artistaSeleccionado.errors || (artistaSeleccionado.errors = {})
        validation.value = value;

        switch (name) {
            case 'name':
                errors.name = validation.validate('Tên', [validation.notBlank])
                break;
            // case 'moneyAmount':
            //     errors.moneyAmount = validation.validate('Số tiền', [validation.number, validation.notBlank])
            //     break;
            case 'successRate':
                errors.successRate = validation.validate('Tỷ lệ thành công ', [validation.notBlank, validation.number, validation.rate])
                break;
            // case 'expectedTurnOver':
            //     errors.expectedTurnOver = validation.validate('Doanh số kì vọng', [validation.number, validation.notBlank])
            //     break;
            case 'opportunityPhaseId':
                errors.opportunityPhaseId = validation.validate('Giai đoạn ', [validation.notBlank])
                break;
            case 'expectedEndDate':
                errors.expectedEndDate = validation.validate('Ngày kì vọng ', [validation.notBlank])
                break;
            default:
                break;
        }
        setArtistaSeleccionado(prevState => ({
            ...prevState,
            [name]: value || null
        }));
        console.log(artistaSeleccionado);
    }

    const createOppotunity = async () => {
        // Check validate khi ấn "Thêm"
        for (const name in artistaSeleccionado) {
            const value = artistaSeleccionado[name]
            handleChange({
                target: { name, value }
            })
        }
        // Nếu chưa valid thì không thêm dữ liệu
        if (!validateForm(artistaSeleccionado.errors))
            return;

        commonFetchAdd(baseURL, artistaSeleccionado).then(responseSource => {
            const d = responseSource.data
            d.sourceId = d.source ? d.source.id : null
            d.customerId = d.customer ? d.customer.id : null
            d.contactId = d.contact ? d.contact.id : null
            d.opportunityPhaseId = d.opportunityPhase ? d.opportunityPhase.id : null
            setData(data.concat(d))
            handleClose();
            toastSuccess("Thêm dữ liệu thành công");
        }).catch(error => {
            if (error.response && error.response.data && error.response.data.errors && error.response.data.errors[0].type === 'duplicate') {
                let errors = artistaSeleccionado.errors || (artistaSeleccionado.errors = {})
                error.response.data.errors.forEach(e => {
                    errors[e.field] = 'Giá trị đã bị trùng';
                });
                setArtistaSeleccionado(prevState => ({
                    ...prevState,
                    errors: errors,
                }));
                console.log(artistaSeleccionado)
            } else {
                ErrorMess(error);
            }
        })
    }


    // thêm product child
    const peticionPostChild = async () => {
        await axios.post(baseURL + artistaSeleccionado.id + "/product/" + artistaSeleccionado.productId, artistaSeleccionado, { headers: { 'Authorization': authHeader().Authorization } })
            .then(response => {
                const d = response.data;
                d.productId = d.product ? d.product.id : null;
                // thêm vào bảng cũ
                setDataProductInfo(dataproductInfo.concat(d));
                // Thêm vào bảng mới
                const newData = data;
                newData.forEach(n => {
                    if (n && n.id === d.opportunityId) {
                        if (Array.isArray(n.productInfos));
                        else n.productInfos = [];
                        n.productInfos.push(d);
                    }
                });
                // sửa cái 1 
                handleClose2();
                toastSuccess("Thêm dữ liệu thành công ");
            }).catch(error => {
                ErrorMess(error);
            })
    }


    const editOpportunity = async () => {
        await axios.put(baseURL + artistaSeleccionado.id, artistaSeleccionado, { headers: { 'Authorization': authHeader().Authorization } })
            .then(response => {
                const dataNueva = data;
                dataNueva.map(id => {
                    if (id.id === artistaSeleccionado.id) {
                        //TODO 2
                        id.customer = response.data.customer;
                        id.customerId = id.customer?.id;

                        id.contact = response.data.contact;
                        id.contactId = id.contact?.id;

                        id.source = response.data.source;
                        id.sourceId = id.source?.id;

                        id.opportunityPhase = response.data.opportunityPhase;
                        id.opportunityPhaseId = id.opportunityPhase?.id;

                        id.fullName = response.data.fullName;


                        id.name = artistaSeleccionado.name;
                        id.moneyAmount = artistaSeleccionado.moneyAmount;
                        id.successRate = artistaSeleccionado.successRate;
                        id.expectedEndDate = artistaSeleccionado.expectedEndDate;
                        id.expectedTurnOver = artistaSeleccionado.expectedTurnOver;
                        id.amount = artistaSeleccionado.amount;
                        id.productCode = artistaSeleccionado.productCode;
                        id.vat = artistaSeleccionado.vat;
                        id.explanation = artistaSeleccionado.explanation;
                        id.totalPrice = artistaSeleccionado.totalPrice;
                        id.unit = artistaSeleccionado.unit;
                    }
                });
                setData(dataNueva);
                abrirCerrarModalEditar();
                toastSuccess("Edit dữ liệu thành công");
            }).catch(error => {
                ErrorMess(error);
            })
    }

    const peticionUpdateProductInfo = async () => {
        await axios.put(baseURL + artistaSeleccionado.opportunityId + "/product/" + artistaSeleccionado.id, artistaSeleccionado, { headers: { 'Authorization': authHeader().Authorization } })
            .then(response => {
                const dataNueva = data;
                dataNueva.map(id => {
                    if (id.id === artistaSeleccionado.id) {
                        id.amount = artistaSeleccionado.amount;
                        id.productCode = artistaSeleccionado.productCode;
                        id.vat = artistaSeleccionado.vat;
                        id.explanation = artistaSeleccionado.explanation;
                        id.totalPrice = artistaSeleccionado.totalPrice;
                        id.unit = artistaSeleccionado.unit;
                        id.price = artistaSeleccionado.price;

                        id.product = response.data.product;
                        id.productId = id.product?.id;

                    }
                });
                setData(dataNueva);
                SettingModalEdit();
                abrirCerrarModalEditar();
                toastSuccess("Edit dữ liệu thành công");
            }).catch(error => {
                ErrorMess(error);
            })
    }



    // xóa dữ liệu
    const deleteOppotunity = async () => {
        await axios.delete(baseURL + artistaSeleccionado.id, { headers: { 'Authorization': authHeader().Authorization } })
            .then(response => {
                const newData = data
                setData(newData.filter(name => name.id !== artistaSeleccionado.id));
                abrirCerrarModalEliminar();
                toastSuccess("Xóa dữ liệu thành công");
            }).catch(error => {
                ErrorMess(error);
            })
    }

    const peticionDeleteChild = async () => {
        await axios.delete(baseURL + artistaSeleccionado.opportunityId + "/product/" + artistaSeleccionado.id, { headers: { 'Authorization': authHeader().Authorization } })
            .then(response => {
                const newData = data
                setData(newData.filter(name => name.id !== artistaSeleccionado.id));
                SettingModalDeleteChild();
                abrirCerrarModalEditar();
                toastSuccess("Xóa dữ liệu thành công");
            }).catch(error => {
                ErrorMess(error);
            })
    }

    const seleccionarArtista = (id, caso) => {
        setArtistaSeleccionado(id);
        peticionGetProductInfo(id);
        (caso === "Editar") ? abrirCerrarModalEditar()
            :
            abrirCerrarModalEliminar()
    }

    const selectModal = (id, caso) => {
        setArtistaSeleccionado(id);
        (caso === "them") ? handleShow2() : ((caso === "sua") ? SettingModalEdit() : SettingModalDeleteChild())

    }
    const viewInformation = () => {
        setModalViewInfor(!modalViewInfor);
    }
    const seleccionarView = (id, caso) => {
        if (caso === "check") {
            viewInformation();
        }
    }

    const abrirCerrarModalInsertar = () => {
        setModalInsertar(!modalInsertar);
    }
    const abrirCerrarModalEditar = () => {
        setModalEditar(!modalEditar);
    }
    const abrirCerrarModalEliminar = () => {
        setModalEliminar(!modalEliminar);
    }

    const SettingModalEdit = () => {
        setEditChild(!EditChild);
    }

    const SettingModalDeleteChild = () => {
        setDeleteChild(!deleteChild);
    }

    useEffect(() => {
        fetchData()
    }, [])

    const getProduct = async () => {
        const responseProduct = await commonFetchDataHandler(baseURLproduct)
        const optionsProduct = responseProduct.data.map(d => ({
            "value": d.id,
            "label": d.name
        }))
        setProduct3({ selectProduct: optionsProduct })
    }

    // hiện lên thong tin bên trong 
    const peticionGetProductInfo = async (id) => {
        await axios({
            method: "GET",
            url: baseURL + id.id + "/product",
            headers: {
                'Authorization': authHeader().Authorization
            }
        }).then(response => {
            response.data.forEach(element => {
                //TODO3
                element.productId = element.product ? element.product.id : null
                element.sourceId = element.source ? element.source.id : null
                element.contactId = element.contact ? element.contact.id : null
                element.customerId = element.customer ? element.customer.id : null
                element.opportunityPhaseId = element.opportunityPhase ? element.opportunityPhase.id : null
            });
            setDataProductInfo(response.data);
        }).catch(error => {
            ErrorMess(error);
        })
    }
    const fetchData = async () => {
        const response = await commonFetchDataHandler(baseURL)
        response.data.forEach(element => {
            //TODO3
            element.sourceId = element.source ? element.source.id : null
            element.contactId = element.contact ? element.contact.id : null
            element.customerId = element.customer ? element.customer.id : null
            element.opportunityPhaseId = element.opportunityPhase ? element.opportunityPhase.id : null
        });
        setData(response.data)

        const responsePhase = await commonFetchDataHandler(baseURLphase)
        const optionsPhase = responsePhase.data.map(d => ({
            "value": d.id,
            "label": d.name
        }))
        optionsPhase.unshift({
            "value": "",
            "label": "-- Không chọn --"
        })
        setPhase3({ selectPhase: optionsPhase })

        const responseContact = await commonFetchDataHandler(baseURLcontact)
        const optionsContact = responseContact.data.map(d => ({
            "value": d.id,
            "label": d.name
        }))
        optionsContact.unshift({
            "value": "",
            "label": "-- Không chọn --"
        })
        setContact3({ selectContact: optionsContact })

        const responseCustomer = await commonFetchDataHandler(baseURLcustomer)
        const optionsCustomer = responseCustomer.data.map(d => ({
            "value": d.id,
            "label": d.name
        }))
        optionsCustomer.unshift({
            "value": "",
            "label": "-- Không chọn --"
        })
        setCustomer3({ selectCustomer: optionsCustomer })

        const responseSource = await commonFetchDataHandler(baseURLsource)
        const optionsSource = responseSource.data.map(d => ({
            "value": d.id,
            "label": d.name
        }))
        optionsSource.unshift({
            "value": "",
            "label": "-- Không chọn --"
        })
        setSource3({ selectSource: optionsSource })

        getProduct();
    }

    const TableOpportunity = (
        <MaterialTable
            title="Cơ Hội"
            columns={
                [
                    {
                        field: 'name', title: 'Tên cơ hội',
                    },
                    {
                        field: 'contact.name', title: 'Liên hệ',
                    },
                    {
                        field: 'customer.name', title: 'Khách Hàng',
                    },
                    {
                        field: 'source.name', title: 'Nguồn gốc',
                    },
                    {
                        field: 'opportunityPhase.name', title: 'Giai đoạn',
                    },
                    {
                        field: 'expectedEndDate', title: 'Ngày kì vọng kết thúc',
                        type: "date"
                    },
                ]
            }
            data={data}
            actions={[
                {
                    icon: 'edit',
                    tooltip: 'Sửa thông tin',
                    // lấy id trong bảng 
                    onClick: (event, rowData) => (hasPerm(user, "ROLE_UPDATE_OPPORTUNITY") ? seleccionarArtista(rowData, "Editar") : missingPermission())
                },
                {
                    icon: 'delete',
                    tooltip: 'Xóa thông tin',
                    onClick: (event, rowData) => (hasPerm(user, "ROLE_DELETE_OPPORTUNITY") ? seleccionarArtista(rowData, "Eliminar") : missingPermission())
                },
                {
                    icon: () => <VisibilityIcon />,
                    tooltip: 'Xem chi tiết',
                    onClick: (event, rowData) => (hasPerm(user, "ROLE_READ_OPPORTUNITY") ? seleccionarView(rowData, "check") : missingPermission())
                },
                {
                    icon: "add_box",
                    tooltip: "Thêm",
                    position: "toolbar",
                    onClick: (event, rowData) => (hasPerm(user, "ROLE_CREATE_OPPORTUNITY") ? handleShow() : missingPermission())
                }

            ]}
            options={{
                headerStyle: {
                    backgroundColor: '#696969',
                    color: '#FFF'
                },
                rowStyle: {
                    backgroundColor: '#EEE',
                },
                actionsColumnIndex: -1,
                pageSize: 10,
                pageSizeOptions: [5, 10, 20, 30, 50, 75, 100],
                exportButton: { csv: true },
                filtering: true,
            }}

            localization={{
                header: {
                    actions: 'Hành động'
                },
                body: {
                    emptyDataSourceMessage: 'Không có dữ liệu để hiển thị',
                }
            }}

            onRowClick={(event, rowData, togglePanel) => togglePanel()}

            detailPanel={(rowData) => {
                return (<MaterialTable
                    title="Hàng Hóa"
                    localization={{
                        header: {
                            actions: 'Hành động'
                        },
                        body: {
                            editRow: { deleteText: 'Bạn có chắc chắn muốn xóa?' },
                            emptyDataSourceMessage: 'Không có dữ liệu để hiển thị ',
                        }
                    }}
                    columns={
                        [
                            {
                                field: 'productCode', title: 'Mã Hàng Hóa', editable: 'never',
                                // width: 120,
                                validate: rowData => validation.validate1("Mã hàng hóa", rowData.productCode, [validation.notBlank]),
                            },
                            {
                                field: 'explanation', title: 'Diễn giải khi bán',
                                // width: 300,
                            },
                            {
                                field: 'unit', title: 'Đơn vị tính',
                                // width: 100,
                            },
                            {
                                field: 'amount', title: 'Số lượng', type: 'numeric',
                                // width: 100,
                                validate: rowData => validation.validate1("Số lượng", rowData.amount, [validation.notBlank, validation.number]),
                            },
                            {
                                field: 'price', title: 'Đơn giá ', type: 'numeric',
                                // width: 100,
                                validate: rowData => validation.validate1("Đơn giá", rowData.price, [validation.notBlank, validation.number, validation.positive]),
                            },
                            {
                                field: 'totalPrice', title: 'Thành Tiền', editable: 'never', type: 'numeric',
                                // width: 100,
                            },
                            {
                                field: 'discount', title: 'Chiết khấu (%)', type: 'numeric',
                                // width: 100,
                                validate: rowData => validation.validate1("Chiết khấu", rowData.discount, [validation.notBlank, validation.number, validation.rate]),
                            },
                            {
                                field: 'discountMoney', title: 'Tiền chiết khấu', editable: 'never', type: 'numeric',
                            },
                            {
                                field: 'vat', title: 'Thuế', type: 'numeric',
                                // width: 100,
                                validate: rowData => validation.validate1("Chiết khấu", rowData.vat, [validation.notBlank, validation.number, validation.rate]),
                            },
                            {
                                field: 'vatMoney', title: ' Tiền Thuế ', editable: 'never', type: 'numeric',
                                // width: 100,
                            },
                            {
                                field: 'totalMoney', title: 'Tổng tiền', editable: 'never', type: 'numeric',
                                // width: 100,
                            }
                        ]
                    }

                    data={rowData.productInfos ? rowData.productInfos : rowData.productInfos = []}

                    actions={[
                        {
                            icon: 'add_box',
                            tooltip: 'Thêm',
                            onClick: () => (hasPerm(user, "ROLE_UPDATE_OPPORTUNITY") ? selectModal(rowData, "them") : missingPermission()),
                            position: 'toolbar'
                        },
                        {
                            icon: 'edit',
                            tooltip: 'Chỉnh sửa ',
                            onClick: (event, rowData) => (hasPerm(user, "ROLE_UPDATE_OPPORTUNITY") ? selectModal(rowData, "sua") : missingPermission()),
                        },
                        {
                            icon: 'delete',
                            tooltip: 'xóa',
                            position: 'toolbarOnSelect',
                            onClick: async (event, oldData) => {
                                if (hasPerm(user, 'ROLE_UPDATE_OPPORTUNITY')) {
                                    let ids = oldData;
                                    ids = Array.isArray(ids) ? ids.map(a => a.id) : [ids.id];
                                    try {
                                        const response = await axios.delete(baseURL + rowData.id + "/product/" + ids.join(','), { headers: { 'Authorization': authHeader().Authorization } });
                                        const dataDelete = [...rowData.productInfos].filter(d => !ids.includes(d.id));
                                        rowData.productInfos = dataDelete;
                                        setData(prevState => ({
                                            ...prevState,
                                            productInfos: dataDelete.filter(d_1 => !ids.includes(d_1.id)),
                                        }));
                                    } catch (error) {
                                        ErrorMess(error);
                                    }
                                } else missingPermission();
                            },
                        }
                    ]}
                    editable={{
                        onRowUpdate: (newData, oldData) => axios.put(baseURL + oldData.opportunityId + "/product/" + oldData.id, newData, { headers: { 'Authorization': authHeader().Authorization } })
                            .then(response => {
                                const dataUpdate = [...rowData.productInfos];
                                dataUpdate.map(d => {
                                    if (d.id === oldData.id) Object.assign(d, response.data)
                                });
                                // setData(prevState => ({
                                //     ...prevState,
                                //     productInfos: dataUpdate
                                // }));
                                const newData = data;
                                newData.forEach(n => {
                                    if (n && n.id === rowData.id) {
                                        n.productInfos = dataUpdate;
                                    }
                                });
                                setData(newData);
                            }).catch(error => {
                                ErrorMess(error);
                            }),
                        onRowDelete: async (oldData) => {
                            let ids = oldData
                            ids = Array.isArray(ids) ? ids.map(a => a.id) : [ids.id]
                            try {
                                const response = await axios.delete(baseURL + oldData.opportunityId + "/product/" + ids.join(','), { headers: { 'Authorization': authHeader().Authorization } });
                                const dataDelete = rowData.productInfos.filter(d => !ids.includes(d.id));
                                setData(prevState => {
                                    return prevState.map(p => {
                                        if (p?.id === rowData?.id) {
                                            p.productInfos = dataDelete;
                                        }
                                        return p;
                                    })
                                });
                            } catch (error) {
                                ErrorMess(error);
                            }
                        },
                    }}
                    options={{
                        actionsColumnIndex: -1,
                        selection: true,
                        pageSize: 5,
                        pageSizeOptions: [5, 10, 20, 30, 50, 75, 100],
                        exportButton: { csv: true },
                        filtering: true,
                        headerStyle: {
                            backgroundColor: '#696969',
                            color: '#FFF'
                        },
                        rowStyle: {
                            backgroundColor: '#EEE',
                        },

                    }}
                    components={{
                        Pagination: (props) => <div>
                            <Grid container style={{ padding: 15, background: "f5f5f5" }}>
                                <Grid sm={1.95} item><Typography variant="subtitle2">Tổng cộng:</Typography></Grid>
                                <Grid sm={2} item align="center"><Typography variant="subtitle2">Số lượng: {rowData.productInfos?.reduce((accumulator, i) => accumulator + i.amount, 0)}</Typography></Grid>
                                <Grid sm={2} item align="center"><Typography variant="subtitle2">Thành tiền: {rowData.productInfos?.reduce((accumulator, i) => accumulator + i.totalPrice, 0)}</Typography></Grid>
                                <Grid sm={2} item align="center"><Typography variant="subtitle2">Tiền chiết khấu: {rowData.productInfos?.reduce((accumulator, i) => accumulator + i.discountMoney, 0)}</Typography></Grid>
                                <Grid sm={2} item align="center"><Typography variant="subtitle2">Tiền thuế: {rowData.productInfos?.reduce((accumulator, i) => accumulator + i.vatMoney, 0)}</Typography></Grid>
                                <Grid sm={2} item align="center"><Typography variant="subtitle2">Tổng tiền: {rowData.productInfos?.reduce((accumulator, i) => accumulator + i.totalMoney, 0)}</Typography></Grid>
                            </Grid>
                            <TablePagination {...props} />
                        </div>
                    }}
                />)
            }}
        />
    )
    const hasError = (name) => {
        return artistaSeleccionado && artistaSeleccionado.errors && artistaSeleccionado.errors[name] &&
            artistaSeleccionado.errors[name].length > 0 &&
            (<span className='error'>{artistaSeleccionado.errors[name]}</span>)
    }
    const ModalInsert = (
        <Modal backdrop="static"
            keyboard={false} size="lg" show={show} onHide={() => handleClose()}>
            <Modal.Header >
                <Modal.Title> Thêm Cơ Hội </Modal.Title>
            </Modal.Header>
            <Modal.Body className="show-grid">
                <Container>
                    <Row>
                        <Col md={6}>
                            <label> Khách hàng </label>
                            <select className="form-control controll" name="customerId" onChange={handleChange} >
                                {customer3.selectCustomer.map((option2, index) => (
                                    <option value={option2.value} key={index}> {option2.label}</option>
                                ))}
                            </select>
                        </Col>
                        <Col md={6}>
                            <label> Liên hệ </label>
                            <select className="form-control controll" name="contactId" onChange={handleChange} >
                                {contact3.selectContact.map((option2, index) => (
                                    <option value={option2.value} key={index}> {option2.label}</option>
                                ))}
                            </select>
                        </Col>
                    </Row>

                    <Row>
                        <Col md={6}>
                            <label  className ="required">Tên cơ hội</label>
                            <input type="text" class="form-control" name="name" onChange={handleChange}></input>
                            {hasError('name')}
                        </Col>
                        <Col md={6}>
                            <label  className ="required">Tỷ lệ thành công</label>
                            <input type="text" class="form-control" name="successRate" onChange={handleChange}></input>
                            {hasError('successRate')}
                        </Col>
                    </Row>

                    <Row>
                        <Col md={6}>
                            <label  className ="required"> Giai Đoạn  </label>
                            <select className="form-control controll" name="opportunityPhaseId" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.opportunityPhaseId} >
                                {phase3.selectPhase.map((option2, index) => (
                                    <option value={option2.value} key={index}> {option2.label}</option>
                                ))}
                            </select>
                            {hasError('opportunityPhaseId')}
                        </Col>
                        <Col md={6}>
                            <label> Nguồn gốc</label>
                            <select className="form-control controll" name="sourceId" onChange={handleChange} >
                                {source3.selectSource.map((option2, index) => (
                                    <option value={option2.value} key={index}> {option2.label}</option>
                                ))}
                            </select>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label  className ="required">Ngày kì vọng kết thúc</label>
                            <input type="date" class="form-control controll" name="expectedEndDate" onChange={handleChange} defaultValue={artistaSeleccionado.expectedEndDate}></input>
                            {hasError('expectedEndDate')}
                        </Col>
                    </Row>
                </Container>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => { clearErrors(); handleClose() }}>
                    Hủy Bỏ
                </Button>
                <Button variant="primary" onClick={() => createOppotunity()} disabled={!validateForm(artistaSeleccionado.errors)} >
                    Thêm
                </Button>
            </Modal.Footer>
        </Modal>
    )

    const ModalEdit = (
        <Modal backdrop="static"
            keyboard={false} size="lg" scrollable={true} show={modalEditar} onHide={() => abrirCerrarModalEditar}>
            <Modal.Header>
                <Modal.Title>Sửa Thông Tin</Modal.Title>
            </Modal.Header>
            <Modal.Body className="show-grid">
                <Container>
                    <Row>
                        <Col md={6}>
                            <label> Khách hàng </label>
                            <select className="form-control controll1" name="customerId" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.customerId} >
                                {customer3.selectCustomer.map((option2, index) => (
                                    <option value={option2.value} key={index}> {option2.label}</option>
                                ))}
                            </select>
                        </Col>
                        <Col md={6}>
                            <label> Liên hệ </label>
                            <select className="form-control controll1" name="contactId" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.contactId} >
                                {contact3.selectContact.map((option2, index) => (
                                    <option value={option2.value} key={index}> {option2.label}</option>
                                ))}
                            </select>
                        </Col>
                    </Row>

                    <Row>
                        <Col md={6}>
                            <label  className ="required">Tên cơ hội</label>
                            <input type="text" class="form-control" name="name" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.name}></input>
                            {hasError('name')}
                        </Col>
                        <Col md={6}>
                            <label> Nguồn gốc</label>
                            <select className="form-control controll1" name="sourceId" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.sourceId} >
                                {source3.selectSource.map((option2, index) => (
                                    <option value={option2.value} key={index}> {option2.label}</option>
                                ))}
                            </select>
                        </Col>
                    </Row>

                    <Row>
                        <Col md={6}>
                            <label  className ="required"> Giai Đoạn  </label>
                            <select className="form-control controll1" name="opportunityPhaseId" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.opportunityPhaseId} >
                                {phase3.selectPhase.map((option2, index) => (
                                    <option value={option2.value} key={index}> {option2.label}</option>
                                ))}
                            </select>
                            {hasError('opportunityPhaseId')}
                        </Col>
                        <Col md={6}>
                            <label  className ="required">Tỷ lệ thành công</label>
                            <input type="text" class="form-control" name="successRate" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.successRate}></input>
                            {hasError('successRate')}
                        </Col>
                    </Row>

                    <Row>
                        <Col md={6}>
                            <label  className ="required">Ngày kì vọng kết thúc</label>
                            <input type="date" class="form-control controll1" name="expectedEndDate" onChange={handleChange} defaultValue={artistaSeleccionado.expectedEndDate} value={artistaSeleccionado && artistaSeleccionado.expectedEndDate}></input>
                            {hasError('expectedEndDate')}
                        </Col>
                    </Row>
                    {/* <Row>
                        <Col md={12}>
                            <MaterialTable
                                title="Hàng Hóa"
                                columns={
                                    [
                                        {
                                            field: 'productCode', title: 'Mã Hàng Hóa',
                                        },
                                        {
                                            field: 'explanation', title: 'Diễn giải',
                                        },
                                        {
                                            field: 'unit', title: 'Đơn vị tính',
                                        },
                                        {
                                            field: 'amount', title: 'Số lượng', type: 'numeric'
                                        },
                                        {
                                            field: 'price', title: 'Đơn giá ', type: 'numeric'
                                        },
                                        {
                                            field: 'totalPrice', title: 'Thành Tiền', type: 'numeric'
                                        },
                                        {
                                            field: 'discount', title: 'Triết khấu', type: 'numeric'
                                        },
                                        {
                                            field: 'vat', title: 'Thuế', type: 'numeric'
                                        },
                                        {
                                            field: 'vatMoney', title: ' Tiền Thuế ', type: 'numeric'
                                        },
                                        {
                                            field: 'totalMoney', title: 'Tổng tiền', type: 'numeric'
                                        }
                                    ]
                                }

                                data={dataproductInfo}

                                actions={[
                                    {
                                        icon: 'add_box',
                                        tooltip: 'Thêm',
                                        onClick: () => handleShow2(),
                                        position: 'toolbar'
                                    },
                                    {
                                        icon: 'edit',
                                        tooltip: 'Chỉnh sửa ',
                                        onClick: (event, rowData) => selectModal(rowData, "sua")
                                    },
                                    {
                                        icon: 'delete',
                                        tooltip: 'xóa',
                                        onClick: (event, rowData) => selectModal(rowData, "xoa")
                                    }
                                ]}
                                options={{
                                    actionsColumnIndex: -1,
                                    fixedColumns: {
                                        left: 1,
                                        right: 0,
                                    },
                                    exportButton: true,
                                    filtering: true,
                                    headerStyle: {
                                        backgroundColor: '#696969',
                                        color: '#FFF',
                                    }
                                }}

                                localization={{
                                    header: {
                                        actions: 'Hành động',
                                    },
                                    body: {
                                        emptyDataSourceMessage: 'Không có dữ liệu để hiển thị',
                                    }
                                }}
                                components={{
                                    Pagination: (props) => <div>
                                        <Grid container style={{ background: "f5f5f5" }}>
                                            <Grid sm={1.5} item><Typography variant="subtitle2">Tổng cộng</Typography></Grid>
                                            <Grid sm={2} item align="center"><Typography variant="subtitle2">Số lượng: {dataproductInfo.reduce((accumulator, i) => accumulator + i.amount, 0)}</Typography></Grid>
                                            <Grid sm={2} item align="center"><Typography variant="subtitle2">Thành tiền: {dataproductInfo.reduce((accumulator, i) => accumulator + i.totalPrice, 0)}</Typography></Grid>
                                            <Grid sm={2} item align="center"><Typography variant="subtitle2">Tiền chiết khấu: {dataproductInfo.reduce((accumulator, i) => accumulator + i.discountMoney, 0)}</Typography></Grid>
                                            <Grid sm={2} item align="center"><Typography variant="subtitle2">Tiền thuế: {dataproductInfo.reduce((accumulator, i) => accumulator + i.vatMoney, 0)}</Typography></Grid>
                                            <Grid sm={2} item align="center"><Typography variant="subtitle2">Tổng tiền: {dataproductInfo.reduce((accumulator, i) => accumulator + i.moneyAmount, 0)}</Typography></Grid>
                                        </Grid>
                                        <TablePagination {...props} />
                                    </div>
                                }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={12}>

                        </Col>
                    </Row>
                    <Row>
                        <Col md={12}>
                            <Button variant="primary" onClick={() => handleShow2()}>
                                Thêm Hàng Hóa
                            </Button>
                        </Col>
                    </Row> */}
                </Container>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => { clearErrors(); abrirCerrarModalEditar() }}>
                    Hủy bỏ
                </Button>
                <Button variant="primary" onClick={() => editOpportunity()} disabled={!validateForm(artistaSeleccionado.errors)}>
                    Sửa
                </Button>
            </Modal.Footer>
        </Modal>
    )

    const ModalView = (
        <Modal backdrop="static"
            keyboard={false} size="lg" scrollable={true} show={modalViewInfor} onHide={() => viewInformation()}>
            <Modal.Header>
                <Modal.Title>Xem Thông Tin Chi Tiết</Modal.Title>
            </Modal.Header>
            <Modal.Body className="show-grid">
                <Container>
                    <Row>
                        <Col md={6}>
                            <label> Khách hàng </label>
                            <select className="form-control controll1" name="customerId" value={artistaSeleccionado && artistaSeleccionado.customerId} >
                                {customer3.selectCustomer.map((option2, index) => (
                                    <option value={option2.value} key={index}> {option2.label}</option>
                                ))}
                            </select>
                        </Col>
                        <Col md={6}>
                            <label> Liên hệ </label>
                            <select className="form-control controll1" name="contactId" value={artistaSeleccionado && artistaSeleccionado.contactId} >
                                {contact3.selectContact.map((option2, index) => (
                                    <option value={option2.value} key={index}> {option2.label}</option>
                                ))}
                            </select>
                        </Col>
                    </Row>

                    <Row>
                        <Col md={6}>
                            <label>Tên cơ hội</label>
                            <input type="text" class="form-control" name="name" value={artistaSeleccionado && artistaSeleccionado.name}></input>
                        </Col>
                        <Col md={6}>
                            <label> Giai Đoạn  </label>
                            <select className="form-control controll1" name="opportunityPhaseId" value={artistaSeleccionado && artistaSeleccionado.opportunityPhaseId} >
                                {phase3.selectPhase.map((option2, index) => (
                                    <option value={option2.value} key={index}> {option2.label}</option>
                                ))}
                            </select>
                        </Col>
                    </Row>

                    <Row>
                        <Col md={6}>
                            <label>Tỷ lệ thành công</label>
                            <input type="text" class="form-control" name="successRate" value={artistaSeleccionado && artistaSeleccionado.successRate}></input>
                        </Col>
                        <Col md={6}>
                            <label>Ngày kì vọng kết thúc</label>
                            <input type="date" class="form-control controll1" name="expectedEndDate" defaultValue={artistaSeleccionado.expectedEndDate} value={artistaSeleccionado && artistaSeleccionado.expectedEndDate}></input>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label> Nguồn gốc</label>
                            <select className="form-control controll1" name="sourceId" value={artistaSeleccionado && artistaSeleccionado.sourceId} >
                                {source3.selectSource.map((option2, index) => (
                                    <option value={option2.value} key={index}> {option2.label}</option>
                                ))}
                            </select>
                        </Col>
                        <Col md={6}>
                            <label > Ngày Tạo </label>
                            <input type="text" class="form-control" name="createdAt" value={artistaSeleccionado && artistaSeleccionado.createdAt}></input>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label > Ngày sửa </label>
                            <input type="text" class="form-control" name="updatedAt" value={artistaSeleccionado && artistaSeleccionado.updatedAt}></input>
                        </Col>
                        <Col md={6}>
                            <label > Người tạo </label>
                            <input type="text" class="form-control" name="createdBy" value={artistaSeleccionado && artistaSeleccionado.createdBy && artistaSeleccionado.createdBy.username} />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label > Người sửa </label>
                            <input type="text" class="form-control" name="updatedBy" value={artistaSeleccionado && artistaSeleccionado.updatedBy && artistaSeleccionado.updatedBy.username} />
                        </Col>

                    </Row>
                </Container>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="primary" onClick={() => viewInformation()}>
                    Hủy Bỏ
                </Button>
            </Modal.Footer>
        </Modal>
    )

    const Delete = (
        <Modal
            show={modalEliminar}
            onHide={() => abrirCerrarModalEliminar()}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header >
                <Modal.Title>Xóa</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p> Bạn có chắc chắn muốn xóa không ? </p>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => abrirCerrarModalEliminar()}>
                    Không
                </Button>
                <Button variant="primary" onClick={() => deleteOppotunity()}>Có</Button>
            </Modal.Footer>
        </Modal>
    )

    const ModalDeleteChild = (
        <Modal backdrop="static"
            keyboard={false} show={deleteChild} onHide={() => SettingModalDeleteChild()}>
            <Modal.Header>
                <Modal.Title>Xóa</Modal.Title>
            </Modal.Header>
            <Modal.Body className="show-grid">
                <p> Bạn có chắc chắn muốn xóa   không ? </p>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => SettingModalDeleteChild()}>
                    Close
                </Button>
                <Button variant="primary" onClick={() => peticionDeleteChild()}>
                    Delete
                </Button>
            </Modal.Footer>
        </Modal>
    )

    const ModalEditChild = (
        <Modal backdrop="static"
            keyboard={false} size="lg" show={EditChild} onHide={() => SettingModalEdit()}>
            <Modal.Header >
                <Modal.Title> Chỉnh Sửa Hàng Hóa  </Modal.Title>
            </Modal.Header>
            <Modal.Body className="show-grid">
                <Container>
                    <Row>
                        <Col md={6}>
                            <label>Mã  Hàng Hóa</label>
                            <input type="text" class="form-control" name="productCode" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.productCode}></input>
                        </Col>
                        <Col md={6}>
                            <label>Diễn giải khi bán</label>
                            <input type="text" class="form-control" name="explanation" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.explanation}></input>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label>Đơn giá bán</label>
                            <input type="text" class="form-control" name="price" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.price}></input>
                        </Col>
                        <Col md={6}>
                            <label> Đơn vị tính chính  </label>
                            <select className="form-control controll" name="unit" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.unit} >
                                <option value=""> - Không chọn - </option>
                                <option value="Bao">Bao</option>
                                <option value="Bình">Bình</option>
                                <option value="Bộ">Bộ</option>
                                <option value="Cái">Cái</option>
                                <option value="Cây">Cây</option>
                                <option value="Chai">Chai</option>
                                <option value="Chiếc">Chiếc</option>
                                <option value="Cuốn">Cuốn</option>
                                <option value="Điếu">Điếu</option>
                                <option value="Gói">Gói</option>
                                <option value="Hộp">Hộp</option>
                                <option value="két">Két</option>
                                <option value="kilogam">Kilogam</option>
                                <option value="két">Quả</option>
                                <option value="Tấn">Tấn </option>
                                <option value="Tạ">Tạ </option>
                                <option value="Yến">Yến</option>
                            </select>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <label>Số lượng</label>
                            <input type="text" class="form-control" name="amount" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.amount}></input>
                        </Col>
                        <Col md={6}>
                            <label>Thuế (%)</label>
                            <input type="text" class="form-control" name="vat" onChange={handleChange} value={artistaSeleccionado && artistaSeleccionado.vat}></input>
                        </Col>
                    </Row>
                </Container>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => SettingModalEdit()}>
                    Hủy bỏ
                </Button>
                <Button variant="primary" onClick={() => peticionUpdateProductInfo()}>
                    Sửa
                </Button>
            </Modal.Footer>
        </Modal>
    )

    const ModalInsertChild = (
        <Modal backdrop="static"
            keyboard={false} size="lg" show={show2} onHide={() => handleClose2()}>
            <Modal.Header >
                <Modal.Title> Chọn Hàng Hóa </Modal.Title>
            </Modal.Header>
            <Modal.Body className="show-grid">
                <Container>
                    <Row>
                        <label> Chọn Hàng Hóa  </label>
                        <select className="form-control" name="productId" onChange={handleChange}  >
                            {product3.selectProduct.map((option2, index) => (
                                <option value={option2.value} key={index}> {option2.label}</option>
                            ))}
                        </select>
                    </Row>
                </Container>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => handleClose2()}  >
                    Hủy bỏ
                </Button>
                <Button variant="primary" onClick={() => peticionPostChild()} >
                    Thêm
                </Button>
            </Modal.Footer>
        </Modal>
    )

    return (
        <div className="container-fluid">

            <div className="row">
                <div className="col-md-12">
                    {TableOpportunity}
                    {ModalEdit}
                    {Delete}
                    {ModalEditChild}
                    {ModalInsert}
                    {ModalInsertChild}
                    {ModalView}
                    {ModalDeleteChild}
                </div>
            </div>
        </div>

    );
}

export default Opportunity;

