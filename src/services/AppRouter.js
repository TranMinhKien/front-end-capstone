import React, {Component} from 'react';
import {Redirect, Switch} from "react-router-dom";
import Login from "../components/login.component";
import Register from "../components/register.component";
import customer from '../components/customer';

import InsertOpportunity from '../components/InsertOpportunity';
import {BrowserRouter as Router, Route, Link, NavLink} from "react-router-dom";
import Potential from '../components/Potential';
import Dashboard from '../components/Dashboard';
import ChangePassword from '../components/ChangePassword';
import ElementOverview from "../v2/components/ElementOverview";
import Tableb from "../v2/components/table";
import Tableb2 from "../v2/components/tabledropdown";
import PotentialEdit from 'v2/features/potential/PotentialEdit';
import PotentialAddNew from 'v2/features/potential/PotentialAddNew';
import CustomerAddNew from '../v2/features/customer/CustomerAddNew';
import CustomerEdit from '../v2/features/customer/CustomerEdit';
import ContactAddNew from '../v2/features/contact/ContactAddNew';

//
import Contact from 'v2/features/contact/contact';
import Opportunity from 'v2/features/opportunity/Opportunity';
import Role from 'v2/features/role/Role';
import User from 'v2/features/user/User';
import Product from 'v2/features/product/product';
import ProductType from 'v2/features/product-type/ProductType';
import Order from 'v2/features/order/Order';
import Invoice from 'v2/features/invoice/Invoice';
import ContactEdit from '../v2/features/contact/ContactEdit';
import ProductTypeAddNew from '../v2/features/product-type/ProductTypeAddNew';
import ProductTypeEdit from '../v2/features/product-type/ProductTypeEdit';
import ProductAddNew from '../v2/features/product/ProductAddNew';
import ProductEdit from '../v2/features/product/ProductEdit';
import OpportunityAddNew from '../v2/features/opportunity/OpportunityAddNew';
import OpportunityEdit from '../v2/features/opportunity/OpportunityEdit';
import OrderAddNew from '../v2/features/order/OrderAddNew';
import OrderEdit from '../v2/features/order/OrderEdit';
import OrderView from '../v2/features/order/OrderView';
import InvoiceAddNew from '../v2/features/invoice/InvoiceAddNew';
import Personal from '../v2/features/persional/Personal';
import InvoiceEdit from '../v2/features/invoice/InvoiceEdit';
import PotentialConvert from 'v2/features/potential/PotentialConvert';
import RoleAddNew from 'v2/features/role/RoleAddNew';
import RoleEdit from 'v2/features/role/RoleEdit';
import PotentialView from 'v2/features/potential/PotentialView';
import CustomerView from 'v2/features/customer/CustomerView';
import ContactView from 'v2/features/contact/ContactView';
import ProductTypeView from 'v2/features/product-type/ProductTypeView';
import ProductView from 'v2/features/product/ProductView';
import OpportunityView from 'v2/features/opportunity/OpportunityView';
import InvoiceView from 'v2/features/invoice/InvoiceView';
import UserView from 'v2/features/user/UserView';
import UserEdit from 'v2/features/user/UserEdit';
import UserAddNew from 'v2/features/user/UserAddNew';
import RoleView from 'v2/features/role/RoleView';


class AppRouter extends Component {
    render() {
        return (

            <Switch>
                {/*<Route exact path={["/", "/home"]} component={Home}/>*/}
                <Redirect exact from={'/'} to={'dashboard'}/>
                <Route exact path="/login" component={Login}/>
                <Route exact path="/register" component={Register}/>
                <Route exact path="/customer" component={customer}/>
                <Route exact path="/contact" component={Contact}/>
                <Route exact path="/opportunity" component={Opportunity}/>
                <Route exact path="/role" component={Role}/>
                <Route exact path="/product" component={Product}/>
                <Route exact path="/productType" component={ProductType}/>
                <Route exact path="/user" component={User}/>
                <Route exact path="/insert" component={InsertOpportunity}/>
                <Route exact path="/order" component={Order}/>
                <Route exact path="/invoice" component={Invoice}/>
                <Route exact path="/dashboard" component={Dashboard}/>
                <Route exact path="/personinfo" component={Personal}/>
                <Route exact path="/changePassword" component={ChangePassword}/>

                <Route exact path="/elements" component={ElementOverview}/>
                <Route exact path="/table" component={Tableb}/>
                <Route exact path="/tabledropdown" component={Tableb2}/>

                <Route exact path="/potential" component={Potential}/>
                <Route exact path="/potential/edit/:id" component={PotentialEdit}/>
                <Route exact path="/potential/add" component={PotentialAddNew}/>
                <Route exact path="/potential/view/:id" component={PotentialView}/>
                <Route exact path="/potential/singleConvert/:id" component={PotentialConvert}/>

                {/*customer*/}
                <Route exact path="/customer/add" component={CustomerAddNew}/>
                <Route exact path="/customer/edit/:id" component={CustomerEdit}/>
                <Route exact path="/customer/view/:id" component={CustomerView}/>


                {/*contact*/}
                <Route exact path="/contact/add" component={ContactAddNew}/>
                <Route exact path="/contact/edit/:id" component={ContactEdit}/>
                <Route exact path="/contact/view/:id" component={ContactView}/>

                {/*user*/}
                <Route exact path="/user/add" component={UserAddNew}/>
                <Route exact path="/user/edit/:id" component={UserEdit}/>
                <Route exact path="/user/view/:id" component={UserView}/>

                {/*role*/}
                <Route exact path="/role/add" component={RoleAddNew}/>
                <Route exact path="/role/edit/:id" component={RoleEdit}/>
                <Route exact path="/role/view/:id" component={RoleView}/>

                {/*product type*/}
                <Route exact path="/productType/add" component={ProductTypeAddNew}/>
                <Route exact path="/productType/edit/:id" component={ProductTypeEdit}/>
                <Route exact path="/productType/view/:id" component={ProductTypeView}/>

                {/*product*/}
                <Route exact path="/product/add" component={ProductAddNew}/>
                <Route exact path="/product/edit/:id" component={ProductEdit}/>
                <Route exact path="/product/view/:id" component={ProductView}/>

                {/*opportunity*/}
                <Route exact path="/opportunity/add" component={OpportunityAddNew}/>
                <Route exact path="/opportunity/add/:id" component={OpportunityAddNew}/>
                <Route exact path="/opportunity/edit/:id" component={OpportunityEdit}/>
                <Route exact path="/opportunity/view/:id" component={OpportunityView}/>

                {/*order*/}
                <Route exact path="/order/add" component={OrderAddNew}/>
                <Route exact path="/order/edit/:id" component={OrderEdit}/>
                <Route exact path="/order/view/:id" component={OrderView} />

                {/*invoice*/}
                <Route exact path="/invoice/add" component={InvoiceAddNew}/>
                <Route exact path="/invoice/edit/:id" component={InvoiceEdit}/>
                <Route exact path="/invoice/view/:id" component={InvoiceView}/>

                {/*convert*/}
                <Route exact path="/opportunity/convert" component={OpportunityAddNew}/>
                <Route exact path="/order/convert" component={OrderAddNew}/>
                <Route exact path="/invoice/convert" component={InvoiceAddNew}/>

                {/* link đến file cũ */}

                {/* <Route exact path="/contact1" component={contact1}/>
                <Route exact path="/opportunity1" component={Opportunity1}/>
                <Route exact path="/role1" component={Role1}/>
                <Route exact path="/product1" component={Product1}/>
                <Route exact path="/productType1" component={ProductType1}/>
                <Route exact path="/user1" component={User1}/>
                <Route exact path="/order1" component={Order1}/>
                <Route exact path="/invoice1" component={Invoice1}/>
                <Route exact path="/personinfo1" component={PersonInfo}/> */}

            </Switch>

        );
    }
}

export default AppRouter;